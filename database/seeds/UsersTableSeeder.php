<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'alyatek',
            'email' => 'alyatek@gmail.com',
            'password' => '$2y$10$6H3jQwwiWcnadsd4vz8B6Ojly5QHJLAm0pOm0kPO3ivNJqPCyZNCG',
            'status' => 1,
            'created_at' => '2018-12-02 15:47:40',
            'updated_at' => '2018-12-02 15:47:40',
        ]);
    }
}
