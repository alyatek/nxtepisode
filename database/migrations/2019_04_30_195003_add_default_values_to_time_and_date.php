<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValuesToTimeAndDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_episodes_schedule', function (Blueprint $table) {
            $table->string('local_date',100)->nullable()->default(null)->after('schedule_time');
            $table->string('local_time',100)->nullable()->default(null)->after('schedule_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_episodes_schedule', function (Blueprint $table) {
            //
        });
    }
}
