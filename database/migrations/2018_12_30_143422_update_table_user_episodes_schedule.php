<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableUserEpisodesSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_episodes_schedule', function (Blueprint $table) {
            $table->renameColumn('schedule', 'schedule_date');
            $table->time('schedule_time')->after('schedule');
            $table->integer('season')->after('show_id');
            $table->integer('episode')->after('season');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
