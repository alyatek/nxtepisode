<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Shows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //'api_id', 'name', 'summary', 'language', 'genres',
        //'runtime', 'premiered', 'official_site', 'schedule', 'rating', 'network', 'externals', 'slug'
        Schema::create('shows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('api_id');
            $table->string('name',250);
            $table->text('summary');
            $table->string('language',100);
            $table->longText('genres');
            $table->integer('runtime');
            $table->date('premiered');
            $table->string('official_site',250)->nullable();
            $table->longText('schedule');
            $table->string('rating',250);
            $table->string('network',250);
            $table->string('externals',250);
            $table->string('slug',250);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shows');
    }
}
