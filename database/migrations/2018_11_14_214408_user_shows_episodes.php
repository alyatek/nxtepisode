<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserShowsEpisodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_shows_episodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('show_id');
            $table->integer('user_id');
            $table->integer('season');
            $table->integer('episode');
            $table->integer('episode_status');
            $table->longText('params')->nullable();
            $table->timestamps();
            $table->integer('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_shows_episodes');
    }
}
