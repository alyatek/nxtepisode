<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomePageController@get_index')->name('home');

#login a likes
Route::middleware('guest')->group(function () {
    Route::get('/login', function(){
        return redirect('/');
    })->name('login');
    Route::get('/register', function(){
        return redirect('/');
    });
    Route::get('/token/{public_hash}', 'Auth\RegisterController@validate_token');
    Route::get('forgot-password', 'Auth\ForgotPasswordController@get_forgot_password_index');
    Route::get('forgot-password/reset/{token}', 'Auth\ForgotPasswordController@get_forgot_password_reset_index');
});

#facebook login
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Route::middleware('guest', 'throttle:60,1')->group(function () {
    Route::post('/register', 'Auth\RegisterController@register');
    Route::post('/login', 'Auth\LoginController@login')->name('authentication.index');
    Route::post('/forgot-password', 'Auth\ForgotPasswordController@request_forgot_password');
    Route::post('/forgot-password/reset/{token}', 'Auth\ForgotPasswordController@handle_forgot_password');
});

Route::get('/logout', function () {
    Auth::logout();
    return redirect(route('home'));
})->name('logout');

Route::get('privacy-policy', function () {
    return view('pages.privacy-policy');
});
Route::get('terms-of-service', function () {
    return view('pages.terms-of-service');
});

#search for shows
Route::get('search/{showQuery}', 'Search\SearchController@get_search_index')->name('search');
#open show page
Route::get('show/{apiid}/{showName}', 'Shows\Show\ShowController@get_show_index')->name('show-page');

#post top5
Route::post('shows/top-5', 'Shows\Show\ShowController@get_top_5');
#post search result
Route::post('search/{showQuery}', 'Search\SearchController@get_search')->name('search-post');
#post retrives show infos
Route::post('show/{apiid}/{showName}', 'Shows\Show\ShowController@get_show_info')->name('show-page-post');
Route::post('show/{apiid}/{showName}/seasons', 'Shows\Seasons\SeasonController@get_seasons')->name('show-page-seasons-post');
Route::post('show/{apiid}/{showName}/season/{seasonnr}/episodes', 'Shows\Episodes\EpisodeController@get_episodes');

#tv shows
Route::get('tv-shows', 'Shows\TvShowsController@get_index');
Route::post('tv-shows', 'Shows\TvShowsController@get_shows_paginated');
Route::post('tv-shows/get/images', 'Shows\TvShowsController@get_shows_paginated_media');
Route::post('tv-shows/get/genres', 'Shows\TvShowsController@get_categories');

#authed request
Route::middleware('auth')->group(function () {
    #account settings
    Route::get('account-settings', 'Users\User\UserController@get_account_settings_index')->name('account-settings');

    #account settings
    Route::post('account-settings/update/email', 'Users\User\UserController@update_user_email')->name('update-email');
    Route::post('account-settings/update/email', 'Users\User\UserController@update_user_email')->name('update-email');
    Route::post('account-settings/update/password', 'Users\User\UserController@update_user_password')->name('update-password');
    Route::post('account-settings/update/timezone', 'Users\User\UserController@update_timezone');
});

#posts authed throttled by 10 every 2 mins
Route::middleware('auth', 'throttle:10,2')->group(function () {

    #account settings
    Route::post('account-settings/update/email', 'Users\User\UserController@update_email_address')->name('update_email_address');

    #watchlist
    Route::post('my-watchlist/add/show', 'Users\UserShows\UserWatchlistController@add_to_watchlist');

});

Route::middleware('auth', 'throttle:50,2')->group(function () {
    Route::post('suggestions', 'SuggestionsController@create');


    #email notification configuration
    Route::post('watchlater/settings/configure/email', 'Users\UserShows\UserScheduleConfigurationsController@handle_new_email_configuration');
    Route::post('watchlater/settings/remove/email', 'Users\UserShows\UserScheduleConfigurationsController@handle_email_configuration_removal');

    Route::post('watchlater/settings/configure/webpush', 'Users\UserShows\UserScheduleConfigurationsController@handle_onesignal_configuration');
    Route::post('watchlater/settings/remove/webpush', 'Users\UserShows\UserScheduleConfigurationsController@handle_onesignal_removal');

    Route::post('/get/configure/facebook-messages', 'Users\UserShows\UserScheduleConfigurationsController@handle_facebook_messages_configuration');
    Route::post('watchlater/settings/remove/facebook-messages', 'Users\UserShows\UserScheduleConfigurationsController@handle_facebook_messages_removal');

});

#more authed requests
Route::middleware('auth')->group(function () {
    // get

    #watchlist
    Route::get('watchlist', 'Users\UserShows\UserWatchlistController@get_index');
    Route::get('watchlist/notification/{notification_key}', 'Users\UserShows\UserWatchlaterController@get_index_validate_notification');
    Route::get('watchlist/notification/{notification_key}/validate', 'Users\UserShows\UserWatchlaterController@validate_notification');
    Route::get('watchlist/notification/validate/all', 'Users\UserShows\UserWatchlaterController@validate_all_notification');
    Route::get('watchlist/watching', 'Users\UserShows\UserCurrentlyWatchingController@get_index');
    Route::get('watchlist/show/{show_id}', 'Users\UserShows\UserShowsController@get_index');

    #favorites
    Route::get('favorites', 'Users\UserShows\UserFavoritesController@get_index');

    #watchlater
    Route::get('watchlater', 'Users\UserShows\UserWatchlaterController@get_index');
    Route::get('watchlater/settings', 'Users\UserShows\UserWatchlaterController@get_index_settings');
    Route::get('watchlater/planned', 'Users\UserShows\UserWatchlaterController@get_index_planned');

    //post

    #watchlist
    Route::post('watchlist', 'Users\UserShows\UserWatchlistController@get_watchlist');
    Route::post('watchlist/show/episodes/season', 'Users\UserShows\UserShowEpisodesController@get');
    Route::post('watchlist/show/episodes/next', 'Users\UserShows\UserWatchlistController@get_show_episode_next_route');
    Route::post('watchlist/show/episodes/current', 'Users\UserShows\UserWatchlistController@get_show_currently_watching_episode');
    Route::post('watchlist/show/episodes/current/start', 'Users\UserShows\UserWatchlistController@update_watching_episode_start_route');
    Route::post('watchlist/show/episodes/current/end', 'Users\UserShows\UserWatchlistController@update_watching_episode_end_route');
    Route::post('watchlist/show/watching/latest', 'Users\UserShows\UserWatchlistController@get_latest_watching');
    Route::post('watchlist/watching', 'Users\UserShows\UserCurrentlyWatchingController@get_watching_data');
    Route::post('watchlist/show/{show_id}', 'Users\UserShows\UserShowsController@get_user_show_info');
    Route::post('watchlist/show/{show_id}/seasons-episodes', 'Users\UserShows\UserShowsController@get_user_show_episodes');

    Route::post('watchlist/show/{show_id}/watch/episode', 'Users\UserShows\UserWatchlistController@handle_watch_single_request');
    Route::post('watchlist/show/{show_id}/watch/season', 'Users\UserShows\UserWatchlistController@handle_watch_season_request');
    Route::post('watchlist/show/{show_id}/unwatch/season', 'Users\UserShows\UserWatchlistController@handle_watch_season_request');
    Route::post('watchlist/show/{show_id}/unwatch/episode', 'Users\UserShows\UserWatchlistController@handle_un_watch_single_request');

    Route::get('watchlist/show/get/watch-next/html/{show_id}', 'Users\UserShows\UserWatchlistController@get_modal_html_content');

    #favorites

    Route::post('favorites', 'Users\UserShows\UserShowsController@get_user_favorites');

    // update
    Route::post('watchlist/update/seasons', 'Users\UserShows\UserWatchlistController@update_watchlist_seasons');
    Route::post('watchlist/update/episodes', 'Users\UserShows\UserWatchlistController@update_watchlist_episodes');
    Route::post('watchlist/show/{show_id}/favorite', 'Users\UserShows\UserShowsController@create_favorite');

    //delete

    #watchlater


    #watchlist
    Route::post('watchlist/show/{show_id}/remove', 'Users\UserShows\UserShowsController@remove_show');
});

#admin routes
Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::get('messages/get', 'MessagesController@get_messages');


    #admin - home - view
    Route::get('/admin-dashboard', function () {
        return view('admin.pages.home');
    });
    #admin - statistics management - view
    Route::get('/admin-dashboard/shows/management/statistics', function () {
        return view('admin.pages.shows.shows-statistics-manager');
    })->name('show-statistics-manager');

    #admin - update shows - view
    Route::get('/admin-dashboard/shows/management/update', function () {
        return view('admin.pages.shows.update-shows');
    })->name('shows-updates');
    Route::get('/admin-dashboard/messages', function () {
        return view('admin.pages.messages.messages');
    })->name('messages');

    #admin - users - gets
    Route::get('/admin/user/get', 'Users\User\UserController@get_users');

    Route::post('/admin/messages/create', 'MessagesController@handle_create_message');
    Route::post('/admin/messages/get', 'MessagesController@get_all_messages');
    Route::post('/admin/messages/remove/{id}', 'MessagesController@remove_message');
    Route::post('/admin/messages/disable/{id}', 'MessagesController@disable_message');
    Route::post('/admin/messages/enable/{id}', 'MessagesController@enable_message');

    #admin - shows - gets
    Route::get('/admin/shows/get', 'Admin\ShowsController@get_shows');


    #admin - users - posts
    Route::post('/admin/user/create', 'Users\User\UserController@create_user');
    Route::post('/admin/user/update/{id}', 'Users\User\UserController@update_user');
    Route::post('/admin/user/disable/{id}', 'Users\User\UserController@disable_user');
    Route::post('/admin/user/enable/{id}', 'Users\User\UserController@enable_user');


    Route::post('/admin/shows-statistics/update/top-5', 'Admin\ShowsController@update_top_5');
    Route::post('/admin/shows-statistics/cache/top-5/clean', 'Admin\ShowsController@clear_top_5_cache');

    #admin - shows - posts
    Route::post('/admin/shows/update/top-5', 'Admin\ShowsController@update_top_5');

//    Route::post('/admin/shows/update/seasons/{id}', 'Admin\ShowsController@update_seasons');
});

#authed with only ajax request
Route::middleware(['ajax', 'auth'])->group(function () {
    #watchlater - gets
    Route::get('watchlater/get/shows', 'Users\UserShows\UserWatchlaterController@get_shows');
    Route::get('watchlater/get/show/episodes/list/{show_id}', 'Users\UserShows\UserWatchlaterController@get_show_next_episodes');

    Route::get('watchlater/settings/get/email/configuration', 'Users\UserShows\UserScheduleConfigurationsController@handle_email_configuration_status');
    Route::get('watchlater/settings/get/facebook-messages/configuration', 'Users\UserShows\UserScheduleConfigurationsController@handle_facebook_messages_configuration_status');

    #watchlaters - posts
    Route::post('watchlater/insert', 'Users\UserShows\UserWatchlaterController@update_request');
    Route::post('watchlater/planned', 'Users\UserShows\UserWatchlaterController@handle_shows_planned_request');
    Route::post('watchlater/planned/remove/schedule', 'Users\UserShows\UserWatchlaterController@handle_schedule_remove_request');
    Route::post('watchlater/get/unread-notifications', 'Users\UserShows\UserWatchlaterController@get_unread_notifications');
    Route::post('watchlater/planned/remove/expired-schedules', 'Users\UserShows\UserWatchlaterController@handle_expired_schedules_request');
});

#tests
Route::get('log', function () {
    \Illuminate\Support\Facades\Log::info('INfo');
    return view('pages.home.home');
});

Route::post('/facebook/webhook', 'FacebookTest@get_contents');

Route::get('/facebook/webhook', function () {
    return response($_GET['hub_challenge'], 200);
});

Route::group(['prefix' => '/facebook'], function () {
    Route::get('/messages/activate/{hash}', 'Api\FacebookRegisterController@register_user');
    Route::get('/messages/disable/{hash}', 'Api\FacebookRegisterController@disable_user');
});

Route::group(['prefix' => 'admin'], function () {
    Route::post('/shows-statistics/update/top-5', 'Admin\ShowsController@update_top_5');
    Route::post('/shows/update/seasons/{id}', 'Admin\ShowsController@update_seasons');
    Voyager::routes();
    Route::get('/home-sections', [
        'as' => 'home-section', 'uses' => 'VoyagerContentController@get_home_section_index'
    ]);
    Route::post('/home-sections/save/user-recommended', [
        'uses' => 'VoyagerContentController@handle_user_recommended'
    ]);
    Route::post('/home-sections/save/popular-this-week', [
        'uses' => 'VoyagerContentController@handle_popular_this_week'
    ]);
    Route::post('/home-sections/save/small-recommended', [
        'uses' => 'VoyagerContentController@handle_small_recommended'
    ]);
});

Route::get('mail-notice', 'Notices\NoticeController@email');
Route::get('mail-notice/notify', 'Notices\NoticeController@test');
Route::get('mail-notice/notify', 'Notices\NoticeController@test');
Route::get('app-notice', 'Notices\NoticeController@notice');
Route::get('app-notice/service', 'Notices\NoticeController@noticeapp');
Route::get('app-notice/web', 'Notices\NoticeController@webpush');
Route::get('test', 'Notices\NoticeController@test');

Broadcast::routes(['middleware' => ['web', 'auth']]);

