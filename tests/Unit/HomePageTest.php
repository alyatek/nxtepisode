<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomePageTest extends TestCase
{
    public function testStatusLoggedOut()
    {
        $response = $this->get('/');
        $response->assertOk(200);
    }

    public function testStatusLoggedIn()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->get('/');
        $response->assertOk(200);
    }

    public function testTop5(){
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->post('/shows/top-5');
        $response->assertJsonFragment(['status' => true]);
    }
}
