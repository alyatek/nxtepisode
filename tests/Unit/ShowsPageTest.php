<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowsPageTest extends TestCase
{
    public function testStatusLoggedOut()
    {
        $response = $this->get('/tv-shows');
        $response->assertStatus(200);
    }

    public function testStatusLoggedIn()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->get('/tv-shows');
        $response->assertStatus(200);
    }

    public function testStatusShowsFetching()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->json(
                             'POST',
                             '/tv-shows',[
                                 'params' => json_encode([])
                             ]
                         );
        $response->assertJsonFragment(['status' => true]);
    }
}
