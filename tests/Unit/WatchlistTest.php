<?php

namespace Tests\Unit;

use App\Models\Users\UserShows\UserShows;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WatchlistTest extends TestCase
{

    /**
     * TEST CASING WATCHLIST MAIN FUNCTIONALITIES
     *
     * !MAKE SURE THE USER FOR AUTHENTICATION ACTUALLY EXISTS!
     *
     * NOT EVERY SINGLE REQUEST AND PAGE IS DONE
     * @TODO MAKE REQUESTING EVERYTHING
     */

    public function testStatusLoggedOut()
    {
        $response = $this->get('/watchlist');
        $response->assertStatus(302);//moved permenantly
        //302 because it redirects instead of 404 or 403
    }

    public function testStatusLoggedIn()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->get('/watchlist');
        $response->assertStatus(200);
    }

    public function testIfShowsExist()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->post('/watchlist');
        $response->assertJsonFragment(['status' => true]);
    }

    public function testIfWatchlistShowContentExists()
    {
        $user     = User::find(3);
        $user_show = UserShows::whereUser_id($user->id)->get()->first();
        $response = $this->actingAs($user)
                         ->post('/watchlist/show/'.$user_show->show_id);
        $response->assertJsonFragment(['status' => true]);
    }

    public function testIfWatchlistShowExists()
    {
        $user     = User::find(3);
        $user_show = UserShows::whereUser_id($user->id)->get()->first();
        $response = $this->actingAs($user)
                         ->post('/watchlist/show/'.$user_show->show_id);
        $response->assertStatus(200);
    }

    public function testIfNextEpisodeIsAvailable()
    {
        $user     = User::find(3);
        $user_show = UserShows::whereUser_id($user->id)->get()->first();
        $response = $this->actingAs($user)
                         ->json(
                             'POST',
                             '/watchlist/show/episodes/next',
                             [
                                 'show_id' => 3 /* !! CHECK IF THIS SHOW EXISTS !!*/
                             ]
                         );
        $response->assertJsonFragment(['status' => true]);
    }

    public function testIfSeasonEpisodesResult()
    {
        $user     = User::find(3);
        $user_show = UserShows::whereUser_id($user->id)->get()->first();
        $response = $this->actingAs($user)
                         ->post("/watchlist/show/".$user_show->show_id."/seasons-episodes");
        $response->assertJsonFragment(['status' => true]);
    }

//
}
