<?php

namespace Tests\Unit;

use App\Models\Shows\Show\Show;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowPageTest extends TestCase
{
    public function testStatusLoggedOut()
    {
        $get_tv_show = Show::get()->first();
        $response = $this->get("/show/$get_tv_show->api_id/$get_tv_show->id-$get_tv_show->slug");
        $response->assertStatus(200);
    }

    public function testStatusLoggedIn()
    {
        $user     = User::find(3);
        $get_tv_show = Show::get()->first();
        $response = $this->actingAs($user)
                         ->get("/show/$get_tv_show->api_id/$get_tv_show->id-$get_tv_show->slug");
        $response->assertStatus(200);
    }

    public function testShowInfoLoggedOut()
    {
        $get_tv_show = Show::get()->first();
        $response = $this->post("/show/$get_tv_show->api_id/$get_tv_show->id-$get_tv_show->slug");
        $response->assertJsonFragment(['status' => true]);
    }

    public function testShowInfoLoggedIn()
    {
        $user     = User::find(3);
        $get_tv_show = Show::get()->first();
        $response = $this->actingAs($user)
                         ->post("/show/$get_tv_show->api_id/$get_tv_show->id-$get_tv_show->slug");
        $response->assertJsonFragment(['status' => true]);
    }

    public function testShowSeasonsLoggedIn()
    {
        $user     = User::find(3);
        $get_tv_show = Show::get()->first();
        $response = $this->actingAs($user)
                         ->post("/show/$get_tv_show->api_id/$get_tv_show->id-$get_tv_show->slug/season/1/episodes");
        $response->assertJsonFragment(['status' => true]);
    }
}
