<?php

namespace Tests\Unit;

use App\Models\Shows\Show\Show;
use App\Models\Users\UserShows\UserShows;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WatchlaterTest extends TestCase
{
    public function testStatusLoggedOut()
    {
        $response = $this->get('/watchlater');
        $response->assertStatus(302);//moved permenantly
        //302 because it redirects instead of 404 or 403
    }

    public function testStatusLoggedIn()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->get('/watchlater');
        $response->assertOk();
    }

    public function testIfShows1Exists()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->withoutMiddleware()
                         ->get('/watchlater/get/shows');
        $response->assertJsonFragment(['status' => true]);
    }

    public function testIfFetchingSchedulingShowWorks()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->withoutMiddleware()
                         ->get('/watchlater/get/show/episodes/list/3');
        $response->assertJsonFragment(['status' => true]);
    }

    public function testWatchLaterPlannedPageLoggedOut()
    {
        $response = $this
            ->get('/watchlater/planned');
        $response->assertStatus(302);
    }

    public function testWatchLaterPlannedPageLoggedIn()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->withoutMiddleware()
                         ->get('/watchlater/planned');
        $response->assertOk();
    }

    public function testWatchLaterPlannedShows()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)
                         ->withoutMiddleware()
                         ->post('/watchlater/planned');
        $response->assertJsonFragment(['status' => true]);
    }

    public function testSettingsGuest()
    {
        $response = $this->get('/watchlater/settings');
        $response->assertStatus(302);
    }

    public function testSettingsAuth()
    {
        $user     = User::find(3);
        $response = $this->actingAs($user)->get('/watchlater/settings');
        $response->assertOk();
    }

    public function testFetchingNextEpisodes()
    {
        $user        = User::find(3);
        $get_tv_show = UserShows::where('user_id', 3)->get(['show_id'])->first();
        $response    = $this->actingAs($user)->get('watchlater/get/show/episodes/list/' . $get_tv_show['show_id']);
        $response->assertOk();
    }
}

