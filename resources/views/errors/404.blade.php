@extends('tmpl.master')

@section('content')
    <div>
        <container>
            <div class="row">
                <div class="col-md-12 text-center">
                    <svg class="icon">
                        <use xlink:href="#404"/>
                    </svg>
                    <p style="margin-top:25px">Page not found.</p>
                    <a class="btn btn-primary" href="{{url('/')}}">Go Home</a>
                </div>
            </div>
        </container>
    </div>
@endsection
