@extends('tmpl.master')

@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="watchlist">My Watchlist</h3>
                </div>
            </div>
            <div class="row latest-watching-notification">
                <div class="col-md-12">
                    <div class="card">
                        <div id="latest-watching" class="card-body text-center text-lg-left">

                        </div>
                    </div>
                </div>
            </div>
            <h5 class="mb-3">Currently in Watchlist</h5>
            <div class="row" id="watchlist">

            </div>
            @if(1==1)
                <div class="row ad-space">
                    <div class="col-md-12 text-center">
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Bottom 1 -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-1071507716766529"
                             data-ad-slot="3127375655"
                             data-ad-format="auto"
                             data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
                <div class="row ad-space">
                    <div class="col-md-12 text-center">
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Unit 4 -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-1071507716766529"
                             data-ad-slot="5907799272"
                             data-ad-format="auto"
                             data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            @endif
        </div>
    </section>

    <div class="modal fade" id="modalCheckIn" tabindex="-1" role="dialog" aria-labelledby="modalCheckInLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCheckInLabel">What do you want to check in?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="ModalStartNext" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Start next episode?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="nextbody">

                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        @if(session()->exists('status'))
        pMessage({
            title: '{{session("status")['title']}}',
            message: '{{session("status")['text']}}',
            type: '{{session("status")['type']}}'
        });
        @endif
        $(function () {
            get_latest_watching();
            indeterminateProgress.start();
            $('[data-toggle="tooltip"]').tooltip();
            $('input').iCheck();
            $.ajax({
                url: APP_URL + '/watchlist',
                method: "post",
                data: {_token: APP_CSRF},
                dataType: 'json',
                success: function (response) {
                    templateWatchlistWatchlist(response);
                },
                error: function (x, y, z) {
                    contentError(x, z);
                }
            });
        });
    </script>
@endsection

@section('underscore_templates')
    <script type="text/template" id="templateNoShows">
        <div class="col-md-12 text-center">
            <svg style="width: 150px;height: 150px;" xmlns="https://www.w3.org/2000/svg" viewBox="0 0 286.92 300">
                <defs>
                    <style>.cls-1 {
                            fill: #ebb044;
                        }

                        .cls-2 {
                            fill: #dda243;
                        }

                        .cls-3 {
                            fill: #ffc85a;
                        }

                        .cls-4 {
                            fill: #3b455a;
                        }</style>
                </defs>
                <title>Artboard 193</title>
                <g id="object">
                    <circle class="cls-1" cx="143.46" cy="150" r="133.5"/>
                    <path class="cls-2"
                          d="M213.3,36.06A133.52,133.52,0,0,1,31,221.75,133.51,133.51,0,1,0,213.3,36.06Z"/>
                    <path class="cls-3"
                          d="M28.1,164.08A123.95,123.95,0,0,1,250.3,88.53,124,124,0,1,0,44.57,225.86,123.38,123.38,0,0,1,28.1,164.08Z"/>
                    <path class="cls-4"
                          d="M88.74,209.33a4.41,4.41,0,0,1-7.17-4.8,66.92,66.92,0,0,1,123.75-1.23,4.4,4.4,0,0,1-7.11,4.86C178.67,189.21,137.2,162.26,88.74,209.33Z"/>
                    <ellipse class="cls-4" cx="94.27" cy="116.63" rx="9.84" ry="16.58"/>
                    <ellipse class="cls-4" cx="192.64" cy="116.63" rx="9.84" ry="16.58"/>
                </g>
            </svg>
            <h3>oh! no...</h3>
            <p>Currently you have no shows in the watchlist.</p>
            <p>Go to TV Shows or search for your shows!</p>
        </div>
    </script>
    <script type="text/template" id="latestWatchingTemplate">
        <div class="row">
            <div class="col-sm-12 col-lg-8 mb-3 mb-lg-0">
                <h5 class="mt-0 mb-1">Latest Watching</h5>
                <p class="card-text"><%= show_name %> - Season <%= season %> Episode <%= episode %></p>
            </div>
            <div class="col-sm-12 col-lg-4 btn-block">
                <a href="{{url('watchlist/watching')}}" class="btn btn-light float-lg-right"><i class="fas fa-arrow-right"></i>Watching
                    List </a>
                <a href="javascript:void(0)" onclick="checkNext(<%= show_id %>)" class="btn btn-primary float-lg-right"><i class="fas fa-check"></i>Check in </a>

            </div>
        </div>
    </script>
    <script type="text/template" id="DNFTemplate">
        <div class="card-body text-center">
            <div class="animated swing delay-1s ">
                <i class="flaticon-information-circular-button-symbol wobble-info"></i>
            </div>
            <p><%= message %></p>
            <p> <b><%= episode_name %></b> <small>from</small> Season <%= season %> - Episode <%= episode %></p>
            <button type="button" class="btn btn-secondary btn-fullw" id="btnUpdateFinishCurrent" onclick="updateFinishCurrent(<%= show_id %>,true)">Do you want to check as finished? </button>
        </div>
    </script>
    <script type="text/template" id="StartNextTemplateSuccess">
        <div class="card-body text-center">
            <div class="animated swing delay-1s ">
                <i class="flaticon-tick wobble-success"></i>
            </div>
            <h4>yay!</h4>
            <p><%= message %></p>
                    </div>
    </script>
    <script type="text/template" id="StartNextTemplateError">
        <div class="card-body text-center">
            <div class="animated swing delay-1s ">
                <i class="flaticon-close wobble-error"></i>
            </div>
            <h4>Sorry!</h4>
            <p><%= error %></p>
                    </div>
    </script>
    <script type="text/template" id="StartNextTemplate">
        <div class="card">
            <div class="card-body text-center">
                <p>The next episode to watch is:</p>
                <h5><%= episodename %></h5>
                <small>from:</small>
                <h5>Season <%= seasonnumber %> - Episode <%= seasonepisode %></h5>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <button onclick="postNextEpisode(<%= showid %>);btnLoading('btnStartWatching')" id="btnStartWatching" class="btn btn-secondary btn-fullw">Begin</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>
@endsection
