@extends('tmpl.master')

@section('content')
    <div class="container">
        @if(session()->has('account_settings_messages'))
            <div class="alert alert-primary" role="alert">
                {!! session('account_settings_messages') !!}
            </div>
        @endif
        <div class="row">
            <div class="col-sm-6" style="margin-bottom: 25px;">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Account Settings</h5>
                        <form id="update_email" method="post" action="{{route('update_email_address')}}">
                            <h6>Change your email address</h6>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       aria-describedby="emailHelp" placeholder="Enter new email address">
                            </div>
                            <button type="submit" id="btn" class="btn btn-primary btn-sm">Change</button>
                            {{csrf_field()}}
                        </form>
                        <hr>
                        <form id="update_password" method="post" action="{{route('update-password')}}">
                            {{csrf_field()}}
                            <h6>Change your password</h6>
                            <div class="form-group">
                                <label for="current_password">Current Password</label>
                                <input type="Password" class="form-control" id="current_password"
                                       name="current_password" aria-describedby="emailHelp"
                                       placeholder="Current password">
                            </div>
                            <div class="form-group">
                                <label for="password">New Password</label>
                                <input type="Password" class="form-control" id="password" name="password"
                                       aria-describedby="emailHelp"
                                       placeholder="New password">
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">Confirm new Password</label>
                                <input type="Password" class="form-control" name="password_confirmation"
                                       id="password_confirmation"
                                       aria-describedby="emailHelp" placeholder="Confirm new password">
                            </div>
                            <button type="submit" id="btnPW" class="btn btn-primary btn-sm">Change</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:25px;">
                        <div class="card">
                            <div class="card-body">
                                <form id="update_timezone" method="post"
                                      action="{{url('/account-settings/update/timezone')}}">
                                    {{csrf_field()}}
                                    <h5 class="card-title">App Settings</h5>
                                    <div class="form-group">
                                        <label for="timezone">I agree with the Privacy Policy</label>
                                        <br>
                                        <select name="timezone" class="form-control select-target w-100" id="timezone">
                                            <option value="">Choose your timezone</option>
                                            @foreach($timezones as $timezone)
                                                <option value="{{$timezone}}" @if(\Illuminate\Support\Facades\Auth::user()->timezone === $timezone) selected @endif>{{$timezone}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-sm btn-primary">Update timezone</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:25px;">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Privacy Policy</h5>
                                <div class="form-group">
                                    <a target="_blank" href="{{url('privacy-policy')}}"><p>Read Privacy Policy</p></a>
                                    <label for="policy1">I agree with the Privacy Policy</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Terms of Service</h5>
                                <div class="form-group">
                                    <a target="_blank" href="{{url('terms-of-service')}}"><p>Read Terms of Service</p>
                                    </a>
                                    <label for="policy1">I agree with the Terms of Service</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        function handleEmailUpdate(res) {
            if (res.status === false) {
                btnEndLoading($('body').find('.fa-spin').parent().attr('id'), 'Change', false);

                pMessage({
                    title: 'oh noes!',
                    message: iterateMessages(res.content.data),
                    type: 'warning'
                });
                return;
            }

            btnEndLoading($('body').find('.fa-spin').parent().attr('id'), 'Change');

            pMessage({
                title: 'yay!',
                message: iterateMessages(res.content.data),
                type: 'success'
            });
        }

        function handlePasswordUpdate(res) {
            if (res.status === false) {
                btnEndLoading($('body').find('.fa-spin').parent().attr('id'), 'Change', false);

                pMessage({
                    title: 'oh noes!',
                    message: iterateValidatorMessages(res.content.data),
                    type: 'warning'
                });
                return;
            }

            btnEndLoading($('body').find('.fa-spin').parent().attr('id'), 'Change');

            pMessage({
                title: 'yay!',
                message: iterateMessages(res.content.data),
                type: 'success'
            });
        }

        $(function () {
            $('#timezone').selectize();
            $('form').submit(function (evt) {
                if ($(this).attr('id') === 'update_email') {
                    evt.preventDefault();
                    btnLoading($(this).find('button').attr('id'));
                    ajaxRequest($(this).attr('action'), $(this).serialize(), $(this).attr('method'), handleEmailUpdate);
                } else if ($(this).attr('id') === 'update_password') {
                    evt.preventDefault();
                    btnLoading('btnPW');
                    ajaxRequest($(this).attr('action'), $(this).serialize(), $(this).attr('method'), handlePasswordUpdate);
                }
            });
        });
    </script>
@endsection
