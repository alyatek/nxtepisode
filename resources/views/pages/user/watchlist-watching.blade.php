@extends('tmpl.master')

@section('content')
    <div class="container watching_list_list">
        <div class="row">
            <div class="col-md-10">
                <h3 class="watching_list">Currently Watching</h3>
            </div>
            <div class="col-md-2 quick-view-icons">
                {{--<a href=""><i class="fas fa-bars fright"></i></a>--}}
                <i class="fas fa-th-list fright"></i>
            </div>
        </div>
        <div class="row" id="watching_list">

        </div>
    </div>

    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="infoModalLabel"><span id="modal_show_name"></span> - Season <span
                            id="modal_season"></span> - Episode <span id="modal_episode"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><b>When you started watching: </b> <span id="modal_started_watching_when"></span>.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalCheckIn" tabindex="-1" role="dialog" aria-labelledby="modalCheckInLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCheckInLabel">What do you want to check in?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="ModalStartNext" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Start next episode?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="nextbody">

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            requestWatchlistWatching(templateWatchlistWatching);

            $('#infoModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var show_name = button.data('name');
                var show_season = button.data('season');
                var show_episode = button.data('episode');
                var show_date = button.data('date');

                var modal = $(this);
                modal.find('#modal_show_name').text(show_name);
                modal.find('#modal_season').text(show_season);
                modal.find('#modal_episode').text(show_episode);
                modal.find('#modal_started_watching_when').text(show_date);
            }).on('show.bs.collapse', function (event) {
                var modal = $(this);
                modal.find('#modal_show_name').text('');
                modal.find('#modal_season').text('');
                modal.find('#modal_episode').text('');
                modal.find('#modal_started_watching_when').text('');
            });
        });
    </script>
@endsection

@section('underscore_templates')
    <script type="text/template" id="emptyBlock">
        <div class="col-md-12">
            <div class="jumbotron text-center">
                <svg class="icon">
                    <use xlink:href="#hello" />
                </svg>
                <h1 class="display-4">hey there!</h1>
                <p>You are currently not watching anything.</p>
                <p>Go to your <a href="{{url('watchlist')}}">watchlist</a> to start watching something.</p>
            </div>
        </div>
    </script>
    <script type="text/template" id="watchingBlockTemplate">
        <% _.each(episodes, function(episode){ %>
        <div class="col-md-6 watchlist-cards">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5><%= episode.show_name %></h5>
                                        <p class="card-text">Watching Season <%= episode.season %> in Episode <%= episode.episode %></p>
                                    </div>
                                    <div class="col-md-6 card-v-align btn-block">
                                        <a class="btn btn-light fright btns-fav" data-toggle="modal" data-target="#infoModal" data-name="<%= episode.show_name %>" data-season="<%= episode.season %>" data-episode="<%= episode.episode %>" data-date="<%= moment(episode.started_at).format('dddd, MMMM Do YYYY') %>"><i class="fas fa-info"></i></a>
                                        <a href="javascript:void(0)" onclick="checkNext(<%= episode.show_id %>,1)" class="btn btn-primary fright "><i class="fas fa-check"></i> Finish</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <% }); %>
    </script>

    <script type="text/template" id="latestWatchingTemplate">
        <div class="row">
            <div class="col-md-8">
                <h5 class="mt-0 mb-1">Latest Watching</h5>
                <p class="card-text"><%= show_name %> - Season <%= season %> Episode <%= episode %></p>
            </div>
            <div class="col-md-4 card-v-align btn-block">
                <a class="btn btn-light fright"><i class="fas fa-arrow-right"></i>Watching
                    List </a>
                <a href="javascript:void(0)" onclick="checkNext(<%= show_id %>)" class="btn btn-primary fright"><i class="fas fa-check"></i>Check in </a>

            </div>
        </div>
    </script>
    <script type="text/template" id="DNFTemplate">
        <div class="card-body text-center">
            <div class="animated swing delay-1s ">
                <i class="flaticon-information-circular-button-symbol wobble-info"></i>
            </div>
            <p><%= message %></p>
            <p> <b><%= episode_name %></b> <small>from</small> Season <%= season %> - Episode <%= episode %></p>
            <button type="button" class="btn btn-secondary btn-fullw" id="btnUpdateFinishCurrent" onclick="updateFinishCurrent(<%= show_id %>,true,true)">Do you want to check as finished? </button>
        </div>
    </script>
    <script type="text/template" id="StartNextTemplateSuccess">
        <div class="card-body text-center">
            <div class="animated swing delay-1s ">
                <i class="flaticon-tick wobble-success"></i>
            </div>
            <h4>yay!</h4>
            <p><%= message %></p>
                    </div>
    </script>
    <script type="text/template" id="StartNextTemplateError">
        <div class="card-body text-center">
            <div class="animated swing delay-1s ">
                <i class="flaticon-close wobble-error"></i>
            </div>
            <h4>Sorry!</h4>
            <p><%= error %></p>
                    </div>
    </script>
    <script type="text/template" id="StartNextTemplate">
        <div class="card">
            <div class="card-body text-center">
                <p>The next episode to watch is:</p>
                <h5><%= episodename %></h5>
                <small>from:</small>
                <h5>Season <%= seasonnumber %> - Episode <%= seasonepisode %></h5>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <button onclick="postNextEpisode(<%= showid %>);btnLoading('btnStartWatching')" id="btnStartWatching" class="btn btn-secondary btn-fullw">Begin</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <a style="margin-top:25px;" class="btn btn-primary" href="{{url('help#next-episode')}}" data-toggle="tooltip" data-placement="bottom" title="What is this?">What does this mean? <i
                                    class="flaticon-information-circular-button-symbol"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>
@endsection
