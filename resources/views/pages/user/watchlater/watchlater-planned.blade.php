@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Schedule your shows so you don't forget to watch them!</h3>
                <p>Choose the dates for the available shows below!</p>
            </div>
        </div>
        @include('pages.user.watchlater.watchlater-menu-bar')
        @if($timezone === null || $timezone === '')
            <div class="alert alert-warning" role="alert">
                <strong>Your timezone is not set!</strong> <span>Please set your timezone because without it we can't use any of our notification methods that you have set up!</span>
                <p class="mb-0">You can do that by going to your <a target="_blank" href="{{url('/account-settings#timezone')}}">Account Settings > Timezone</a> and choosing the correct one.</p>
            </div>
        @endif
        <div class="row mb-4">
            <div class="col-md-12">
                <button onclick="requestRemoveExpiredPlanned()" class="btn btn-secondary float-left">Clear expired</button>
            </div>
        </div>
        <div class="row" id="shows-list-content">

        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        function fireConfirm(show_id, season ,episode_nr) {
            Swal.fire({
                title: 'Canceling schedule',
                text: "This will remove the current schedule.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: 'grey',
                confirmButtonText: 'Ye!',
                cancelButtonText: 'Nope!'
            }).then((result) => {
                if (result.value) {
                    requestRemoveCurrentSchedule(show_id, season, episode_nr);
                }
            })
        }
        function handleRequestShowsPlanned(res) {
            if(res.status === false){
                let template = _.template($('#noShowsPlannedTemplate').html());
                $('#shows-list-content').html(template({}));
                return;
            }

            let template = _.template($('#show-list').html());
            $('#shows-list-content').html(template({shows: res.content.shows, planned:res.content.planned}));
        }

        function handleRequestExpiredPlanned (res) {
            if(res.status === false){
                messageMessage(iterateMessages(res.content.message));
                return;
            }

            messageSuccess(iterateMessages(res.content.message));
            requestShowsPlanned();
        }

        function handleRemoveCurrentScheduleRequest (res) {
            Swal.fire({
                type: 'success',
                title: 'Noice',
                html: iterateMessages(res.content.message)
            });

            requestShowsPlanned();
        }

        function requestShowsPlanned() {
            ajaxRequest(APP_URL_FULL, {_token: APP_CSRF}, 'post', handleRequestShowsPlanned,false,true);
        }

        function requestRemoveExpiredPlanned () {
            ajaxRequest(APP_URL_FULL+'/remove/expired-schedules', {_token: APP_CSRF}, 'post', handleRequestExpiredPlanned);
        }

        function requestRemoveCurrentSchedule (show_id, season, episode) {
            ajaxRequest(APP_URL_FULL+'/remove/schedule',
                {_token: APP_CSRF, show_id : show_id, season : season, episode : episode},
                'post', handleRemoveCurrentScheduleRequest, false);
        }

        function updateTimes() {
            $('small#times').each(function(){
                let time = $(this).data('time');
                $(this).html(moment(time, 'YYYY-MM-DD hh:mm:s').fromNow());
            });
        }

        $(function () {
            requestShowsPlanned();
            setInterval(updateTimes,1000);
        });
    </script>
@stop

@section('underscore_templates')
    <template id="noShowsPlannedTemplate">
        <div class="col-md-12 text-center">
            <h2>Hey! You there!</h2>
            <p>You don't have anything planned...</p>
        </div>
    </template>
    <script type="text/template" id="show-list">
        <% _.each(planned,function(data,key){%>
        <div class="col-md-12 mb-4">
            <h4><%= shows[key].name %></h4>
            <div class="list-group">
                <% _.each(data, function(episode){ %>
                <div class="row">
                    <div class="col-md-9">
                        <a href="#"  class="list-group-item list-group-item-action">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">Season <%= episode.season %> - Episode <%= episode.episode %></h5>
                                <small id="times" data-time="<%= episode.schedule_date+' '+episode.schedule_time %>"><%= moment(episode.schedule_date+' '+episode.schedule_time, 'YYYY-MM-DD hh:mm:s').fromNow() %></small>
                            </div>
                            <small>Planned for <%= moment(episode.schedule_date+' '+episode.schedule_time, 'YYYY-MM-DD hh:mm:s').format('MMMM Do YYYY, h:mm a') %></small>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="javascript:void(0)"  class="list-group-item list-group-item-action">
                            <div class="text-center">
                                <button onclick="fireConfirm(<%= key %> , <%= episode.season %> ,<%= episode.episode %>)" class="btn btn-dark ml-2"><i class="fas fa-ban"></i></button>
                            </div>
                        </a>
                    </div>
                </div>

                <% }); %>
            </div>
        </div>
        <% });%>
    </script>
@stop
