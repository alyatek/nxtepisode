@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Schedule your shows so you don't forget to watch them!</h3>
                <p>Choose the dates for the available shows below!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="messages">

            </div>
        </div>
        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Unit 4 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="5907799272"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        @endif
        @include('pages.user.watchlater.watchlater-menu-bar')
        @if($timezone === null || $timezone === '')
            <div class="alert alert-warning" role="alert">
                <h5>Your timezone is not set!</h5> <span>Please set your timezone because without it we can't use any of our notification methods that you have set up!</span>
                <p class="mb-0">You can do that by going to your <a target="_blank"
                                                                    href="{{url('/account-settings#timezone')}}">Account
                        Settings > Timezone</a> and choosing the correct one.</p>
            </div>
        @endif
        <div class="row align-items-center" id="shows-list">
        </div>
        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Bottom 1 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="3127375655"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Unit 4 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="5907799272"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        @endif
    </div>

    <div class="modal fade" id="modalEpisodesScheduler" tabindex="-1" role="dialog"
         aria-labelledby="modalEpisodesSchedulerlabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Choose the dates</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form_schedule" method="post">
                    {{csrf_field()}}
                    <div class="modal-body" id="modal-content">
                        ...
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Schedule</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addShowToList" tabindex="-1" role="dialog" aria-labelledby="addShowToListLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">uhh this is awkward</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-content-add-show">
                    asd
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        function handleShowsRequest(res) {
            let template = _.template($('#shows-template').html());
            $('#shows-list').html(template({shows: res.content.message.shows}));
        }

        function handleEpisodesListRequest(res) {
            if (res.status === false) {
                if (res.status_insert === true) {
                    let template_modal = _.template($('#addShowToListTemplate').html());

                    const api_id = parsePlanURL(APP_URL_FULL)[1];
                    const id = parsePlanURL(APP_URL_FULL)[0];

                    $('#modal-content-add-show').html(template_modal({
                        show: res.content.show,
                        show_media: res.content.show_media,
                        messages: res.content.message,
                        sid: api_id,
                        id: id,
                    }));
                    $('#addShowToList').modal('show');
                    return;
                }
                messageFailed(iterateMessages(res.content.message));
                return;
            }
            if (res.status === true) {
                let template_modal = _.template($('#modal-content-template').html());
                $('#modal-content').html(template_modal({
                    episodes: res.content.message.episodes,
                    premium: res.premium,
                    show_id: res.content.message.show_id,
                    filled: res.filled
                }));
                $('.datepicker').pickadate({
                    formatSubmit: 'yyyy-mm-dd',
                    min: new Date({{now()->tz((\Illuminate\Support\Facades\Auth::user()->timezone ? \Illuminate\Support\Facades\Auth::user()->timezone : 'Europe/London'))->year}}, ({{now()->tz((\Illuminate\Support\Facades\Auth::user()->timezone ? \Illuminate\Support\Facades\Auth::user()->timezone : 'Europe/London'))->month}} -1),{{now()->tz((\Illuminate\Support\Facades\Auth::user()->timezone ? \Illuminate\Support\Facades\Auth::user()->timezone : 'Europe/London'))->day}}),
                });
                $('.timepicker').pickatime({
                    formatSubmit: 'HH:i',
                });
                $('[data-toggle="tooltip"]').tooltip();
                $("#modalEpisodesScheduler").modal();
                return;
            }
        }

        function handleInsertSchedule(res) {
            if (res.status === false) {
                if (res.content.params.invalid_dates && res.content.params.invalid_dates === true) {
                    const dates = res.content.params.invalid_dates_episodes;
                    _.each(dates, function (epid) {
                        $('#date_episode_' + epid).addClass('is-invalid');
                        $('#date_episode_' + epid).parent().addClass('has-danger');
                    });
                }
                pMessage({
                    title: 'Dang!',
                    message: iterateMessages(res.content.message),
                    type: 'warning'
                });
                return;
            }
            pMessage({
                title: 'N1!',
                message: iterateMessages(res.content.message),
                type: 'success'
            });
        }

        function requestEpisodesList(show_id) {
            ajaxRequest(APP_URL + '/watchlater/get/show/episodes/list/' + show_id, {}, 'get', handleEpisodesListRequest, false, true);
        }

        function requestInsertSchedule(form) {
            ajaxRequest(APP_URL_FULL + '/insert', $(form).serialize(), 'post', handleInsertSchedule, false, true);
        }

        function refreshContent(show_id) {
            setTimeout(function () {
                ajaxRequest(APP_URL + '/watchlater/get/shows', {}, 'get', handleShowsRequest);
                requestEpisodesList(show_id);
                $('#addShowToList').modal('hide');
            }, 1700);
        }

        $(function () {
            ajaxRequest(APP_URL + '/watchlater/get/shows', {}, 'get', handleShowsRequest);

            //check url
            // console.log(parsePlanURL(APP_URL_FULL));
            if (id = parsePlanURL(APP_URL_FULL)) {
                requestEpisodesList(id[0]);
            }

            // $('.datepicker').pickadate();
            $('#form_schedule').submit(function (evt) {
                evt.preventDefault();
                $('[id^=date_episode_]').removeClass('is-invalid').parent().removeClass('has-danger');
                // $('[id^=date_episode_]');
                requestInsertSchedule($(this));
            });
        });
    </script>
@stop
@section('underscore_templates')
    <script type="text/template" id="addShowToListTemplate">
        <div class="row">
            <div class="col-md-12 text-center">
                <img class="w-50 rounded mb-4" src="<%= show_media.small.path %>">
                <h2><%= show.name %></h2>
                <p> <%= iterateMessages(messages) %> </p>

                <button id="<%= sid %>" onclick="addToMyWatchlist(<%= sid %>); refreshContent(<%= id %>)" class="btn btn-light w-100">Add to my list</button>
            </div>
        </div>
    </script>
    <script type="text/template" id="modal-content-template">
        <p>You have the following episodes to be scheduled.</p>
        <% if(_.isEmpty(episodes)){ %>
        <h3 style="text-align: center;">You finished all the episode.</h3>
        <p style="text-align: center;">You have no episodes left to watch you addict.</p>
        <% } %>
        <ul class="list-group " style="margin-bottom:25px;">
            <input type="hidden" name="show_id" value="<%= show_id %>">
            <% _.each(episodes, function(episode){ %>
            <li class="list-group-item">
                <div class="row">
                    <div class="col-md-4 vert-align">
                        <i>Season: </i><%= episode.episode.season %> - <i>Episode</i>: <%= episode.episode.episode %>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group vert-align">
                            <% let date_value; %>
                            <% let time_value; %>
                            <% _.each(filled, function(epis){ %>
                                <% if (epis.episode === episode.episode.episode) {%>
                                    <% date_value = epis.schedule_date %>
                                    <% time_value = epis.schedule_time %>
                                <% } %>
                            <% }); %>

                            <% if(date_value){ %>
                                <span data-toggle="tooltip" data-placement="top" title="Want to edit? Go to Scheduled Shows to find out more!" style="float:right;"><%= $.format.date(date_value+" 00:00:00", "d MMMM, yyyy")%></span>
                            <% } else { %>
                                <input <%= (date_value ? 'disabled' : '') %> placeholder="Pick a date" type="hidden" style="" class="datepicker form-control" id="date_episode_<%= episode.episode.episode %>" name="date_episode[<%= episode.episode.episode %>]" >
                            <% } %>

                        </div>
                    </div>
                    <div class="col-md-4">
                    <div class="form-group vert-align">
                            <% if(date_value){ %>
                                        <span data-toggle="tooltip" data-placement="top" title="Want to edit? Go to Scheduled Shows to find out more!" ><%= $.format.date("1999-00-00 "+time_value, "hh:mm a") %></span>
                                    <% } else { %>
                                        <input <%= (date_value ? 'disabled' : '') %> type="hidden" placeholder="Pick a time" class="timepicker form-control float-right" id="time_episode_<%= episode.episode.episode %>" name="time_episode[<%= episode.episode.episode %>]" >
                            <% } %>
                    </div>
                    </div>
                </div>

            </li>
            <% }); %>

        </ul>

            <% if(premium.status === false){ %>
            <div class="text-center">
                <small><%= iterateMessages(premium.message) %></small>
            </div>
            <% } %>
    </script>
    <script type="text/template" id="shows-template">
        <% _.each (shows,function(show){%>
        <div class="col-sm col-md-3 card-watchlater text-center">
            <div class="card">
                <div class="card-body d-flex" style="height:150px;">
                    <div class="text-center m-auto justify-content-center align-self-center">
                        <h5 class="card-title"><%= show.name %></h5>
                        <a href="javascript:void(0)" onclick="requestEpisodesList(<%= show.id %>)" class="btn btn-primary">Schedule</a>
                    </div>
                </div>
            </div>
        </div>
        <% }); %>
    </script>
@stop
