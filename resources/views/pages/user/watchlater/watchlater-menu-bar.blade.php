<div class="row">
    <div class="col">
        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
            <button type="button" onclick="window.location.href = '{{url('watchlater')}}';" class="btn btn-secondary {{($page === 'watchlater' ? 'active' : '')}}" >Scheduling Shows</button>
            <button type="button" onclick="window.location.href = '{{url('watchlater/planned')}}';" class="btn btn-secondary {{($page === 'planned' ? 'active' : '')}}">already scheduled</button>
        </div>
        <div class="float-md-right mt-4 mt-md-0" role="group" aria-label="Button group with nested dropdown">
            <a href="{{url('watchlater/settings')}}" class="btn btn-secondary  {{($page === 'settings' ? 'active' : '')}}"><i class="flaticon-cog"></i> Settings</a>
        </div>
    </div>
</div>
<br>
