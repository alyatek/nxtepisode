@extends('tmpl.master')

@section('content')
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(function () {
            OneSignal.on('subscriptionChange', function (isSubscribed) {
                if (isSubscribed === true) {
                    OneSignal.getUserId(function (userId) {
                        requestWebPushNotification(userId);
                        console.log("OneSignal User ID:", userId);
                    });
                } else if (isSubscribed === false) {
                    requestWebPushNotificationRemoval();
                }
                // console.log("The user's subscription state is now:", isSubscribed);
            });
            OneSignal.init({
                @if(config('app.debug') === true)
                appId: "a0e03e55-e210-4785-8c4d-464aa9747437",
                @else
                appId: "5a931250-dad9-487b-b427-b143fe680737",
                @endif
            });
        });
    </script>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Watchlater Settings</h3>
                <p>Configure your preferences to best adapt your needs for planing your shows!</p>
            </div>
        </div>
        @include('pages.user.watchlater.watchlater-menu-bar')
        @if(session()->has('facebook_registration'))
            @if($data = session()->get('facebook_registration'))
                @if($data['status'] === true)
                    <div class="alert alert-primary" role="alert">
                        {!! $data['message'] !!}
                    </div>
                @elseif($data['status'] === false)
                    <div class="alert alert-info" role="alert">
                        {{$data['message']}}
                    </div>
                @endif
            @endif
        @endif
        
        @if($timezone === null || $timezone === '')
            <div class="alert alert-warning" role="alert">
                <strong>Your timezone is not set!</strong> <span>Please set your timezone because without it we can't use any of our notification methods that you have set up!</span>
                <p class="mb-0">You can do that by going to your <a target="_blank" href="{{url('/account-settings#timezone')}}">Account Settings > Timezone</a> and choosing the correct one.</p>
            </div>
        @endif
        <div class="row">
            <div class="container">
                {{--<div class="card">--}}
                {{--<div class="card-body">--}}
                <div class="row watchlater-blocks-settings card-deck">
                    <div class="mb-3 col-sm-12 col-md-3">
                        <div class="card card-fluid">
                            <div class="card-body text-center">
                                <img class="settings-icons mb-4" src="{{url('img/school-bell.svg')}}"
                                     alt="app notifications">
                                <h5 class="card-title">App Notifications</h5>
                                <p class="card-text">Alert in the website.</p>
                                <button href="#" class="btn btn-primary " disabled>Active</button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3 col-sm-12 col-md-3">
                        <div class="card card-fluid">
                            <div class="card-body text-center">
                                <img class="settings-icons ml-3 mb-4" src="{{url('img/mail.svg')}}"
                                     alt="app notifications">
                                <h5 class="card-title">Email</h5>
                                <p class="card-text">Get notified via email.</p>
                                {{--<a href="#" class="btn btn-primary">Enable</a>--}}
                                <span id="email-notifications"></span>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3 col-sm-12 col-md-3">
                        <div class="card card-fluid">
                            <div class="card-body text-center">
                                <img class="settings-icons mb-4" src="{{url('img/announcer.svg')}}"
                                     alt="app notifications">
                                <h5 class="card-title">Web Notifications</h5>
                                <p class="card-text">Notified from your browser.</p>
                                {{--<a class='onesignal-customlink-container'></a>--}}
                                <span id="webpush-section"></span>
                                {{--<a href="#" class="btn btn-primary">Enable</a>--}}
                            </div>
                        </div>
                    </div>
                    <div class="mb-3 col-sm-12 col-md-3">
                        <div class="card card-fluid">
                            <div class="card-body text-center">
                                <img class="settings-icons mb-4" src="{{url('img/messenger.svg')}}"
                                     alt="facebook messages">
                                <h5 class="card-title">Facebook Messages</h5>
                                <p class="card-text">We'll <i>message</i> you.</p>
                                <span id="how-to-facebook-messages">
                                    @if(!config('app.facebook_messages_status'))
                                        <button class="disabled btn btn-dark">Temporary unavailable</button>
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <!-- Email Notifications -->
    <div class="modal fade" id="email-notifications-modal" tabindex="-1" role="dialog"
         aria-labelledby="emailNotificationsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="emailNotificationsModalLabel">Configuring Email Notifications</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form-email-notification">
                    <div class="modal-body">
                        {{--<h5>In which email do you want to be notified?</h5>--}}
                        {{--<br>--}}

                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">By configuring this method you agree to receive
                                notification emails.</label>
                            <input type="hidden" class="form-control" id="notificationEmailInput"
                                   name="notificationEmailInput"
                                   value="{{\Illuminate\Support\Facades\Auth::user()->email}}">
                            <small id="emailHelp" class="form-text text-muted">You can edit or opt out of this
                                notification anytime you want.
                            </small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save configuration</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Webpush Notifications -->
    <div class="modal fade" id="webpush-notifications-modal" tabindex="-1" role="dialog"
         aria-labelledby="emailNotificationsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="emailNotificationsModalLabel">Configuring Web Notifications</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if(\Browser::isDesktop())
                        @if(\Browser::isSafari())
                            <p>Currently we do not support Safari.</p>
                        @elseif (\Browser::isIE() )
                            <p>We do not support this disgrace of a browser.</p>
                            <p>Please upgrade to Chrome or Firefox, or whatever, we don't care, as long as it is not
                                this.</p>
                        @else
                            <h6>Get updated with a browser notification when it's time to watch a show you planned!</h6>
                            <span class='onesignal-customlink-container'></span>
                            <br><br>
                            <p>By allowing this feature you will additionally get:</p>
                            <ul>
                                <li>Occasional notifications when you forgot to finish watching an episode</li>
                                <li>More to come in the future.
                                    <small>(Don't worry! We will warn you when they come out and you will be able to
                                        opt-out if you are not interested.)
                                    </small>
                                </li>
                            </ul>
                            <hr>
                            <h6>Want the notifications on your mobile as well?</h6>
                            <p>Go to this page on your device and make this configuration there.</p>
                        @endif
                    @endif

                    @if(strpos(strtolower(\Browser::platformName()), 'ios') !== false)
                        <p>We are sorry but currently we do not support iOS devices.</p>
                    @elseif(strpos(strtolower(\Browser::platformName()), 'android') !== false)
                        <h6>Get updated with a browser notification when it's time to watch a show you planned!</h6>
                        <span class='onesignal-customlink-container'></span>
                        <br><br>
                        <p>By allowing this feature you will additionally get:</p>
                        <ul>
                            <li>Occasional notifications when you forgot to finish watching an episode</li>
                            <li>More to come in the future.
                                <small>(Don't worry! We will warn you when they come out and you will be able to
                                    opt-out if you are not interested.)
                                </small>
                            </li>
                        </ul>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')

    <script type="text/javascript">
        function requestWebPushNotification(userId) {
            ajaxRequest(APP_URL_FULL + '/configure/webpush', {
                _token: APP_CSRF,
                onesignal_player_id: userId,
                device: '{{strpos(strtolower(\Browser::platformName()), 'android')}}',
            }, 'post', handleWebPushNotificationRequest);
        }

        function requestEmailNotification() {
            ajaxRequest(APP_URL_FULL + '/configure/email', $('#form-email-notification').serialize(), 'post', handleEmailNotificationRequest);
        }

        function requestEmailNotificationStatus() {
            ajaxRequest(APP_URL_FULL + '/get/email/configuration', {}, 'get', handleEmailNotificationStatus, false, true)
        }

        function requestWebNotificationStatus() {
            // ajaxRequest(APP_URL_FULL + '/webpush/status', {_token: APP_CSRF}, 'post', handleWebPushNotificationStatusRequest);
            let template = _.template($('#web-notifications-enabled').html());
            $('#webpush-section').html(template());
        }

        function requestEmailNotificationRemoval() {
            ajaxRequest(APP_URL_FULL + '/remove/email', {_token: APP_CSRF}, 'post', handleEmailNotificationRemovalRequest);
        }

        // function requestWebPushNotificationRemoval() {
        // }

        function requestFacebookMessageStatus() {
            @if(config('app.facebook_messages_status'))
            ajaxRequest(APP_URL_FULL + '/get/facebook-messages/configuration', {}, 'get', handleFacebookMessageStatus);
            @endif
        }

        function requestDisableFacebookMessages() {
            ajaxRequest(APP_URL_FULL + '/remove/facebook-messages', {_token: APP_CSRF}, 'post', handleFacebookMessageDisable);
        }

        function handleFacebookMessageDisable(res) {
            messageMessage(iterateMessages(res.content.message));
        }

        function handleFacebookMessageStatus(res) {
            if (res.status === false) {
                messageFailed();
                return;
            }

            if (res.content.params.configuration === true) {
                let template = _.template($('#facebook-message-template-configured').html());
                $('#how-to-facebook-messages').html(template());
                return;
            }

            let template = _.template($('#facebook-message-template-unconfigured').html());
            $('#how-to-facebook-messages').html(template());

        }

        function handleWebPushNotificationRequest(res) {
            if (res.status === false) {
                pMessage({
                    title: 'Po poo Po poo pooooo!',
                    message: iterateMessages(res.content.message),
                    type: 'warning'
                });
                $('body').find('.modal').modal('toggle');
                $('.modal-backdrop').remove();
                return;
            }
            pMessage({
                title: 'Huray',
                message: iterateMessages(res.content.message),
                type: 'success'
            });
        }

        function handleEmailNotificationRequest(res) {
            if (res.status === false) {
                pMessage({
                    title: 'Po poo Po poo pooooo!',
                    message: iterateMessages(res.content.message),
                    type: 'warning'
                });
                $('body').find('.modal').modal('toggle');
                $('.modal-backdrop').remove();
                return;
            }
            pMessage({
                title: 'Huray',
                message: iterateMessages(res.content.message),
                type: 'success'
            });
            $('body').find('.modal').fadeOut(1500).modal('toggle');
            $('.modal-backdrop').remove();
            requestEmailNotificationStatus();
        }

        function handleEmailNotificationStatus(res) {
            if (res.content.configured === false) {
                let template = _.template($('#not-configured-email-notification-template').html());
                $('#email-notifications').html(template);
                readyToFormatForms();
                return;
            }
            //
            let template = _.template($('#configured-email-notification-template').html());
            $('#email-notifications').html(template({email: res.content.data.method_value}));
            readyToFormatForms();
        }

        function handleWebPushNotificationStatusRequest(res) {

        }

        function handleEmailNotificationRemovalRequest(res) {
            if (res.status === false) {
                pMessage({
                    title: 'Nope!',
                    message: iterateMessages(res.content.message),
                    type: 'warning'
                });
                requestEmailNotificationStatus();
                return;
            }

            pMessage({
                title: 'Ok Boe!',
                message: iterateMessages(res.content.message),
                type: 'success'
            });
            requestEmailNotificationStatus();
        }

        function readyToFormatForms() {
            $('#form-email-notification').submit(function (evt) {
                evt.preventDefault();
                requestEmailNotification();
            });
        }

        $(function () {
            requestEmailNotificationStatus();
            requestWebNotificationStatus();
            requestFacebookMessageStatus();

            $('[data-toggle="popover"]').popover({
                trigger: 'focus',
                html: true
            });
        })
    </script>
@stop

@section('underscore_templates')
    @if(config('app.facebook_messages_status'))
        <script type="text/template" id="facebook-message-template-unconfigured">
            <button class="btn btn-primary" data-fancybox data-src="#facebook-message-message">
                How to
            </button>
            <div style="display: none;" id="facebook-message-message">
                <h3>yeeee boi!</h3>
                <p>You are now in a very important quest, in which if you complete it, great things will happen, like...
                    humm... you
                    will be notified via a Facebook Message when is time to a show you planned.</p>
                <p>
                    <small>Don't worry we wont spam you with useless crap, we might occasionally send you
                        "notifications"
                        when you are watching an episode for a long time(<i>cause you might have forgot to check in</i>).
                    </small>
                </p>
                <br>
                <p>How to:</p>
                <ul>
                    <li>Just go to our <a target="_blank" class="btn btn-primary btn-sm"
                                          href="{{config('app.facebook_url')}}">Facebook Page</a> and send us a message
                        with
                        the content of <b><i>NOTIFICATION</i></b> and you will receive a link to active your account.
                    </li>
                    <li>After activating that's it, it's done. noice.</li>
                </ul>
                <small>Psst! Just one thing. Sometimes you might not receive right away the link to activate, it can
                    take a
                    while, because Facebook needs to tell us that you sent us a message and they don't always send it
                    instantly.
                </small>
            </div>
        </script>
        <script type="text/template" id="facebook-message-template-configured">
            <button class="btn btn-primary" data-fancybox data-src="#facebook-message-message">
                How to
            </button>
            <div style="display: none;" id="facebook-message-message">
                <h3>Hi there!</h3>
                <p>You have this service currently enabled</p>
                <hr>
                <h5>How to disable</h5>
                <p>You can disable this service by following the next steps</p>
                <ul>
                    <li>Press the button bellow and we will send you a message to your Facebook with a deactivation link
                        (we
                        need to verify if it truly you).
                    </li>
                    <br>
                    <li style="list-style-type: none;">
                        <button onclick="requestDisableFacebookMessages()" class="btn btn-dark btn-sm">Disable Facebook
                            Messages
                        </button>
                    </li>
                </ul>
            </div>
        </script>
    @endif
    <script type="text/template" id="web-notifications-enabled">
        <button class="btn btn-primary" data-toggle="modal" data-target="#webpush-notifications-modal">
            Configure
        </button>
    </script>
    <script type="text/template" id="not-configured-email-notification-template">
        <button class="btn btn-primary" data-toggle="modal" data-target="#email-notifications-modal">
            Enable
        </button>
        <!-- Email Notifications Configuration Modal -->
    </script>
    <script type="text/template" id="configured-email-notification-template">

        {{--<button type="button" class="btn btn-secondary btn-sm"  data-toggle="modal" data-target="#email-notifications-modal">Edit</button>--}}
        <button type="button" onclick="requestEmailNotificationRemoval()" class="btn btn-default">Disable
        </button>
        <!-- Email Notifications Configuration Modal -->
    </script>
    @if(collect($premium)->get('premium') === false)
        <script type="text/template" id="premium-features">
            <hr>
            <h4>Get Premium Notifications!</h4>
            <p>By acquiring premium you can configure the following notifications styles:</p>
            <div class="row text-center">
                <div class="col-md-4">
                    <i class="flaticon-calendar-11 premium-notifications"></i>
                    <h4>Google Calendar</h4>
                    <p>Get notified by the most popular calendar out there!</p>
                </div>
                <div class="col-md-4">
                    <i class="flaticon-notification premium-notifications"></i>
                    <h4>Browser Notifications</h4>
                    <p>When it's show time you will get a notification from your browser.</p>
                </div>
                <div class="col-md-4">
                    <i class="flaticon-smartphone premium-notifications"></i>
                    <h4>SMS Notifications</h4>
                    <p>With this you will never miss a show!</p>
                </div>
            </div>
        </script>
    @elseif(collect($premium)->get('premium') === true)
    @endif
@stop
