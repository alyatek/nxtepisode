@extends('tmpl.master')

@section('content')
    <section class="content">
        <div class="container">
            <h1>Favorites</h1>
            <div class="row" id="pageContent">

            </div>
            @if(1==1)
                <div class="row ad-space">
                    <div class="col-md-12 text-center">Ads</div>
                </div>
                <div class="row ad-space">
                    <div class="col-md-12 text-center">Ads</div>
                </div>
            @endif
        </div>
    </section>
@stop

@section('js')
    <script>
        $(function () {
            requestUserFavorites(templateUserFavorites);
        });
    </script>
@stop

@section('underscore_templates')
    <script type="text/template" id="templateFavorites">
        <% _.each(shows, function(show){ %>
        <div class="col-md-2" id="<%= show.slug %>-card">
            <div class="shadow p-3 mb-5 bg-white rounded shadow-custom ">
                <div class="card bg-dark text-white card-clear-bg card-height">
                    <img class="card-img img-fluid card-img-fix" src="<%= (show.media.path ? show.media.path : APP_URL+'/img/img-not-found.png' ) %>" alt="">
                    <div class="card-img-overlay text-center card-favorite"><a href="javascript:void(0)" onclick="requestShowFavorite_favorites(<%= show.id %>, '<%= show.slug %>')" class="btn btn-link btn-icon btn-icon-fav btn-favorite" data-toggle="tooltip" data-placement="top" title="Press to remove as favorite."><i class="ifav fas fa-heart icon-favorites"></i></a></div>

                    <div class="card-img-overlay cfade card-img-overlay-bottom">

                        <a href="http://localhost/nxtepisode/public/watchlist/show/<%= show.id %>"><h5 class="card-title" style="">
                                <%= show.name %></h5></a></div>
                </div>
            </div>
        </div>
        <% }); %>
    </script>
    <script type="text/template" id="templateNoFavorites">
        <div class="col-md-12">
            <div class="jumbotron text-center">
                <svg class="icon">
                    <use xlink:href="#broken-heart"/>
                </svg>
                <h1 class="display-4">oh noooo!</h1>
                <p class="lead"><%= message %></p>
            </div>
        </div>
    </script>
@stop
