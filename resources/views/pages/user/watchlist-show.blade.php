@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="col-md-12 parallax h-100 mb-4 rounded">
            <div class="row" style="padding: 20px 0 0 0;" id="show_info">

            </div>
            @if(1==1)
                <div class="row ad-space">
                    <div class="col-md-12 text-center">
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Unit 4 -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-1071507716766529"
                             data-ad-slot="5907799272"
                             data-ad-format="auto"
                             data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            @endif
        </div>
        <div class="row mb-4" id="currently_watching_area"
             @if($currently_watching === false) style="display:none;" @endif>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body align-middle" id="currently_watching_content">
                        <span class="spinner-grow spinner-grow-1-3s  text-secondary align-middle" role="status">
                        </span>
                        @if($currently_watching !== false)
                            <span class="">You are currently watching
                            <b><span id="currently_watching_name">{{$currently_watching['name']}}</span></b> from Season
                            <b><span id="currently_watching_season">{{$currently_watching['season']}}</span></b> - Episode
                            <b><span id="currently_watching_episode">{{$currently_watching['episode']}}</span></b> - <span
                                        id="currently_watching_episode_finish_message"><a onclick="callWatchNext(true)"
                                                                                          href="javascript:void(0)">Do you want to finish it?</a></span> </span>
                        @else
                            <span class="">You are currently watching
                            <b><span id="currently_watching_name"></span></b> from Season
                            <b><span id="currently_watching_season"></span></b> - Episode
                            <b><span id="currently_watching_episode"></span></b> - <span
                                        id="currently_watching_episode_finish_message"><a onclick="callWatchNext(true)"
                                                                                          href="javascript:void(0)">Do you want to finish it?</a></span> </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-4">
            <span class="d-none d-lg-block">
                <div class="col-md-12 ">
                    <button onclick="callWatchNext(true)" class="btn btn-secondary"><i class="far fa-eye"></i> Watch episode</button>
                    <button onclick="sendToPlans()" class="btn btn-secondary ml-1"><i class="far fa-calendar"></i> Plan Episodes</button>
                    <button class="btn btn-secondary ml-1" data-toggle="collapse" href="#about_show" role="button"><i
                                class="far fa-question-circle"></i> About
                    </button>
                </div>
            </span>
            <span class="d-lg-none col-md-12 no-gutters">
                <div class="col-md-12 mb-3">
                    <button onclick="callWatchNext(true)" class="btn btn-secondary w-100 "><i class="far fa-eye"></i> Watch episode</button>
                </div>
                <div class="col-md-12 mb-3">
                    <button onclick="sendToPlans()" class="btn btn-secondary w-100 "><i class="far fa-calendar"></i> Plan Episodes</button>
                </div>
                <div class="col-md-12 mb-3 w-100">
                    <button class="btn btn-secondary w-100 " data-toggle="collapse" href="#about_show" role="button"><i
                                class="far fa-question-circle"></i> About
                    </button>
                </div>
            </span>
        </div>
        <div class="row show-seasons">
            <div class="col-md-12 collapse mb-4" id="about_show">

            </div>
            <div class="col-md-12" id="SeasonsEpisodes">

            </div>
        </div>
        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Bottom 1 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="3127375655"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Unit 3 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="1212385102"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        @endif
    </div>
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Season 1</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row latest-watching-notification">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h5 class="mt-0 mb-1">Episode 1</h5>
                                            <p class="card-text">Quisque velit nisi, pretium ut lacinia in, elementum id
                                                enim. Donec sollicitudin molestie malesuada. Quisque velit nisi, pretium
                                                ut lacinia in, elementum id enim.</p>
                                            <p class="episode-rating"><i class="fas fa-star"></i><i
                                                        class="fas fa-star"></i><i class="fas fa-star"></i><i
                                                        class="fas fa-star"></i><i class="far fa-star"></i></p>
                                        </div>
                                        <div class="col-md-4 card-v-align btn-block">
                                            <a href="" class="btn btn-success fright btn-icon"><i
                                                        class="fas fa-eye "></i></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row latest-watching-notification">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h5 class="mt-0 mb-1">Episode 2</h5>
                                            <p class="card-text">Quisque velit nisi, pretium ut lacinia in, elementum id
                                                enim. Donec sollicitudin molestie malesuada. Quisque velit nisi, pretium
                                                ut lacinia in, elementum id enim.</p>
                                        </div>
                                        <div class="col-md-4 card-v-align btn-block">
                                            <a href="" class="btn btn-secondary fright btn-icon"><i
                                                        class="fas fa-eye-slash"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none;" id="watch-next-episode">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        function sendToPlans() {
            const show_id = $('#show_id').val();
            const api_id = $('#api_id').val();

            window.location.href = `/watchlater?plan=${show_id}&api_id=${api_id}`;
        }

        function requestEpisodeWatched(season, episode) {
            ajaxRequest(APP_URL_FULL + '/watch/episode', {
                episode: episode,
                season: season,
                show_id: parseFloat($('#show_id').val()),
                _token: APP_CSRF
            }, 'post', handleEpisodeWatched)
        }

        function requestUnWatchEpisode(season, episode) {
            ajaxRequest(APP_URL_FULL + '/unwatch/episode', {
                episode: episode,
                season: season,
                show_id: parseFloat($('#show_id').val()),
                _token: APP_CSRF
            }, 'post', handleEpisodeUnWatched)
        }

        function requestSeasonWatched(season) {
            ajaxRequest(APP_URL_FULL + '/watch/season', {
                season: season,
                show_id: parseFloat($('#show_id').val()),
                _token: APP_CSRF
            }, 'post', handleSeasonWatched)
        }

        function handleEpisodeUnWatched(res) {
            $('[data-toggle="tooltip"], .tooltip').tooltip("hide");

            if (res.status === false) {
                messageFailed(iterateMessages(res.content.message));
                return;
            }

            let buttonsTemplate = _.template($('#episodeNotWatchedButtons').html());
            $('#buttons_' + res.content.data.show.episode + '' + res.content.data.show.season).html(buttonsTemplate({
                season: res.content.data.show.season,
                episode: res.content.data.show.episode
            }));
            $('[data-toggle="popover"]').popover({html: true, trigger: 'focus'});
            $('[data-toggle="tooltip"]').tooltip();
            messageMessage(iterateMessages(res.content.message));
        }

        function handleEpisodeWatched(res) {
            $('[data-toggle="tooltip"], .tooltip').tooltip("hide");

            if (res.status === false) {
                messageFailed(iterateMessages(res.content.message));
                return;
            }

            let buttonsTemplate = _.template($('#episodeWatchedButtons').html());
            $('#buttons_' + res.content.data.show.episode + '' + res.content.data.show.season).html(buttonsTemplate({
                params: res.content.data.params,
                episode: res.content.data.show.episode,
                season: res.content.data.show.season,
            }));
            $('[data-toggle="popover"]').popover({html: true, trigger: 'focus'});
            $('[data-toggle="tooltip"]').tooltip();
            messageMessage(iterateMessages(res.content.message));
        }

        function handleSeasonWatched(res) {
            $('[data-toggle="tooltip"], .tooltip').tooltip("hide");

            if (res.status === false) {
                messageFailed(iterateMessages(res.content.message));
                return;
            }

            // let unwatchtemplate = _.template($('#btnSectionUnWatchSeasonTemplate').html());
            requestWatchlistShowSeasonsEpisodes(templateWatchlistShowSeasonsEpisodes);
            // $('#btnSectionWatchSeason'+res.content.season).html(unwatchtemplate({season:res.content.season}));

            $('[data-toggle="popover"]').popover({html: true, trigger: 'focus'});
            $('[data-toggle="tooltip"]').tooltip();
            messageMessage(iterateMessages(res.content.message));
        }

        $(function () {
            requestWatchlistShowInfo(templateWatchlistShowInfo);
            requestWatchlistShowSeasonsEpisodes(templateWatchlistShowSeasonsEpisodes);

            $('[data-toggle="popover"]').popover();
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop

@section('underscore_templates')
    <script type="text/template" id="about_show_template">
        <div class="card card-body">
            <div class="text-center text-md-left">
                <h5>Summary</h5>
                <p><%= (data.summary ? data.summary : 'Sorry. Can\'t get any info about this.') %></p>
            </div>
            <div class="row text-center text-md-left">
                <div class="col-sm-6 col-md-3"><h5><i class="far fa-flag"></i> Language</h5><p><%= (data.language ? data.language: 'N/A') %></p></div>
                <div class="col-sm-6 col-md-3"><h5><i class="fas fa-stopwatch"></i> Runtime</h5><p><%= (data.runtime ? data.runtime+' Mins ~' : 'How embarassing... we don\'t know.') %> </p></div>
                <div class="col-sm-6 col-md-3"><h5><i class="fas fa-star"></i> Premiered</h5><p><%= (data.premiered ? moment(data.premiered).format('Do MMMM YYYY') : 'Our dumb developer didn\'t figure out to get it.') %></p></div>
                <div class="col-sm-6 col-md-3">
                    <h5>
                        <i class="fas fa-link"></i> Links
                    </h5>
                    <p>
                        <%= s.toSentenceSerial(externals_clean, " / ", " / ") %></a>
                    </p>
                    </div>

            </div>
            <div class="row text-center text-md-left">
                <div class="col-md-12">
                    <h5><i class="far fa-calendar-alt"></i> TV Schedule</h5>
                    <p>Every Tuesday @ 10 PM</p>

<!--                    <a href=""> <i class="far fa-calendar"></i> I want to match my plans to match this schedule</a>-->
                </div>
            </div>
        </div>
    </script>
    <script type="text/template" id="episodeWatchedButtons">
        <span data-toggle="tooltip" data-placement="left" title="Watched"><i class="flaticon-tick"></i></span>
        <button onclick="requestUnWatchEpisode(<%= season %>, <%= episode %>)" class="btn btn-link" style="padding-right: 0px;"> <i class="fas fa-times"></i></button>
        <button class="btn btn-link" data-toggle="popover" data-placement="top" title="Info" data-content="<i class='flaticon-play'></i> Your started watching in: <br>

                                                    <span class='tooltip-indent'><%= (params.started_at === '2000-01-01 00:00:00' ?  moment(params.ended_at.date).format('Do MMMM YYYY @ hA') : moment(params.started_at).format('Do MMMM YYYY @ hA')) %></span>

                                                    <br><i class='flaticon-stop-button-1'></i> You ended watching in: <br> <span class='tooltip-indent'><%= moment(params.ended_at.date).format('Do MMMM YYYY @ hA') %></span>"><i class="flaticon-information-circular-button-symbol"></i></button>
    </script>
    <script type="text/template" id="episodeNotWatchedButtons">
        <span class="float-right" id="buttons_<%= episode+season%>">
        <span data-toggle="tooltip" data-placement="left" title="" data-original-title="To watch"><i class="flaticon-calendar-3"></i></span> <a style="margin-left:15px; margin-right:10px;" href="javascript:void(0)" onclick="requestEpisodeWatched(<%= season %>,<%= episode %>);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Set as watched">
            <i class="far fa-eye"></i>
        </a>
        </span>
    </script>
    <template id="btnSectionWatchSeasonTemplate">
        <a onclick="requestSeasonWatched(<%= season %>)" data-toggle="tooltip" data-placement="top" title="Set episodes as watched" role="button" class="btn btn-default float-lg-right">
            <i class="far fa-check-circle"></i>
        </a>
    </template>
    <template id="btnSectionUnWatchSeasonTemplate">
        <a onclick="requestSeasonUnWatched(<%= season %>)" data-toggle="tooltip" data-placement="top" title="Set episodes as not watched" role="button" class="btn btn-warning float-lg-right">
            <i class="far fa-times-circle"></i>
        </a>
    </template>

    <script type="text/template" id="showSeasonsEpisodesTemplates">
        <% _.each(seasons_episodes, function(season,key){ %>

        <% if(episodes[key] !== undefined){ %>
        <div class="row latest-watching-notification">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row text-center text-lg-left">
                            <div class="col-sm-6 mb-3 mb-lg-0">
                                <h5 class="">Season <%= season.season.number %></h5>
                                    <p class="card-text"><%= season.episodes.length %> Episodes</p>
                            </div>
                            <div class="col-sm-6 btn-block">
                                <span id="btnSectionWatchSeason<%= season.season.number %>">
                                    <a href="javascript:void(0)" onclick="requestSeasonWatched(<%= season.season.number %>)" data-toggle="tooltip" data-placement="top" title="Set season as watched" role="button" class="ml-2 btn btn-default float-lg-right shadow-sm">
                                        <i class="far fa-check-circle"></i>
                                    </a>
                                </span>
                                <a data-toggle="collapse" href="#collapseSeason<%= season.season.number %>" role="button" aria-expanded="false" aria-controls="collapseExample" class="btn btn-secondary float-lg-right">
                                    <i class="fas fa-list-ol"></i>Episodes
                                </a>
                            </div>
                            </div>
                            <div class="collapse" id="collapseSeason<%= season.season.number %>">
                                <hr>
                                <ul class="list-group">
                                      <% _.each(season.episodes,function(episode){ %>
                                            <li class="list-group-item">
                                            <div class="row text-center text-lg-left">
                                            <div class="col col-md-9">
                                                <%= episode.episode %> - <%= episodes[season.season.number][episode.episode].name %>
                                                <br><span><small>(Aired: <%= moment(episodes[season.season.number][episode.episode].airdate+' '+episodes[season.season.number][episode.episode].airtime).format('Do MMMM YYYY @ hA') %>)</small></span>
                                            </div>
                                            <div class="col col-md-3">
                                                <span class="float-right" id="buttons_<%=episode.episode%><%=episode.season%>">
                                                <%= (parseInt(episode.episode_status) === 1 ? '<span data-toggle="tooltip" data-placement="left" title="Watched"><i class="flaticon-tick"></i></span>' : '<span data-toggle="tooltip" data-placement="left" title="To watch"><i class="flaticon-calendar-3"></i></span> <a style="margin-left:15px; margin-right:10px;" href="javascript:void(0)" onclick="requestEpisodeWatched('+episode.season+','+episode.episode+');" data-toggle="tooltip" data-placement="top" title="Set as watched"><i class="far fa-eye"></i></a>') %>

                                                <% if(!_.isEmpty(episode.params)) { %>
                                                    <% const params = $.parseJSON(episode.params); %>
                                                    <button onclick="requestUnWatchEpisode(<%= episode.season %>, <%= episode.episode %>)" class="btn btn-link" style="padding-right: 0px;"> <i class="fas fa-times"></i></button>
                                                    <button class="btn btn-link" data-toggle="popover" data-placement="top" title="Info" data-content="<i class='flaticon-play'></i> You started watching in: <br>

                                                    <span class='tooltip-indent'><%= (params.started_at === '2000-01-01 00:00:00' ?  moment(params.ended_at.date).format('Do MMMM YYYY @ hA') : moment(params.started_at).format('Do MMMM YYYY @ hA')) %></span>

                                                    <br><i class='flaticon-stop-button-1'></i> You ended watching in: <br> <span class='tooltip-indent'><%= moment(params.ended_at.date).format('Do MMMM YYYY @ hA') %></span>"><i class="flaticon-information-circular-button-symbol"></i></button>

                                                <% } %>
                                                </span>
                                            </div>
                                            </div>
                                            </li>
                                      <% }); %>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <% } %>
        <% }); %>
    </script>

    <script type="text/template" id="showInfoTemplate">
        <div class="col-md-8 text-center text-md-left">
            <h3 class="show-title title-inverse"> <%= show_name %> <a target="_blank" href="<%= APP_URL %>/show/<%= api_id %>/<%= slug %>"><small class="text-muted"><i class="fas fa-external-link-alt"></i></small></a></h3>
            <h6 class="show-genres genres-inverse">
            <% var length = (genres.length - 1)%>
            <% _.each(genres, function(genre, i){ %>
                <% if(length !== i){  %>
                    <%= genre %>,
                <% } else { %>
                    <%= genre %>
                <% } %>
            <% });%>
            </h6>
        </div>
        <div class="col-sm col-md-4 card-v-align mb-4 mb-md-0 mt-md-2">
            <div class="btn-group btn-group-card float-md-right">
                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Options
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#" id="removeBtn" data-id="<%= show_id %>"><i class="fas fa-times"></i>Remove show</a>
                </div>
            </div>

            @if($favorite === 1)
                <a href="javascript:void(0)" id="favoriteBtn" onclick="requestShowFavorite(<%= show_id %>,handleUpdatedFavorite)" class="btn btn-link btn-icon btn-icon-fav fright btn-favorite" style="display:none">
                    <i class="ifav far fa-heart"></i>
                </a>
                <a href="javascript:void(0)" id="favoritedBtn" onclick="requestShowFavorite(<%= show_id %>,handleUpdatedFavorite)" class="btn btn-link btn-icon btn-icon-fav fright btn-favorite">
                    <i class="ifav fas fa-heart"></i>
                </a>
            @else
<a href="javascript:void(0)" id="favoriteBtn" onclick="requestShowFavorite(<%= show_id %>,handleUpdatedFavorite)" class="btn btn-link btn-icon btn-icon-fav fright btn-favorite">
                    <i class="ifav far fa-heart"></i>
                </a>
                <a href="javascript:void(0)" id="favoritedBtn" onclick="requestShowFavorite(<%= show_id %>,handleUpdatedFavorite)" class="btn btn-link btn-icon btn-icon-fav fright btn-favorite hide_el">
                    <i class="ifav fas fa-heart"></i>
                </a>
            @endif

        </div>
        <input type="hidden" id="show_id" value="<%= show_id %>">
        <input type="hidden" id="api_id" value="<%= api_id %>">
    </script>
@endsection
