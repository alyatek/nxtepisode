@extends('tmpl.master')

@section('content')
    <input type="hidden" name="params" id="params" value="{{$params}}">
    <input type="hidden" id="page" value="1">
    <div class="container-fluid px-5">
        <div class="row px-5">
            <div class="col-12">
                <h1 class="t-upper">TV Shows</h1>
                <p class="t-upper">Find your favorites!</p>
            </div>
        </div>
        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Bottom 1 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="3127375655"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Bottom 1 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="3127375655"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        @endif
        <div class="row px-5">
            <div class="">
                <div class="card-body">
                    <div class="row" id="shows">

                    </div>
                    <div class="row">
                        <div class="col-md-12" id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Bottom 1 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="3127375655"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Bottom 1 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="3127375655"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        @endif
    </div>
@stop

@section('js')
    <script type="text/javascript">

        function templateTvShows(res) {

            if (res.status !== true) {
                contentError('500 - ', 'Error');
                return;
            }

            let template_pagination = _.template($('#paginationTemplate').html());
            let template_shows = _.template($('#paginationShows').html());

            $('#shows').html(template_shows({shows: res.pagination.data, media: res.media}));
            $('#page').val(res.pagination.current_page);

            $('#pagination').html(template_pagination({
                next: res.pagination.next_page_url,
                previous: res.pagination.prev_page_url,
                pages: res.pagination.last_page,
                base_path: res.pagination.path,
                current_page: res.pagination.current_page,
                // params: page_params
            }));
        }

        function templateGenres(res) {
            if (res.status === false) {
                alert();
                return;
            }

            let template = _.template($('#genresTemplate').html());
            $('#categories').html(template({categories: res.genres}));
        }
        const page = JSON.parse($('#params').val());
        let params = '';
        if(page.page !== 0){
            params = "?page="+page.page;
        }
        ajaxRequest(APP_URL_FULL+params, {_token: APP_CSRF, params: $('#params').val()}, 'post', templateTvShows);
        // ajaxRequest(APP_URL+'/tv-shows/get/genres',{_token: APP_CSRF}, 'post', templateGenres)
    </script>
@stop

@section('underscore_templates')
    <script type="text/template" id="genresTemplate">
        <button class="btn btn-outline-dark dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Choose a category
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <% _.each(categories,function(categorie){ %>
            <a class="dropdown-item" href=""><%= categorie.genre %></a>
            <% }); %>
        </div>
    </script>
    <script type="text/template" id="paginationShows">
        <% let c = 1 %>

        <% _.each(shows,function(show){ %>
        <% if(c === 6){ %>

        <% } else if(c === 1) { %>
        <div class="row">
            <% } %>

            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
                <a href="<%= APP_URL+'/show/'+show.api_id+'/'+show.slug %>"><div class="shadow p-3 mb-5 bg-white rounded shadow-custom ">
                    <div class="card bg-dark text-white card-clear-bg card-height"><img
                            class="card-img img-fluid card-img-fix" src="<%= (media[show.id] ? media[show.id].original.path : APP_URL+'/img/img-not-found.png') %>" alt="<%= show.name %> tv show">
                        <div class="card-img-overlay cfade card-img-overlay-bottom">
                            <a href="<%= APP_URL+'/show/'+show.api_id+'/'+show.slug %>"><h5 class="card-title"><%= show.name %></h5>
                            </a></div>
                    </div>
                    </a>
                </div>
            </div>

            <% if(c === 6){ %>
</div>
                <% c = 1; %>
            <% } else { %>
            <% c++;%>
            <% } %>
        <% }); %>
    </script>
    <script type="text/template" id="paginationTemplate">
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                <li class="page-item <%= (current_page === 1 ? 'disabled' : '') %>"><a class="page-link" href="<%= previous %>">Previous</a></li>
                    <% var i; %>
                    <% for (i=1; i <= pages; i++) {  %>
                        <li class="page-item <%= (current_page === i ? 'active' : '') %>"><a class="page-link" href="<%= base_path %>?page=<%= i %>"><%= i %></a></li>
                    <% } %>
                <li class="page-item <%= (current_page === pages ? 'disabled' : '') %>"><a class="page-link" href="<%= next %>">Next</a></li>
            </ul>
        </nav>
    </script>
@stop
