@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            @if(1==1)
                <div class="row ad-space">
                    <div class="col-md-12 text-center">
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Unit 4 -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-1071507716766529"
                             data-ad-slot="5907799272"
                             data-ad-format="auto"
                             data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
                <div class="row ad-space">
                    <div class="col-md-12 text-center">
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Bottom 1 -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-1071507716766529"
                             data-ad-slot="3127375655"
                             data-ad-format="auto"
                             data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <h3 class="watchlist t-upper">Find your shows</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_search" action="{{url('/search/')}}">
                            <div class="form-group col-xs-6">
                                <div class=" inner-addon left-addon">
                                    <svg xmlns="https://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="in"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>
                                    <input type="text" name="search_input" id="search_input" onClick="this.select();" class="form-control text-uppercase search-bar rounded" value="{{$search}}" />
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="row show-search">
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script type="text/javascript">

        $(function(){

            $('#form_search').submit(function (evt) {
                evt.preventDefault();
                if(_.isEmpty($(this).find('input').val())){
                    $(this).find('input').parent().addClass('has-danger');
                    return;
                }
                doSearch(this);
            });

            makeSearchRequest('post', '{{url()->current()}}', {_token: '{{csrf_token()}}'});
        });
    </script>
@endsection
