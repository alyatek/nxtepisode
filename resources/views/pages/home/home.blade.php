@extends('tmpl.master')

@section('content')
    <div class="container">
        @if (session()->has('status_login'))
            <div class="row justify-content-md-center">
                <div class="col-md-12 w-auto">
                    <div class="alert alert-primary" role="alert">
                        <p class="mb-0"><i class="flaticon-tick"></i> {!! session()->get('status_login') !!}</p>
                    </div>
                </div>
            </div>
        @endif
        <h3 class="watchlist text-uppercase">Find tv shows</h3>
        <div class="row">
            <div class="col-md-12">
                <form id="form_search" action="{{url('/search/')}}">
                    <div class="form-group col-xs-6">
                        <div class=" inner-addon left-addon">
                            <svg xmlns="https://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="in">
                                <circle cx="11" cy="11" r="8"></circle>
                                <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                            </svg>
                            <input type="text" name="search_input" id="search_input"
                                   class="form-control t-upper search-bar rounded"
                                   placeholder="The Flash, Shameless, Supernatural, ..."/>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <h4 class="text-uppercase w-light mt-4 mb-4">Most Popular</h4>
        <div class="row text-center" id="contentMostPopular">

        </div>
        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Unit 3 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="1212385102"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Top 1 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="6400406254"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        @endif
    </div>

    @if(!empty($recommends))
        <div class="container">
            <div class="row row-eq-height">
                <div class="col col-md-8">
                    <div class="card h-100 shadow-sm">
                        <div class="card-body">
                            @if($recommends['popular_this_week'] !== null)
                                <h5 class="card-title t-upper">Popular this week</h5>
                                <h6 class="card-subtitle mb-2 text-muted t-cap">{{$recommends['popular_this_week']['subtitle_message']}}</h6>
                                <ul class="list-unstyled mt-4">
                                    <li class="media">
                                        <a href="{{url('show/'.$recommends['popular_this_week']['show_1']['data'][0]['api_id'].'/'.$recommends['popular_this_week']['show_1']['data'][0]['slug'])}}">
                                            <img src="{{$recommends['popular_this_week']['show_1']['image']['path']}}"
                                                 style="width: 125px;" class="mr-3 shadow"
                                                 alt="{{$recommends['popular_this_week']['show_1']['data'][0]['name']}}">
                                        </a>
                                        <div class="media-body">
                                            <a href="{{url('show/'.$recommends['popular_this_week']['show_1']['data'][0]['api_id'].'/'.$recommends['popular_this_week']['show_1']['data'][0]['slug'])}}">
                                                <h5 class="mt-0 mb-1">{{$recommends['popular_this_week']['show_1']['data'][0]['name']}}</h5>
                                            </a>
                                            <p class="t-cap">{{trim($recommends['popular_this_week']['show_1']['message'],150)}}</p>
                                            <p class="t-cap">{!! substr($recommends['popular_this_week']['show_1']['data'][0]['summary'],0,150) !!}</p>
                                            <a href="javascript:void(0)"
                                               onclick="addToMyWatchlist({{$recommends['popular_this_week']['show_1']['data'][0]['api_id']}})"
                                               class="btn btn-default btn-sm border-top shadow-sm card-link rounded">
                                                <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16"
                                                     viewBox="0 0 24 24"
                                                     fill="none" stroke="currentColor" stroke-width="2"
                                                     stroke-linecap="round"
                                                     stroke-linejoin="round" class="cicon">
                                                    <line x1="12" y1="5" x2="12" y2="19"></line>
                                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                                </svg>
                                                <span class="text-uppercase">Watchlist</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="media my-4">
                                        <a href="{{url('show/'.$recommends['popular_this_week']['show_2']['data'][0]['api_id'].'/'.$recommends['popular_this_week']['show_2']['data'][0]['slug'])}}">
                                            <img src="{{$recommends['popular_this_week']['show_2']['image']['path']}}"
                                                 style="width: 125px;" class="mr-3 shadow"
                                                 alt="{{$recommends['popular_this_week']['show_2']['data'][0]['name']}}">
                                        </a>
                                        <div class="media-body">
                                            <a href="{{url('show/'.$recommends['popular_this_week']['show_2']['data'][0]['api_id'].'/'.$recommends['popular_this_week']['show_2']['data'][0]['slug'])}}">
                                                <h5 class="mt-0 mb-1">{{$recommends['popular_this_week']['show_2']['data'][0]['name']}}</h5>
                                            </a>
                                            <p class="t-cap">{{trim($recommends['popular_this_week']['show_2']['message'],150)}}</p>
                                            <p class="t-cap">{!! substr($recommends['popular_this_week']['show_2']['data'][0]['summary'],0,150) !!}</p>
                                            <a href="javascript:void(0)"
                                               onclick="addToMyWatchlist({{$recommends['popular_this_week']['show_2']['data'][0]['api_id']}})"
                                               class="btn btn-default btn-sm border-top shadow-sm card-link rounded">
                                                <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16"
                                                     viewBox="0 0 24 24"
                                                     fill="none" stroke="currentColor" stroke-width="2"
                                                     stroke-linecap="round"
                                                     stroke-linejoin="round" class="cicon">
                                                    <line x1="12" y1="5" x2="12" y2="19"></line>
                                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                                </svg>
                                                <span class="text-uppercase">Watchlist</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <a href="{{url('show/'.$recommends['popular_this_week']['show_3']['data'][0]['api_id'].'/'.$recommends['popular_this_week']['show_3']['data'][0]['slug'])}}">
                                            <img src="{{$recommends['popular_this_week']['show_3']['image']['path']}}"
                                                 style="width: 125px;" class="mr-3 shadow"
                                                 alt="{{$recommends['popular_this_week']['show_3']['data'][0]['name']}}">
                                        </a>
                                        <div class="media-body">
                                            <a href="{{url('show/'.$recommends['popular_this_week']['show_3']['data'][0]['api_id'].'/'.$recommends['popular_this_week']['show_3']['data'][0]['slug'])}}">
                                                <h5 class="mt-0 mb-1">{{$recommends['popular_this_week']['show_3']['data'][0]['name']}}</h5>
                                            </a>
                                            <p class="t-cap">{{substr($recommends['popular_this_week']['show_3']['message'],150)}}</p>
                                            <p class="t-cap">{!! substr($recommends['popular_this_week']['show_3']['data'][0]['summary'],0,150) !!}</p>
                                            <a href="javascript:void(0)"
                                               onclick="addToMyWatchlist({{$recommends['popular_this_week']['show_3']['data'][0]['api_id']}})"
                                               class="btn btn-default btn-sm border-top shadow-sm card-link rounded">
                                                <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16"
                                                     viewBox="0 0 24 24"
                                                     fill="none" stroke="currentColor" stroke-width="2"
                                                     stroke-linecap="round"
                                                     stroke-linejoin="round" class="cicon">
                                                    <line x1="12" y1="5" x2="12" y2="19"></line>
                                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                                </svg>
                                                <span class="text-uppercase">Watchlist</span>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            @else
                                <h4>Did not find anything.</h4>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 mt-4 mt-md-0 ">
                    <div class="card h-auto shadow-sm">
                        <div class="card-body">
                            @if($recommends['user_recommends'] !== null)
                                <a href="{{url("/show/{$recommends['user_recommends']->api_id}/{$recommends['user_recommends']->slug}")}}">
                                    <h5 class="card-title t-upper">{{$recommends['user_recommends']->name}}</h5>
                                </a>
                                <h6 class="card-subtitle mb-2 text-muted t-cap">{{($recommends['user_recommends']->message ? $recommends['user_recommends']->message : '')}}</h6>
                                <a href="{{url("/show/{$recommends['user_recommends']->api_id}/{$recommends['user_recommends']->slug}")}}">
                                    <img src="{{url($recommends['user_recommends']->image)}}"
                                         class="mt-3 mb-3 img-responsive w-100 rounded shadow" alt="">
                                </a>
                                <p class="card-text">{!! substr($recommends['user_recommends']->summary, 0,200) !!}</p>
                                <a href="javascript:void(0)"
                                   onclick="addToMyWatchlist({{$recommends['user_recommends']->api_id}})"
                                   class="btn btn-default border-top shadow-sm btn-sm card-link rounded">
                                    <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="cicon">
                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                    <span class="text-uppercase">Watchlist</span>
                                </a>
                            @else
                                <h4>Did not find anything.</h4>
                            @endif
                        </div>
                    </div>
                    <div class="card mt-3 h-auto mb-0 shadow-sm">
                        <div class="card-body">
                            @if($recommends['small_recommended'] !== null)
                                <h5 class="card-title t-upper">{{$recommends['small_recommended']['show']['name']}}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">{{$recommends['small_recommended']['message']}}</h6>
                                <p class="card-text">{!! substr($recommends['small_recommended']['show']['summary'],0,250) !!}</p>
                                <a href="javascript:void(0)"
                                   onclick="addToMyWatchlist({{$recommends['small_recommended']['show']['api_id']}})"
                                   class="btn btn-default border-top shadow-sm border-top shadow-sm btn-sm card-link rounded">
                                    <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="cicon">
                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                    <span class="text-uppercase">Watchlist</span>
                                </a>
                            @else
                                <h5>Did not find anything</h5>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @if(1==1)
                <div class="row ad-space">
                    <div class="col-md-12 text-center">
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Bottom 1 -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-1071507716766529"
                             data-ad-slot="3127375655"
                             data-ad-format="auto"
                             data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
                <div class="row ad-space">
                    <div class="col-md-12 text-center">
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Unit 4 -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-1071507716766529"
                             data-ad-slot="5907799272"
                             data-ad-format="auto"
                             data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            @endif
        </div>
    @endif
@endsection

@section('js')
    <script type="text/javascript">
        function handleSuggestion(res) {
            if (res.status === false) {
                messageFailed(iterateMessages(res.content.message));
                return;
            }
            messageSuccess(iterateMessages(res.content.message));
        }

        function requestFormInsertSuggestion(data) {
            ajaxRequest(APP_URL + '/suggestions', data, 'post', handleSuggestion)
        }

        $(function () {
            $('.aniview').AniView();
            $('#form_search').submit(function (evt) {
                evt.preventDefault();
                if (_.isEmpty($(this).find('input').val())) {
                    $(this).find('input').parent().addClass('has-danger');
                    return;
                }
                doSearch(this);
            });
            $('#suggestions-form').submit(function (evt) {
                evt.preventDefault();
                $('#feedback-suggestion').css('display', 'none');
                //validate
                console.log($(this).find('textarea').val());
                if (!$(this).find('textarea').val()) {
                    $('#feedback-suggestion').css('display', 'block');
                    return;
                }

                requestFormInsertSuggestion($(this).serialize());
            });
            $('[data-toggle="tooltip"]').tooltip();
            requestTop5(templateTop5HomePage);
        });
    </script>

@endsection

@section('underscore_templates')
    <script type="text/template" id="homeTop5Template">
        <% _.each(data,function(show){ %>
        <div class="col-lg">
            <div class="shadow p-3 mb-5 bg-white rounded shadow-custom ">
                <a href="<%= APP_URL+'/show/'+show.api_id+'/'+show.slug %>">
                <div class="card bg-dark text-white card-clear-bg card-height"><img
                            class="card-img img-fluid card-img-fix"
                            src="<%= (show.media ? show.media.small.path : APP_URL+'/img/img-not-found.png')%>"
                        alt="<%= show.slug %> tv show">
                    <div class="card-img-overlay cfade card-img-overlay-bottom">
                        <a href="<%= APP_URL+'/show/'+show.api_id+'/'+show.slug %>"><h5 class="card-title"><%= show.name %></h5>
                        </a></div>
                </div>
                </a>
            </div>
        </div>
        <% }); %>
    </script>
    <script type="text/template" id="homeTop5TemplateError">
        <div class="col-md-12 text-center">
            <h3>Could not get our most popular shows!</h3>
            <p>Try again later, thank you.</p>
        </div>
    </script>
@stop
