@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6" id="content">
                <h3>Register</h3>
                <p>Create an account to manage your favorite shows! </p>
                <form method="post" action="{{url('register')}}" id="register_form" data-smk-icon="fas fa-exclamation">
                    <div class="form-group">
                        <a href="{{url('login/facebook')}}" class="btn btn-primary btn-fullw" style=""><i class="fab fa-facebook"></i> Register with Facebook </a>
                    </div>
                    <div class="form-group text-center">
                        <p>OR CREATE AN ACCOUNT</p>
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username"
                               placeholder="Enter a username" data-smk-msg="A username is required." required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email"
                               placeholder="Enter your email" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" data-smk-pattern="[0-9a-zA-Z._^%$#!~@,-]+" id="password" name="password"
                               placeholder="Password" >
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Password Confirmation</label>
                        <input type="password" class="form-control" data-smk-pattern="[0-9a-zA-Z._^%$#!~@,-]+" id="password_confirmation" name="password_confirmation"
                               placeholder="Password Confirmation" >
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="tos" name="tos" data-smk-msg="You need to agree with our terms and conditions to have an account in nxtepisode." required>
                        <label class="form-check-label" for="tos">I accept the <a target="_blank" href="{{url('terms-of-service')}}">Terms of Service</a></label>
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="privacy_policy" name="privacy_policy" data-smk-msg="You need to agree with our privacy policy to have an account in nxtepisode." required>
                        <label class="form-check-label" for="privacy_policy">I accept the <a target="_blank" href="{{url('privacy-policy')}}">Privacy Policy</a></label>
                    </div>
                    <div class="alert" style="display:none;" role="alert">

                    </div>
                    {{csrf_field()}}
                    <div class="form-group">
                        <button id="btnSubmit" type="submit" class="btn btn-dark btn-fullw">Register</button>
                    </div>
                </form>
            </div>
            <div class="col-md-3"></div>
        </div>

        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">Ads</div>
            </div>
            <div class="row ad-space">
                <div class="col-md-12 text-center">Ads</div>
            </div>
        @endif
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(function () {
            $('#register_form').submit(function (evt) {
                // $('#register_form').smkValidate();

                evt.preventDefault();
                // if( $('#register_form').smkValidate() ){
                    handleFormRequest(this);
                // }
            });
        });
    </script>
@endsection
