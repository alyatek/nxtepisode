@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron text-center">
                    @if($registration == true)
                        <h1 class="display-4">Hello, {{$user->username}}!</h1>
                        <p class="lead">Your account was validated with success!</p>
                        <hr class="my-4">
                        <p>Login to have access to all the functionalities.</p>
                        <p class="lead">
                            <a class="btn btn-light" href="#" role="button">Go to login</a>
                        </p>
                    @endif

                    @if($registration == false)
                            <h1 class="display-4">OOooOOoOOops!</h1>
                            <p class="lead">We couldn't validate your account!</p>
                            <hr class="my-4">
                            <p>Your account might have already been activated.</p>
                            <p>If you cannot access your account please send an email to <a href="mailto:help@nxtepisode.com?subject=Account Token Error">help@nxtepisode.com</a></p>
                    @endif


                </div>
            </div>
        </div>
    </div>
@endsection