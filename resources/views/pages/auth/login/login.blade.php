@extends('tmpl.master')

@section('content')
    <div class="container">
        @if (session()->has('status_login'))
            <div class="row justify-content-md-center">
                <div class="col-md-6 w-auto">
                    <div class="alert alert-warning" role="alert">
                        <p class="mb-0"><i class="fas fa-exclamation"></i>{!! session()->get('status_login') !!}</p>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h3>Login</h3>
                <p>Login to manage your shows. </p>
                <form method="post" action="{{url('login')}}" id="login_form">
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email"
                               placeholder="Enter your email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="Enter your Password">
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="remember" name="remember">
                        <label class="form-check-label" for="remember">Remember me</label>
                    </div>
                    <div class="alert" style="display:none"></div>
                    {{csrf_field()}}
                    <div class="form-group">
                        <button id="btnSubmit" type="submit" class="btn btn-dark btn-fullw">Login</button>
                    </div>
                    <div class="form-group">
                        <a href="{{url('login/facebook')}}" class="btn btn-primary btn-fullw" style=""><i
                                    class="fab fa-facebook"></i> Login with Facebook </a>
                    </div>
                </form>
                <p>Did you forget your password? <a href="{{url('forgot-password')}}">Recover it here.</a></p>
            </div>
            <div class="col-md-3"></div>
        </div>

        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">Ads</div>
            </div>
            <div class="row ad-space">
                <div class="col-md-12 text-center">Ads</div>
            </div>
        @endif
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            $('#login_form').submit(function (evt) {
                evt.preventDefault();
                handleFormRequest(this);
            });
        });
    </script>
@endsection
