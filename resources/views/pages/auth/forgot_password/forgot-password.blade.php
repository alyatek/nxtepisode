@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto" id="content">
                <h3>Recover your password</h3>
                <form>
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="email">Submit your email address and we’ll send you a link to reset your
                            password.</label>
                        <input class="form-control" type="email" name="email" id="email"
                               placeholder="Your account email.">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-dark">Send reset link</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script type="text/javascript">
        function handleForgotPassword (res) {
            if(res.status === false){
                pMessage({
                    title: 'No!',
                    message: iterateMessages(res.data.content),
                    type: 'warning',
                });
                return;
            }
            let template = _.template($('#contentSuccess').html());
            $('#content').fadeIn(1000).html(template({message:iterateMessages(res.data.content)}));
            $('#content').animateCss('bounce');
        }
        $(function(){
            $('form').submit(function(evt){
                evt.preventDefault();
                ajaxRequest(APP_URL+'/forgot-password',$('form').serialize(),'post', handleForgotPassword);
            });
        });
    </script>
@endsection

@section('underscore_templates')
    <script type="text/template" id="contentSuccess">
        <div class="text-center">
            <i class="flaticon-tick wobble-success-2x"></i>
            <h4><%= message %></h4>
        </div>
    </script>
@endsection
