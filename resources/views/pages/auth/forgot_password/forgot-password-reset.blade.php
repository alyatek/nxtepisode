@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto" id="content">
                <h3>Set your new password</h3>
                <form>
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="password">Password.</label>
                        <input class="form-control" type="password" name="password" id="password"
                               placeholder="New password.">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Password confirmation.</label>
                        <input class="form-control" type="password" name="password_confirmation"
                               id="password_confirmation"
                               placeholder="Confirm your password.">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-dark">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script type="text/javascript">
        function handleForgotPasswordReset(res) {
            if (res.status === false) {
                pMessage({
                    title: 'No!',
                    message: iterateValidatorMessages(res.data.content),
                    type: 'warning',
                });
                return;
            }
            console.log(iterateMessages(res.data.content));
            let template = _.template($('#contentSuccess').html());
            $('#content').fadeIn(1000).html(template({message: iterateMessages(res.data.content)}));
            $('#content').animateCss('bounce');
        }

        $(function () {
            $('form').submit(function (evt) {
                evt.preventDefault();
                ajaxRequest(APP_URL_FULL, $('form').serialize(), 'post', handleForgotPasswordReset);
            });
        });
    </script>
@endsection

@section('underscore_templates')
    <script type="text/template" id="contentSuccess">
        <div class="text-center">
            <i class="flaticon-tick wobble-success-2x"></i>
            <h4><%= message %></h4>
        </div>
    </script>
@endsection
