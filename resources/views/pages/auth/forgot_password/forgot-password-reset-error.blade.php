@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto" id="content">
                <div class="text-center">
                    <i class="flaticon-close wobble-error-2x"></i>
                    <h4>This reset request doesn't exist or it was already used.</h4>
        </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script type="text/javascript">
        function handleForgotPasswordReset(res) {
            if (res.status === false) {
                pMessage({
                    title: 'No!',
                    message: iterateValidatorMessages(res.data.content),
                    type: 'warning',
                });
                return;
            }
            console.log(iterateMessages(res.data.content));
            let template = _.template($('#contentSuccess').html());
            $('#content').fadeIn(1000).html(template({message: iterateMessages(res.data.content)}));
            $('#content').animateCss('bounce');
        }

        $(function () {
            $('form').submit(function (evt) {
                evt.preventDefault();
                ajaxRequest(APP_URL_FULL, $('form').serialize(), 'post', handleForgotPasswordReset);
            });
        });
    </script>
@endsection
