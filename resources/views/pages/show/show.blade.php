@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row" id="show_info">

        </div>

        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Top 1 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="6400406254"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        @endif
        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Bottom 1 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1071507716766529"
                         data-ad-slot="3127375655"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        @endif

        <div class="row show-seasons">
            <input type="hidden" id="in_watchlist" value="{{$in_watchlist}}">
            <div class="col-md-12" id="show_seasons">

            </div>
        </div>

        <div class="row ad-space">
            <div class="col-md-12 text-center">
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Top 1 -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-1071507716766529"
                     data-ad-slot="6400406254"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
        <div class="row ad-space">
            <div class="col-md-12 text-center">
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Top 1 -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-1071507716766529"
                     data-ad-slot="6400406254"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            makeShowPage('post', '{{url()->current()}}', {_token: '{{csrf_token()}}'});
        });
    </script>
@endsection
