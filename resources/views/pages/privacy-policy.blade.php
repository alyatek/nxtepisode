@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Privacy Policy</h1>
                <hr>
                <h4><strong><span style="font-family: Calibri; font-size: 14.0000pt;">Who can you contact to find out which data are processed and how?</span></strong></h4>
                <p><span style="font-family: Calibri;">If you have any questions regarding the processing of your personal data on our site, please contact us at</span><span style="font-family: Calibri; color: #0000ff;">&nbsp;info@nxtepisode.com</span><span style="font-family: Calibri;">&nbsp;or using </span><span style="font-family: Calibri; color: #0000ff;">Contact form</span><span style="font-family: Calibri;">.</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <h4><strong><span style="font-family: Calibri; font-size: 14.0000pt;">Which data do we process about you?</span></strong></h4>
                <p><span style="font-family: Calibri;">We only process personal data required to provide professional services. If your personal data are processed based on your consent, the list or extent of the data is specified directly in the consent form to the extent necessary to meet the purpose for which the personal data are processed.</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <p><span style="font-family: Calibri;">We mainly process the following personal data:</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <ul>
                    <li><span style="font-family: Calibri;">Contact information: e-mail address, IP address and country of your computer</span></li>
                    <li><span style="font-family: Calibri;">Cookies: If you have given us a permission to process your personal data when using our site, we process your data through cookies as well.</span></li>
                </ul>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <p><span style="font-family: Calibri;">Our site works with cookies that are necessary for its proper functioning, and through which we do not process any personal data; therefore, your consent to their use on our site is not required.</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <h4><strong><span style="font-family: Calibri; font-size: 14.0000pt;">Why do we need your personal data?</span></strong></h4>
                <p><span style="font-family: Calibri;">We need your personal data processed by Google Analytics to analyse the traffic on our website and, in particular, to verify that you and other users still find our site interesting and it is not losing traffic.</span></p>
                <p><span style="font-family: Calibri;">We need your personal data processed by Google AdSense to personalize ads and, in particular, to ensure that the advertising on our website will be shown primarily to those users who may be interested in advertised content, services and products.</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <h4><strong><span style="font-family: Calibri; font-size: 14.0000pt;">How to withdraw your consent?</span></strong></h4>
                <p><span style="font-family: Calibri;">You can withdraw your consent to processing of personal data at any time. If you no longer agree to your personal data being processed by us, simply let us know by writing to our email address </span><span style="font-family: Calibri; color: #0000ff;">info@nxtepisode.com </span><span style="font-family: Calibri;">or using </span><span style="font-family: Calibri; color: #0000ff;">Contact form</span><span style="font-family: Calibri;">. Of course, the withdrawal of your consent will not affect the lawfulness of processing of your personal data in the period from giving consent until its withdrawal by you.</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <h4><strong><span style="font-family: Calibri; font-size: 14.0000pt;">Who do I talk to, if there is a problem?</span></strong></h4>
                <p><span style="font-family: Calibri;">If you believe, we have violated your personal data protection rights, you may contact us at the following email address: info@nxtepisode.com.</span></p>
                <h4><strong><span style="font-family: Calibri; font-size: 14.0000pt;">What are your rights?</span></strong></h4>
                <p><span style="font-family: Calibri;">In relation to the protection of personal data, you have the following rights:</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <p><strong><span style="font-family: Calibri;">the right of access by the data subject</span></strong><span style="font-family: Calibri;">, you have the right to obtain from us confirmation as to whether or not personal data concerning you are being processed, and, where that is the case, access to the personal data and detailed information on all matters related to processing of such personal data (Your right of access to personal data is governed by Section 21 of Act no. 18/2018 Coll. on the Protection of Personal Data and Amendments to Other Acts and Article 15 of the Regulation of the European Parliament and the Council (EU) No. 2016/679). Upon your request, we are required to provide you with the personal data we process about you. We are entitled to an appropriate financial compensation corresponding to the administrative costs for a repeated provision of such data;</span></p>
                <p><strong><span style="font-family: Calibri;">the right to rectification</span></strong><span style="font-family: Calibri;">, specifically the rectification of inaccurate personal data concerning you and completion of incomplete personal data (Your right to rectification is governed by Section 22 of Act no. 18/2018 Coll. on the Protection of Personal Data and Amendments to Other Acts and Article 16 of the Regulation of the European Parliament and the Council (EU) No. 2016/679);</span></p>
                <p><span style="font-family: Calibri;">the right to erasure &lsquo;right to be forgotten&rsquo; (Your right to personal data erasure is governed by Section 23 of Act no. 18/2018 Coll. on the Protection of Personal Data and Amendments to Other Acts and Article 17 of the Regulation of the European Parliament and the Council (EU) No. 2016/679) if:</span></p>
                <p><span style="font-family: Calibri;">(a) the personal data are no longer necessary in relation to the purposes for which they were collected or otherwise processed,</span></p>
                <p><span style="font-family: Calibri;">(b) the data subject withdraws consent on which the processing is based according to point (a) of Article 6(1), or point (a) of Article 9(2), and where there is no other legal ground for the processing,</span></p>
                <p><span style="font-family: Calibri;">(c) the data subject objects to the processing pursuant to Article 21(1) and there are no overriding legitimate grounds for the processing, or the data subject objects to the processing pursuant to Article 21(2), </span></p>
                <p><span style="font-family: Calibri;">(d) the personal data have been unlawfully processed; (e) the personal data have to be erased for compliance with a legal obligation in Union or Member State law to which the controller is subject, </span></p>
                <p><span style="font-family: Calibri;">(f) the personal data have been collected in relation to the offer of information society services referred to in Article 8(1).</span></p>
                <p><span style="font-family: Calibri;">You cannot exercise the right to erasure if:</span></p>
                <p><span style="font-family: Calibri;">(a) for exercising the right of freedom of expression and information,</span></p>
                <p><span style="font-family: Calibri;">(b) for compliance with a legal obligation which requires processing by Union or Member State law to which the controller is subject or for the performance of a task carried out in the public interest or in the exercise of official authority vested in the controller, </span></p>
                <p><span style="font-family: Calibri;">(c) for reasons of public interest in the area of public health in accordance with points (h) and (i) of Article 9(2) as well as Article 9(3), </span></p>
                <p><span style="font-family: Calibri;">(d) for archiving purposes in the public interest, scientific or historical research purposes or statistical purposes in accordance with Article 89(1) in so far as the right referred to in paragraph 1 is likely to render impossible or seriously impair the achievement of the objectives of that processing; or</span></p>
                <p><span style="font-family: Calibri;">(e) for the establishment, exercise or defence of legal claims. </span></p>
                <p><strong><span style="font-family: Calibri;">the right to restriction of processing</span></strong><span style="font-family: Calibri;">&nbsp;(Your right to restrict the processing of personal data is governed by Section 24 of Act no. 18/2018 Coll. on the Protection of Personal Data and Amendments to Other Acts and Article 18 of the Regulation of the European Parliament and the Council (EU) No. 2016/679), if</span></p>
                <p><span style="font-family: Calibri;">(a) the accuracy of the personal data is contested by you, for a period enabling us to verify the accuracy of the personal data, </span></p>
                <p><span style="font-family: Calibri;">(b) the processing is unlawful and you oppose the erasure of the personal data and request the restriction of their use instead, </span></p>
                <p><span style="font-family: Calibri;">(c) we no longer need the personal data for the purposes of the processing, but they are required by you for the establishment, exercise or defence of legal claims,</span></p>
                <p><span style="font-family: Calibri;">(d) the data subject has objected to processing pursuant to Article 21(1) pending the verification whether the legitimate grounds of the controller override those of the data subject.</span></p>
                <p><span style="font-family: Calibri;">If restrictions on processing of personal data have been put in place, we may process in addition to storing your personal data only with your consent or for the purpose of exercising a legal claim, protecting individuals or for reasons of public interest;</span></p>
                <p><span style="font-family: Calibri;">the right to object (Your right to object to processing of personal data is governed by Section 27 of Act no. 18/2018 Coll. on the Protection of Personal Data and Amendments to Other Acts and Article 21 of the Regulation of the European Parliament and the Council (EU) No. 2016/679), if personal data are processed on a legal basis pursuant to Section 13 paragraph 1 letter e) or letter f) of Act no. 18/2018 Coll. on the Protection of Personal Data and Amendments to Other Acts or Article 6 paragraph 1 letter e) or f) of the Regulation of the European Parliament and the Council (EU) No. 2016/679. In such a case, we may not process personal data unless we demonstrate a legitimate requirement for the processing of personal data that outweighs your rights or interests or grounds for making a legal claim;</span></p>
                <p><strong><span style="font-family: Calibri;">the right to data portability</span></strong><span style="font-family: Calibri;">&nbsp;(Your right to data portability is governed by Section 26 of Act no. 18/2018 Coll. on the Protection of Personal Data and Amendments to Other Acts and Article 20 of the Regulation of the European Parliament and the Council (EU) No. 2016/679), in a structured, commonly used and machine-readable format, entitles you to transfer this personal data to another operator if this is technically possible and if the processing is carried out by automated means and on a legal basis pursuant to Section 13 paragraph 1 letter a) or letter b) of Act no. 18/2018 Coll. on the Protection of Personal Data and Amendments to Other Acts or Article 6 paragraph 1 letter a) or letter b) of the Regulation of the European Parliament and the Council (EU) No. 2016/679, i.e. if we process personal data based on your consent or for the fulfilment of a contract/contractual obligation.</span></p>
                <h4><strong><span style="font-family: Calibri; font-size: 14.0000pt;">How long will be your personal data stored with us?</span></strong></h4>
                <p><span style="font-family: Calibri;">We keep your personal data for the time required to fulfil the purpose for which it was processed; however, no longer than the closure of our company or our last legal successor as the subject of the law in the event of our/their closure without a further legal successor.</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <h4><strong><span style="font-family: Calibri; font-size: 14.0000pt;">Do you have to provide us with your personal data?</span></strong></h4>
                <p><span style="font-family: Calibri;">You have provided us with your personal data voluntarily through your free consent, and you were not required to grant us the consent to the processing of personal data.</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <h4><strong><span style="font-family: Calibri; font-size: 14.0000pt;">How we protect your information</span></strong></h4>
                <p><span style="font-family: Calibri;">We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
                <h4><strong><span style="font-family: Calibri; font-size: 14.0000pt;">Changes to this privacy policy</span></strong></h4>
                <p><span style="font-family: Calibri;">nxtepisode.com has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</span></p>
                <p><span style="font-family: Calibri;">&nbsp;</span></p>
            </div>
        </div>

        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">Ads</div>
            </div>
            <div class="row ad-space">
                <div class="col-md-12 text-center">Ads</div>
            </div>
        @endif
    </div>
@endsection
