@extends('tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Terms of Service</h1>
                <hr>
                <h4><strong><span>Introduction</span></strong></h4>
                <p><span>Welcome to nxtepisode.com. This website is owned and operated by nxtepisode.com. By visiting our website and accessing the information, resources, services and products we provide, you understand and agree to accept and adhere to the following terms and conditions as stated in this policy (hereafter referred to as 'Terms of Use'), along with the terms and conditions as stated in our Privacy Policy (https://www.nxtepisode.com/privacy-policy). </span>
                </p>
                <p><span>&nbsp;</span></p>
                <p><span>We reserve the right to change this User Agreement from time to time without notice. You acknowledge and agree that it is your responsibility to review this Terms of Use periodically to familiarize yourself with any modifications. Your continued use of this site after such modifications will constitute acknowledgment and agreement of the modified terms and conditions.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <h4><strong><span>Responsible Use and Conduct</span></strong></h4>
                <p><span>By visiting our website and accessing the information, resources, services and products we provide for you, either directly or indirectly (hereafter referred to as 'Resources'), you agree to use these Resources only for the purposes intended as permitted by </span>
                </p>
                <p><span>(a) the terms of this Terms of Use, and</span></p>
                <p><span>(b) applicable laws, regulations and generally accepted online practices or guidelines. </span>
                </p>
                <p><span>&nbsp;</span></p>
                <p><span>Wherein, you understand that:</span></p>
                <p><span>&nbsp;</span></p>
                <p><span>&nbsp;</span></p>
                <ol>
                    <li><span>In order to access our Resources, you may be required to provide certain information about yourself (such as identification, contact details, etc.) as part of the registration process, or as part of your ability to use the Resources. You agree that any information you provide will always be accurate, correct, and up to date.</span>
                    </li>
                    <li><span>You are responsible for maintaining the confidentiality of any login information associated with any account you use to access our Resources. Accordingly, you are responsible for all activities that occur under your account.</span>
                    </li>
                    <li><span>Accessing (or attempting to access) any of our Resources by any means other than through the means we provide, is strictly prohibited. You specifically agree not to access (or attempt to access) any of our Resources through any automated, unethical or unconventional means.</span>
                    </li>
                    <li><span>Engaging in any activity that disrupts or interferes with our Resources, including the servers and/or networks to which our Resources are located or connected, is strictly prohibited.</span>
                    </li>
                    <li><span>Attempting to copy, duplicate, reproduce, sell, trade, or resell our Resources is strictly prohibited.</span>
                    </li>
                    <li><span>You are solely responsible any consequences, losses, or damages that we may directly or indirectly incur or suffer due to any unauthorized activities conducted by you, as explained above, and may incur criminal or civil liability.</span>
                    </li>
                    <li><span>&nbsp;</span></li>
                    <li><span>We may provide various open communication tools on our website, such as comments and product ratings, various social media services, etc. You understand that generally we do not pre-screen or monitor the content posted by users of these various communication tools, which means that if you choose to use these tools to submit any type of content to our website, then it is your personal responsibility to use these tools in a responsible and ethical manner. By posting information or otherwise using any open communication tools as mentioned, you agree that you will not upload, post, share, or otherwise distribute any content that:</span>
                    </li>
                    <li><span>&nbsp;</span>
                        <ul>
                            <li><span>Is illegal, threatening, defamatory, abusive, harassing, degrading, intimidating, fraudulent, deceptive, invasive, racist, or contains any type of suggestive, inappropriate, or explicit language;</span>
                            </li>
                            <li><span>Infringes on any trademark, patent, trade secret, copyright, or other proprietary right of any party;</span>
                            </li>
                            <li><span>Contains any type of unauthorized or unsolicited advertising;</span></li>
                            <li><span>Impersonates any person or entity, including any nxtepisode.com employees or representatives.</span>
                            </li>
                        </ul>
                    </li>
                </ol>
                <p style="padding-left: 60px;"><span>We have the right at our sole discretion to remove any content that, we feel in our judgment does not comply with this Terms of Use, along with any content that we feel is otherwise offensive, harmful, objectionable, inaccurate, or violates any 3rd party copyrights or trademarks. We are not responsible for any delay or failure in removing such content. If you post content that we choose to remove, you hereby consent to such removal, and consent to waive any claim against us.</span>
                </p>
                <ol>
                    <li><span>We do not assume any liability for any content posted by you or any other 3rd party users of our website. However, any content posted by you using any open communication tools on our website, provided that it doesn't violate or infringe on any 3rd party copyrights or trademarks, becomes the property of nxtepisode.com, and as such, gives us a perpetual, irrevocable, worldwide, royalty-free, exclusive license to reproduce, modify, adapt, translate, publish, publicly display and/or distribute as we see fit. This only refers and applies to content posted via open communication tools as described, and does not refer to information that is provided as part of the registration process, necessary in order to use our Resources. All information provided as part of our registration process is covered by our privacy policy.</span>
                    </li>
                    <li><span>You agree to indemnify and hold harmless nxtepisode.com and affiliates, and their directors, officers, managers, employees, donors, agents, and licensors, from and against all losses, expenses, damages and costs, including reasonable attorneys' fees, resulting from any violation of this User Agreement or the failure to fulfill any obligations relating to your account incurred by you or any other person using your account. We reserve the right to take over the exclusive defense of any claim for which we are entitled to indemnification under this Terms of Use. In such event, you shall provide us with such cooperation as is reasonably requested by us.</span>
                    </li>
                </ol>
                <p><span>&nbsp;</span></p>
                <h4><strong><span>Copyrights/Trademarks</span></strong></h4>
                <p><span>nxtepisode.com respects the intellectual property of others, and we ask our users to do the same. nxtepisode.com has no responsibility for content on other web sites that you may find or access through nxtepisode.com products or services. Material available on or through other web sites may be protected by copyright and the intellectual property laws of the other countries or companies. The Terms of use of those web sites, and not the nxtepisode.com Terms of Use, govern your use of that material. </span>
                </p>
                <p><span>&nbsp;</span></p>
                <p><span>&nbsp;</span></p>
                <h6><strong><span style="font-family: Calibri; font-size: 12.0000pt;">Content</span></strong></h6>
                <p><span>All content included on this site, such as but not limited to graphics, logos, button icons, images, data compilations is the property of nxtepisode.com or it's content suppliers.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <h6><strong><span style="font-family: Calibri; font-size: 12.0000pt;">User Violations</span></strong>
                </h6>
                <p><span>It is nxtepisode.com&rsquo;s policy, in appropriate circumstances and at its discretion, to disable and/or terminate the accounts of users who may infringe or repeatedly infringe the copyrights or other intellectual property rights of nxtepisode.com and/or others.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <h6><strong><span
                                style="font-family: Calibri; font-size: 12.0000pt;">Factual Information</span></strong>
                </h6>
                <p><span>It is our belief information such as but not limited to Show Names, Episode Titles, Air Dates, Cast And Crew are not able to be copyrighted.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <h6><strong><span
                                style="font-family: Calibri; font-size: 12.0000pt;">Third Party Information</span></strong>
                </h6>
                <p><span>Items not included in the above categories such as long text passages that are not the original works of nxtepisode.com or nxtepisode.com members include sources of information with link to this source.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <h6><strong><span style="font-family: Calibri; font-size: 12.0000pt;">Third Party Images</span></strong>
                </h6>
                <p><span>The pictures found in here such as, but not limited to, TV Show Images, Person Images, are believed to be covered under Fair Use, which means that they are used strictly to illustrate or promote shows/individuals, and for non-profit purposes. nxtepisode.com does not have explicit permission to use them, so please do not link to them.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <h6><strong><span
                                style="font-family: Calibri; font-size: 12.0000pt;">Copyright Violations</span></strong>
                </h6>
                <p><span>If you believe that your work has been copied in a way that constitutes copyright infringement, or that your intellectual property rights have been otherwise violated, please contact us at info@nxtepisode.com</span>
                </p>
                <p><span>&nbsp;</span></p>
                <p><span>This site is made by fans and for fans, and all profits grossed from the pay per click advertisements go exclusively towards the maintenance of the site, i.e., adding new features and improving the layout designs. No copyright infringement is intended.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <h4><strong><span>Termination of Use</span></strong></h4>
                <p><span>You agree that we may, at our sole discretion, suspend or terminate your access to all or part of our website and Resources with or without notice and for any reason, including, without limitation, breach of this Terms of Use. Any suspected illegal, fraudulent or abusive activity may be grounds for terminating your relationship and may be referred to appropriate law enforcement authorities. Upon suspension or termination, your right to use the Resources we provide will immediately cease, and we reserve the right to remove or delete any information that you may have on file with us, including any account or login information.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <h4><strong><span>Governing Law</span></strong></h4>
                <p><span>This website is controlled by nxtepisode.com. It can be accessed by most countries around the world. By accessing our website, you agree that the statutes and laws of our state, without regard to the conflict of laws and the United Nations Convention on the International Sales of Goods, will apply to all matters relating to the use of this website and the purchase of any products or services through this site. </span>
                </p>
                <p><span>&nbsp;</span></p>
                <p><span>Furthermore, any action to enforce this User Agreement shall be brought in the federal or state courts You hereby agree to personal jurisdiction by such courts, and waive any jurisdictional, venue, or inconvenient forum objections to such courts.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <h4><strong><span>Guarantee</span></strong></h4>
                <p><span>UNLESS OTHERWISE EXPRESSED, nxtepisode.COM EXPRESSLY DISCLAIMS ALL WARRANTIES AND CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <h4><strong><span>Contact Information</span></strong></h4>
                <p><span>If you have any questions about this Terms of Use, the practices of this site, or your dealings with this site, please contact us at info@nxtepisode.com or using Contact form.</span>
                </p>
                <p><span>&nbsp;</span></p>
                <p><span>This document was last updated on December 24, 2018</span></p>
            </div>
        </div>
        @if(1==1)
            <div class="row ad-space">
                <div class="col-md-12 text-center">Ads</div>
            </div>
            <div class="row ad-space">
                <div class="col-md-12 text-center">Ads</div>
            </div>
        @endif

    </div>
@endsection
