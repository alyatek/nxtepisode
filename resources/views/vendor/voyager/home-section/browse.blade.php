@extends('voyager::master')

@section('page_title', 'Customize Home')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            Customize Home
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        @if(session('alert'))
            <div class="alerts">
                <div class="alert alert-{{session('alert')['type']}}">
                    @foreach(session('alert')['msg'] as $msg)
                        {{ $msg }}
                    @endforeach
                </div>
            </div>
        @endif

        @if($errors->all())
            <div class="alerts">
                <div class="alert alert-danger">
                    @foreach($errors->all() as $msg)
                        <p>{{ $msg }}</p>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>User Recommended</h4>
                                <hr>
                                <div>
                                    <p>Currently Used: <strong>{{($user_recommended_type !== null ? $user_recommended_type : '')}}</strong></p>
                                </div>
                                <form method="post" action="{{url('admin/home-sections/save/user-recommended')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <h5 class="text-uppercase">Use generated</h5>
                                        <button class="btn" name="type" value="1">Activate</button>
                                    </div>
                                    <div class="">
                                        <h5 class="text-uppercase">Use custom</h5>
                                        <input class="form-control" name="show_id" type="text" placeholder="Show ID">
                                        <br>
                                        <input class="form-control" name="message" type="text" placeholder="Message">
                                        <br>
                                        <input type="file" name="image" accept="image/*">
                                        <button class="btn" name="type" value="2">Activate</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <h4>Popular This Week</h4>
                                <hr>
                                <div>
                                    <p>Currently Used: <strong>{{($popular_this_week_type !== null ? $popular_this_week_type :'')}}</strong></p>
                                </div>
                                <form method="post" action="{{url('admin/home-sections/save/popular-this-week')}}">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <h5 class="text-uppercase">Use generated</h5>
                                        <button class="btn" name="type" value="1">Activate</button>
                                    </div>
                                    <div class="">
                                        <h5 class="text-uppercase">Use custom</h5>
                                        <div class="form-group">
                                            <label for="">Subtitle Message</label>
                                            <input name="subtitle_message" class="form-control" type="text" placeholder="Message">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Show 1</label>
                                                    <input name="show_id_1" class="form-control" type="text" placeholder="Show ID">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Show Message</label>
                                                    <input name="show_message_1" class="form-control" type="text" placeholder="Show ID">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Show 2</label>
                                                    <input name="show_id_2" class="form-control" type="text" placeholder="Show ID">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Show Message</label>
                                                    <input name="show_message_2" class="form-control" type="text" placeholder="Show ID">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Show 3</label>
                                                    <input name="show_id_3" class="form-control" type="text" placeholder="Show ID">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Show Message</label>
                                                    <input name="show_message_3" class="form-control" type="text" placeholder="Show ID">
                                                </div>
                                            </div>
                                        </div>
                                        <button name="type" value="2" class="btn">Activate</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <h4>Small Recommended</h4>
                                <hr>
                                <div class="">
                                    <div>
                                        <p>Currently Used: <strong>{{($small_recommended_type !== null ? $small_recommended_type :'')}}</strong></p>
                                    </div>
                                    <form method="post" action="{{url('admin/home-sections/save/small-recommended')}}">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <h5 class="text-uppercase">Use generated</h5>
                                            <button class="btn" name="type" value="1">Activate</button>
                                        </div>
                                        <div class="">
                                            <h5 class="text-uppercase">Use custom</h5>
                                            <div class="form-group">
                                                <label for="">Show ID</label>
                                                <input name="show_id" class="form-control" type="text" placeholder="Show ID">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Subtitle Message</label>
                                                <input name="subtitle_message" class="form-control" type="text" placeholder="Subtitle Message">
                                            </div>
                                            <button name="type" value="2" class="btn">Activate</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

