<ul class="sidebar navbar-nav">
    <li class="nav-item active">
        <a class="nav-link" href="{{url('admin-dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Shows</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Management:</h6>
            <a class="dropdown-item" href="{{route('show-statistics-manager')}}">Statistics</a>
            <a class="dropdown-item" href="{{route('shows-updates')}}">Updates</a>
        </div>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('messages')}}">
            <i class="fas fa-exclamation-triangle"></i>
            <span>Messages</span>
        </a>
    </li>
</ul>
