@extends('admin.tmpl.master')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Update Top 5 Shows
                </div>
                <div class="card-body text-center">
                    <a href="javascript:void(0)" onclick="requestTop5Shows()" class="btn btn-primary">Update</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Update Top 5 Shows
                </div>
                <div class="card-body">
                    <a href="javascript:void(0)" onclick="" class="btn btn-primary">Update</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Update Top 5 Shows
                </div>
                <div class="card-body">
                    <a href="javascript:void(0)" onclick="" class="btn btn-primary">Update</a>
                </div>
            </div>
        </div>
    </div>
@stop
