@extends('admin.tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Update shows episodes</h3>
                <p>This only updates the shows episodes.</p>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Show</th>
                        <th>Last Updated</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody id="shows_data">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        function handleUpdate(res) {
            console.log(res);
            alert(res)
        }

        function update(id) {
            ajaxRequest(APP_URL + '/admin/shows/update/seasons/' + id, {_token: APP_CSRF}, 'post', handleUpdate);
        }

        function templateShows(res) {
            console.log(res);
            let template = _.template($('#table').html());
            $('#shows_data').html(template({shows: res}));
            var nodes = document.querySelectorAll('span#dates');
            try{
                timeago.render(nodes);
            } catch (e) {

            }

            $('#dataTable').DataTable();
        }

        function requestData() {
            ajaxRequest(APP_URL + '/admin/shows/get', {}, 'get', templateShows)
        }

        $(function () {
            requestData();
        });
    </script>
@stop

@section('templates')
    <script type="text/template" id="table">
        <% _.each(shows, function(show){ %>
        <tr>
            <td><%= show.id %></td>
            <td><%= show.name %> <small>(<%= show.premiered %>)</small></td>
            <td>
                <span style="display:none"><%= (show.seasons ? show.seasons.updated_at : 0) %></span>
                <% if(show.seasons) {%>
                <span id="dates" class="badge badge-secondary" datetime="<%= (show.seasons ? show.seasons.updated_at : 'Never') %>"></span>
                <% } else { %>
                <span class="badge badge-secondary">Never (Means nobody ever opened the seasons or added to is own watchlist)</span>
                <% } %>

            </td>
            <td class="text-right"><button onclick="update(<%= show.id %>)" type="button" class="btn btn-primary"> update </button></td>
        </tr>
        <% }); %>
    </script>

@stop
