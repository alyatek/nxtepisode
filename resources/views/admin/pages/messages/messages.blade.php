@extends('admin.tmpl.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Create messages to be displayed as warning in the website</h3>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <p for="create-message-button">Create a new Message</p>
                <button data-toggle="modal" data-target="#newMessageModal" class="btn btn-primary"
                        id="create-message-button">Create
                </button>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h4>Messages list</h4>
                <p>The last message is the current being displayed.</p>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Message</th>
                        <th scope="col">Type</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="message-body">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="newMessageModal" aria-labelledby="newMessageModal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formCreateMessage">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="title">Message Title</label>
                            <input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp"
                                   placeholder="Title">
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" id="message" name="message" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="type">Message type</label>
                            <select name="type" class="form-control" id="type">
                                <option value="primary">Info
                                    <small>(Primary)</small>
                                </option>
                                <option value="warning">Warning
                                    <small>(Warning)</small>
                                </option>
                                <option value="danger">Critical
                                    <small>(Danger)</small>
                                </option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" onclick="submitFormCreateMessage()" class="btn btn-primary">Create</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script type="text/javascript">
        function handleCreateMessageRequest(res) {
            requestMessages();
        }

        function handleMessagesRequest(res) {
            let template = _.template($('#table-template').html());
            $('#message-body').html(template({messages:res}));
        }

        function handleMessagesDelete (res) {
            requestMessages();

        }

        function handleMessagesDisable (res) {
            requestMessages();
        }

        function handleMessagesEnable (res) {
            requestMessages();
        }

        function submitFormCreateMessage() {
            ajaxRequest(APP_URL + '/admin/messages/create', $('#formCreateMessage').serialize(), 'post', handleCreateMessageRequest)
        }

        function requestMessages(){
            ajaxRequest(APP_URL + '/admin/messages/get', {_token: APP_CSRF}, 'post', handleMessagesRequest)
        }

        function requestMessageDelete(id) {
            ajaxRequest(APP_URL + '/admin/messages/remove/'+id, {_token: APP_CSRF}, 'post', handleMessagesDelete)
        }

        function requestMessageDisable(id) {
            ajaxRequest(APP_URL + '/admin/messages/disable/'+id, {_token: APP_CSRF}, 'post', handleMessagesDisable)
        }

        function requestMessageEnabled(id) {
            ajaxRequest(APP_URL + '/admin/messages/enable/'+id, {_token: APP_CSRF}, 'post', handleMessagesEnable)
        }

        $(function () {
            requestMessages();
        });
    </script>
@stop

@section('templates')
    <script type="text/template" id="table-template">
        <% _.each(messages,function(message){ %>
        <tr>
            <th><%= message.id %></th>
            <td><%= message.title %></th>
            <td><%= message.text %></th>
            <td><%= message.type %></th>
            <td><%= (message.status === 1 ? 'Active' : 'Disabled') %></th>
            <td>
            <% if(message.status === 1){ %>
            <button onclick="requestMessageDisable(<%= message.id %>)" type="button" class="btn btn-warning">Disable</button>
            <% } else { %>
            <button onclick="requestMessageEnabled(<%= message.id %>)" type="button" class="btn btn-primary">Enable</button>
            <% } %>
            <button onclick="requestMessageDelete(<%= message.id %>)" type="button" class="btn btn-danger">Remove</button>
            </th>
        </tr>
        <% }); %>
    </script>
@stop
