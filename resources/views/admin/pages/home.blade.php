@extends('admin.tmpl.master')

@section('content')
    <div class="row">
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-primary o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-comments"></i>
                    </div>
                    <div class="mr-5">26 New Messages!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-left">View Details</span>
                    <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-list"></i>
                    </div>
                    <div class="mr-5">11 New Tasks!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-left">View Details</span>
                    <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-shopping-cart"></i>
                    </div>
                    <div class="mr-5">123 New Orders!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-left">View Details</span>
                    <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-danger o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-life-ring"></i>
                    </div>
                    <div class="mr-5">13 New Tickets!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-left">View Details</span>
                    <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
            </div>
        </div>
    </div>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Admin</th>
                        <th>Status</th>
                        <th>Created @ / Updated @</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody id="users_data">

                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(function () {
            requestUsers(templateUsersTable);
        });
    </script>
@stop

@section('templates')
    <script type="text/template" id="usersTableTemplate">
        <% _.each(users,function(user){ %>
        <tr>
            <td><%= user.id %></td>
            <td><%= user.username %></td>
            <td><%= user.email %></td>
            <td><%= (user.is_admin === 1 ? 'Yes' : 'No') %></td>
            <td><%= (user.status === 1 ? 'Enabled' : 'Disabled') %></td>
            <td><%= user.created_at %>  / <%= user.updated_at %></td>
            <td>
            <div class="row">
            <button type="button" class="actionbtn btn <%= (user.status === 1 ? 'btn-danger' : 'btn-success')%> btn-sm"
            onclick="<%= (user.status === 1 ? 'removeUser('+user.id+')' : 'enableUser('+user.id+')') %>"><i class="fas fa-user-slash"></i></button>
                <button type="button" class="actionbtn btn <%=(user.is_admin === 1 ? 'btn-danger' : 'btn-success')%> btn-sm" onclick="adminUser()"><i class="fas fa-crown"></i></button>
            </div>
            </td>
        </tr>
        <% }); %>
    </script>
@stop
