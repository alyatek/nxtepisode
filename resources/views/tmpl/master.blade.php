<!DOCTYPE html>
<html lang="en">
<head>
    <meta id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ pagetitle()->get() }}</title>
    <link rel="icon" href="{{url('img/icon.png')}}">
    @if (isset($seo_data))
        @foreach($seo_data as $seo)
            {!! $seo !!}
        @endforeach
    @else
        <meta name="description"
              content="Manage and plan your shows in the most efficient and easiest way ever. We are here to revolutionize the way you manage your TV Shows!">
        <meta name="keywords"
              content="show manager, tv show management, plan tv show, show plan, plan my next episode, tv show planner, show tracking, tv show tracking, tv episodes tracking, track my show, tracking tv shows">
        <meta property="og:title" content="{{config('app.name')}}"/>
        <meta property="og:type" content="website">
        <meta property="og:url" content="{{url('/')}}">
        <meta property="og:image" content="{{url('img/meta_logo.png')}}">
        <meta property="og:description"
              content="Manage and plan your shows in the most efficient and easiest way ever. We are here to revolutionize the way you manage your TV Shows!">
    @endif
    @include('tmpl.includes.head')

    @if(config('app.debug') === false)
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112402441-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', 'UA-112402441-1');
        </script>

        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-1071507716766529",
                enable_page_level_ads: true
            });
        </script>
    @endif
</head>
<body>

@include('tmpl.includes.navbar')

<section class="content pt-3">
    <div class="container" id="messages">

    </div>
    @yield('content')
</section>

<footer class="footer p-4 m-5 rounded shadow">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <img class="footer-logo" src="{{url('img/logo-white.png')}}" alt="nxtepisode logo">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <a href="mailto:nxt@nxtepisode.com">nxt@nxtepisode.com</a>
                <p><a href="{{url('privacy-policy')}}">Privacy Policy</a> | <a href="{{url('terms-of-service')}}">Terms
                        and Conditions</a></p>
            </div>
        </div>
    </div>
</footer>

@include('tmpl.includes.footer')

<script src="{{url('/assets/aniview/jquery.aniview.js')}}"></script>
<script type="text/javascript">
    var APP_URL = '{{url("")}}';
    var APP_URL_FULL = location.protocol + '//' + location.host + location.pathname;
    var APP_CSRF = $('#csrf-token').attr('content');
</script>
@yield('underscore_templates')
@yield('js')
<script type="text/javascript">
    function handleMessagesRequest(res) {
        let template = _.template($('#messages-template').html());
        if (res.status === true) {
            if (!_.isEmpty(res.content.message)) {
                $('#messages').html(template({message: res.content.message}));
            }
        }
    }

    $(function () {
        // ajaxRequest(APP_URL + '/messages/get', {}, 'get', handleMessagesRequest);
    });
</script>
<script src="https://js.pusher.com/4.3/pusher.min.js"></script>
<script type="text/javascript">
    var APP_URL = '{{url("")}}';
    var APP_URL_FULL = location.protocol + '//' + location.host + location.pathname;
    var APP_CSRF = $('[name="csrf-token"]').attr('content');
    var APP_USER = {!! collect(Illuminate\Support\Facades\Auth::user())->only(['id','username','email']) !!} ;

    function handleUnreadNotifications(res) {
        if (res.status === false) {
            return;
        }

        if (res.content.count === 0) {
            let template = _.template($('#notifications-empty-template').html());
            $('#notifications-dropdown').html(template);
            return;
        }

        let template = _.template($('#notifications-template').html());
        $('#notifications-dropdown').html(template({
            count: res.content.count,
            notifications: res.content.notifications
        }));
    }

    function handleRemoveNotification(res) {
        requestUnreadNotifications();
    }

    function handleRemoveAllNotification(res) {
        requestUnreadNotifications();
    }

    function requestUnreadNotifications() {
        ajaxRequest(APP_URL + '/watchlater/get/unread-notifications', {_token: APP_CSRF}, 'post', handleUnreadNotifications);
    }

    function requestRemoveNotification(btn, key) {
        ajaxRequest(APP_URL + '/watchlist/notification/' + key + '/validate', {}, 'get', handleRemoveNotification);
    }

    function requestRemoveAllNotification() {
        ajaxRequest(APP_URL + '/watchlist/notification/validate/all', {}, 'get', handleRemoveAllNotification);
    }

    function popupWarningWatchTime(data) {
        iziToast.show({
            title: 'Hey ' + data.user_name + '!',
            message: 'You planned to watch Episode ' + data.show_episode_number + ' from Season ' + data.show_season + ' of <b>' + data.show_name + '</b> now @ <b>' + data.time + '</b>',
            theme: 'dark',
            position: 'topCenter',
            timeout: 7500,
            icon: 'far fa-clock'
        });
    }

    const socket = new Pusher('af171efe48bdebdf4777', {
        cluster: 'eu',
        authEndpoint: APP_URL + '/broadcasting/auth',
        auth: {
            headers: {'X-CSRF-Token': APP_CSRF},
        },
        forceTLS: true
    });

    const channel = socket.subscribe('private-user-notify-' + APP_USER.id);

    channel.bind('user-notify', function (data) {
        requestUnreadNotifications();
        popupWarningWatchTime(data);
    });

    $(function () {
        @if(\Illuminate\Support\Facades\Auth::user())
        requestUnreadNotifications();
        @endif
    });
</script>
<script type="text/template" id="notifications-empty-template">
    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
       aria-haspopup="true" aria-expanded="false">
        <i class="far fa-bell-slash fa-fw"></i>
        {{--<span class="badge badge-danger" id="watch-notifications-nr">4</span>--}}
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
        <h6 class="dropdown-header text-uppercase">Watch Notifications</h6>
        <div id="watch-notifications">
            <h6 class="dropdown-header">Currently you have no notifications. <br> Head on to Watchlater to plan some
                episodes!</h6>
            <a class="dropdown-item" href="{{url('watchlater')}}">Go to Watchlater</a>
        </div>
    </div>
</script>

<script type="text/template" id="notifications-template">
    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
       aria-haspopup="true" aria-expanded="false">
        <i class="far fa-bell fa-fw"></i>
        <span class="badge badge-danger" id="watch-notifications-nr"><%= count %></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown" style="width:350px;">
        <h6 class="dropdown-header">Watch Notifications</h6>
        <div id="watch-notifications">
            <% _.each(notifications, function (notification) { %>
            <li class="dropdown">
                <div class="px-4 py-3">
                    <div class="row">
                        <div class="col-md-10">
                            <h6><%= notification.time %> - Time to watch <%= notification.show_name %></h4>
                            <p class="text-muted">Episode <%= notification.show_episode_number %> from Season <%= notification.show_season %></p>
                        </div>
                        <div class="col-md-2">
                            <a class="btn btn-link btn-sm" onclick="javascript:void(0);requestRemoveNotification(this, '<%= notification.unique_id %>')"><i class="fas fa-times " style="margin-top:10px;"></i></a>
                        </div>
                    </div>
                </div>
            </li>
            <% }); %>
            <li class="dropdown">
                <div class="px-4 py-3">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a class="btn btn-link btn-sm" onclick="javascript:void(0);requestRemoveAllNotification()"><i class="fas fa-times "></i>Remove All</a>
                        </div>
                    </div>
                </div>
            </li>
        </div>
    </div>
</script>

<script type="text/template" id="messages-template">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-<%= message.type %>" role="alert">
                <h4 class="alert-heading"><%= message.title %></h4>
                <p><%= message.text %></p>
            </div>
        </div>
    </div>
</script>
</body>
</html>
