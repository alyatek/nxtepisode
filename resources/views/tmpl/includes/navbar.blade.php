<nav class="navbar navbar-expand-lg navbar-light bg-light mt-4 mr-2 ml-2 mr-md-5 ml-md-5 mb-0 @if(Auth::check()) rounded-top @else rounded @endif shadow-lg">
    <a class="navbar-brand" href="{{route('home')}}"><img class="header-logo" src="{{url('img/logo.png')}}" alt="logo"></a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" aria-controls="navbarTogglerDemo03"
            aria-expanded="false" aria-label="Toggle navigation" data-target="#navbarSupportedContent">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse text-center text-lg-left" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{url('tv-shows')}}">

                    <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="cicon ">
                        <rect x="2" y="7" width="20" height="15" rx="2" ry="2"></rect>
                        <polyline points="17 2 12 7 7 2"></polyline>
                    </svg>
                    <span>shows</span></a>
            </li>
        </ul>
        @if(Auth::check())
            <ul class="navbar-nav ">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                        <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="cicon">
                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                            <circle cx="12" cy="7" r="4"></circle>
                        </svg>
                        <span>Hello, {{Auth::user()->username}}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('account-settings')}}">

                            <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="cicon">
                                <circle cx="12" cy="12" r="3"></circle>
                                <path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path>
                            </svg>
                            <span>Account Settings</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{url('logout')}}">

                            <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="cicon">
                                <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                                <polyline points="16 17 21 12 16 7"></polyline>
                                <line x1="21" y1="12" x2="9" y2="12"></line>
                            </svg>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        @endif
        @if(!Auth::check())
            <a class="btn btn-light shadow-sm" {{--href="{{url('login')}}"--}} data-fancybox data-src="#selectableModal"
               href="javascript:;">
                <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="cicon">
                    <path d="M15 3h4a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2h-4"></path>
                    <polyline points="10 17 15 12 10 7"></polyline>
                    <line x1="15" y1="12" x2="3" y2="12"></line>
                </svg>
                <span>Login</span>
            </a>

            <a href="javascript:void(0)" class="btn btn-light shadow-sm ml-3" {{--href="{{url('register')}}"--}} data-fancybox data-src="#selectableModal">
                <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="cicon">
                    <path d="M15 3h4a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2h-4"></path>
                    <polyline points="10 17 15 12 10 7"></polyline>
                    <line x1="15" y1="12" x2="3" y2="12"></line>
                </svg>
                <span>register</span>
            </a>

            <div style="display: none;" class="w-auto" id="selectableModal">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            <a href="{{url('/login/facebook')}}" class="btn btn-primary w-100 w-md-50"
                               style=""><i class="fab fa-facebook"></i> enter with Facebook </a>
                            <p class="mt-3 mb-3">OR</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-lg-6 pr-md-5">
                            <h4 class="t-upper" data-selectable="true">Login</h4>
                            <p data-selectable="true">Already have an account? Just loggin.</p>
                            <form method="post" action="{{url('login')}}" id="login_form">
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="Enter your email">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password"
                                           placeholder="Enter your Password">
                                </div>
                                <div class="form-group form-check" data-selectable="true">
                                    <input type="checkbox" class="form-check-input" id="remember" name="remember">
                                    <label class="form-check-label" for="remember">Remember me</label>
                                </div>
                                <div id="alogin" class="alert" style="display:none"></div>
                                {{csrf_field()}}
                                <div class="form-group">
                                    <button id="btnSubmit" type="submit" class="btn btn-dark btn-fullw">Login</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 mt-3 mt-md-0 col-lg-6 border-md-left pl-md-5">
                            <h4 class="t-upper" data-selectable="true">Register</h4>
                            <p>No account no worries, just sign up! Takes less than 2 minutes.</p>
                            <form method="post" action="{{url('register')}}" id="register_form"
                                  data-smk-icon="fas fa-exclamation">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" id="username" name="username"
                                           placeholder="Enter a username" data-smk-msg="A username is required."
                                           required="">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="Enter your email" required="">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control"
                                           data-smk-pattern="[0-9a-zA-Z._^%$#!~@,-]+" id="password" name="password"
                                           placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">Password Confirmation</label>
                                    <input type="password" class="form-control"
                                           data-smk-pattern="[0-9a-zA-Z._^%$#!~@,-]+" id="password_confirmation"
                                           name="password_confirmation" placeholder="Password Confirmation">
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="tos" name="tos" data-smk-msg="You need to agree with our terms and conditions to have an account in nxtepisode." required>
                                    <label class="form-check-label" for="tos">I accept the <a target="_blank" class="grey" href="{{url('terms-of-service')}}">Terms of Service</a></label>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="privacy_policy" name="privacy_policy" data-smk-msg="You need to agree with our privacy policy to have an account in nxtepisode." required>
                                    <label class="form-check-label" for="privacy_policy">I accept the <a target="_blank" href="{{url('privacy-policy')}}">Privacy Policy</a></label>
                                </div>
                                <div id="aregister" class="alert" style="display:none;" role="alert">

                                </div>
                                {{csrf_field()}}
                                <div class="form-group">
                                    <button id="btnSubmitRegister" type="submit" class="btn btn-dark btn-fullw">Register
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</nav>

@if(Auth::check())
    <nav class="navbar navbar-expand navbar-dark bg-dark mt-0 mx-2 mx-md-5 mb-5 rounded-bottom shadow-lg">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" aria-controls="navBarShows"
                aria-expanded="false" aria-label="Toggle navigation" data-target="#navBarShows">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navBarShows">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    {{--<div class="btn-group">--}}
                    <a class="nav-link text-center" id="watchlist-item" href="{{url('watchlist')}}">
                        <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="cicon">
                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                            <circle cx="12" cy="12" r="3"></circle>
                        </svg>
                        watchlist</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-center" href="{{url('favorites')}}"><i class="far fa-heart"></i>
                        favorites</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-center" href="{{url('watchlater')}}"><i class="far fa-clock"></i> watchlater</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown no-arrow mx-1" id="notifications-dropdown">

                </li>
            </ul>
        </div>
    </nav>
@endif()
