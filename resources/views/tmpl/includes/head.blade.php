{{--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"--}}
      {{--integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">--}}
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="{{url('assets/bootstrap/css/bootstrap.flaty.css')}}">
<!-- Fontawesome CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
      integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
@if(isset($utilities))
    @foreach($utilities as $u => $utility)
        @if(isset($utility['css']))
            <link rel="stylesheet" href="{{$utility['css']}}">
        @endif
    @endforeach
@endif

@include('tmpl.includes.sprites')

<link rel="stylesheet" href="{{url('assets/pnotify/pnotify.custom.min.css')}}">
<link rel="stylesheet" href="{{url('assets/flaticons/flaticon.css')}}">
<link rel="stylesheet" href="{{url('assets/iziToast/dist/css/iziToast.min.css')}}">
<link rel="stylesheet" href="{{url('assets/mprogress/mprogress.min.css')}}">
<link rel="stylesheet" href="{{url('/assets/animate.css/animate.min.css')}}">
@if(!Auth::check())
    <link rel="stylesheet" href="{{url('assets/fancybox/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/icheck/skins/flat/flat.css')}}">
@endif
<link rel="stylesheet" href="{{url('css/app.css')}}">
<link rel="manifest" href="{{url('manifest.json')}}"/>
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>

