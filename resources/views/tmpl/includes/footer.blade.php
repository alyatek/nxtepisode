<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

@if(isset($utilities))
    @foreach($utilities as $u => $utility)
        @if(isset($utility['js']))
            <script src="{{$utility['js']}}" type="text/javascript"></script>
        @endif
    @endforeach
@endif
<script type="text/javascript" src="{{url('assets/pnotify/pnotify.custom.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/mprogress/mprogress.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/iziToast/dist/js/iziToast.min.js')}}"></script>
@if(!Auth::check())

    <script type="text/javascript" src="{{url('assets/fancybox/jquery.fancybox.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('input').iCheck({checkboxClass: 'icheckbox_flat', radioClass: 'iradio_flat'});
            $('#login_form').submit(function (evt) {
                evt.preventDefault();
                handleFormRequest(this, 'alogin');
            });
            $('#register_form').submit(function (evt) {
                evt.preventDefault();
                handleFormRequest(this, 'aregister');
            });
        });
    </script>
@endif
<script type="text/javascript" src="{{url('js/app.js')}}"></script>


<script type="text/javascript">
    $(function () {
        @if(isset($utilities))
        @foreach($utilities as $u => $utility)
        @if(isset($utility['ready']))
        {!! $utility['ready'] !!}
        @endif
        @endforeach
        @endif
    });
</script>
