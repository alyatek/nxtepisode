<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 26-03-2019
 * Time: 21:35
 */

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class MyActions extends AbstractAction
{
    public function getTitle()
    {
        return 'Update Episodes';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right mr-1',
            'onclick' => "requestUpdateEpisodes(this,".$this->data->{$this->data->getKeyName()}.")",
            'style' => 'margin-right:5px',
        ];
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'shows';
    }

    public function getDefaultRoute()
    {
        return 'javascript:void(0)';
    }
}
