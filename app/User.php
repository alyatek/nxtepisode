<?php

namespace App;

use App\Models\Users\UserShows\UserScheduleConfigurations;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'username', 'email', 'password', 'privacy_policy', 'tos'
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden
        = [
            'password', 'remember_token',
        ];

//            $schedule_configurations = new UserScheduleConfigurations();
//            $id = collect($schedule_configurations->get_webpush_value_from_user(1))->get('method_value');
//            return $id;


    public function routeNotificationForOneSignal ()
    {
        $schedule_configurations = new UserScheduleConfigurations();
        $pc                      = collect($schedule_configurations->get_webpush_pc_value_from_user($this->id))->get('method_value');
        $android                 = collect($schedule_configurations->get_webpush_android_value_from_user($this->id));
        $ids = [$pc];

        if($android->isNotEmpty()){
            $ids[] = $android->get('method_value');
        }

        return $ids;
//        return '5486d225-fb93-4cfc-85ff-5c7983e1a835';
    }

    public function user_token ()
    {
        return $this->hasOne('App\User\AccountTokens', 'user_id');
    }

    public function is_admin ()
    {
        return $this->is_admin;
    }

    public function is_enabled ()
    {
        return $this->status;
    }

    public function scopeTimezone($query){
        return $query->select('timezone');
    }


}
