<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class AccountTokens extends Model
{
    protected $table = 'user_tokens';
}
