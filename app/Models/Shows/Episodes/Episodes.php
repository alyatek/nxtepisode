<?php

namespace App\Models\episodess\Episodess;

use Illuminate\Database\Eloquent\Model;

class Episodes extends Model
{
    protected $table = 'episodes';
    protected $fillable = ['show_id','episodes'];
    
    public static function get_episodes_data($params, $params_w, $lang = 'en')
    {

    }
    
    public static function insert_episodes($params)
    {

    }
    
    public static function update_episodes($params, $paramsw)
    {

    }
    
    public static function update_episodes_data($params, $paramsw)
    {

    }
    
    public static function disable_episodes($params)
    {

    }
    
    public static function disable_episodes_data($params)
    {

    }
}
