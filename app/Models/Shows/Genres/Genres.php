<?php

namespace App\Models\Shows\Genres;

use Illuminate\Database\Eloquent\Model;

class Genres extends Model
{
    protected $table = 'genres';
}
