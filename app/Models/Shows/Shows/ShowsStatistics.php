<?php

namespace App\Models\Shows\Shows;

use Illuminate\Database\Eloquent\Model;

class ShowsStatistics extends Model
{
    protected $table = 'shows_statistics';
    protected $fillable = [
        'name','params','status'
    ];
}
