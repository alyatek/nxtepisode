<?php

namespace App\Models\Shows\Show;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    protected $table = 'shows';

    public $fillable = ['api_id', 'name', 'summary', 'language', 'genres',
                        'runtime', 'premiered', 'official_site', 'schedule', 'rating', 'network', 'externals', 'slug',];

    public function media()
    {
        return $this->hasMany('App\Models\Shows\Shows\ShowMedia', 'show_id');
    }


    public function get_show_name_from_slug ($apiid,$show_slug) {
        return $this->select('name','id')->where('slug',$show_slug)->where('api_id',$apiid)->get()->first();
    }

    public function get_show_slug ($show_id)
    {
        return collect($this->select('slug')->where('id', $show_id)->get()->first())->get('slug');
    }
    
    public function get_show_data_plain ($show_id) {
        return $this->select('name','summary','language','slug','genres','rating')->where('id',$show_id)->get()->first();
    }

    public function get_show ($show_id) {
        return $this->select('id')->find($show_id);
    }

    public function get_show_network ($show_id) {
        return $this->whereId($show_id)->get(['network']);
    }
}
