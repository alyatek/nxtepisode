<?php

namespace App\Models\Shows\Shows;

use Illuminate\Database\Eloquent\Model;
use App\Models\Shows\Show\Show;

class ShowMedia extends Model
{
    protected $table = 'show_media';

    public $fillable = [
        'show_id','size','type','storage','path'
    ];

    public function get_media ($showid) {
        try {
            $get = $this->select(['size','path'])->where(['status'=>1, 'show_id'=>$showid])->get();
        } catch (\Exception $e) {
            return ['status' => false];
        }
        return ['status' => true, 'message' => ['content'=> $get]];

    }

    public function get ($showid) {
            return $this->select(['size','path'])->where(['status'=>1, 'show_id'=>$showid])->get();
    }
}
