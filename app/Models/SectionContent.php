<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectionContent extends Model
{
    public $table = "section_content";
    protected $fillable = ['page', 'section', 'data', 'active',];

    public function scopeUserRecommended ($query) {
        return $query->where(['page' => 'home', 'section' => 'user_recommended']);
    }

    public function scopePopularThisWeek ($query) {
        return $query->where(['page' => 'home', 'section' => 'popular_this_week']);
    }

    public function scopeSmallRecommended ($query) {
        return $query->where(['page' => 'home', 'section' => 'small_recommended']);
    }

    public function scopeActive ($query) {
        return $query->where(['active' => 1]);
    }
}
