<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class FacebookCodes extends Model
{
    public $table = 'facebook_codes';

    public function store_disable_code($user_id, $hash)
    {
        $this->hash    = $hash;
        $this->user_id = $user_id;
        $this->sender_id = 1111111;
        $this->expires_at = now()->addDay();
        $this->type = 'removal';
        $this->save();
        return $this;
    }
}
