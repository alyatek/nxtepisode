<?php

namespace App\Models\Users\User;

use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model
{
    public    $table    = 'password_resets';
    protected $fillable = ['email', 'token', 'ip'];
}
