<?php

namespace App\Models\Users\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Model
{

    use Notifiable;
    protected $table = 'users';

    public function create($username, $password, $email, $privacy_policy = 1, $tos = 1, $admin = 0, $status = 1)
    {
        $this->username       = $username;
        $this->password       = $password;
        $this->email          = $email;
        $this->privacy_policy = $privacy_policy;
        $this->tos            = $tos;
        $this->is_admin          = $admin;
        $this->status         = $status;

        $this->save();

        return $this;
    }

    public function get_by_email ($email) {
        return $this->where('email',$email)->get()->first();
    }

    public function get_by_id ($id) {
        return $this->where('id',$id)->get()->first();
    }
}
