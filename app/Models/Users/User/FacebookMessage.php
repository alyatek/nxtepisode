<?php

namespace App\Models\Users\User;

use Illuminate\Database\Eloquent\Model;

class FacebookMessage extends Model
{
    public $table = 'users_facebook_message';

    protected $fillable = ['user_id','sender_id'];

    public function create($user_id, $sender_id)
    {
        $this->user_id   = $user_id;
        $this->sender_id = $sender_id;

        $this->save();

        return $this;
    }

    public function get_sender_id ($user_id) {
        return $this->whereUser_id($user_id)->get(['sender_id'])->first();
    }

    public function get_configuration_status ($user_id, $status = 1) {
        return $this->whereUser_id($user_id)->whereStatus($status)->get()->first();
    }

    public function disable ($user_id) {
        $user_facebook = $this->whereUser_id($user_id)->get()->first();
        $user_facebook->status = 0;
        $user_facebook->save();
        return $user_facebook;
    }
}
