<?php

namespace App\Models\Users\User;

use Illuminate\Database\Eloquent\Model;

class Facebook extends Model
{
    public    $table = "users_facebook";
    protected $fillable
                     = [
            'user_id', 'facebook_id', 'name', 'avatar'
        ];

    public function create ($user_id, $facebook_id, $user_name, $avatar)
    {
        $this->user_id     = $user_id;
        $this->facebook_id = $facebook_id;
        $this->name        = $user_name;
        $this->avatar      = $avatar;

        $this->save();

        return $this;
    }
}
