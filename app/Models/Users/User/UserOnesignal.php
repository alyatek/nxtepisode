<?php

namespace App\Models\Users\User;

use Illuminate\Database\Eloquent\Model;

class UserOnesignal extends Model
{
    public $table = 'users_onesignal';
    public $fillable = ['user_id', 'player_id','status'];


}
