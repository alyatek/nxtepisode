<?php

namespace App\Models\Users\User;

use Illuminate\Database\Eloquent\Model;

class UserLanguage extends Model
{
    protected $table = 'user_language';
}
