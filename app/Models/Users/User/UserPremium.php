<?php

namespace App\Models\Users\User;

use Illuminate\Database\Eloquent\Model;

class UserPremium extends Model
{
    public $table = 'premium_users';
    protected $fillable = ['user_id','premium_level','status'];
}
