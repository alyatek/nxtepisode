<?php

namespace App\Models\Users\UserShows;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserScheduleConfigurations extends Model
{
    protected $fillable = [
        'user_id', 'configuration_method' , 'method_value' , 'created_at', 'updated_at' , 'status'
    ];

    /**
     * Creates a new entry method for the scheduling settings
     *
     * @param int $user_id
     * @param $method_value
     * @param $method_configuration
     * @return UserScheduleConfigurations
     */
    public function create($user_id, $method_value, $method_configuration)
    {

        $this->method_value         = $method_value;
        $this->configuration_method = $method_configuration;
        $this->user_id              = $user_id;

        $this->save();

        return $this;
    }

    /**
     * Gets method according to the requested params
     *
     * @param int $user_id
     * @param $method
     * @param int $status
     * @return mixed
     */
    public function get_user_method($user_id, $method, $status = 1)
    {
        #returns the query
        return $this->select('method_value', 'configuration_method')
            ->where('status', $status)
            ->where('configuration_method', $method)
            ->where('user_id', $user_id)->get()->first();
    }

    /**
     * Gets all users with a specific method
     */
    public function get_users_with_method ($method, $status = 1) {
        return $this->select('user_id')->where('configuration_method',$method)->get();
    }

    /**
     * Gets all the methods for a user
     *
     * @param int $user_id
     * @param int $status
     * @return mixed
     */
    public function get_user($user_id, $status = 1)
    {
        #returns the query
        return $this->select('method_value', 'configuration_method')->where('status', $status)->where('user_id', $user_id)->get();
    }

    /**
     * Gets all record info from an email match
     * Mainly used to check if exists
     *
     * @param $email
     * @param int $status
     * @return mixed
     */
    public function get_configuration_from_mail ($email, $status = 1) {
        return $this->where('method_value',$email)->get()->first();
    }

    public function get_webpush_pc_value_from_user ($user_id) {
        return $this->select('method_value')->where('user_id',$user_id)->where('configuration_method','onesignal_pc')->get()->first();
    }

    public function get_webpush_android_value_from_user ($user_id) {
        return $this->select('method_value')->where('user_id',$user_id)->where('configuration_method','onesignal_android')->get()->first();
    }

    /**
     * Gets all the entries in the DB
     * Main usage if for admin dashboard
     *
     * @param int $status
     * @return mixed
     */
    public function get_all($status = 1)
    {
        #returns the query
        return $this->where('status', $status)->get();
    }

    public function update_user_method($user_id, $method_value, $method_configuration)
    {
        $data               = $this->where('user_id', $user_id)->where('configuration_method', $method_configuration)->get()->first();
        $data->method_value = $method_value;
        $data->save();

        return $data;
    }

    public function remove_email_configuration ($email) {
        $delete = $this->where('method_value',$email)->get()->first();
        $delete->delete($delete);
        return $delete;
    }

    public function remove()
    {

    }
}
