<?php

namespace App\Models\Users\UserShows;

use Illuminate\Database\Eloquent\Model;

class UserShows extends Model
{
    protected $table = 'user_shows';
}
