<?php

namespace App\Models\Users\UserShows;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserCurrentlyWatching extends Model
{
    public $table = 'user_currently_watching';

    public function get_current ($user_id, $show_id)
    {
        return $this->where('user_id', $user_id)->where('show_id', $show_id)->where('ended_at', '2000-01-01 00:00:00')->get();
    }

    public function update_currently_watching ($user_id, $show_id, $season, $episode)
    {
        return UserCurrentlyWatching::where('user_id', $user_id)
            ->where('show_id', $show_id)
            ->update([
            'season'     => $season,
            'episode'    => $episode,
            'started_at' => Carbon::now(),
            'ended_at'   => Carbon::now()->addHour()
        ]);
    }
}
