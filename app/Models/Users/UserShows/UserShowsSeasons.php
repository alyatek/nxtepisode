<?php

namespace App\Models\Users\UserShows;

use Illuminate\Database\Eloquent\Model;

class UserShowsSeasons extends Model
{
    public $table = 'user_shows_seasons';
}
