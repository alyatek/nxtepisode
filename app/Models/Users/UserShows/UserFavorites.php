<?php

namespace App\Models\Users\UserShows;

use Illuminate\Database\Eloquent\Model;

class UserFavorites extends Model
{
    protected $table = 'user_favorites';
    public $fillable = ['show_id','user_id','status','params'];
}

