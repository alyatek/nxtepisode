<?php

namespace App\Models\Users\UserShows;

use Illuminate\Database\Eloquent\Model;

class LimitTracker extends Model
{
    public $table = 'show_limit_tracker';

    public function create($user_id, $show_id)
    {
        $this->user_id       = $user_id;
        $this->show_id       = $show_id;
        $this->current_limit = 8;

        $this->save();

        return $this;
    }

    public function get($user_id, $show_id)
    {
        return $this->select('current_limit')->where('user_id', $user_id)->where('show_id', $show_id)->get()->first();
    }

    public function update_limit ($user_id,$show_id,$new_limit) {
        return $this->where('user_id',$user_id)->where('show_id',$show_id)->update(['current_limit'=>$new_limit]);
    }

    public function remove()
    {

    }
}
