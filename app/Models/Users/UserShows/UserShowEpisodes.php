<?php

namespace App\Models\Users\UserShows;

use Illuminate\Database\Eloquent\Model;

class UserShowEpisodes extends Model
{
    public $table = 'user_shows_episodes';

    public $fillable = [
        'show_id', 'user_id', 'episode', 'season', 'episode_status', 'params'
    ];

    public function get_next($show_id, $user_id)
    {
        return $this
            ->where('user_id', $user_id)->where('show_id', $show_id)
            ->where('episode_status', 0)->where('params', null)
            ->orderBy('season', 'ASC')->orderBy('episode', 'ASC')
            ->get()->first();
    }

    public function get_episode($user_id, $show_id, $episode, $season)
    {
        return $this
            ->where('user_id', $user_id)->where('show_id', $show_id)
            ->where('episode', $episode)->where('season', $season)
            ->get()->first();
    }

    public function get_next_three($show_id, $user_id, $currently_watching = false)
    {
        return $this
            ->where(['show_id' => $show_id, 'user_id' => $user_id])
            ->where('episode_status', 0)
            ->whereParams(null)
            ->orderBy('season', 'ASC')
            ->orderBy('episode', 'ASC')
            ->limit(($currently_watching ? 4 : 3))->get();
    }

    public function update_watched($user_id, $show_id, $episode, $season, $compiled_params)
    {
        return $this
            ->where('user_id', $user_id)->where('show_id', $show_id)->where('episode', $episode)
            ->where('season', $season)
            ->update(
                [
                    'episode_status' => 1,
                    'params'         => $compiled_params
                ]
            );
    }

    public function update_un_watched($user_id, $show_id, $episode, $season, $compiled_params)
    {
        $compiled_params = collect([])->toJson();
        return $this
            ->where('user_id', $user_id)->where('show_id', $show_id)->where('episode', $episode)
            ->where('season', $season)
            ->update(
                [
                    'episode_status' => 0,
                    'params'         => null,
                ]
            );
    }

    public function update_episodes_per_season($user_id, $show_id, $season, $params)
    {
        return UserShowEpisodes::where('user_id', $user_id)
                               ->where('show_id', $show_id)
                               ->where('season', $season)
                               ->where('episode_status', 0)
                               ->update(
                                   [
                                       'episode_status' => 1,
                                       'params'         => $params
                                   ]
                               );
    }

    public function update_unwatch_episodes_per_season($user_id, $show_id, $season)
    {
        return UserShowEpisodes::where('user_id', $user_id)
                               ->where('show_id', $show_id)
                               ->where('season', $season)
                               ->where('episode_status', 1)
                               ->update(
                                   [
                                       'episode_status' => 0,
                                       'params'         => null
                                   ]
                               );
    }

}
