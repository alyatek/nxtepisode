<?php

namespace App\Models\Users\UserShows;

use Illuminate\Database\Eloquent\Model;

class UserShowsSchedule extends Model
{
    public    $table = 'user_episodes_schedule';
    protected $fillable
                     = [
            'user_id', 'show_id', 'season', 'episode', 'schedule_date', 'schedule_time', 'status', 'watched', 'local_time', 'local_date'
        ];

    public function create($content)
    {

        $insert = new UserShowsSchedule();

        $insert->user_id       = $content['user_id'];
        $insert->show_id       = $content['show_id'];
        $insert->season        = $content['season'];
        $insert->episode       = $content['episode'];
        $insert->schedule_date = "2019-03-03";
        $insert->schedule_time = "15:00";
        $insert->save();
        return $insert;
    }

    public function get_shows($user_id)
    {
        return $this->where('user_id', $user_id)->get();
    }

    public function get_schedule($show_id, $season, $episode, $user_id)
    {
        return UserShowsSchedule::where('show_id', $show_id)
                                ->where('user_id', $user_id)
                                ->where('season', $season)
                                ->where('episode', $episode)
                                ->get();
    }

    public function get_shows_plain($user_id)
    {
        return $this
            ->select('show_id', 'season', 'episode', 'schedule_date', 'schedule_time')
            ->where('user_id', $user_id)
            ->where('status', 1)
            ->where('schedule_date', '!=', null)
            ->where('watched', 0)->get();
    }

    public function get_information_for_current_schedule($hour_start, $hour_end, $day)
    {
        return $this
            ->where('schedule_time', '>', $hour_start)
            ->where('schedule_time', '<', $hour_end)
            ->where('schedule_date', $day)
            ->get();
    }

    public function get_users_for_current_schedule($hour_start, $hour_end, $day)
    {
        return $this
            ->select('user_id', 'show_id', 'season', 'episode', 'schedule_time')
//            ->where('schedule_time', '>', $hour_start)
//            ->where('schedule_time', '<', $hour_end)
//            ->where('schedule_date', $day)
            ->where('local_time', '>', $hour_start)
            ->where('local_time', '<', $hour_end)
            ->where('local_date', $day)
            ->get();
    }


    public function update_status_expired($user_id, $date, $hour, $status = 1)
    {
        return UserShowsSchedule::where('user_id', $user_id)
                                ->where('status', 1)
                                ->where('schedule_date', '!=', null)
                                ->where('schedule_time', '<', $hour)
                                ->where('schedule_date', '<=', $date)
                                ->update(
                                    [
                                        'status' => $status
                                    ]
                                );
    }

    public function update_bulk_watch($user_id, $show_id, $season)
    {
        return $this->where('user_id', $user_id)->where('show_id', $show_id)->where('season', $season)
                    ->update(['watched' => 1]);

    }

    public function remove($show_id, $user_id)
    {
        try {
            $delete = $this->where('user_id', $user_id)->where('show_id', $show_id)->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false];
        }
        return $delete;
    }

    public function remove_single($show_id, $season, $episode, $user_id)
    {
        return $this->where('show_id', $show_id)
                    ->where('user_id', $user_id)
                    ->where('season', $season)
                    ->where('episode', $episode)->delete();
    }

    public function remove_bulk ($ids) {
        return $this->whereIn('id', $ids)->delete();
    }

    /**
     * SCOPES
     */

    public function scopeScheduleDateOlderThan($query, $date){
        return $query->where('schedule_date', '<', $date);
    }

    public function scopeSchedulTimeOlderThan($query, $time){
        return $query->where('schedule_time', '<', $time);
    }

    public function scopeWatched($query, $watched = 1){
        return $query->whereWatched($watched);
    }
}
