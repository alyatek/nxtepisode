<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LatestWatching extends JsonResource
{
    protected $status;

    public function status ($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray ($request)
    {
        return parent::toArray($request);
    }

}
