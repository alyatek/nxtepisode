<?php

namespace App\Http\Controllers\Shows\Seasons;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Shows\Show\ShowController;
use App\Models\Shows\Seasons\Season;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Shows\Show\Show;
use Illuminate\Support\Facades\DB;

class SeasonController extends Controller
{
    public function __construct()
    {
        $this->season = new Season();
    }

    /**
     * Returns all the seasons from a show with season informations
     * @called router / get_show
     *
     * @param null $showid - API ID
     * @param null $showName - SLUG NAME
     * @param int  $status
     *
     * @return array
     */
    public function get_seasons($showid = null, $showName = null, $status = 1)
    {
        if ($showName != null) {
            $show = new Show();
            try {
                $showid = $show->select('id')->where(['slug' => $showName])->get()->first();
            } catch (\Exception $e) {
                return ['status' => false, 'message' => ['content' => 'Error']];
            }

            $showid = $showid['id'];

        }

        $seasons = new Season();

        try {
            $get = $seasons->where(['status' => $status, 'show_id' => $showid])->get()->first();
        } catch (\Exception $e) {
            return [
                'status'  => false,
                'message' => [
                    $e
                ]
            ];
        }

        if (!$get) {
            $insert = $this->create_seasons($showid);

            if ($insert['status'] == true) {
//                return [
//                    'status'  => true,
//                    'message' => [
//                        'content' => $insert['message']['content']
//                    ]
//                ];

                try {
                    $get = $seasons->where(['status' => $status, 'show_id' => $showid])->get()->first();
                } catch (\Exception $e) {
                    return [
                        'status'  => false,
                        'message' => [
                            $e
                        ]
                    ];
                }
            } else {
                return [
                    'status'  => false,
                    'message' => [
                        'content' => 'Could not insert'
                    ]
                ];
            }
        }

        $seasons       = json_decode($get['seasons'], true);
        $seasons['id'] = $showid;

        foreach ($seasons as $key => $season) {
            if (isset($season['premiereDate']) && $season['premiereDate'] === null) {
                unset($seasons[$key]);
            }
        }

        return [
            'status'  => true,
            'message' => [
                'content' => $seasons,
                'show_id' => $showid,
            ]
        ];
    }

    /**
     * Gets the number of the last season of a show
     *
     * @param     $show_id
     * @param int $info - 0 for simple and 1 for full info
     *
     * @return array
     */
    public function get_last_season($show_id, $info = 0)
    {
        //fetches seasons
        $seasons = $this->season->where('show_id', $show_id)->get()->first();

        //makes object for the seasons json
        $seasons_data = collect(json_decode(collect($seasons)->get('seasons')))->last();

        $last_season_nr = $seasons_data->number;

        $data = $seasons_data;

        switch ($info) {
            case 0:
                $data = ['last_season_nr' => $last_season_nr];
                break;
        }

        return ['status' => true, 'data' => $data];

    }

    public function get_season_aired($show_id, $season)
    {
        $get = collect(Season::where('show_id', $show_id)->get()->first());
        if ($get->isEmpty()) { //show doesnt exist
            return ['status' => false, 'content' => ['allow' => false]];
        }

        $seasons = collect(json_decode($get->get('seasons')));
        if (!$seasons->get(($season - 1))) { //season not found
            return ['status' => false, 'content' => ['allow' => false]];
        }

        $wseason = collect($seasons->get(($season - 1)));
        if ($wseason->get('premiereDate') === null) { //season is not aired yet
            return ['status' => true, 'content' => ['allow' => false]];
        }

        ///season is aired
        return ['status' => true, 'content' => ['allow' => true]];
    }

    /**
     * Creates seasons for a show
     * Its called when there is no result in get_seasons
     *
     * @param     $showid
     * @param int $status
     *
     * @return array
     */
    public function create_seasons($showid, $status = 1)
    {
        $show        = new Show();
        $api         = new ApiController();
        $show_id_api = $show->select('api_id')->where(['id' => $showid, 'status' => $status])->get()->first();
        $show_id_api = $show_id_api['api_id'];

        $get_api = $api->show_seasons($show_id_api);

        if ($get_api['status'] == true) {
            $season = new Season();

            DB::beginTransaction();
            try {
                $create = $season->create([
                    'show_id' => $showid,
                    'seasons' => json_encode($get_api['content']),
                ]);
            } catch (\Exception $e) {
                DB::rollBack();
                return [
                    'status'  => false,
                    'message' => [
                        'content' => $e
                    ]
                ];
            }

            if ($create) {
                DB::commit();
                unset($create['id']);
                unset($create['created_at']);
                unset($create['updated_at']);
                unset($create['show_id']);
                unset($create['status']);

                return [
                    'status'  => true,
                    'message' => [
                        'content' => json_decode($create, true)
                    ]
                ];
            }

            DB::rollBack();

            return [
                'status'  => false,
                'message' => [
                    'content' => 'Unkown error.'
                ]
            ];

        }

        return [
            'status'  => false,
            'message' => [
                'content' => 'Could not get seasons from api'
            ]
        ];
    }

}

