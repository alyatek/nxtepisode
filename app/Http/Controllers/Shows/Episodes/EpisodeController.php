<?php

namespace App\Http\Controllers\Shows\Episodes;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Shows\Show\ShowController;
use App\Http\Controllers\Users\UserShows\UserShowEpisodesController;
use App\Models\episodess\Episodess\Episodes;
use App\Models\Shows\Seasons\Season;
use App\Models\Shows\Show\Show;
use App\Models\Users\UserShows\UserShowEpisodes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EpisodeController extends Controller
{
    public function __construct ()
    {
        $this->episodes = new Episodes();
    }

    public function get_episode ($show_id = 0, $season_nr = 0, $episode, $status = 1)
    {
        //get all episodes
        $all = $this->get_episodes(null, null, $season_nr, $show_id, $status);

        if ($all['status'] === false) {
            return ['status' => false, 'error' => 'Error getting season i guess?'];
        }

        $episodes = collect($all['message']['content']['episodes']);
//        $episode = ($episode - 1);

        if (!isset($episodes[$episode])) { //THIS EPISODE DOES NOT EXISTS SO IT NEEDS TO GET TO THE NEXT
            //dirty hack i guess
            //if the episodes dont exist means the no user has updated the check by manual check
            //so it needs to create the users episodes and that checks that episodes for the show dont exist
            //so it creates it

            //do the episodes exist?
            $exist = UserShowEpisodes::where('show_id', $show_id)->where('user_id', Auth::user()->id)->get()->first();

            if (collect($exist)->isEmpty()) {
                $user_show_episodes = new UserShowEpisodesController();
                $create             = $user_show_episodes->create($show_id, Auth::user()->id);
                if ($create['status'] === false) {
                    return ['status' => false, 'content' => ['message' => ['Failed on creation']]];
                }
            }

            $all = $this->get_episodes(null, null, $season_nr, $show_id, $status);
            $collected = collect($all['message']['content']['episodes']);

            if ($all['status'] === false) {
                return ['status' => false, 'error' => 'Error getting season i guess?'];
            }

            if (isset($all['message']['content']['episodes'][$season_nr])) {
                return ['status' => false, 'error' => 'episode not found'];
            }

            if($collected->get($season_nr) === null){
                return ['status' => false, 'error' => 'no more episodes'];
            }

            $episodes = collect($all['message']['content']['episodes'][$season_nr]);
        }

        return [
            'status'  => true,
            'content' => [
                'message' => [
                    'episode'    => $episodes[$episode],
                    'episode_nr' => $episode,
                    'season_nr'  => $season_nr,
                ],
            ],
        ];
    }

    public function get_episodes ($apiid = null, $slug = null, $season_nr = null, $show_id = null, $status = 1)
    {

        if ($apiid != null) {
            //get show id from api
            $showid = Show::select('id')->where(['api_id' => $apiid])->get()->first()->toArray();
            if ($showid) {
                $showid = $showid['id'];
            }
        } elseif ($slug != null) {
            //get show id from slug
            $showid = Show::select('id')->where(['slug' => $slug])->get()->first()->toArray();
            if ($showid) {
                $showid = $showid['id'];
            }
        } elseif ($show_id != null) {
            $showid = $show_id;
        }

        try {
            $get = Episodes::where([
                'status'  => $status,
                'show_id' => $showid,
            ])->get()->first();
        } catch (\Exception $e) {
            return ['status' => false, 'message' => ['content' => ['Error getting results.', $e]]];
        }

        //nothing on db -> insert
        if ($get === null) {
            $insert = $this->insert_episodes($showid, $season_nr);

            if ($insert === false) {
                return ['status' => false, 'message' => ['content' => ['Error getting episodes.']]];
            }

            $get = $insert['message']['content']['episodes'];
        }

        $content_to_array = json_decode($get['episodes'], true);

        if ($season_nr !== null) {
            //error is sometimes thrown when adding show to watchlist
            //weirdest hack ever - i cant even understand how does this fix it
            if(isset($content_to_array[$season_nr])){
                $content_to_array = $content_to_array[$season_nr];
            }else{
                $content_to_array = $content_to_array[1];
            }
        }

        return ['status' => true, 'message' => ['content' => ['episodes' => $content_to_array]]];
    }

    public function get_last_episode_of_season ($show_id, $season) {
        $episodes_of_season = $this->get_episodes(null,null,$season, $show_id);
        if($episodes_of_season['status'] === false){return false;}

        $episodes = $episodes_of_season['message']['content']['episodes'];
        $keys = array_keys($episodes);

        $last_episode_nr = collect($keys)->last();

        return $last_episode_nr;
    }
    /**
     * Gets the last episode of a season
     *
     * @param int $show_id
     * @param int $season_nr
     * @return array
     */
    public function get_last_episode ($show_id = 0, $season_nr = 0) {
        $episodes = $this->episodes->where('show_id', $show_id)->where('status',1)->get()->first();

        if(collect($episodes)->isEmpty()){
            return ['status' => false];
        }

        $episodes = json_decode(collect($episodes)->get('episodes'), true);

        $season_episodes = collect($episodes)->get($season_nr);

        foreach ($season_episodes as $key => $episode){
            $season_episodes[$key]['episode'] = $key;
        }

        $last_episode = collect($season_episodes)->last();

        return ['status' => true, 'content' => ['last_episode' => $last_episode]];
    }

    public function get_previous_episode ($show_id, $season, $episode) {
        $get_episodes = $this->get_episodes(null,null,$season,$show_id);

        $episodes = collect(collect($get_episodes['message']['content'])->get('episodes'));

        $previous_episode = 1;

        if($episodes->get(($episode - 1)) !== null){
            $previous_episode = ($episode - 1);
        }

        if($episodes->get(($episode - 1)) === null){
            $season = ($season - 1);
            if($season === 0){
                $season = 1;
                $previous_episode = 1;
            } else {
                $get_episodes = $this->get_episodes(null,null,($season - 1),$show_id);
                $episodes = collect(collect($get_episodes['message']['content'])->get('episodes'));
                $keys = $episodes->keys();
                $previous_episode = $keys->last();
            }
        }

        return ['episode' => $previous_episode , 'season' => $season];
    }

    public function insert_episodes ($showid = 0, $season_nr = 0)
    {
        //make controllers calls
        $api   = new ApiController();
        $shows = new ShowController();

        //make models calls
        $show = new Show();

        //get show api id
        $api_id = $shows->get_show_api_id($showid);

        if ($api_id['status'] == false) {
            return ['status' => false, 'message' => ['content' => ['Error getting api id.']]];
        }

        $api_id = $api_id['message']['content']['api_id'];

        //get respective episodes
        $episodes = $api->show_episode_list($api_id);

        if ($episodes['status'] == false) {
            return ['status' => false, 'message' => ['content' => ['Error getting episodes list.']]];
        }

        $sorted        = $this->formatEpisodesPerSeasonFromApi($episodes['content']);
        $array_to_json = json_encode($sorted);

        DB::beginTransaction();
        try {
            $episodes           = new Episodes();
            $episodes->show_id  = $showid;
            $episodes->episodes = $array_to_json;
            $episodes->save();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => ['content' => ['Error making episodes.', $e]]];
        }

        if ($episodes) {
            DB::commit();
            return ['status' => true, 'message' => ['content' => ['episodes' => $episodes->toArray()]]];
        }

        DB::rollBack();
        return ['status' => false, 'message' => ['content' => ['Error making episodes.']]];
    }

    public function formatEpisodesPerSeasonFromApi ($episodes)
    {
        $sorted = [];

        foreach ($episodes as $key => $episode) {
            $sorted[$episode['season']][$episode['number']] = $episode;
            unset($sorted[$episode['season']][$episode['number']]['season']);
            unset($sorted[$episode['season']][$episode['number']]['number']);
        }

        return $sorted;
    }
}
