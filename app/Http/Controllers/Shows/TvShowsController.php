<?php

namespace App\Http\Controllers\Shows;

use App\Http\Controllers\Shows\Genres\GenresController;
use App\Http\Controllers\Shows\Show\ShowMediaController;
use App\Http\Controllers\Template\UtilitiesController;
use App\Models\Shows\Show\Show;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TvShowsController extends Controller
{
    public function get_index (Request $request)
    {
        $collection_params = intval($request->page);
//        $collection_params = $collection_params->forget('page');
        $collection_params = json_encode(['page'=>$collection_params]);
        pagetitle('Shows');
        return view('pages.tv-shows', [
            'utilities' => [
                UtilitiesController::utility()['underscore'],
                UtilitiesController::utility()['shave'],
                UtilitiesController::utility()['dateformat'],
                UtilitiesController::utility()['store.js'],
            ],
            'params' => $collection_params
        ]);
    }

    public function get_shows_paginated (Request $request)
    {
        $get = DB::table('shows')->paginate(15);

        $get = collect($get);

        $itemsTransformed = $get
            ->map(function ($item) {
                return [
                    'id' => $item,
                ];
            })->toArray();
        $show_ids         = [];
        $items            = collect($itemsTransformed['data']['id'])->toArray();

        foreach ($items as $key => $show) {
            $show_ids[] = $show->id;
        }

        $media = $this->get_shows_paginated_media($show_ids);

        return ['status' => true, 'pagination' => $get, 'media' => $media, 'params' => $this->params_for_url(json_decode($request->params,true))];
    }

    public function get_shows_paginated_media ($shows_ids)
    {
        $media       = [];
        $shows_media = new ShowMediaController();

        foreach ($shows_ids as $key => $show_id) {
            $get_media = $shows_media->get_media($show_id);
            if ($get_media['status'] === false) {
                continue;
            }
            $media[$show_id] = $get_media['message']['content'];
        }

        return $media;
    }

    public function get_categories () {
        if(Cache::get('genres')){
            return Cache::get('genres');
        }

        $genres = new GenresController();

        $get = $genres->get();

        $get = collect($get);

        if($get->isEmpty()){
            return ['status' => false,'content' =>['data' =>['Opsie Dopsie! Could not get the categories. Sorry!']]];
        }

        $collection = $get->map(function($genre){
            return collect($genre->toArray())
                ->only(['genre','slug','id'])
                ->all();
        });

        Cache::put('genres', ['status' => true, 'genres' => $collection],43200);

        return Cache::get('genres');
    }

    public function contact_params ($params) {
        $concated = '';

        foreach($params as $param => $val){
            $concated .= ' '.$param.' LIKE "%'.$val.'%"';
        }

        return $concated;
    }

    public function params_for_url ($params) {
        $url = '';
        foreach ($params as $param => $value) {
            $url .= '&'.$param.'='.$value;
        }
        return $url;
    }
}
