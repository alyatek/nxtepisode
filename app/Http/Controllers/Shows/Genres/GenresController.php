<?php

namespace App\Http\Controllers\Shows\Genres;

use App\Models\Shows\Genres\Genres;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GenresController extends Controller
{
    public function create () {

    }

    public function get ($status = 1) {
        $get = Genres::where('status', $status)->get();
        return $get;
    }

    public function update () {

    }

    public function remove () {

    }
}
