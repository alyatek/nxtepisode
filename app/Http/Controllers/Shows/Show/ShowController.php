<?php

namespace App\Http\Controllers\Shows\Show;

use App\Http\Controllers\Admin\ShowsController;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Shows\Seasons\SeasonController;
use App\Http\Controllers\Template\UtilitiesController;
use App\Models\Shows\Show\Show;
//use Illuminate\Filesystem\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\DB;
use App\Models\Users\UserShows\UserShows;
use function MongoDB\BSON\toJSON;


class ShowController extends Controller
{
    public function __construct()
    {
        $this->show = new Show();
    }

    public function get_show_index($api, $slug)
    {
        $show = $this->show->get_show_name_from_slug($api, $slug);
        $in_watchlist = "yes";
        if (Auth::check() && $show && collect(UserShows::where(['user_id' => Auth::user()->id, 'show_id' => $show->id])->get())->isEmpty())
            $in_watchlist = "no";
        pagetitle(collect($show)->get('name'));

        return view('pages.show.show', [
            'utilities' => [
                UtilitiesController::utility()['underscore'],
                UtilitiesController::utility()['shave'],
                UtilitiesController::utility()['dateformat'],
                UtilitiesController::utility()['moment.js'],
            ],
            'in_watchlist' => $in_watchlist,
            'seo_data' => [
                '<meta name="description" content="Manage and plan your shows in the most efficient and easiest way ever. We are here to revolutionize the way you manage your TV Shows!">',
                '<meta name="keywords" content="show manager, tv show management, plan tv show, show plan, plan my next episode, tv show planner, show tracking, tv show tracking, tv episodes tracking, track my show, tracking tv shows">',
                '<meta property="og:title" content="' . config('app.name') . '" />',
                '<meta property="og:type" content="website" >',
                '<meta property="og:url" content="' . url('/') . '" >',
                '<meta property="og:image" content="' . url('img/meta_logo.png') . '" >',
                '<meta property="og:description" content="Manage and plan your shows in the most efficient and easiest way ever. We are here to revolutionize the way you manage your TV Shows!" >',
            ]
        ]);
    }

    public function get_show_info_api($showApiID)
    {
        $api = new ApiController();
        $get = $api->show_info($showApiID);
        return $get;
    }

    /**
     * @param null $showId => IS API ID
     * @param null $showName
     * @param null $date
     * @param int $pretty
     *
     * @return array
     */
    public function get_show($showId = null, $showName = null, $date = null, $pretty = 0)
    {
        $show = new Show();

        try {
            $get = $show->where(['api_id' => $showId, 'status' => 1])->get()->first();
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => [
                    'Could not fetch show',
                ],
            ];
        }


        if (!$get) {
            $showData = $this->get_show_info_api($showId);

            if($showData === false){
                return lazyr(false, ['content' => ['Can\'t find show']]);
            }

            if ($showData['status'] === true && isset($showData['content'])) {

                DB::beginTransaction();

                $create = $this->create_show($showData['content']);

                if (!$create) {
                    DB::rollBack();
                    return ['status' => false, 'message' => ['Error']];
                }

                DB::commit();

            }
            $get = $show->where(['api_id' => $showId, 'status' => 1])->get()->first();
        }

        $media = new ShowMediaController();
        $seasons = new SeasonController();

        $images = $media->get_media($get['id']);
        $show_seasons = $seasons->get_seasons($get['id'], null);

        if ($show_seasons['status'] == true) {
            $get['seasons'] = $show_seasons['message']['content'];
        }

        if ($images['status'] == true) {
            $get['media'] = $images['message']['content'];
        } else {
            $get['media'] = [];
        }

        if ($pretty == 1) {
            unset($get['id']);

            unset($get['created_at']);
            unset($get['updated_at']);
            unset($get['status']);
        }

        return [
            'status' => true,
            'message' => [
                'content' => $get,
            ],
        ];

    }

    public function get_show_data($show_id, $status = 1)
    {
        $get = Show::where('id', $show_id)->where('status', $status)->get()->first();
        return $get;
    }

    public function get_show_data_simple($show_id, $status = 1)
    {
        $get = Show::select('id', 'name', 'slug')->where('id', $show_id)->where('status', $status)->get()->first();
        return $get;
    }

    public function get_show_api_id($id)
    {
        try {
            $get = Show::where(['id' => $id])->get()->first();
        } catch (\Exception $e) {
            return ['status' => false, 'content' => ['message' => ['Error fetching api id']]];
        }

        return ['status' => true, 'message' => ['content' => $get->toArray()]];
    }

    public function get_show_info($apiid, $showName, $show_id = 0)
    {
        if ($show_id !== 0) {
            $get_show = $this->get_show($apiid, $showName, $date = null, 1);
            return $get_show;
        }

        $get_show = $this->get_show($apiid, $showName, $date = null, 1);
        return $get_show;
    }

    public function get_top_5()
    {
        if (Cache::has('top_5')) {
            return Cache::get('top_5');
        }

        $admin_user_shows = new ShowsController();
        $shows_media = new ShowMediaController();
        $top_5 = collect($admin_user_shows->get_top_5());
        $top_5_data = [];

        foreach ($top_5 as $key => $show) {
            $show_data = collect($this->get_show_data($show->show_id));
            $show_media = $shows_media->get_media($show->show_id);
            $top_5_data[$show_data->get('slug')] = $show_data;
            $top_5_data[$show_data->get('slug')]['media'] = null;
            if ($show_media['status'] === true) {
                $top_5_data[$show_data->get('slug')]['media'] = $show_media['message']['content'];
            }
        }

//        $cache = Cache::remember('top_5', 21600, ['status' => true,]);

        Cache::add('top_5',
            collect(['status' => true, 'content' => ['data' => ['top_5' => $top_5_data]]]),
            21600);

        return Cache::get('top_5');
    }

    public function create_show($data)
    {
        $slugify = new Slugify();
        $show = new Show();

        DB::beginTransaction();
        try {
            $create = $show->create([
                'api_id' => $data['id'],
                'name' => $data['name'],
                'summary' => $data['summary'],
                'language' => $data['language'],
                'runtime' => $data['runtime'],
                'premiered' => $data['premiered'],
                'genres' => json_encode($data['genres']),
                'official_site' => $data['officialSite'],
                'schedule' => json_encode($data['schedule']),
                'rating' => json_encode($data['rating']),
                'network' => json_encode($data['network']),
                'externals' => json_encode($data['externals']),
                'slug' => $slugify->slugify($data['name']),
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => ['content' => $e]];
        }
        DB::commit();
        return ['status' => true, 'message' => ['content' => $create]];
    }
}
