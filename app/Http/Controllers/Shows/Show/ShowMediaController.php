<?php

namespace App\Http\Controllers\Shows\Show;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Shows\Shows\ShowMedia;
use App\Models\Shows\Show\Show;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
//use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;


class ShowMediaController extends Controller
{
    public function get_media($showid)
    {
        $media = new ShowMedia();

        $show_media = $media->get_media($showid);

        if ($show_media['status'] == true) {

            if(count($show_media['message']['content']) == 0){
                $make = $this->make_media($showid);

                if ($make['status'] == true) {
                    try {
                        $show_media = $media->get_media($showid);
                    }
                    catch (\Exception $e) {
                        return ['status' => false];
                    }
                }
                else {
                    return ['status' => false, 'message' => ['content' => 'could not insert images']];
                }
            }

            $media = $show_media['message']['content']->keyBy('size');

            return ['status' => true, 'message' => ['content' => $media]];
        }

        return ['status' => false, 'message' => ['content' => 'Did not perfom query']];
    }

    public function make_media($showid)
    {
        $show     = new Show();
        $show_api_id = $show->select('api_id')->where(['id' => $showid])->get()->first()->toArray();
        $show_slug = $show->get_show_slug($showid);

        if (!empty($show_api_id)) {
            $api = new ApiController();

            $get = $api->show_info($show_api_id['api_id']);

            if ($get['status'] == true) {
                $get_media = $get['content']['image'];
            }

            if($get_media === null){
                return ['status' => false];
            }

            $saved_media = $this->save_media_from_external($show_slug,collect($get_media)->get('original'));

            foreach ($saved_media as $key => $image) {
                $insert = $this->insert_media($showid, $image, $key, 'external', 'cover');
                if ($insert['status'] == false) {
                    return ['status' => false];
                }
            }

            return ['status' => true];
        }
    }

    public function insert_media($showid, $path, $size, $type, $ttype)
    {
        $showmedia = new ShowMedia();

        $showmedia->show_id = $showid;
        $showmedia->size    = $size;
        $showmedia->storage = $type;
        $showmedia->path    = $path;
        $showmedia->type    = $ttype;

        try {
            DB::beginTransaction();
            $showmedia->save();
        }
        catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => ['content' => $e]];
        }

        DB::commit();

        return ['status' => true];
    }

    public function save_media($path, $slug)
    {
        try {

        }
        catch (\Exception $e) {
            return ['status' => false, 'e' => $e];
        }

        return ['status' => true];
    }

    public function save_media_from_external ($show_slug, $image_path) {
        if (!file_exists(storage_path('app/public/shows/'.$show_slug))) {
            try {
                Storage::makeDirectory('public/shows/'.$show_slug,0755);
            } catch (\Exception $e) {
                return ['status' => false];
            }
        }

        $img_ext = '.'.pathinfo($image_path,PATHINFO_EXTENSION);

        #3 types

        #small - thumbnail
        Image::make(file_get_contents($image_path))
            ->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save(storage_path('app/public/shows/'.$show_slug.'/'.$show_slug.'-small'.$img_ext));
        $path_small = 'storage/shows/'.$show_slug.'/'.$show_slug.'-small'.$img_ext;

        #medium - thumbnail
        Image::make(file_get_contents($image_path))
            ->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save(storage_path('app/public/shows/'.$show_slug.'/'.$show_slug.'-medium'.$img_ext));
        $path_medium = 'storage/shows/'.$show_slug.'/'.$show_slug.'-medium'.$img_ext;

        #original
        Image::make(file_get_contents($image_path))
            ->save(storage_path('app/public/shows/'.$show_slug.'/'.$show_slug.'-original'.$img_ext));
        $path_original = ('storage/shows/'.$show_slug.'/'.$show_slug.'-original'.$img_ext);


        return ['small' => $path_small,'medium' => $path_medium,'original' => $path_original];
//        Storage::put('public/shows/'.$filename, new File($path));
    }
}
