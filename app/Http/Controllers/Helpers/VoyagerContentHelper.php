<?php

namespace App\Http\Controllers\Helpers;

use App\Models\SectionContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class VoyagerContentHelper extends Controller
{

    public function __construct() {
        $this->section_content = new SectionContent();
    }

    protected function validate_type($data)
    {
        if (!isset($data->type) || $data->type > 2 || $data->type < 1)
            return redirect()->back()
                             ->with(
                                 [
                                     'alert' => [
                                         'type' => 'warning',
                                         'msg'  => ['Type unkown']
                                     ]
                                 ]
                             );
    }

    protected function validate_popular_this_week_data ($data) {
        $validator = \Validator::make($data->all(), [
            'subtitle_message' => 'required',
            'show_id_1' => 'required',
            'show_message_1' => 'required',
            'show_id_2' => 'required',
            'show_message_2' => 'required',
            'show_id_3' => 'required',
            'show_message_3' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                             ->withErrors($validator);
        }
    }

    protected function get_user_recommended_data () {
        if(!$get = $this->section_content->UserRecommended()->active()->get(['data'])->first())return null;
        $data = json_decode($get->data);
        return $data;
    }

    protected function get_popular_this_week_data () {
        if(!$get = $this->section_content->PopularThisWeek()->active()->get(['data'])->first()) return null;
        $data = json_decode($get->data);
        return $data;
    }

    protected function get_small_recommended_data () {
        if(!$get = $this->section_content->SmallRecommended()->active()->get(['data'])->first()) return null;
        $data = json_decode($get->data);
        return $data;
    }

    protected function store_file ($file) {
//        $path = $file->storeAs('/public/sections-content/', 'anew');
        $image = Image::make($file);

        $image->fit(250, 140, function ($constraint) {
            $constraint->aspectRatio();
        });

        if(Storage::put('public/sections-content/250x140'.date('s').'.'.substr($file->getMimeType(), strpos($file->getMimeType(), "/") + 1), (string) $image->encode()))
            return 'storage/sections-content/250x140'.date('s').'.'.substr($file->getMimeType(), strpos($file->getMimeType(), "/") + 1);

        return null;
    }
}
