<?php

namespace App\Http\Controllers;

use App\Suggestions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SuggestionsController extends Controller
{
    public function __construct ()
    {
        $this->suggestions = new Suggestions();
    }

    public function create (Request $request) {
        if(collect($request->suggestion)->isEmpty()){
            return ['status' => false, 'content' => ['message' => ['Nothing was written']]];
        }

        $create = collect($this->suggestions->create($request->suggestion, Auth::user()->id));

        if($create->isEmpty()){
            return ['status' => false, 'content' => ['message' => ['We don\'t want your suggestion! AH, just kidding, something happend and we couldn\'t store it. Try again later? Please?']]];
        }

        return ['status' => true, 'content' => ['message' => ['N1! If we decide to implement that you will hear from us soon. Thank you.']]];

    }
}
