<?php

namespace App\Http\Controllers;

use App\Models\Messages;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;

class MessagesController extends Controller
{
    public function __construct (Messages $messages)
    {
        $this->messages = $messages;
    }

    /**
     * handle the request creation from admin dashboard
     *
     * @param Request $request
     *
     * @return array
     */
    public function handle_create_message (Request $request)
    {
        $title   = $request->title;
        $message = $request->message;
        $type    = $request->type;

        $create = $this->create_message($title, $message, $type);

        return $create;
    }

    /**
     * creates the message in the database
     *
     * @param null $title
     * @param null $message
     */
    public function create_message ($title = null, $message = null, $type = null)
    {
        $this->messages->title = $title;
        $this->messages->text  = $message;
        $this->messages->type  = $type;

        $this->messages->save();
        return $this->messages;
    }

    /**
     * get the messages to be displayed in the website
     *
     * @return array
     */
    public function get_messages ()
    {
        //gets the last message in db to be sent to the site
        $get_all = Messages::select('title', 'text', 'type')->where('status', 1)->orderBy('created_at', 'DESC')->get()->first();
        return response()->json(['status' => true, 'content' => ['message' => $get_all]],200) ;
    }

    /**
     * gets messages in the cache
     */
    public function get_cached_messages ()
    {
        return ['status' => false];
    }

    /**
     * gets the messages for the admin dashboard with ids and controls
     */
    public function get_all_messages ()
    {
        // gets all the messages for the dashboard table
        $get_all = Messages::get();
        return $get_all;
    }

    public function disable_message ($id)
    {
        $message         = Messages::find($id);
        $message->status = 0;
        $message->save();
        return $message;
    }

    public function enable_message ($id)
    {
        $message         = Messages::find($id);
        $message->status = 1;
        $message->save();
        return $message;
    }

    public function update_message ($id)
    {

    }

    public function remove_message ($id)
    {
        return Messages::where('id', $id)->delete();
    }

    public function cache_messages ($time = 0)
    {

    }
}
