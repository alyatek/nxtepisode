<?php

namespace App\Http\Controllers\Notices;

use App\Events\NotifyUsers;
use App\Notifications\ShowNotice;
use App\Services\Notices\NoticesService;
use App\User;
use DeviceDetector\Parser\Client\Browser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Mockery\Matcher\Not;

class NoticeController extends Controller
{
    public function __construct()
    {
        $this->notices = new NoticesService();
    }

    /**
     * Sends emails to all users with email configured within the current hour
     * @return array
     */
    public function email () {
        return $this->notices->mail_notice();
    }

    public function noticeapp () {
        return $this->notices->in_app_notice();
    }

    public function webpush () {
        return $this->notices->web_push_notice();
    }

    public function fmessage () {
        return $this->notices->facebook_message();
    }

    public function notice () {
        $uid = 1;
        event(new NotifyUsers());
    }

    public function test () {
        return [\Browser::isDesktop()];
    }
}
