<?php

namespace App\Http\Controllers;

use App\Services\VoyagerContent\VoyagerHomeContentService;
use Illuminate\Http\Request;

class VoyagerContentController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function __construct()
    {
        $this->home_content_service = new VoyagerHomeContentService();
    }

    public function get_home_section_index()
    {
        $type_recommended_user = $this->home_content_service->get_user_recommended_type();
        $type_popular_this_week = $this->home_content_service->get_popular_this_week_type();
        $type_small_recommended = $this->home_content_service->get_small_recommended_type();
        return view(
            'vendor.voyager.home-section.browse',
            [
                'user_recommended_type' => $type_recommended_user,
                'popular_this_week_type' => $type_popular_this_week,
                'small_recommended_type' => $type_small_recommended,
            ]
        );
    }

    public function handle_user_recommended(Request $request)
    {
        return $this->home_content_service->save_user_recommended($request);
    }

    public function handle_popular_this_week(Request $request)
    {
        return $this->home_content_service->save_popular_this_week($request);
    }

    public function handle_small_recommended(Request $request)
    {
        return $this->home_content_service->save_small_recommended($request);
    }
}
