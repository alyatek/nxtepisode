<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * Makes the general call to the API
     * @param $url -> formated before calling
     * @return array -> true or false
     */
    public function call($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return ['status' => false, 'message' => [$err]];
        } else {
            return ['status' => true, 'content' => json_decode($response, true)];
        }

        return ['status' => false, 'message' => ['Unkown error']];
    }

    /**
     * Searches a show
     * @param $search
     * @return array
     */
    public function search($search)
    {
        //search url : http://api.tvmaze.com/search/shows?q=girls
        $urlfriendly = preg_replace('/\W+/', '-', strtolower($search));
        $url = 'http://api.tvmaze.com/search/shows?q=' . $urlfriendly;
        $get = $this->call($url);
        return $get;
    }

    /**
     * Searches for a show retrieving a single result
     * @param $search
     * @return array
     */
    public function search_single($search)
    {
        //search url : http://api.tvmaze.com/singlesearch/shows?q=girls
        $urlfriendly = preg_replace('/\W+/', '-', strtolower($search));
        $url = 'http://api.tvmaze.com/singlesearch/shows?q=' . $urlfriendly;
        $get = $this->call($url);
        return $get;
    }

    /**
     * Searches for a show retrieving a single result and the respective episodes
     * @param $search
     * @return array
     */
    public function search_single_with_episodes($search)
    {
        //search url : http://api.tvmaze.com/singlesearch/shows?q=girls&embed=episodes
        $urlfriendly = preg_replace('/\W+/', '-', strtolower($search));
        $url = 'http://api.tvmaze.com/singlesearch/shows?q=' . $urlfriendly . '&embed=episodes';
        $get = $this->call($url);
        return $get;
    }

    /**
     * Get IMDB information for the show
     * @param $imdbid
     * @return array
     */
    public function lookup_imdb($imdbid)
    {
        //search url : http://api.tvmaze.com/search/shows?q=girls
        $urlfriendly = preg_replace('/\W+/', '-', strtolower($imdbid));
        $url = 'http://api.tvmaze.com/lookup/shows?imdb=' . $urlfriendly;
        $get = $this->call($url);
        return $get;
    }

    /**
     * Gets the scheduled shows to air on a specific day and country
     * @param $country_iso
     * @param $date
     * @return array
     */
    public function schedule_country_day($country_iso, $date)
    {
        //search url : http://api.tvmaze.com/schedule?country=US&date=2014-12-01
        $url = 'http://api.tvmaze.com/schedule?country=' .$country_iso. '&date='. $date ;
        $get = $this->call($url);
        return $get;
    }

    /**
     * Gets the information of a show
     * @param $showid
     * @return array
     */
    public function show_info ($showid) {
        //search url : http://api.tvmaze.com/shows/1/episodes
        $url = 'http://api.tvmaze.com/shows/' .$showid ;
        $get = $this->call($url);
        return $get;
    }

    /**
     * Gets the information of a show with the cast
     * @param $showid
     * @return array
     */
    public function show_info_with_cast ($showid) {
        //search url : http://api.tvmaze.com/shows/1/episodes
        $url = 'http://api.tvmaze.com/shows/' .$showid. '?embed=cast' ;
        $get = $this->call($url);
        return $get;
    }

    /**
     * Gets the episode list of a show
     * @param $showid
     * @return array
     */
    public function show_episode_list ($showid) {
        //search url : http://api.tvmaze.com/shows/1/episodes
        $url = 'http://api.tvmaze.com/shows/' .$showid. '/episodes' ;
        $get = $this->call($url);
        return $get;
    }

    /**
     * Gets the seasons information of a show
     * @param $showid
     * @return array
     */
    public function show_seasons ($showid) {
        //search url : http://api.tvmaze.com/shows/1/seasons
        $url = 'http://api.tvmaze.com/shows/' .$showid. '/seasons' ;
        $get = $this->call($url);
        return $get;
    }

    /**
     * Gets the episodes
     * @param $showid
     * @return array
     */
    /*public function show_episodes_season ($showid) {
        //search url : http://api.tvmaze.com/seasons/1/episodes
        $url = 'http://api.tvmaze.com/seasons/' .$showid. '/seasons' ;
        $get = $this->call($url);
        return $get;
    }*/

    /**
     * Gets the shows other names
     * @param $showid
     * @return array
     */
    public function show_akas ($showid) {
        //search url : http://api.tvmaze.com/shows/1/akas
        $url = 'http://api.tvmaze.com/shows/' .$showid. '/akas' ;
        $get = $this->call($url);
        return $get;
    }
}
