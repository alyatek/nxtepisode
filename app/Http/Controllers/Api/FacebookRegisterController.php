<?php

namespace App\Http\Controllers\Api;

use App\Services\Api\FacebookServiceRegisterUser;
use App\Services\Api\FacebookServiceRemoveUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FacebookRegisterController extends Controller
{
    public function __construct()
    {
        $this->facebook_service_register_user = new FacebookServiceRegisterUser();
        $this->facebook_service_remove_user   = new FacebookServiceRemoveUser();
    }

    public function register_user($hash)
    {
        //this needs to be validated here because we need to set a flash message
        if (auth()->user() === null) {
            session()->flash(
                'status_login',
                '  You need an account for that. <br> Create one and use the link while logged in to complete the activation.'
            );
            return redirect(url('login'));
        }
        return $this->facebook_service_register_user->handle_register_request($hash);
    }

    public function disable_user($hash)
    {
//this needs to be validated here because we need to set a flash message
        if (auth()->user() === null) {
            session()->flash(
                'status_login',
                '  You need an account for that. <br>.'
            );
            return redirect(url('login'));
        }
        return $this->facebook_service_remove_user->disable_user($hash);
    }
}
