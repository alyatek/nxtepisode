<?php

namespace App\Http\Controllers\Auth;

use App\Mail\ConfirmationEmail;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Template\UtilitiesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get index of register page
     */
    public function get_index()
    {
        return view('pages.auth.register.register', [
            'utilities' => [
                UtilitiesController::utility()['icheck'],
//                UtilitiesController::utility()['elementui'],
                UtilitiesController::utility()['pnotify'],
                UtilitiesController::utility()['smoke'],
            ]
        ]);
    }

    public function login (Request $request){
        dd($request->all());
    }

    /**
     * Make Registration
     */
    public function register(Request $request)
    {

        if($request->password !== $request->password_confirmation){
            return response()->json(['status' => false, "message" => [0 => 'Passwords do not match.']]);
        }

        //validate
        $validate = $this->validator([
            'username' => $request->username,
            'email' => $request->email,
            'password' => $request->password,
            'tos' => $request->tos,
            'privacy_policy' => $request->privacy_policy
        ]);

        if (isset($validate['status'])) {
            return [
                "status" => false,
                "message" => $validate['message']
            ];
        }

        //create
        DB::beginTransaction();
        $create = $this->create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => $request->password,
        ]);

        if ($create) {
            if (isset($create['status'])) {
                DB::rollBack();
                return [
                    'status' => false,
                    'message' => ['An error occurred. Try again later.', 'Error: 9'],
                ];
            }

            $create_token = $this->token($create);

            if ($create_token['status'] == true) {

                try {
                    Mail::to($create->email)
                        ->send(new ConfirmationEmail($create));
                } catch (\Exception $e) {
                    DB::rollBack();
                    return [
                        'status' => false,
                        'message' => ['An error occurred. Try again later.', 'Error: 10'],
                    ];
                }

                DB::commit();
                return [
                    'status' => true,
                    'message' => [
                        'Successfully registered. A confirmation email was sent to ' . $create->email
                    ]
                ];
            }

            DB::rollBack();
            return ['status' => false, 'message' => ['Unkown error again']];
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'username' => 'required|string|max:45|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'tos' => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'message' => $validator->messages(),
            ];
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        try {
            return User::create([
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => ['Database error'],
                'error' => $e
            ];
        }
    }

    /**
     * Creates a token for the user validation account
     * @param $data
     * @return array
     */
    protected function token($data)
    {
        //create token
        $token = hash('md5', $data['email'] . '--');
        $b64 = base64_encode($token);
        $userid = $data['id'];

        try {
            $create_token = User\AccountTokens::insert([
                'user_id' => $userid,
                'token' => $token,
                'public_hash' => $b64
            ]);
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => [
                    'Could not create token'
                ]
            ];
        }

        if ($create_token) {
            return [
                'status' => true
            ];
        }
    }

    /**
     * Validates the token from the url and enables the account of the user
     * @param $public_hash
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function validate_token($public_hash)
    {
        try {
            $token_data = User\AccountTokens::where(['public_hash' => $public_hash, 'status' => 1])->get()->first();
        } catch (\Exception $e) {
            return ['status' => false];
        }

        if($token_data){
            try {
                $update_user = \App\User::where(['id' => $token_data->user_id])->update(['status' => 1]);
            } catch (\Exception $e) {
                return ['status' => false, 'e' => $e];
            }

            if ($update_user == 1) {

                //remove token
                $remove_token = User\AccountTokens::where(['user_id' => $token_data->user_id])->forceDelete();

                if($remove_token != 1){
                    //log error
                }
                Log::info('New user registered!');
                return view('pages.auth.register.token_validation', [
                    'registration' => true,
                    'user' => \App\User::where(['id' => $token_data->user_id])->get()->first(),
                ]);
            }
        }

        return view('pages.auth.register.token_validation', ['registration' => false]);
    }
}
