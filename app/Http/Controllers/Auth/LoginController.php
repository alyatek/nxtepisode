<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Template\UtilitiesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Services\Auth\PlatformsLoginService;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->platforms_logins = new PlatformsLoginService();
    }

    public function get_index()
    {

        return view(
            'pages.auth.login.login', [
            'utilities' => [
                UtilitiesController::utility()['icheck'],
                UtilitiesController::utility()['pnotify'],
                UtilitiesController::utility()['underscore'],
            ]
        ]
        );
    }

    public function login(Request $request)
    {
        //validate
        $validate = $this->validator(
            [
                'username' => $request->username,
                'email'    => $request->email,
                'password' => $request->password,
                'tos'      => $request->tos
            ]
        );

        if (isset($validate['status'])) {
            return [
                "status"  => false,
                "message" => $validate['message']
            ];
        }

        //make login
        $credentials = $request->only('email', 'password');

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => 1], $request->remember)) {
            return [
                'status'   => true,
                'message'  => ['Login was a success'],
                'redirect' => url('/')
            ];
        }

        return [
            'status'  => false,
            'message' => ['Your credentials do not match'],
        ];

    }

    protected function validator(array $data)
    {
        //validate
        $validator = Validator::make(
            $data, [
            'email'    => 'required|string|max:255',
            'password' => 'required|string|max:255',
        ]
        );

        if ($validator->fails()) {
            return [
                'status'  => false,
                'message' => $validator->messages(),
            ];
        }

    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')
                        ->scopes(['pages_messaging', 'pages_messaging_subscriptions'])
                        ->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->setScopes(['pages_messaging'])->user();

        return $this->platforms_logins->login_user_with_facebook(
            $user->getName(), $user->getEmail(), $user->getId(), $user->getNickname(), $user->getName(), $user->getAvatar()
        );
    }
}
