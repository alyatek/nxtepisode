<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Template\UtilitiesController;
use App\Mail\PasswordResetEmail;
use App\Models\Users\User\ResetPassword;
use App\Models\Users\User\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct ()
    {
        $this->middleware('guest');
    }

    public function get_forgot_password_index ()
    {
        return view('pages.auth.forgot_password.forgot-password', [
            'utilities' => [
                UtilitiesController::utility()['animatecss'],
                UtilitiesController::utility()['underscore'],
            ],
        ]);
    }

    public function get_forgot_password_reset_index ($token)
    {
        $get_token = ResetPassword::where('token',$token)->get();

        if(collect($get_token)->isEmpty()){
            return view('pages.auth.forgot_password.forgot-password-reset-error', [
                'utilities' => [
                    UtilitiesController::utility()['animatecss'],
                    UtilitiesController::utility()['underscore'],
                ],
            ]);
        }

        return view('pages.auth.forgot_password.forgot-password-reset', [
            'utilities' => [
                UtilitiesController::utility()['animatecss'],
                UtilitiesController::utility()['underscore'],
            ],
        ]);
    }

    public function request_forgot_password (Request $request)
    {
        if (collect($request->email)->isEmpty()) {
            return ['status' => false, 'data' => ['content' => ['Oopsie doopsie, no email is set.']]];
        }

        $email = $request->email;

        //check if email exists
        $validate_mail = User::where('email', $email)->get()->first();

        if (collect($validate_mail)->isEmpty()) {
            return ['status' => false, 'data' => ['content' => ['There is not account with this email address']]];
        }

        $ip   = $request->ip;
        $hash = md5($email . $ip . 'asalt');

        $create_record = $this->create_record($email, $hash, $ip);

        if (!$create_record) {
            return ['status' => false];
        }

        Mail::to($email)
            ->send(new PasswordResetEmail(['token' => $hash, 'ip' => $ip]));

        return ['status' => true, 'data' => ['content' => ['Reset link  was sent to ' . $email . '.']]];
    }

    public function handle_forgot_password ($token, Request $request)
    {
        $validate = Validator::make($request->all(), [
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validate->fails()) {
            return ['status' => false, 'data' => ['content' => $validate->errors()]];
        }

        $password_hash       = Hash::make($request->password);
        $get_user_from_token = $this->get($token);
        if (collect($get_user_from_token)->isEmpty()) {
            return ['status' => false];
        }
        $email = $get_user_from_token->email;

        $update = $this->update($password_hash, $email);

        if ($update === 0) {
            return ['status' => false, 'data' => ['content' => ['Could not update right now.']]];
        }
        ResetPassword::where('token',$token)->delete();
        return ['status' => true, 'data' => ['content' => ['Your password was updated.']]];
    }

    public function create_record ($email, $token, $ip)
    {
        $create = new ResetPassword();

        $create->email = $email;
        $create->token = $token;
        $create->ip    = $ip;

        $create->save();

        return $create;
    }

    public function get ($token)
    {
        $get = ResetPassword::select('email')->where('token', $token)->get()->first();
        return $get;
    }

    public function update ($password, $email)
    {
        $update = User::where('email', $email)->update(['password' => $password]);
        return $update;
    }

    public function remove ()
    {

    }
}
