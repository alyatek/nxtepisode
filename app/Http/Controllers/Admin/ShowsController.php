<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Shows\Episodes\EpisodeController;
use App\Http\Controllers\Users\UserShows\UserShowsController;
use App\Models\episodess\Episodess\Episodes;
use App\Models\Shows\Seasons\Season;
use App\Models\Shows\Show\Show;
use App\Models\Shows\Shows\ShowsStatistics;
use App\Models\Users\UserShows\UserShows;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use function MongoDB\BSON\toJSON;

class ShowsController extends Controller
{
    public function get_top_5 ()
    {
        $get = UserShows::select('show_id')
            ->groupBy('show_id')
            ->orderByRaw('COUNT(*) DESC')
            ->limit(5)
            ->get();
        return $get;
    }

    public function update_top_5 ()
    {
        $statistics = new ShowsStatisticsController();

        $get_id_top_5 = $this->get_top_5();

        $get_statistics = $statistics->get();

        if(collect($get_statistics)->get('id')){
            $id = collect($get_statistics)->get('id');
        }

        if ($get_statistics === null) {
            $create = $statistics->create('top_5');
            if ($create === 0) {
                return ['status' => false];
            }
            $id = $create;
        }

        $update = $statistics->update($id, json_encode($get_id_top_5));

        if ($update === 0) {
            return ['status' => false];
        }
        return ['status' => true];
    }

    public function clear_top_5_cache () {
        dd([1]);
        Cache::forget('top_5');
        return response()->json(['status' => true],200);
    }

    public function get_shows ()
    {
        $shows = Show::select('name', 'id', 'premiered')->get();

        foreach ($shows as $key => $show) {
            $get_seasons_updated    = Episodes::select('updated_at')->where('show_id', $show['id'])->get()->first();
            $shows[$key]['seasons'] = $get_seasons_updated;
        }

        return $shows;
    }

    public function update_seasons ( int $show_id)
    {
        //get show api id
        $get_api = Show::select('api_id')->where('id', $show_id)->get()->first();

        if(!$get_api){
            return 0;
        }

        //get show seasons from api
        $api          = new ApiController();
        $get_episodes = $api->show_episode_list(collect($get_api)->get('api_id'));

        //format the episodes
        $episodes = new EpisodeController();
        $formated = $episodes->formatEpisodesPerSeasonFromApi($get_episodes['content']);

        //update the show seasons
        $update = Episodes::where('show_id', $show_id)->update(['episodes' => json_encode($formated)]);

        //return answer
        return response()->json([$update]);
    }
}
