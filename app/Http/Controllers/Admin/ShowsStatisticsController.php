<?php

namespace App\Http\Controllers\Admin;

use App\Models\Shows\Shows\ShowsStatistics;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShowsStatisticsController extends Controller
{
    public function create ($name)
    {
        $statistics         = new ShowsStatistics();
        $statistics->name   = $name;
        $statistics->status = 1;
        $statistics->save();
        return $statistics->id;
    }

    public function get ()
    {
        $get = ShowsStatistics::where('name', 'top_5')->get()->first();
        return $get;
    }

    public function update ($id, $params)
    {
        $update = ShowsStatistics::find($id);

        $update->params = $params;

        return $update->save();
    }

    public function remove ()
    {

    }
}
