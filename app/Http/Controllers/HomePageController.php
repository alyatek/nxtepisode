<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Template\UtilitiesController;
use App\Services\AppContent\AppContentService;
use App\Services\VoyagerContent\VoyagerHomeContentService;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public function __construct() {
        $this->app_content = new AppContentService();
    }

    public function get_index()
    {
        pagetitle('Manage TV Shows');
        $user_recommended = $this->app_content->get_user_recommended();
        $popular_this_week = $this->app_content->get_popular_this_week();
        $small_recommended = $this->app_content->get_small_recommended();

//        dd($popular_this_week);
        return view('pages.home.home', [
            'utilities' => [
                UtilitiesController::utility()['underscore'],
                UtilitiesController::utility()['shave'],
                UtilitiesController::utility()['dateformat'],
            ],
            'recommends' => [
                'user_recommends' => $user_recommended[0],
                'popular_this_week' => $popular_this_week,
                'small_recommended' => $small_recommended,
            ],
            'seo_data' => [
                '<meta name="description" content="Manage and plan your shows in the most efficient and easiest way ever. We are here to revolutionize the way you manage your TV Shows!">',
                '<meta name="keywords" content="show manager, tv show management, plan tv show, show plan, plan my next episode, tv show planner, show tracking, tv show tracking, tv episodes tracking, track my show, tracking tv shows">',
                '<meta property="og:title" content="'.config('app.name').'" />',
                '<meta property="og:type" content="website" >',
                '<meta property="og:url" content="'.url('/').'" >',
                '<meta property="og:image" content="'.url('img/meta_logo.png').'" >',
                '<meta property="og:description" content="Manage and plan your shows in the most efficient and easiest way ever. We are here to revolutionize the way you manage your TV Shows!" >',
            ]
        ]);
    }
}
