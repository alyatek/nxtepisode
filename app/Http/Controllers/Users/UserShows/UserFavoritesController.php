<?php

namespace App\Http\Controllers\Users\UserShows;

use App\Models\Users\UserShows\UserFavorites;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Template\UtilitiesController;

class UserFavoritesController extends Controller
{
    public function create ($show_id, $user_id)
    {
        DB::beginTransaction();
        try {
            $user_favs          = new UserFavorites();
            $user_favs->user_id = $user_id;
            $user_favs->show_id = $show_id;
            $user_favs->status  = 1;
            $user_favs->save();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'e' => $e];
        }
        DB::commit();
        return ['status' => true];
    }

    public function get ($show_id, $user_id)
    {
        try {
            $get = UserFavorites::where('user_id', $user_id)->where('show_id', $show_id)->get()->first();
        } catch (\Exception $e) {
            return ['status' => false];
        }
        return ['status' => true, 'content' => ['data' => $get]];
    }

    public function get_all ($user_id, $status = 1) {
        $get = UserFavorites::where('user_id',$user_id)->where('status',$status)->get();
        return $get;
    }

    public function get_index ()
    {
        pagetitle('Favorites');
        return view('pages.user.watchlist-favorites', [
            'utilities' => [
                UtilitiesController::utility()['underscore'],
                UtilitiesController::utility()['shave'],
                UtilitiesController::utility()['moment.js'],
                UtilitiesController::utility()['animatecss'],
                UtilitiesController::utility()['sweetalert'],
                UtilitiesController::utility()['sprites'],
            ],
        ]);
    }

    public function update ($show_id, $user_id)
    {

    }

    public function remove ($show_id, $user_id)
    {
        try {
            $remove = UserFavorites::where('user_id', $user_id)->where('show_id', $show_id)->get()->first();
            $remove->delete();
        } catch (\Exception $e) {
            return ['status' => false];
        }

        return ['status' => true];
    }

    public function delete ($show_id, $user_id)
    {
        try {
            $delete = UserFavorites::where('user_id', $user_id)->where('show_id', $show_id)->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false];
        }
        return $delete;
    }
}
