<?php

namespace App\Http\Controllers\Users\UserShows;

use App\Models\Users\User\FacebookMessage;
use App\Models\Users\UserShows\UserScheduleConfigurations;
use App\Models\Users\UserShows\UserShowsSchedule;
use App\Services\Api\FacebookServiceRemoveUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\UserWatchlaterSettingsService;

class UserScheduleConfigurationsController extends Controller
{

    /**
     * UserScheduleConfigurationsController constructor.
     * Declares the usable variables
     */
    public function __construct()
    {
        $this->user                      = Auth::user();
        $this->user_shows_configurations = new UserScheduleConfigurations();
        $this->user_watchlater_service   = new UserWatchlaterSettingsService();
        $this->user_facebook_messages    = new FacebookMessage();
        $this->user_facebook_disable     = new FacebookServiceRemoveUser();
//        $this->post = new Request();
    }

    public function handle_new_email_configuration(Request $request)
    {
        $method_value = $request->notificationEmailInput;
        return $this->user_watchlater_service->make_method($method_value, 'email');
    }

    public function handle_onesignal_configuration(Request $request)
    {
        $method_value = $request->onesignal_player_id;

        $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (stripos($ua, 'mobile') !== false) {
            if (stripos($ua, 'android') !== false) {
                $method_configuration = 'onesignal_android';
                return $this->user_watchlater_service->make_method($method_value, $method_configuration);
            }
        }
        #set method depends from device
        if (\Browser::isMobile()) {
            if (strpos(strtolower(\Browser::platformName()), 'ios') !== false) {
                return response()->json(['status' => false, 'content' => ['message' => ['This device is not yet supported.']]]);
            }
            $method_configuration = 'onesignal_android';
        } elseif (!\Browser::isMobile()) {
            $method_configuration = 'onesignal_pc';
        }

        return $this->user_watchlater_service->make_method($method_value, $method_configuration);
    }

    public function handle_facebook_messages_configuration(Request $request)
    {
        //@todo
    }

    public function handle_email_configuration_status()
    {
        return $this->user_watchlater_service->user_method_status('email');
    }

    public function handle_facebook_messages_configuration_status()
    {
        $get = collect($this->user_facebook_messages->get_configuration_status(Auth::user()->id));
        if ($get->isEmpty()) {
            return lazyr(true, ['No configuration found.'], ['configuration' => false], 200);
        }
        return lazyr(true, ['Facebook Messages are configured.'], ['configuration' => true], 200);
    }

    public function handle_facebook_messages_removal()
    {
        //fires event in which makes and send the message to disable
        $this->user_facebook_disable->send_disable_message(Auth::user()->id);
    }

    public function handle_email_configuration_removal()
    {
        return $this->user_watchlater_service->remove_email_method();
    }
}
