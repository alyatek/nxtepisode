<?php

namespace App\Http\Controllers\Users\UserShows;

use App\Http\Controllers\Shows\Episodes\EpisodeController;
use App\Http\Controllers\Shows\Seasons\SeasonController;
use App\Http\Controllers\Shows\Show\ShowController;
use App\Models\episodess\Episodess\Episodes;
use App\Models\Shows\Seasons\Season;
use App\Models\Shows\Show\Show;
use App\Models\Users\UserShows\UserShowEpisodes;
use App\Services\UserShows\UserEpisodesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserShowEpisodesController extends Controller
{
    public function __construct()
    {
        $this->seasons               = new SeasonController();
        $this->episodes              = new EpisodeController();
        $this->user_episodes_service = new UserEpisodesService();
    }

    public function create($show_id = 0, $user_id = 0)
    {
        #return error in case id of show is not set
        if ($show_id === 0) {
            return ['status' => false, 'content' => ['message' => ['Error.']]];
        }
        # get user id if none is given
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        // i need nr of seasons so i can loop each
        $seasons      = new SeasonController();
        $show_seasons = $seasons->get_seasons($show_id);

        #if somehow the showseasons return error
        if ($show_seasons['status'] === false) {
            return [
                'status'  => false,
                'content' => [
                    'message' => 'Error inserting episodes',
                ],
            ];
        } //if fails

        //count seasons
        $nr_seasons = (count($show_seasons['message']['content']) - 1); // needs to remove one because there is and ID key for the id of show

        $episodes      = new EpisodeController();
        $show_episodes = [];

        #creates array with only keys which are the id of each episode for the respective seasons
        for ($i = 1; $i <= $nr_seasons; $i++) {
            $get_episodes = $episodes->get_episodes(null, null, $i, $show_id);
            if ($get_episodes['status'] === false) {
                $episodes[$i] = null;
                continue;
            }
            foreach ($get_episodes['message']['content']['episodes'] as $key => $episode) {
                $show_episodes[$i][$key] = $key;
            }
//            $show_episodes[$i] = $get_episodes['message']['content']['episodes'];
        }

        DB::beginTransaction();

        #loop a create to insert in the db
        try {
            foreach ($show_episodes as $season => $episodes) {
                foreach ($episodes as $key => $episode) {
                    $insert_episodes                 = new UserShowEpisodes();
                    $insert_episodes->show_id        = $show_id;
                    $insert_episodes->user_id        = $user_id;
                    $insert_episodes->season         = $season;
                    $insert_episodes->episode        = $key;
                    $insert_episodes->episode_status = 0;
                    $insert_episodes->save();
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'status'  => false,
                'content' => [
                    ['message' => [$e]],
                ],
            ];
        }

        DB::commit();
        return [
            'status'  => true,
            'content' => ['message' => ['Success! YAY']],
        ];
    }

    /**
     * Gets all episodes from a season for a user.
     * ATTENTION - This only gets the episode from a certain season to display in the user checkin episodes - kinda used
     * this in the wrong way.
     * If null -> creates
     *
     * @param int     $season_id
     * @param int     $user_id
     * @param int     $show_id
     * @param Request $request
     *
     * @return
     */
    public function get($season_id = 0, $user_id = 0, $show_id = 0, Request $request)
    {
        $post = false;

        if ($show_id === 0) { // post request
            $post      = true;
            $show_id   = $request->show_id;
            $season_id = $request->season_id;
        }

        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $episodes      = new EpisodeController();
        $show_episodes = $episodes->get_episodes(null, null, $season_id, $show_id);

        if ($post === true) { //post request -  wtfff
            if ($show_episodes['status'] === false) {
                return response()->json(['status' => false, 'content' => ['message' => ['Error getting episodes']]]);
            }

            return response()->json(
                [
                    'status'   => true,
                    'episodes' => [
                        $show_episodes['message']['content']['episodes'],
                    ],
                    'show_id'  => $show_id,
                ]
            );
        }

        if ($show_episodes['status'] === false) {
            return ['status' => false, 'content' => ['message' => ['Error getting episodes']]];
        }

        return [
            'status'   => true,
            'episodes' => [
                $show_episodes['message']['content']['episodes'],
            ],
            'show_id'  => $show_id,
        ];
    }

    public function get_all($user_id = 0, $show_id = 0, $status = 1)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
//            return ['status' => false];
        }

        $get = UserShowEpisodes::where('user_id', $user_id)->where('show_id', $show_id)->get();

        if ($get === null) { //doesnt have in db
            $episodes = $this->create($show_id);
            if ($episodes['status'] === false) {
                return ['status' => false];
            }
        } else {
            //in this case is in the db so
            //it need to verify if the last time the episodes were watched are no
            //that far a part from the time it was updated
            $this->episode_difference($show_id,$user_id);
        }

        $get = UserShowEpisodes::where('user_id', $user_id)->where('show_id', $show_id)->get();

        if (collect($get)->isNotEmpty()) {
            return ['status' => true, 'content' => $get];
        }

    }

    /**
     * This will compare the two last episodes from a user show to a normal show to see if
     * the user has the show up to date
     *
     * @param $user_id
     * @param $show_id
     *
     */
    public function get_episodes_difference($user_id, $show_id)
    {
        //get last episode and season from the show
        $last_season  = $this->seasons->get_last_season($show_id);
        $last_episode = $this->episodes->get_last_episode_of_season($show_id, $last_season['data']['last_season_nr']);

        //get last episode and season of user from the show
        $last_user_episode = collect(
            UserShowEpisodes::select('episode', 'season')
                            ->where('show_id', $show_id)
                            ->where('user_id', $user_id)
                            ->get()->last()
        );
        //compares if all lasts are the same
//        if ($last_user_episode->get('season') !== $last_season['data']['last_season_nr'] &&
//            $last_user_episode->get('episode') !== $last_episode) {
//            //returns false in case they are not exactly the same meaning the user is not up to date
//            return ['status' => false, 'season' => $last_user_episode->get('season'), 'episode' => $last_user_episode->get('episode')];
//        }
        if ($last_user_episode->get('season') === $last_season['data']['last_season_nr'] &&
            $last_user_episode->get('episode') === $last_episode) {
            return ['status' => true];
        }

        //returns false in case they are not exactly the same meaning the user is not up to date
        return ['status' => false, 'season' => $last_user_episode->get('season'), 'episode' => $last_user_episode->get('episode')];

    }

    public function get_latest_updated($user_id = 0, $show_id = 0, $status = 1)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $get = UserShowEpisodes::where('user_id', $user_id)
                               ->where('show_id', $show_id)
                               ->where('episode_status', 1)
                               ->orderBy('season', 'desc')->orderBy('episode', 'desc')->orderBy('updated_at')
                               ->get()->first();

        if ($get === null) {  //means 2 thing - might not be inserted in DB at all OR
            // its in DB but nothing has been watched so the result
            // must have to be the first episode of the first season

            #check if there is atleast one result regarding this show
            $one = UserShowEpisodes::where('show_id', $show_id)->where('user_id', $user_id)->limit(1)->get();

            if (collect($one)->isEmpty()) { #needs to insert all the things - call function above of all
                $insert_all = $this->get_all($user_id, $show_id);

                if ($insert_all['status'] === false) {
                    return ['status' => false, 'error' => 12];
                }
            }

            #repeat this shit to check if there is any
            $one = UserShowEpisodes::where('show_id', $show_id)->where('user_id', $user_id)->limit(1)->get();

            #if yes good - if not oh shit fudeo a serio
            if (collect($one)->isEmpty()) {
                return ['status' => false, 'error' => 'is empty'];
            }

            #repeat the get above
            $get = UserShowEpisodes::where('user_id', $user_id)
                                   ->where('show_id', $show_id)
                                   ->where('episode_status', 0)
                                   ->where('episode', 1)
                                   ->where('season', 1)
                                   ->orderBy('season', 'asc')->orderBy('episode', 'asc')->orderBy('updated_at', 'asc')
                                   ->get()->first();

        }

        if (collect($get)->isNotEmpty()) {
            return ['status' => true, 'content' => $get];
        }

        return ['status' => false, 'error' => 'failed all'];
    }

    /**
     * Updates an episode status
     */
    public function update($show_id, $season_nr, $episode_nr, $episode_status, $user_id = 0, $params = [])
    {
        $params = json_encode($params);
        try {
            $update = UserShowEpisodes::where('show_id', $show_id)
                                      ->where('user_id', $user_id)
                                      ->where('season', $season_nr)
                                      ->where('episode', $episode_nr)
                                      ->update(
                                          [
                                              'episode_status' => $episode_status,
                                              'params'         => $params,
                                          ]
                                      );
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'status'  => false,
                'content' => [
                    'message' => ['Error: ' . $e],
                ],
            ];
        }

        if ($update) {
            return ['status' => true];
        }

        return [
            'status'  => false,
            'content' => [
                'message' => ['Error: ', $update],
            ],
        ];
    }

    /**
     * Updates bulk episodes of a seasons
     */
    public function update_season($show_id, $season_nr, $episode_status, $user_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        try {
            UserShowEpisodes::where('user_id', $user_id)
                            ->where('show_id', $show_id)
                            ->where('season', $season_nr)
                            ->update(['episode_status' => $episode_status]);
        } catch (\Exception $e) {
            return [
                'status'  => false,
                'message' => ['content' => ['Error:' . $e]],
            ];
        }

        return ['status' => true];
    }

    public function delete($show_id, $user_id)
    {
        try {
            $delete = UserShowEpisodes::where('user_id', $user_id)->where('show_id', $show_id)->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false];
        }
        return $delete;
    }

    public function episodes_difference ($show_id, $user_id) {
        $check_episodes = $this->get_episodes_difference($user_id, $show_id);

        if ($check_episodes['status'] === false) {
            //update the fucking show for the user
            $make_update = $this->user_episodes_service->bundle_missing_episodes($show_id, $check_episodes['season'], $check_episodes['episode'], $user_id);

            if ($make_update === false) {
                return ['status' => false];
            }
        }
    }
}
