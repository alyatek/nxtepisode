<?php

namespace App\Http\Controllers\Users\UserShows;

use App\Http\Controllers\Shows\Seasons\SeasonController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users\UserShows\UserShowsSeasons;
use Illuminate\Support\Facades\DB;

class UserShowsSeasonsController extends Controller
{
    public function create ($params = [])
    {
        if ($params['custom'] === true) {
            $seasons = new SeasonController();
            $seasons = $seasons->get_seasons($params['show_id']);

            $insert = [];

            if ($seasons['status'] === true) {
                $c = 0;
                foreach ($seasons['message']['content'] as $key => $season) {
                    if ($season['number'] === null) {
                        continue;
                    }
                    $insert[$c]['user_id']       = $params['user_id'];
                    $insert[$c]['show_id']       = $params['show_id'];
                    $insert[$c]['season']        = $season['number'];
                    $insert[$c]['season_status'] = 0;
                    $c++;
                }
            }
        }

        try {
            DB::beginTransaction();
            foreach ($insert as $key => $season) {
                $save = new UserShowsSeasons();

                $save->user_id       = $season['user_id'];
                $save->show_id       = $season['show_id'];
                $save->season        = $season['season'];
                $save->season_status = $season['season_status'];

                $save->save($insert);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, $e];
        }

        DB::commit();
        return ['status' => true];

    }

    public function get ($params = [])
    {
        try {
            $get = UserShowsSeasons::where(['user_id' => $params['user_id']])->get();
        } catch (\Exception $e) {
            return ['status' => false];
        }

        if ($get) {
            return ['status' => true, 'content' => $get];
        }

        return ['status' => false];
    }

    public function get_next_3 ($show_id, $user_id = 0) {

    }

    public function update ($params = [])
    {
        try {
            $update = UserShowsSeasons::where('show_id', $params['show_id'])
                ->where('user_id', $params['show_id'])
                ->where('season', $params['season'])
                ->update(['season_status' => $params['season_status']]);
        } catch (\Exception $e) {
            return ['status' => false];
        }

        if (collect($update)->isEmpty()) {
            return ['status' => false];
        }
    }

    public function delete ($show_id, $user_id)
    {
        try {
            $delete = UserShowsSeasons::where('user_id',$user_id)->where('show_id',$show_id)->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false];
        }
        return $delete;
    }
}
