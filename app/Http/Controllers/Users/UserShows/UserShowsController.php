<?php

namespace App\Http\Controllers\Users\UserShows;

use App\Http\Controllers\Shows\Episodes\EpisodeController;
use App\Http\Controllers\Shows\Seasons\SeasonController;
use App\Http\Controllers\Shows\Show\ShowController;
use App\Http\Controllers\Shows\Show\ShowMediaController;
use App\Models\Shows\Show\Show;
use App\Models\Shows\Shows\ShowMedia;
use App\Models\Users\UserShows\UserShowEpisodes;
use App\Models\Users\UserShows\UserShows;
use App\Models\Users\UserShows\UserShowsSchedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Template\UtilitiesController;

class UserShowsController extends Controller
{

    public function __construct()
    {
        $this->show_media = new ShowMedia();
        $this->episodes   = new EpisodeController();
//        $this->user_watchlist = new UserWatchlistController();
    }

    public function create_favorite($show_id = 0, $user_id = 0)

    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }
        $user_favs = new UserFavoritesController();
        $get       = $user_favs->get($show_id, $user_id);

        if ($get['status'] === false) {
            return [
                'status' => false,
                'status'
            ];
        }

        if ($get['content']['data'] === null) {
            $create = $user_favs->create($show_id, $user_id);

            if ($create['status'] === false) {
                return [
                    'status' => false,
                    'create' => $create['e']
                ];
            }

            return [
                'status'  => true,
                'content' => ['data' => ['favorite_status' => 'added']]
            ];
        }

        $update = $user_favs->remove($show_id, $user_id);

        if ($update['status'] === false) {
            return [
                'status' => false,
                'remove'
            ];
        }

        return [
            'status'  => true,
            'content' => ['data' => ['favorite_status' => 'removed']]
        ];
    }

    /**
     * Gets user show index for watchlist info
     *
     * @param $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_index($id)
    {
        $user_favs = new UserFavoritesController();
        $favorite  = $user_favs->get($id, Auth::user()->id);

        $favorite_status = 0;

        if ($favorite['status'] === true) {
            if ($favorite['content']['data'] !== null) {
                $favorite_status = 1;
            }
        }

        //for some fucking retarded reason this needs to be initilised here or else it will give nesting error
        $user_watchlist     = new UserWatchlistController();
        $get_current        = $user_watchlist->get_show_currently_watching_episode(Auth::user()->id, $id);
        $currently_watching = false;

        if ($get_current['status'] === true) {
            //if the date is 2000-01-01 00:00:00 means the user is still watching it
            if ($get_current['content']['message']['currently_watching']['ended_at'] === '2000-01-01 00:00:00') {
                $data = $get_current['content']['message']['currently_watching'];

                $episode_data       = $this->episodes->get_episode($id, $data['season'], $data['episode']);
                $episode_data       = $episode_data['content']['message']['episode'];
                $currently_watching = [
                    'episode' => $data['episode'],
                    'season'  => $data['season'],
                    'name'    => $episode_data['name']
                ];
            }
        }

        return view(
            'pages.user.watchlist-show', [
                                           'favorite'           => $favorite_status,
                                           'utilities'          => [
                                               UtilitiesController::utility()['underscore'],
                                               UtilitiesController::utility()['shave'],
                                               UtilitiesController::utility()['moment.js'],
                                               UtilitiesController::utility()['underscore.string'],
                                               UtilitiesController::utility()['animatecss'],
                                               UtilitiesController::utility()['sweetalert'],
                                               UtilitiesController::utility()['fancybox'],
                                           ],
                                           'currently_watching' => $currently_watching,
                                       ]
        );
    }

    public function get_user_show($show_id, $user_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $get = UserShows::where('show_id', $show_id)->where('user_id', $user_id)->get();

        if (collect($get)->isEmpty()) {
            return false;
        }

        return true;
    }

    /**
     * Gets info for the header of watchlist show page
     *
     * @param int $show_id
     *
     * @return array
     */
    public function get_user_show_info($show_id = 0)
    {
        $user_show = $this->get_user_show($show_id);

        if ($user_show === false) {
            return ['status' => false];
        }

        try {
            $get_show_info = Show::select(
                'name AS show_name',
                'genres', 'slug', 'api_id', 'summary', 'language', 'runtime',
                'premiered', 'official_site', 'schedule', 'rating', 'network', 'externals'
            )
                                 ->where('id', $show_id)->firstOrFail();
        } catch (\Exception $e) {
            return ['status' => false];
        }
        $get_show_info['media']   = $this->show_media->get($show_id);
        $get_show_info['show_id'] = $show_id;
        $get_show_info['genres']  = json_decode($get_show_info['genres']);
        return ['status' => true, 'content' => ['data' => $get_show_info]];
    }

    /**
     * Retrieves episodes in their seasons with season information and user data about that episode
     *
     * @param int $show_id
     * @param int $user_id
     *
     * @return array
     */
    public function get_user_show_episodes($show_id = 0, $user_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $user_episodes    = new UserShowEpisodesController();
        $show_seasons     = new SeasonController();
        $get_show_seasons = $show_seasons->get_seasons($show_id);

        //check if there is episodes to update
        $user_episodes->episodes_difference($show_id,$user_id);

        if ($get_show_seasons['status'] === false) {
            return ['status' => false];
        }

//        return $get_show_seasons['message']['content'];
        unset($get_show_seasons['message']['content']['id']);
        $data = [];

        foreach ($get_show_seasons['message']['content'] as $season) {
            $season_nr                    = $season['number'];
            $get                          = UserShowEpisodes::where('show_id', $show_id)
                                                            ->where('user_id', $user_id)
                                                            ->where('season', $season_nr)
                                                            ->get();
            $data[$season_nr]['season']   = $season;
            $data[$season_nr]['episodes'] = $get;
        }


        $episodes = $this->episodes->get_episodes(null, null, null, $show_id);

        return ['status' => true, 'content' => ['data' => $data, 'show_episodes' => $episodes]];

    }

    public function get_user_favorites($user_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $favorites     = new UserFavoritesController();
        $get_favorites = $favorites->get_all($user_id);

        if ($get_favorites === null) {
            return ['status' => false];
        }

        $data = [];

        $show       = new ShowController();
        $show_media = new ShowMediaController();

        foreach ($get_favorites as $favorite) {
            $get_data              = $show->get_show_data_simple($favorite->show_id);
            $data[$get_data->slug] = $get_data;

            $get_media                      = $show_media->get_media($favorite->show_id);
            $data[$get_data->slug]['media'] = [];

            if ($get_media['status'] === true) {
                $data[$get_data->slug]['media'] = $get_media['message']['content']->get('original');
            }
        }

        if (collect($data)->isEmpty()) {
            return [
                'status'  => false,
                'content' => [
                    'data'      => [
                        'You have no favorites.',
                        'Go to your <a href="' . url('watchlist') . '">watchlist </a>to find out some!'
                    ],
                    'favorites' => false
                ]
            ];
        }
        return [
            'status'  => true,
            'content' => ['data' => ['shows' => $data]]
        ];
    }

    /**
     * Gets the show setup status
     *
     * @param     $show_id
     * @param int $user_id
     *
     * @return array
     */
    public function get_show_setup_status($show_id, $user_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        try {
            $get = UserShows::select(['setup'])->where('user_id', $user_id)->where('show_id', $show_id)->get()->first();
        } catch (\Exception $e) {
            Log::debug('Error 102');
            return ['status' => false];
        }

        if (collect($get)->isNotEmpty()) {
            return [
                'status'  => true,
                'content' => ['data' => $get]
            ];
        }

        return ['status' => false];
    }

    /**
     * Updates the show setup status
     *
     * @param     $show_id
     * @param int $user_id
     * @param int $show_status
     *
     * @return array
     */
    public function update_show_setup_status($show_id, $user_id = 0, $show_status = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        try {
            $update = UserShows::where('user_id', $user_id)->where('show_id', $show_id)
                               ->update(['setup' => $show_status]);
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false];
        }

        DB::commit();
        return ['status' => true];
    }

    public function remove_show($show_id, $user_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $seasons            = new UserShowsSeasonsController();
        $episodes           = new UserShowEpisodesController();
        $currently_watching = new UserCurrentlyWatchingController();
        $favorites          = new UserFavoritesController();
        $scheduled          = new UserShowsSchedule();

        DB::beginTransaction();

        $seasons_r = $seasons->delete($show_id, $user_id); //remove user seasons

        if ($seasons_r['status'] === false) {
            return ['status' => false];
        }

        $schedule_r = $scheduled->remove($show_id, $user_id);

        $episodes_r = $episodes->delete($show_id, $user_id); //remove user episodes

        $currently_watching_r = $currently_watching->delete($show_id, $user_id); //remove user currently watching

        $favorites_r = $favorites->delete($show_id, $user_id); //remove user favorite

        try {
            UserShows::where('user_id', $user_id)->where('show_id', $show_id)->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false];
        }

        DB::commit();
//        $request = new Request();
        session()->flash(
            'status', [
                        'title' => 'Removed',
                        'text'  => 'Removed with success',
                        'type'  => 'success'
                    ]
        );
        return ['status' => true];
    }
}

