<?php

namespace App\Http\Controllers\Users\UserShows;

use App\Http\Controllers\Shows\Episodes\EpisodeController;
use App\Http\Controllers\Shows\Seasons\SeasonController;
use App\Http\Controllers\Shows\Show\ShowController;
use App\Http\Controllers\Shows\Show\ShowMediaController;
use App\Http\Resources\LatestWatching;
use App\Models\Shows\Show\Show;
use App\Models\Users\UserShows\UserCurrentlyWatching;
use App\Models\Users\UserShows\UserShowEpisodes;
use App\Models\Users\UserShows\UserShows;
use App\Services\UserShows\CurrentlyWatchingService;
use App\Services\UserShows\WatchEpisodeService;
use App\Services\Notices\NoticesService;
use App\Services\UserShows\WatchSeasonService;
use App\Services\Watchlater\UserWatchlaterShowsScheduledService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Template\UtilitiesController;
use App\Http\Controllers\Users\UserShows\UserShowsSeasonsController;
use Illuminate\Support\Facades\Log;

/**
 * Class UserWatchlistController
 * Gets the responses to the front end and makes the logic
 *
 * @package App\Http\Controllers\Users\UserShows
 */
class UserWatchlistController extends Controller
{
    public function __construct()
    {
        $this->watch_epi                        = new WatchEpisodeService();
        $this->watch_season                     = new WatchSeasonService();
        $this->episode                          = new EpisodeController();
        $this->watchlater_show_schedule_service = new UserWatchlaterShowsScheduledService();
        $this->currently_watching               = new CurrentlyWatchingService();
        $this->episodes_controller              = new EpisodeController();

    }

    public function handle_watch_single_request(Request $request)
    {
        $user_id = Auth::user()->id;

        #this same stupid shit is as well on bulk season thing but there is a slight difference because it needs to fetch the last episode of a season to achieve this
        // sketchiest shit ever - needs tons of work
        // let me explain, for the poor fucker who will probably be me, understand
        // in order to the scheduler work when a bulk season is updated to watched
        // it needs to make the currently watching ended and to be the last episode of that given season he just watched
        // so the scheduler reads after that

        $c = new UserCurrentlyWatchingController(); #initiliaze shit the old way pleb
        // needs to validate if is there a currently watching record in the db
        $get = $c->get($user_id, $request->show_id);

        if (collect($get)->get('status') === false) {
            return response()->json(['status' => false, 'content' => ['message' => ['Cannot complete this action sir.']]]);
        }

        if (collect($get['content']['message'])->isEmpty()) {
            //creates record if not exist
            $c->create($user_id, $request->show_id, $request->season, $request->episode);
        }

        UserCurrentlyWatching::where('user_id', $user_id)
                             ->where('show_id', $request->show_id)
                             ->update(
                                 [
                                     'started_at' => Carbon::now(),
                                     'ended_at'   => Carbon::now()->addHour(),
                                     'season'     => $request->season,
                                     'episode'    => $request->episode
                                 ]
                             );

        // end of sketchy shit

        return $this->watch_epi->watch_episode($user_id, $request->show_id, $request->episode, $request->season);
    }

    public function handle_un_watch_single_request(Request $request)
    {
        $user_id = Auth::user()->id;

        #this same stupid shit is as well on bulk season thing but there is a slight difference because it needs to fetch the last episode of a season to achieve this
        // sketchiest shit ever - needs tons of work
        // let me explain, for the poor fucker who will probably be me, understand
        // in order to the scheduler work when a bulk season is updated to watched
        // it needs to make the currently watching ended and to be the last episode of that given season he just watched
        // so the scheduler reads after that

        $get_previous_episode = collect($this->episodes_controller->get_previous_episode($request->show_id, $request->season, $request->episode));

        $c = new UserCurrentlyWatchingController(); #initiliaze shit the old way pleb
        // needs to validate if is there a currently watching record in the db
        $get = $c->get($user_id, $request->show_id);

        if (collect($get)->get('status') === false) {
            return response()->json(['status' => false, 'content' => ['message' => ['Cannot complete this action sir.']]]);
        }

        if (collect($get['content']['message'])->isEmpty()) {
            //creates record if not exist
            $c->create($user_id, $request->show_id, $get_previous_episode->get('season'), $get_previous_episode->get('episode'));
        }

        UserCurrentlyWatching::where('user_id', $user_id)
                             ->where('show_id', $request->show_id)
                             ->update(
                                 [
                                     'started_at' => Carbon::now(),
                                     'ended_at'   => Carbon::now()->addHour(),
                                     'season'     => $get_previous_episode->get('season'),
                                     'episode'    => $get_previous_episode->get('episode')
                                 ]
                             );

        // end of sketchy shit

        return $this->watch_epi->un_watch_episode($user_id, $request->show_id, $request->episode, $request->season);
    }

    public function handle_watch_season_request(Request $request)
    {
        $user_id = Auth::user()->id;

        // sketchiest shit ever - needs tons of work
        // let me explain, for the poor fucker who will probably be me, understand
        // in order to the scheduler work when a bulk season is updated to watched
        // it needs to make the currently watching ended and to be the last episode of that given season he just watched
        // so the scheduler reads after that

        // in order to achieve this we need to know the last episode of that season
        $last_episode = $this->episodes_controller->get_last_episode($request->show_id, $user_id, $request->season);
        if (collect($last_episode)->get('status') === false) {
            return response()->json(['status' => false, 'content' => ['message' => ['Cannot complete this action sir.']]]);
        }

        $c = new UserCurrentlyWatchingController(); #initiliaze shit the old way pleb
        // needs to validate if is there a currently watching record in the db
        $get = $c->get($user_id, $request->show_id);

        if (collect($get)->get('status') === false) {
            return response()->json(['status' => false, 'content' => ['message' => ['Cannot complete this action sir.']]]);
        }

        if (collect($get['content']['message'])->isEmpty()) {
            //creates record if not exist
            $c->create($user_id, $request->show_id, $request->season, collect($last_episode)->get('content')['last_episode']['episode']);
        }

        UserCurrentlyWatching::where('user_id', $user_id)
                             ->where('show_id', $request->show_id)
                             ->update(
                                 [
                                     'started_at' => Carbon::now(),
                                     'ended_at'   => Carbon::now()->addHour(),
                                     'season'     => $request->season,
                                     'episode'    => collect($last_episode)->get('content')['last_episode']['episode']
                                 ]
                             );

        // end of sketchy shit

        return $this->watch_season->watch_season($user_id, $request->show_id, $request->season);
    }

    public function handle_unwatch_season_request(Request $request)
    {
        $user_id = Auth::user()->id;
        return $this->watch_season->un_watch_season($user_id, $request->show_id, $request->season);
    }

    public function add_to_watchlist($show_id = null, $user_id = null, Request $request)
    {
        if (!$show_id) {
            //from api id from request to show id
            $show_id = Show::select('id')->where(['api_id' => $request->id])->first();
            if (!$show_id) {
                //show might not exist in the db so it needs to be added
                $shows = new ShowController();
                $get   = $shows->get_show($request->id);
                if ($get['status'] === true) {
                    $show_id = $get['message']['content']['id'];
                }
            } else {
                $show_id = $show_id->toArray();
                $show_id = $show_id['id'];
            }
        }

        if (!$user_id) {
            $user_id = Auth::user()->id;
        }

        $get = $this->get_user_show($user_id, $show_id);

        if ($get['status'] === true) {
            if (collect($get['content']['message'])->isEmpty()) {
                //insert
                $insert = $this->insert_show_to_user_watchlist($show_id, $user_id);

                if ($insert['status'] === true) {
                    return response()->json(['status' => true, 'content' => ['message' => $insert['content']['message']]]);
                }

                return response()->json(['status' => false, 'content' => ['message' => ['Error adding this show.']]]);
            }
            return response()->json(['status' => true, 'content' => ['message' => ['Show already in your watchlist.']]]);
        }

        return response()->json(['status' => false, 'content' => ['message' => ['Error with process.']]]);

    }

    public function insert_show_to_user_watchlist($show_id, $user_id)
    {
        DB::beginTransaction();
        try {
            $show = new UserShows();

            $show->show_id = $show_id;
            $show->user_id = $user_id;

            $show->save();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'content' => ['message' => ['Error adding to watchlist.', $e]]];
        }

        if (!$show) {
            DB::rollBack();
            return ['status' => false, 'content' => ['message' => ['Error adding to watchlist.']]];
        }

        $user_seasons_controller = new UserShowsSeasonsController();
        $seasons                 = $user_seasons_controller->create(['user_id' => $user_id, 'show_id' => $show_id, 'custom' => true]);

        if ($seasons['status'] === false) {
            DB::rollBack();
            return ['status' => false, 'content' => ['message' => ['Error adding to watchlist.']]];
        }

        $user_episodes_controller = new UserShowEpisodesController();
        $episodes                 = $user_episodes_controller->create($show_id);

//        $user_watchlist_watching = new UserCurrentlyWatchingController();
//        $watchlist = $user_watchlist_watching->create($user_id,$show_id,1,1);

        if ($episodes['status'] === false) {
            DB::rollBack();
            return ['status' => false, 'content' => ['message' => ['Error adding to watchlist.']]];
        }

        DB::commit();
        return ['status' => true, 'content' => ['message' => ['Show added to your watchlist!']]];
    }

    public function get_index()
    {
        pagetitle('Watchlist');
        return view(
            'pages.user.watchlist', [
                                      'utilities' => [
                                          UtilitiesController::utility()['icheck'],
                                          UtilitiesController::utility()['underscore'],
                                          UtilitiesController::utility()['shave'],
                                          UtilitiesController::utility()['sprites'],
                                          UtilitiesController::utility()['animatecss'],
                                          UtilitiesController::utility()['dateformat'],
                                          UtilitiesController::utility()['moment.js'],
                                          UtilitiesController::utility()['icheck'],
                                          UtilitiesController::utility()['tinysort'],
                                      ],
                                  ]
        );
    }

    public function get_watchlist($public = 1)
    {
        $get = $this->get_user_watchlist(Auth::user()->id);
        if ($get === false) {
            return $get;
        }

        $shows = [];

        try {
            $shows_media = new ShowMediaController();
            $user_shows  = new UserShowsController();
            $loop        = 0;

            if ($get['status'] === false) {
                return $get;
            }
            foreach ($get['content']['message'] as $user_show) {
                $get_show       = Show::where(['id' => $user_show['show_id']])->first();
                $get_show_media = $shows_media->get_media($user_show['show_id']);

                if ($get_show_media['status'] === true) {
                    $get_show['media'] = $get_show_media['message']['content'];
                } else {
                    $get_show['media'] = [];
                }

                $show_setup_status = $user_shows->get_show_setup_status($user_show['show_id'], Auth::user()->id);

                if ($show_setup_status['status'] === false) {
                    Log::debug('Failed on each loop of getting the shows for the watchlist');
                }

                $shows[$loop]          = $get_show;
                $shows[$loop]['setup'] = $show_setup_status['content']['data']->setup;

                $loop++;
            }
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false, 'content' => [
                    'shows' => 0, 'message' => [
                        'Error getting shows.',
                        $e
                    ],
                ],
                ], 500
            );
        }


        return response()->json(
            [
                'status'  => true,
                'content' =>
                    [
                        'message' => [
                            'shows' => $shows
                        ],
                    ]
            ]
        );
    }

    public function get_user_watchlist($user_id, $status = 1)
    {
        try {
            $get = UserShows::where(['user_id' => $user_id, 'status' => $status])->get();
        } catch (\Exception $e) {
            return ['status' => false, 'content' => ['message' => ['Error getting watchlist.']]];
        }

        if (collect($get)->isEmpty()) {
            return ['status' => false, 'content' => ['message' => ['Watchlist is empty.']]];
        }

        return ['status' => true, 'content' => ['message' => $get->toArray()]];
    }

    public function get_user_show($user_id, $show_id)
    {
        //check if already in
        try {
            $get = UserShows::where(['show_id' => $show_id, 'user_id' => $user_id])->get();
        } catch (\Exception $e) {
            return ['status' => false, 'content' => ['message' => ['Error retrieving content.', $e]]];
        }

        return ['status' => true, 'content' => ['message' => $get]];
    }

    public function get_shows_currently_watching_episodes($user_id = 0)
    {

    }

    public function get_show_currently_watching_episode($user_id = 0, $show_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        if ($show_id === 0) {
            $request = new Request();
            $show_id = $request->show_id;
        }

        $user_currently_watching = new UserCurrentlyWatchingController();
        $get                     = $user_currently_watching->get($user_id, $show_id);

        if ($get['status'] === false) {
            return [
                'status'  => false,
                'content' => [
                    'message' => [
                        'Error! Could\'t get ',
                    ],
                ],
            ];
        }

        //gotta finish
        return ['status' => true, 'content' => ['message' => ['currently_watching' => $get['content']['message'][0]]]];
    }

    public function get_show_episode_next_route(Request $request)
    {
        return $this->get_show_episode_next(0, $request->show_id);
    }

    public function get_show_episode_next($user_id = 0, $show_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        //update episodes of show in case there is any
        $user_episodes = new UserShowEpisodesController();
        $user_episodes->episodes_difference($show_id, $user_id);

        //before next it needs to verify if is there a current one going on
        $current_has_finished = $this->get_show_currently_watching_episode($user_id, $show_id);

        // in case something goes wrong
        if ($current_has_finished['status'] === false) {
            return ['status' => false];
        }

        // if he wasnt stopped by the currently watching then can proceeed
        $user_currently_watching = new UserCurrentlyWatchingController();
        $get                     = $user_currently_watching->get_next($user_id, $show_id);

        if ($get['status'] === false) {
            if ($get['content']) {
                return $get;
            }
            return response()->json(['status' => false, ['error']]);
        }

        $service            = new WatchEpisodeService();
        $is_episode_debuted = $service->is_episode_debut_valid(
            $get['content']['message']['episode_nr'],
            $get['content']['message']['season_nr'],
            $show_id
        );

        $episodes = new EpisodeController();
        if ($current_has_finished['content']['message']['currently_watching'] === null) {
            $episodes     = new EpisodeController();
            $episode_info = $episodes->get_episode($show_id, 1, 1);

            $current_has_finished = $this->get_show_currently_watching_episode($user_id, $show_id);
            // in case something goes wrong
            if ($current_has_finished['status'] === false) {
                return ['status' => false];
            }
        }

        //makes the info to return to user about not being allow to get next because he hasnt finished the current
        if ($current_has_finished['content']['message']['currently_watching']['ended_at'] === null ||
            $current_has_finished['content']['message']['currently_watching']['ended_at'] === '2000-01-01 00:00:00') {
            $episode_info = $episodes->get_episode(
                $show_id,
                $current_has_finished['content']['message']['currently_watching']['season'],
                $current_has_finished['content']['message']['currently_watching']['episode']
            );

            if ($episode_info['status'] === false) {
                return ['status' => false, 'error' => 'failed getting episode info'];
            }

            $episode_name = $episode_info['content']['message']['episode']['name'];


            return response()->json(
                [
                    'status'       => true,
                    'next_allowed' => false,
                    'content'      => ['message' => 'Finish the current episode.'],
                    'episode'      => [
                        'season'  => $current_has_finished['content']['message']['currently_watching']['season'],
                        'episode' => $current_has_finished['content']['message']['currently_watching']['episode'],
                        'name'    => $episode_name,
                    ],
                ]
            );
        }

        if ($is_episode_debuted['status'] === false) {
            return ['status' => false];
        }

        if ($is_episode_debuted['allow'] === false) {
            return response()->json(
                [
                    'status'       => true,
                    'next_allowed' => false,
                    'content'      => [
                        'message' => [
                            'The following episode has not debuted yet.'
                        ]
                    ],
                    'params' => [
                        'air_date' => $is_episode_debuted['air_date'],
                    ]
                ]
            );
        }

        return response()->json(
            [
                'status'       => true,
                'next_allowed' => true,
                'content'      => ['message' => []],
                'episode'      => [
                    'season'  => $get['content']['message']['season_nr'],
                    'episode' => $get['content']['message']['episode_nr'],
                    'name'    => $get['content']['message']['episode']['name'],
                ],
            ]
        );


//        return response()->json(['status' => true, 'content' => $get['content']]);
    }

    public function get_latest_watching($user_id = 0)
    {

        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $get = UserCurrentlyWatching::select('show_id', 'season', 'episode', 'started_at')->where('user_id', $user_id)
                                    ->orderBy('started_at', 'desc')
                                    ->get()->first();

        if (!$get) {
            return ['status' => false];
        }

        $show = Show::select('name')->where('id', $get['show_id'])->get()->first();

        if (!$show) {
            return ['status' => false];
        }

        return [
            'status' => true, 'content' => [
                'data' => [
                    'show_id'    => $get['show_id'],
                    'show_name'  => $show['name'],
                    'season'     => $get['season'],
                    'episode'    => $get['episode'],
                    'started_at' => $get['started_at'],
                ],
            ],
        ];
    }


    public function get_modal_html_content($show_id)
    {
        $get_watch_status = $this->get_show_episode_next(Auth::user()->id, $show_id);

        $why_the_fuck = collect($get_watch_status)->get('original');

        if ($why_the_fuck['next_allowed'] === false) {
            if(!collect($why_the_fuck)->get('episode')){
                //this happens when the episode has not debuted yet and needs to send a message
                $content = '<div class="text-center"><h3>' . $why_the_fuck['content']['message'][0] . '</h3><p>This episode will air in '.Carbon::parse($why_the_fuck['params']['air_date'])->format('l jS \\of F Y').'.</p></div>';
                //@TODO this shouldnt be with a zero in case i decide to add more messages
            } else {
                $content = '<div class="text-center"><h3>' . $why_the_fuck['content']['message'] . '</h3><p>You have to finish the episode <b>' . $why_the_fuck['episode']['name'] . '</b> from <b>Season ' . $why_the_fuck['episode']['season'] . ' - <b>Episode ' . $why_the_fuck['episode']['episode'] . '</b></b></p><button id="finishWatchingBtn" class="btn btn-dark" onclick="updateFinishCurrent(' . $show_id . ', false,true,1)">Finish watching</button></div>';
            }
        } else {

            if($why_the_fuck !== null){
                $content = '<div style="display: none;" id="watch-next-episode"> <div class="row">'
                    . '<div class="col-md-12 text-center">'
                    . '<h3>Do you want to start watching?</h3> <p><b>' . $why_the_fuck['episode']['name'] . '</b> from <b>Season ' . $why_the_fuck['episode']['season'] . '</b> - <b>Episode ' . $why_the_fuck['episode']['episode'] . '</b></p>'
                    . '<button id="startWatchingBtn" class="btn btn-dark" onclick="startWatchingEpisode(' . $show_id . ', $(this))">Yes! Start Watching</button> </div></div></div>';
            }else{
                $content = '<div style="display: none;" id="watch-next-episode"> <div class="row">'
                    . '<div class="col-md-12 text-center">'
                    . '<h3>Looks like it is the end... for now, maybe.</h3> <p>You have no more episodes left to watch.</p>'
                    . '</div></div></div>';
            }

        }
        return response($content);

    }

    public function update_watchlist_seasons($user_id = 0, $show_id = 0, $seasons = [], Request $request)
    {
        if ($user_id === 0 && $show_id === 0) {
            if ($request->seasons) {
                $request->seasons;
            } else {
                return ['status' => false, 'content' => ['message' => ['No seasons are selected!']], 'error' => 1];
            }
            $user_id = Auth::user()->id;
            $show_id = $request->show_id;
        }

        $user_seasons_controller = new UserShowsSeasonsController();
        $exists                  = $user_seasons_controller->get(['user_id' => $user_id]);

        if ($exists['status'] === false) {
            return response()->json(['status' => 'false', 'error' => 3]);
        }

        $user_shows   = new UserShowsController();
        $setup_status = $user_shows->get_show_setup_status($show_id, $user_id);

        if ($setup_status['status'] === false) {
            return response()->json(['status' => true, 'content' => ['message' => 'Failed getting show status.']]);
        }

        if ($setup_status['content']['data']->setup === 1) {
            return response()->json(['status' => false, 'content' => ['message' => 'Oh boy! How did you get here? You already configured this show. Check F.A.Q to know more about this error.']]);
        }

        if (!collect($exists['content'])->isEmpty()) {
            $user_shows_seasons_controller  = new UserShowsSeasonsController();
            $user_shows_episodes_controller = new UserShowEpisodesController();
            $seasons                        = new SeasonController();

            $show_seasons = $seasons->get_seasons($show_id);

            if ($show_seasons['status'] === false) {
                return response()->json(['status' => 'false', 'error' => 4]);
            }

            $show_seasons            = $show_seasons['message']['content'];
            $user_seasons_collection = $request->seasons;

            DB::beginTransaction();
            foreach ($show_seasons as $key => $season) {
                if (in_array($key, $user_seasons_collection)) {

                    $user_shows_seasons_controller->update(
                        [
                            'user_id'       => $user_id,
                            'show_id'       => $show_id,
                            'season'        => $season['number'],
                            'season_status' => 1,
                        ]
                    );
                    $user_shows_episodes_controller->update_season($show_id, $season['number'], 1);
                } else {
                    if ($key !== 'id') {
                        $user_shows_seasons_controller->update(
                            [
                                'user_id'       => $user_id,
                                'show_id'       => $show_id,
                                'season'        => $season['number'],
                                'season_status' => 0,
                            ]
                        );
                        $user_shows_episodes_controller->update_season($show_id, $season['number'], 0);
                    }
                }
            }
        }

        DB::commit();
        $update_user_setup = $user_shows->update_show_setup_status($show_id, $user_id, 1);

        if ($update_user_setup['status'] === false) {
            Log::debug('Did not update user show setup status');
            Log::critical('Did not update user show setup status');
        }

        return response()->json(['status' => true]);
    }

    public function update_watchlist_episodes($user_id = 0, $show_id = 0, $season_nr = 0, Request $request)
    {
        if ($user_id === 0 && $show_id === 0 && $season_nr === 0) {
            if ($request->episodes) {
                $user_episodes = $request->episodes;
            } else {
                return ['status' => false, 'content' => ['message' => ['No episodes are selected!']], 'error' => 1];
            }
            $user_id   = Auth::user()->id;
            $show_id   = $request->show_id;
            $season_nr = $request->season_id;
        }

        $episodes     = new EpisodeController();
        $get_episodes = $episodes->get_episodes(null, null, $season_nr, $show_id);

        if ($get_episodes['status'] === false) {
            return [
                'status' => false,
            ];
        }

        $user_shows   = new UserShowsController();
        $setup_status = $user_shows->get_show_setup_status($show_id, $user_id);

        if ($setup_status['status'] === false) {
            return response()->json(['status' => false, 'content' => ['message' => 'Failed getting show status.']]);
        }

        if ($setup_status['content']['data']->setup === 1) {
            return response()->json(['status' => false, 'content' => ['message' => 'Oh boy! How did you get here? You already configured this show. Check F.A.Q to know more about this error.']]);
        }

        //checks if the last episode checked here is bigger than the one in db
        //if so it updates to match the checkboxes
        $user_currently_watching              = new UserCurrentlyWatchingController();
        $last_episode                         = array_last($request->episodes);
        $call_check_update_currently_watching = $user_currently_watching->update_last($user_id, $show_id, $season_nr, $last_episode);

        if ($call_check_update_currently_watching['status'] === false) {
            Log::debug('Failed ');
        }

        $user_shows_episodes = new UserShowEpisodesController();

        DB::beginTransaction();
        foreach ($get_episodes['message']['content']['episodes'] as $episode_nr => $episode_content) {
            if (in_array($episode_nr, $request->episodes)) {
                $update = $user_shows_episodes->update($show_id, $season_nr, $episode_nr, 1, $user_id);
            } else {
                $update = $user_shows_episodes->update($show_id, $season_nr, $episode_nr, 0, $user_id);
            }

            if ($update['status'] === false) {
                DB::rollBack();
                return response()->json(['status' => false]);
            }
        }
        DB::commit();
        $update_user_setup = $user_shows->update_show_setup_status($show_id, $user_id, 1);

        if ($update_user_setup['status'] === false) {
            Log::debug('Did not update user show setup status');
            Log::critical('Did not update user show setup status');
        }

        return response()->json(
            [
                'status' => true,
            ]
        );
    }

    public function update_watching_episode_start_route(Request $request)
    {
        return $this->update_watching_episode_start(0, $request->show_id);
    }

    public function update_watching_episode_start($user_id = 0, $show_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $current_timestamp = Carbon::now();

        $user_currently_watching_controller = new UserCurrentlyWatchingController();

        $current_watching = $user_currently_watching_controller->get_next($user_id, $show_id);

        if ($current_watching['status'] === false) {
            return ['status' => false, 'false debug'];
        }

        $update = $user_currently_watching_controller->update_starting($user_id, $show_id, $current_watching['content']['message']['season_nr'], $current_watching['content']['message']['episode_nr'], $current_timestamp);

        if ($update['status'] === false) {

            return response()->json(
                [
                    'status'  => false,
                    'content' => [
                        'message' => [
                            $update['error'],
                        ],
                    ],
                ]
            );
        }

        if ($current_watching['content']['message']['season_nr'] === 1 && $current_watching['content']['message']['episode_nr'] === 1) {
            $user_shows = new UserShowsController();

            $update_user_setup = $user_shows->update_show_setup_status($show_id, $user_id, 1);

            if ($update_user_setup['status'] === false) {
                Log::debug('Did not update user show setup status');
                Log::critical('Did not update user show setup status');
            }
        }

        $watchlater                    = new UserWatchlaterController();
        $check_as_watched_in_scheduler = $watchlater->update_status($user_id, $show_id, $current_watching['content']['message']['season_nr'], $current_watching['content']['message']['episode_nr'], 1);
        $get_episode                   = $this->episode->get_episode($show_id, $current_watching['content']['message']['season_nr'], $current_watching['content']['message']['episode_nr']);

        return response()->json(
            [
                'status'  => true,
                'content' => [
                    'message' => [
                        'Started watching!'
                    ]
                ],
                'episode' => [
                    'name'    => ($get_episode['status'] === true ? $get_episode['content']['message']['episode']['name'] : 'Uknown Episode'),
                    'season'  => $current_watching['content']['message']['season_nr'],
                    'episode' => $current_watching['content']['message']['episode_nr'],
                ]
            ]
        );
    }

    public function update_watching_episode_end_route(Request $request)
    {
        return $this->update_watching_episode_end(0, $request->show_id);
    }

    public function update_watching_episode_end($user_id = 0, $show_id = 0)
    {
        // note
        // when user marks as watched the timestamps of when this episode started and ended watchinn
        // have to go to user shows episodes as params

        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $user_currently_watching = new UserCurrentlyWatchingController();
        $currently_watching_info = $user_currently_watching->get($user_id, $show_id);
        $now                     = Carbon::now();

        if ($currently_watching_info['status'] === false) {
            return response()->json(['status' => false]);
        }

        $episode    = $currently_watching_info['content']['message'][0]['episode'];
        $season     = $currently_watching_info['content']['message'][0]['season'];
        $started_at = $currently_watching_info['content']['message'][0]['started_at'];

        $timestamps = [
            'started_at' => $started_at,
            'ended_at'   => $now,
        ];

        $user_show_episodes       = new UserShowEpisodesController();
        $update_watchlist_episode = $user_show_episodes->update($show_id, $season, $episode, 1, $user_id, $timestamps);
//
        if ($update_watchlist_episode['status'] === false) {
            return response()->json(['status' => false, 'content' => ['message' => ['Could not update.']]]);
        }

        $update_currently_watching_update = $user_currently_watching->update_ended($user_id, $show_id, $now);

        if ($update_currently_watching_update['status'] === false) {
            return response()->json(['status' => false, 'content' => ['message' => ['Error while updating']]]);
        }

        if ($season === 1 && $episode === 1) {
            $user_shows = new UserShowsController();

            $update_user_setup = $user_shows->update_show_setup_status($show_id, $user_id, 1);

            if ($update_user_setup['status'] === false) {
                Log::debug('Did not update user show setup status');
                Log::critical('Did not update user show setup status');
            }
        }

        $watchlater                    = new UserWatchlaterController();
        $check_as_watched_in_scheduler = $watchlater->update_status($user_id, $show_id, $season, $episode, 1);

        if ($check_as_watched_in_scheduler) {

        }

        return ['status' => true, 'content' => ['message' => ['Finished watching!']]];
    }
}
