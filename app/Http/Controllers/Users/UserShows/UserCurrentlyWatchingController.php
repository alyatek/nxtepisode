<?php

namespace App\Http\Controllers\Users\UserShows;

use App\Http\Controllers\Shows\Episodes\EpisodeController;
use App\Http\Controllers\Shows\Seasons\SeasonController;
use App\Http\Controllers\Template\UtilitiesController;
use App\Http\Resources\LatestWatching;
use App\Models\Shows\Show\Show;
use App\Models\Users\UserShows\UserCurrentlyWatching;
use App\Models\Users\UserShows\UserShowEpisodes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserCurrentlyWatchingController extends Controller
{
    public function __construct()
    {
        $this->user_currently_watching = new UserCurrentlyWatching ();
    }

    /**
     * Inserts in the DB a new record of show currently watching episode
     *
     * @param int $user_id
     * @param     $show_id
     * @param     $season
     * @param     $episode
     *
     * @return array
     */
    function create($user_id = 0, $show_id, $season, $episode)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $user_currently_watching = new UserCurrentlyWatching();

        try {
            $user_currently_watching->user_id    = $user_id;
            $user_currently_watching->show_id    = $show_id;
            $user_currently_watching->season     = $season;
            $user_currently_watching->episode    = $episode;
            $user_currently_watching->started_at = '2000-01-01 00:00:00';
            $user_currently_watching->params     = '{}';

            $user_currently_watching->save();
        } catch (\Exception $e) {
            return ['status' => false, 'error' => $e];
        }
        return ['status' => true];

    }

    function get_index()
    {
        return view(
            'pages.user.watchlist-watching',
            [
                'utilities' => [
                    UtilitiesController::utility()['underscore'],
                    UtilitiesController::utility()['shave'],
                    UtilitiesController::utility()['dateformat'],
                    UtilitiesController::utility()['moment.js'],
                    UtilitiesController::utility()['sprites'],
                ],
            ]
        );
    }

    /**
     * Gets all the current information on a certain show in the watching
     *
     * @param     $user_id
     * @param int $show_id
     *
     * @return array
     */
    function get($user_id, $show_id = 0) // gets the current one i guess?
    {
        try {
            $get = UserCurrentlyWatching::where('user_id', $user_id)
                                        ->where('show_id', $show_id)
                                        ->get()->first();
        } catch (\Exception $e) {
            return ['status' => false, 'error' => $e];
        }

        return ['status' => true, 'content' => ['message' => [$get]]];
    }

    function get_watching($user_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        try {
            $get = UserCurrentlyWatching::select('episode', 'season', 'show_id', 'started_at')
                                        ->where('user_id', $user_id)
                                        ->where('ended_at', '2000-01-01 00:00:00')
                                        ->get();
        } catch (\Exception $e) {
            return ['status' => false];
        }

        return ['status' => true, 'content' => ['data' => $get]];
    }

    public function is_user_currently_watching($user_id, $show_id)
    {
        $get = $this->user_currently_watching->whereUser_id($user_id)->whereShow_id($show_id)->get()->first();

        //this might happen because a user might never not watched an episode
        //so the entry doesnt exist causing it to give null
        if ($get === null) {
            return ['status' => false , 'watching' => false];
        }

        $get['status']   = true;
        $get['watching'] = false;

        if (data_get($get, 'ended_at') === '2000-01-01 00:00:00' ||
            data_get($get, 'ended_at') === '' ||
            collect(data_get($get, 'ended_at'))->isEmpty()) {
            $get['watching'] = true;
        }

        return $get;
    }

    function get_watching_data($user_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        $watching = $this->get_watching($user_id);

        if ($watching['status'] === false) {
            return ['status' => false];
        }

        $episodes = [];

        $loop = 0;
        foreach ($watching['content']['data'] as $episode) {
            $get                          = Show::select('name')->where('id', $episode['show_id'])->get()->first();
            $episodes[$loop]              = $episode;
            $episodes[$loop]['show_name'] = ($get['name'] ? $get['name'] : 'Unkown Name');
            $loop++;
        }

        return ['status' => true, 'content' => ['data' => $episodes]];
    }

    /**
     * Figures out wich will be the next episode to watch according to the one in the db.
     * Or if none is in the db it creates
     *
     * @param     $user_id
     * @param int $show_id
     *
     * @return array
     */
    function get_next($user_id, $show_id = 0) //gets the next
    {
        // In this one goes the logic
        // If it doesnt exists a result by getting from the function get above
        // Then insert in the DB the after the latest one checked

        $get = UserCurrentlyWatching::select('episode', 'season')
                                    ->where('user_id', $user_id)
                                    ->where('show_id', $show_id)
                                    ->get()
                                    ->first();

        if ($get !== null) { //has the episode //retrieve it // get NEXT
            $episode = new EpisodeController();

            $user_episodes = new UserShowEpisodes();

            //this new method of getting the latest episode doesnt need any sums
            // to either get the next episode or next episode from the next season
            $fetch_next = collect($user_episodes->get_next($show_id, $user_id));

            $seasons_controller = new SeasonController();
            //we need to validate if the season is already aired or not to prevent the user to start watching something
            //that doesnt yet exist
            $get_season_status = collect(
                $seasons_controller
                    ->get_season_aired($show_id, $fetch_next->get('season'))
            );

            if ($get_season_status->get('status') === true && $get_season_status->get('content')['allow'] === false) {
                //if the season is not aired yet the following will be sent
                return [
                    'status'  => false,
                    'content' => [
                        'message' => [
                            'No more episodes found to watch next! Looks like you already watched everything!'
                        ]
                    ]
                ];
            }

            $try_get_next = $episode->get_episode(
                $show_id,
                $fetch_next->get('season'),
                $fetch_next->get('episode')
            );

            if ($try_get_next['status'] === false) {
                return [
                    'status'  => false,
                    'content' => [
                        'message' => [
                            'No more episodes found to watch next! Looks like you already watched everything!'
                        ]
                    ]
                ];
            }

            return ['status' => true, 'content' => $try_get_next['content']];
        }

        if ($get === null) { //doesnt have //needs to insert
            $user_show_episodes       = new UserShowEpisodesController();
            $get_last_episode_checked = $user_show_episodes->get_latest_updated($user_id, $show_id);

            if ($get_last_episode_checked['status'] === false) {
                return ['status' => false, 'error' => 2];
            }

            $last_episode_episode = $get_last_episode_checked['content']['episode'];
            $last_episode_season  = $get_last_episode_checked['content']['season'];

            $insert = $this->create($user_id, $show_id, $last_episode_season, $last_episode_episode);

            if ($insert['status'] === false) {
                return ['status' => false, 'error' => $insert];
            }
        }

        $get = UserCurrentlyWatching::select('episode', 'season')
                                    ->where('user_id', $user_id)
                                    ->where('show_id', $show_id)
                                    ->get()
                                    ->first();

        if ($get === null) {
            return [
                'status'  => false,
                'content' => ['message' => ['Failed']],
            ];
        }

        $current_user_episode_data = $get;

        // sum to get the next episode?
        $episode = new EpisodeController();
        //check if summing the episode will give return
        $try_get_next = $episode
            ->get_episode($show_id, $current_user_episode_data['season'], ($current_user_episode_data['episode'] + 1));

        //if not check if the episode 1 of the sum of the next season gets result
        if ($try_get_next['status'] === false) {
            $try_get_next = $episode->get_episode($show_id, ($current_user_episode_data['season'] + 1), 1);
        }

        if ($try_get_next['status'] === false) {
            return ['status' => true, 'content' => ['message' => ['No more episodes found to watch next! Looks like you already watched everything!']]];
        }

        return ['status' => true, 'content' => $try_get_next['content']];
    }

    function update($user_id = 0, $show_id = null, $season = null, $episode = null, $time_start = '0000-00-00 00:00:00', $time_end = '0000-00-00 00:00:00', $params = [])
    {

    }

    function update_starting($user_id = 0, $show_id, $season, $episode, $time)
    {
        try {
            $update = UserCurrentlyWatching::where('user_id', $user_id)
                                           ->where('show_id', $show_id)
                                           ->update(['started_at' => $time, 'ended_at' => '2000-01-01 00:00:00', 'season' => $season, 'episode' => $episode]);
        } catch (\Exception $e) {
            return [
                'status' => false,
                'error'  => $e,
            ];
        }

        if ($update) {
            return [
                'status' => true,
            ];
        }
    }

    function update_ended($user_id = 0, $show_id, $time)
    {
        try {
            $update = UserCurrentlyWatching::where('user_id', $user_id)
                                           ->where('show_id', $show_id)
                                           ->update(['ended_at' => $time]);
        } catch (\Exception $e) {
            return [
                'status' => false,
                'error'  => $e,
            ];
        }

        if ($update) {
            return [
                'status' => true,
            ];
        }
    }

    function update_last($user_id, $show_id, $season_nr, $last_episode)
    {

        $get_current = $this->get($user_id, $show_id);

        if ($get_current['status'] === false) {
            return ['status' => false];
        }

        $current_data = $get_current['content']['message'][0];

        $current_episode = $current_data['episode'];
        $current_season  = $current_data['season'];

        if ($season_nr > $current_season) {
            $update = UserCurrentlyWatching::where('user_id', $user_id)
                                           ->where('show_id', $show_id)
                                           ->update(['season' => $season_nr, 'episode' => $last_episode, 'ended_at' => Carbon::now(), 'started_at' => Carbon::now()]);


            if ($update) {
                return ['status' => true];
            }

            Log::debug('failed 99');
            return ['status' => false];
        }

        if ($season_nr == $current_season) {
            if ($last_episode > $current_episode) {
                $update = UserCurrentlyWatching::where('user_id', $user_id)
                                               ->where('show_id', $show_id)
                                               ->update(['season' => $season_nr, 'episode' => $last_episode, 'ended_at' => Carbon::now(), 'started_at' => Carbon::now()]);


                if ($update) {
                    return ['status' => true];
                }

                Log::debug('failed 100');
                return ['status' => false];
            }
        }
        Log::debug('failed 101');
        return ['status' => false];
    }

    function delete($show_id, $user_id)
    {
        try {
            $delete = UserCurrentlyWatching::where('user_id', $user_id)->where('show_id', $show_id)->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false];
        }
        return $delete;
    }
}
