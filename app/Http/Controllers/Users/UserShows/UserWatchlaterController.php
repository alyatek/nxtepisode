<?php

namespace App\Http\Controllers\Users\UserShows;


use App\Http\Controllers\Shows\Seasons\SeasonController;
use App\Http\Controllers\Shows\Show\ShowMediaController;
use App\Http\Controllers\Users\User\UserPremiumController;
use App\Models\Shows\Show\Show;
use App\Models\Users\UserShows\UserShows;
use App\Models\Users\UserShows\UserShowsSchedule;
use App\Models\Users\UserShows\UserShowEpisodes;
use App\Services\Notices\NoticesService;
use App\Services\TimezoneService;
use App\Services\UserShows\WatchLaterService;
use App\Services\Watchlater\UserWatchlaterShowsScheduledService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Template\UtilitiesController;
use Illuminate\Support\Facades\Log;

class UserWatchlaterController extends Controller
{

    /**
     * Sets the pointers to other functions
     */
    public function __construct()
    {
        $this->user_premium                            = new UserPremiumController();
        $this->user_watchlater_shows_scheduled_service = new UserWatchlaterShowsScheduledService();
        $this->notices                                 = new NoticesService();
        $this->service                                 = new UserWatchlaterShowsScheduledService();
        $this->shows                                   = new Show();
        $this->user_show_schedules                     = new UserShowsSchedule();
        $this->watchlater_service                      = new WatchLaterService();
        $this->user_episodes                           = new UserShowEpisodes();
        $this->user_currently_watching_c               = new UserCurrentlyWatchingController();
        $this->user_shows                              = new UserShowsController();
        $this->show_media                              = new ShowMediaController();
        $this->timezone                                = new TimezoneService();
    }

    /**
     * Creates a new entry in the db for a show to watchlater
     */
    public function create($data)
    {
        $info = collect($data);

        $userschedule                = new UserShowsSchedule();
        $userschedule->user_id       = $info->get('user_id');
        $userschedule->show_id       = $info->get('show_id');
        $userschedule->season        = $info->get('season');
        $userschedule->episode       = $info->get('episode');
        $userschedule->schedule_date = $info->get('schedule_date');
        $userschedule->schedule_time = $info->get('schedule_time');
        $userschedule->local_time    = $info->get('local_time');
        $userschedule->local_date     = $info->get('local_day');

        $userschedule->save();

        return $userschedule;
    }

    public function get()
    {

    }

    public function get_index_validate_notification($key)
    {
        $notices = new NoticesService();
        $notices->verify_email_notification($key);

        return redirect('watchlist');
    }

    public function validate_notification($key)
    {
        $notices = new NoticesService();
        $notices->verify_email_notification($key);
        return ['status' => true];
    }

    public function validate_all_notification()
    {
        $notices = new NoticesService();
        $notices->verify_all_email_notification();
        return ['status' => true];
    }

    public function get_shows()
    {
        $user_watchlist = new UserWatchlistController();
        $get_user_shows = $user_watchlist->get_watchlist(1);
        return $get_user_shows;
    }

    /**
     * Gets the limit per user for the number of shows he can schedule
     * and retrieves how many more he can plan
     */
    public function get_schedule_limit()
    {
        #TODO
    }

    public function get_show_next_episodes($show_id)
    {
        //ATTENTION
        //to get the right numbers of episodes to send
        //we need to verify if the user is premium or not
        //even tho premium is not implemented a table with no data will be there
        //if there is a registry of the user in the database it returns 15 episodes function
        //if not return the 3 episodes functions

        //validate if user has episodes on watchlist

        if(collect(UserShows::where(['user_id' => Auth::user()->id, 'show_id' => $show_id])->get())->isEmpty())
            return lazyr(false, ['This show is not on your watchlist.']);

        $user_premium = new UserPremiumController();

        $get = $user_premium->get_user();

        //clean expired shows
        $this->user_watchlater_shows_scheduled_service->clean_expired_unwatched_shows();

        //check if there any new episodes for this show and insert them
        $user_episodes = new UserShowEpisodesController();
        $user_episodes->episodes_difference($show_id, Auth::user()->id);

        if (collect($get)->get('premium') === false) {
            return $this->get_show_next_three_episodes($show_id);
        }

        //check condition for other functions
    }

    public function get_show_next_three_episodes($show_id, $user_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }


        $is_currently_watching = $this->user_currently_watching_c->is_user_currently_watching($user_id, $show_id);

        //this is needed to make them match the array of the previous stupidity i made
        //the episodes array need to have the keys in 1,2,3 order or the frontend wont work
        if ($is_currently_watching['watching'] === true) {
            //this will always return 4 max so the first result needs to be removed
            $next_episodes = $this->user_episodes->get_next_three($show_id, $user_id, true);
            //the array needs to invert because it comes with the currently watching
            //and reversing makes it go "arround"
            //this is still cleaner than the previous shit that was making this (it was fucking use foreach with 3 queries)
            $next_episodes = collect($next_episodes)->reverse()->take(3);
        } else {
            $next_episodes = $this->user_episodes->get_next_three($show_id, $user_id);
        }

        if(collect($next_episodes)->isEmpty()){
            return lazyr(false,['No episodes left to watch']);
        }
        //small fix to make the arrays keys match to 1,2,3 for front end to work without changes
        foreach ($next_episodes as $k => $content) {
            //it adds the key for 1,2,3 and puts the content of the episode in an episode key
            $new_next_episodes[$k + 1]['episode'] = $content;
        }

        $episodes_filled = $this->get_count_episodes($show_id, $user_id);
        $filled          = $episodes_filled['episodes_filled'];

        return [
            'status'  => true,
            'content' => [
                'message' => [
                    'show_id'  => $show_id,
                    'episodes' => $new_next_episodes,
                ],
            ],
            'premium' => [
                'status'  => false,
                'message' => [
                    'You have a limit of 3 dates/episodes scheduling because you currently do not have premium. <br> Find out more about premium <a href="' . url('info/premium') . '">here</a>',
                ],
            ],
            'filled'  => $filled,
        ];
    }

    public function get_count_episodes($show_id, $user_id = 0)
    {
        if ($user_id === 0) {
            $user_id = Auth::user()->id;
        }

        //get user status and retrace
        $user_premium = new UserPremiumController();
        $get          = $user_premium->get_user();

        if (collect($get)->get('premium') === false) {
            $get_episodes_in_db = UserShowsSchedule::select('schedule_date', 'schedule_time', 'season', 'episode')
                                                   ->where('show_id', $show_id)->where('user_id', $user_id)
                                                   ->where('schedule_date', null)->where('watched', 0)->get();

            if (collect($get_episodes_in_db)->count() === 0) {
                $get_episodes_in_db_check_first_time = UserShowsSchedule::select('schedule_date', 'schedule_time', 'season', 'episode')
                                                                        ->where('watched', 0)
                                                                        ->where('show_id', $show_id)
                                                                        ->where('user_id', $user_id)->get();

                if (collect($get_episodes_in_db_check_first_time)->count() === 3) {
                    return [
                        'status'          => false,
                        'message'         => 'You have 0 episodes left to change for this show.',
                        'episodes_filled' => $get_episodes_in_db_check_first_time,
                    ];
                }

            }

            $get_episodes_in_db_check_first_time = UserShowsSchedule::select('schedule_date', 'schedule_time', 'season', 'episode')
                                                                    ->where('watched', 0)
                                                                    ->where('show_id', $show_id)
                                                                    ->where('user_id', $user_id)->get();

            if (collect($get_episodes_in_db_check_first_time)->count() > 0) {
                $get_filled = UserShowsSchedule::select('schedule_date', 'schedule_time', 'season', 'episode')
                                               ->where('show_id', $show_id)->where('user_id', $user_id)
                                               ->where('watched', 0)->whereRaw('`schedule_date` != \'NULL\'')->get();
                return [
                    'status'          => true,
                    'episodes_filled' => $get_filled
                ];
            }
        }
    }

    public function get_index()
    {
        return view(
            'pages.user.watchlater.watchlater',
            [
                'utilities' => [
                    UtilitiesController::utility()['underscore'],
                    UtilitiesController::utility()['shave'],
                    UtilitiesController::utility()['dateformat'],
                    UtilitiesController::utility()['pickadate.time.css'],
                    UtilitiesController::utility()['pickadate.time.date.css'],
                    UtilitiesController::utility()['pickadate.time.time.css'],
                    UtilitiesController::utility()['pickadate'],
                    UtilitiesController::utility()['pickadate.time'],
                    UtilitiesController::utility()['pickadate.date'],
                ],
                'page'      => 'watchlater',
                'timezone'  => $this->timezone->get_user_timezone()
            ]
        );
    }

    public function get_index_settings()
    {
        return view(
            'pages.user.watchlater.watchlater-settings',
            [
                'utilities' =>
                    [
                        UtilitiesController::utility()['underscore'],
                        UtilitiesController::utility()['shave'],
                        UtilitiesController::utility()['dateformat'],
                        UtilitiesController::utility()['pickadate.time.css'],
                        UtilitiesController::utility()['pickadate.time.date.css'],
                        UtilitiesController::utility()['pickadate.time.time.css'],
                        UtilitiesController::utility()['pickadate'],
                        UtilitiesController::utility()['pickadate.time'],
                        UtilitiesController::utility()['pickadate.date'],
                        UtilitiesController::utility()['fancybox'],
                    ],
                'page'      => 'settings',
                #this is needed for the sub menu buttons highlations
                'premium'   => $this->user_premium->get_user(),
                'timezone'  => $this->timezone->get_user_timezone()
            ]
        );
    }

    public function get_index_planned()
    {

        return view(
            'pages.user.watchlater.watchlater-planned',
            [
                'utilities' => [
                    UtilitiesController::utility()['underscore'],
                    UtilitiesController::utility()['shave'],
                    UtilitiesController::utility()['dateformat'],
                    UtilitiesController::utility()['pickadate.time.css'],
                    UtilitiesController::utility()['pickadate.time.date.css'],
                    UtilitiesController::utility()['pickadate.time.time.css'],
                    UtilitiesController::utility()['pickadate'],
                    UtilitiesController::utility()['pickadate.time'],
                    UtilitiesController::utility()['pickadate.date'],
                    UtilitiesController::utility()['moment.js'],
                    UtilitiesController::utility()['sweetalert'],
                ],
                'page'      => 'planned',
                #this is needed for the sub menu buttons highlations
                'premium'   => $this->user_premium->get_user(),
                'timezone'  => $this->timezone->get_user_timezone()
            ]
        );
    }

    public function update_request(Request $request)
    {
        $validation_counter = 0;
        foreach($request->all()['date_episode'] as $k => $each){
            if($request->date_episode[$k] === null && $request->time_episode[$k] === null){
                $validation_counter++;
            }
        }

        if($validation_counter === 3){
            return lazyr(false, ['Looks like nothing was set up. Check and try again! 😀'],[],200);
        }

        $validate_show = $this->shows->get_show($request->show_id);

        if (collect($validate_show)->isEmpty()) {
            return [
                'status'  => false,
                'content' => ['message' => ['Boy are you in the future? Cuz that show doesn\'t yet exist!']]
            ];
        }

        //validate dates inputed to check if none is previous from today
        $dates         = $request->date_episode;
        $invalid_dates = [];
        foreach ($dates as $episode => $date) {
            if (collect($date)->isEmpty()) continue;
            if (Carbon::parse($date) < now()->subDay(1)) $invalid_dates[$episode] = $episode;
        }

        if (collect($invalid_dates)->isNotEmpty()) {
            return lazyr(
                false,
                [
                    'You have dates older than today! Fix that please before scheduling.'
                ],
                [
                    'invalid_dates'          => true,
                    'invalid_dates_episodes' => $invalid_dates
                ]
            );
        }

//        $get_only_dates = Arr::pluck($request->);

        #todo: needs better implementation method
//        $get_show_limit = $this->service->get_limit_plans_of_show($request->show_id);
//
//        if($get_show_limit['status'] === false){
//            return ['status' => false, 'content' => ['message' => ['Sorry man but you reached your planning limits (8 max)!']]];
//        }

        $get_validation = $this->get_count_episodes($request->show_id);

        if ($get_validation['status'] === false) {
            return [
                'status'  => false,
                'content' => ['message' => [$get_validation['message']]]
            ];
        }

        $get_episodes = $this->get_show_next_episodes($request->show_id);

        if ($get_episodes['status'] === false) {
            return false;
        }
        $dates = [];

        $episodes = $get_episodes['content']['message']['episodes'];

        foreach ($episodes as $key => $episode) {
            $dates[$episode['episode']['episode']]['user_id']       = Auth::user()->id;
            $dates[$episode['episode']['episode']]['show_id']       = $request->show_id;
            $dates[$episode['episode']['episode']]['season']        = $episode['episode']['season'];
            $dates[$episode['episode']['episode']]['schedule_date'] = collect($request->date_episode)->get($episode['episode']['episode']);
            $dates[$episode['episode']['episode']]['episode']       = $episode['episode']['episode'];

            //time for user
            $time         = '15:00';
            $time_request = collect($request->time_episode);

            if ($time_request->get($episode['episode']['episode']) !== null) {
                $time = collect($request->time_episode)->get($episode['episode']['episode']);
            }

            $dates[$episode['episode']['episode']]['schedule_time'] = $time;

            //get time for server based on user input
            $compiled_user_time = $dates[$episode['episode']['episode']]['schedule_date'] . ' ' . $time;
            if (Auth::user()->timezone === null || Auth::user()->timezone === '') {
                $user_timezone_set_status = false;
                $user_timezone            = 'Europe/Lisbon';
            } else {
                $user_timezone = Auth::user()->timezone;
            }

            //user time zone and time according to what was submited
            $utz = \Carbon\Carbon::parse($compiled_user_time, new \DateTimeZone($user_timezone));
            //get the time for our server to interpret
            $svtime                                              = \Carbon\Carbon::createFromTimestamp($utz->timestamp, 'Europe/Lisbon')
                                                                                 ->toTimeString();
            $svday                                               = \Carbon\Carbon::createFromTimestamp($utz->timestamp, 'Europe/Lisbon')
                                                                                 ->toDateString();
            $dates[$episode['episode']['episode']]['local_time'] = $svtime;
            $dates[$episode['episode']['episode']]['local_day']  = $svday;
        }

        foreach ($dates as $key => $date) {
            $get = UserShowsSchedule::where('user_id', $date['user_id'])
                                    ->where('show_id', $date['show_id'])
                                    ->where('season', $date['season'])
                                    ->where('episode', $date['episode'])
                                    ->where('watched', 0)
                                    ->get()->first();

            if (collect($get)->isEmpty()) {
                $this->create($date);
                continue;
            }

            if (collect($get)->isNotEmpty()) {
                $this->update($date);
            }

//            if (collect($get)->isNotEmpty()) {
//                if (!collect($get)->get('schedule_date')) {
//                    $this->update($date);
//                    continue;
//                }
//            }
//            if (collect($get)->get('episode') !== $date['episode']) {
//                $this->create($date);
//            }
        }

        if (collect($dates)->isEmpty()) {
            return [
                'status'  => false,
                'content' => ['message' => ['Error setting up the dates :(']]
            ];
        }

//        $update_limit_tracker = $this->user_watchlater_shows_scheduled_service->update_limit_lower($request->show_id, Auth::user()->id);

        return [
            'status'  => true,
            'content' => ['message' => ['Planned with no problems! When the time reaches you will be notified!']]
        ];
    }

    public function update($date)
    {
        $date                        = collect($date);
        $userschedule                = new UserShowsSchedule();
        $userschedule->user_id       = $date->get('user_id');
        $userschedule->show_id       = $date->get('show_id');
        $userschedule->season        = $date->get('season');
        $userschedule->episode       = $date->get('episode');
        $userschedule->schedule_date = $date->get('schedule_date');
        $userschedule->schedule_time = $date->get('schedule_time');
        $userschedule->local_time    = $date->get('local_time');
        $userschedule->local_date    = $date->get('local_day');

        $userschedule->save();

//        $update = UserShowsSchedule::where([
//            'show_id' => $date['show_id'],
//            'user_id' => $date['user_id'],
//            'season'  => $date['season'],
//            'episode' => $date['episode'],
//            'watched' => 0,
//        ])
//            ->update([
//                'schedule_date' => $date['schedule_date'],
//                'schedule_time' => $date['schedule_time'],
//            ]);

        return $userschedule;
    }

    public function update_status($user_id, $show_id, $season, $episode, $status)
    {
        $update = UserShowsSchedule::where(
            [
                'show_id' => $show_id,
                'user_id' => $user_id,
                'season'  => $season,
                'episode' => $episode,
                'watched' => 0,
            ]
        )
                                   ->update(
                                       [
                                           'watched' => $status,
                                       ]
                                   );

        return $update;
    }

    public function remove()
    {

    }

    public function handle_shows_planned_request()
    {
        return $this->user_watchlater_shows_scheduled_service->get_shows_planned();
    }

    public function handle_expired_schedules_request()
    {
        return $this->watchlater_service->remove_expired_schedules();
    }

    public function handle_schedule_remove_request(Request $request)
    {
        if (!$request->show_id || !$request->season || !$request->episode) {
            return response()->json(['status' => false, 'content' => ['message' => ['Something weird happened. Refresh the page and try again.']]]);
        }

        return $this->watchlater_service->remove_schedule($request->show_id, $request->season, $request->episode);

    }

    public function get_unread_notifications()
    {
        return $this->notices->get_unread_notifications();
    }

}
