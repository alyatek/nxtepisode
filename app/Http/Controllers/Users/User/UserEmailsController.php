<?php

namespace App\Http\Controllers\Users\User;

use App\Http\Controllers\Controller;
use App\Models\Users\User\User;

class UserEmailsController extends Controller
{
//    public function create ($email) {
//
//    }

    public function get ($email) {
        $get = User::select('id')->where('email',$email)->get();
        return $get;
    }

    public function update ($user_id,$email) {
        $update = User::where('id',$user_id)->update(['email'=>$email]);
        return $update;
    }
    
    public function delete ($user_id, $email) {

    }
}
