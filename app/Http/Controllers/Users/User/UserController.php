<?php

namespace App\Http\Controllers\Users\User;

use App\Http\Controllers\Template\UtilitiesController;
use App\Models\Users\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function create_user()
    {

    }

    public function get_users()
    {
        $get = User::get();
        return $get;
    }

    public function get_user($id)
    {
        $get = User::findOrFail($id);
        return $get;
    }

    public function update_user($id)
    {

    }

    public function disable_user($id)
    {
        try {
            $update = User::where('id', $id)->update(['status' => 0]);
        } catch (\Exception $e) {
            dd($e);
        }
        return $update;
    }

    public function enable_user($id)
    {
        try {
            $update = User::where('id', $id)->update(['status' => 1]);
        } catch (\Exception $e) {
            dd($e);
        }
        return $update;
    }

    public function get_account_settings_index()
    {
        pagetitle('Account Settings');
        $get_time_zones = json_decode(file_get_contents(storage_path('app/timezones.json')));
        return view(
            'pages.user.account-settings', [
                                             'utilities' => [
                                                 UtilitiesController::utility()['icheck'],
                                                 UtilitiesController::utility()['underscore'],
                                                 UtilitiesController::utility()['selectize'],
                                             ],
                                             'timezones' => $get_time_zones
                                         ]
        );
    }

    public function update_email_address(Request $request)
    {
        $user_emails = new UserEmailsController();

        if (!$request->email) {
            return ['status' => false, 'content' => ['data' => ['Looks like somebody didn\'t fill out the email field.']]];
        }

        $email = $request->email;

        $get_current = $user_emails->get($email);

        if (collect($get_current)->isNotEmpty()) { //this email is the current one
            return ['status' => false, 'content' => ['data' => ['You did not change your email.']]];
        }

        $user_id = Auth::user()->id;

        $update = $user_emails->update($user_id, $email);

        if ($update !== 1) {
            return ['status' => false, 'content' => ['data' => ['Could not update. Please try again later']]];
        }

        return ['status' => true, 'content' => ['data' => ['Updated with success!']]];
    }

    public function update_user_password(Request $request)
    {
        $validate = Validator::make(
            $request->all(), [
                               'current_password' => 'required',
                               'password'         => 'required|same:password_confirmation',
                           ]
        );

        if ($validate->fails()) {
            return ['status' => false, 'content' => ['data' => $validate->errors()]];
        }

        $user_id       = Auth::user()->id;
        $user_password = new UserPasswordController();

        $get_current_password = $user_password->get($user_id);
        $get_current_password = collect($get_current_password);

        if ($get_current_password->isEmpty()) {
            return ['status' => false, 'content' => ['data' => ['Unkown error.']]];
        }

        //validate current pws
        if (!Hash::check($request->current_password, $get_current_password->get('password'))) {
            return ['status' => false, 'content' => ['data' => ['Uhmm, that is not your current password.']]];
        }

        $new_password = Hash::make($request->password);

        $update = $user_password->update($new_password, $user_id);

        if ($update === 0) {
            return ['status' => false, 'content' => ['data' => ['Could not update password.']]];
        }

        return ['status' => true, 'content' => ['data' => ['Updated with success!']]];
    }

    public function update_timezone(Request $request)
    {
        if (!$request->timezone) {
            session()->flash('account_settings_messages', "That can't go empty. <i>sigh... </i> the timezone...");
            return redirect('account-settings');
        }

        $get_time_zones = json_decode(file_get_contents(storage_path('app/timezones.json')), true);

        if (!isset($get_time_zones[$request->timezone])) {
            session()->flash('account_settings_messages', "Unkown error happened. Try again.");
            return redirect('account-settings');
        }

        if(Auth::user()->timezone === $request->timezone){
            session()->flash('account_settings_messages', "u changed nuthin...");
            return redirect('account-settings');
        }

        //update
        $user           = \App\User::find(Auth::user()->id);
        $user->timezone = $request->timezone;
        $user           = $user->save();

        if ($user > 0) {
            session()->flash('account_settings_messages', "Timezone set with success");
            return redirect('account-settings');
        }

        session()->flash('account_settings_messages', "Unkown error happened. Try again.");
        return redirect('account-settings');

    }
}
