<?php

namespace App\Http\Controllers\Users\User;

use App\Models\Users\User\UserPremium;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserPremiumController extends Controller
{
    public function get_user ($user_id = 0) {
        if($user_id === 0){
            $user_id = Auth::user()->id;
        }

        $get = UserPremium::where('user_id', $user_id)->get()->first();

        if(collect($get)->isEmpty()){
            return ['status' => true, 'premium' => false];
        }

        return ['status' => true, 'premium' => true, 'level' => collect($get)->get('premium_level')];
    }
}
