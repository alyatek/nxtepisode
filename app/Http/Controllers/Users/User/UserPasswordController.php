<?php

namespace App\Http\Controllers\Users\User;

use App\Models\Users\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserPasswordController extends Controller
{
    public function get ($user_id,$status = 1) {
        $get = User::select('password')->where('id' , $user_id)->where('status',$status)->get()->first();
        return $get;
    }

    public function update ($password, $user_id) {
        $update = User::where('id',$user_id)->update(['password'=>$password]);
        return $update;
    }

    public function remove () {

    }
}
