<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Template\UtilitiesController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class SearchController extends Controller
{
    public function get_search_index ($showQuery) {
        pagetitle('Searching for shows');
        return view('pages.search.search', [
            'utilities'=>[
                UtilitiesController::utility()['shave'],
                UtilitiesController::utility()['underscore'],
                UtilitiesController::utility()['dateformat'],
            ],
            'search' => Str::title(Str::slug($showQuery, ' ')),
        ]);
    }

    public function get_search ($showQuery) {
        $api = new ApiController;

        $search = $api->search($showQuery);
        return $search;
    }
}
