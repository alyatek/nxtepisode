<?php

namespace App\Http\Controllers\Template;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UtilitiesController extends Controller
{
    public static function utility ()
    {
        return [
            'elementui'               => [
                'css' => url('/assets/elementui/dist/css/jquery.message.css'),
                'js'  => url('/assets/elementui/dist/js/jquery.message.js'),
            ],
            'hideShowPassword'        => [
                'js' => url('/assets/hideShowPassword/hideShowPassword.js'),
            ],
            'animatecss'              => [
                'css' => url('/assets/animate.css/animate.min.css'),
            ],
            'icheck'                  => [
                'js'    => url('/assets/icheck/icheck.min.js'),
                'css'   => url('/assets/icheck/skins/flat/flat.css'),
                'ready' => "$('input').iCheck({checkboxClass: 'icheckbox_flat',radioClass: 'iradio_flat'});",
            ],
            'jquery-confirm'          => [
                'css' => url('/assets/jquery-confirm/dist/jquery-confirm.min.css'),
                'js'  => url('/assets/jquery-confirm/dist/jquery-confirm.min.js'),
            ],
            'pnotify'                 => [
                'css' => url('/assets/pnotify/pnotify.custom.min.css'),
                'js'  => url('/assets/pnotify/pnotify.custom.min.js'),
            ],
            'smoke'                   => [
                'css' => url('/assets/smoke/css/smoke.min.css'),
                'js'  => url('/assets/smoke/js/smoke.min.js'),
            ],
            'shave'                   => [
                'js' => url('/assets/shave/dist/jquery.shave.min.js'),
            ],
            'underscore'              => [
                'js' => url('/assets/underscore.js/underscore-min.js'),
            ],

            'underscore.string'               => [
                'js' => url('/assets/underscore.js/underscore.string.min.js'),
            ],
            'dateformat'              => [
                'js' => url('/assets/dateFormat/dist/jquery-dateformat.min.js'),
            ],
            'sprites'                 => [
                'sprite' => 'sprite',
            ],
            'moment.js'               => [
                'js' => url('/assets/moment.js/moment.js'),
            ],
            'sweetalert'              => [
                'js' => 'https://cdn.jsdelivr.net/npm/sweetalert2@7.31.1/dist/sweetalert2.all.min.js',
            ],
            'store.js'                => [
                'js' => url('assets/store.js/dist/store.legacy.min.js'),
            ],
            'tinysort'                => [
                'js' => 'https://cdnjs.cloudflare.com/ajax/libs/tinysort/3.2.5/jquery.tinysort.js',
                'js' => 'https://cdnjs.cloudflare.com/ajax/libs/tinysort/3.2.5/jquery.tinysort.min.js',
                'js' => 'https://cdnjs.cloudflare.com/ajax/libs/tinysort/3.2.5/tinysort.charorder.js',
                'js' => 'https://cdnjs.cloudflare.com/ajax/libs/tinysort/3.2.5/tinysort.charorder.min.js',
                'js' => 'https://cdnjs.cloudflare.com/ajax/libs/tinysort/3.2.5/tinysort.js',
                'js' => 'https://cdnjs.cloudflare.com/ajax/libs/tinysort/3.2.5/tinysort.min.js',
            ],
            'pickadate'               => [
                'js' => url('assets/pickadate/lib/compressed/picker.js'),

            ],
            'pickadate.time.css'      => [
                'css' => url('assets/pickadate/lib/compressed/themes/classic.css'),
            ],
            'pickadate.time.date.css' => [
                'css' => url('assets/pickadate/lib/compressed/themes/classic.date.css'),
            ],
            'pickadate.time.time.css' => [
                'css' => url('assets/pickadate/lib/compressed/themes/classic.time.css'),
            ],
            'pickadate.time'          => [
                'js' => url('assets/pickadate/lib/compressed/picker.time.js'),
            ],
            'pickadate.date'          => [
                'js' => url('assets/pickadate/lib/compressed/picker.date.js'),
            ],
            'fancybox' => [
                'css' => url('assets/fancybox/jquery.fancybox.min.css'),
                'js' => url('assets/fancybox/jquery.fancybox.min.js'),
            ],
            'nice-select' =>[
                'css' => url('assets/jquery-nice-select/css/nice-select.css'),
                'js' => url('assets/jquery-nice-select/js/jquery.nice-select.min.js'),
            ],
            'tether' =>[
                'css' => url('assets/select-new/dist/css/select-theme-default.css'),
                'js' => url('assets/tether/dist/js/tether.min.js'),
            ],
            'select-new' =>[
                'css' => url('assets/select-new/dist/css/select-theme-default.css'),
                'js' => url('assets/select-new/dist/js/select.min.js'),
            ],
            'selectize' =>[
                'css' => url('assets/selectize.js/dist/css/selectize.css'),
                'css' => url('assets/selectize.js/dist/css/selectize.bootstrap3.css'),
                'js' => url('assets/selectize.js/dist/js/standalone/selectize.min.js'),
//                'js' => url('assets/selectize.js/dist/js/selectize.min.js'),
            ],
        ];
    }
}
