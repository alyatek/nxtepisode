<?php

namespace App\Http\Controllers;

use App\Services\Api\FacebookService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class FacebookTest extends Controller
{

    public function get_contents()
    {
        Storage::append('file.txt', file_get_contents('php://input'));
        $fservice = new FacebookService();
        $fservice->handle_facebook_message_webhook(file_get_contents('php://input'));
        return response('true',200);
    }
}
