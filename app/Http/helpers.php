<?php

if (!function_exists('lazyr')) {
    /**
     * This is a helper to make responding as json as fast as possible with the basic params
     *
     * @param bool  $status
     * @param array $message
     * @param array $params
     * @param int   $response_status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    function lazyr($status = true, $message = [], $params = [], $response_status = 200)
    {
        return response()->json([
            'status' => $status,
            'content' => [
                'message' => $message,
                'params' => $params,
            ]
        ], $response_status);
    }

    /**
     * This helper takes a current cache value and increments the array of it
     */
    function increment_cache ($key_name, $new_value_key = '', $new_value_value = '', $time) {
        $current_content = \Illuminate\Support\Facades\Cache::get($key_name);

        $current_content[$new_value_key] = $new_value_value;

        \Illuminate\Support\Facades\Cache::put($key_name, $current_content,$time);
    }
}
