<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestions extends Model
{
    public $table = 'users_suggestions';
    protected $fillable = [
        'user_id','suggestions'
    ];

    public function create ($suggestion, $user_id) {
        $this->user_id = $user_id;
        $this->suggestion = $suggestion;

        $this->save();

        return $this;
    }
}
