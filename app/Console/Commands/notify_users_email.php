<?php

namespace App\Console\Commands;

use App\Services\Notices\NoticesService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class notify_users_email extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify_users:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command fires the email notifications for the configured users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Started email noticies @ '.Carbon::now());
        $notices = new NoticesService();
        $notices->mail_notice();
        Log::info('Finished email noticies @ '.Carbon::now());
        return;
    }
}
