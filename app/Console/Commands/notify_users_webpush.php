<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Notices\NoticesService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class notify_users_webpush extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify_users:webpush';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifies all users with webpushed configurations.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Started webpush noticies @ '.Carbon::now());
        $notices = new NoticesService();
        $notices->web_push_notice();
        Log::info('Finished webpush noticies @ '.Carbon::now());
        return;
    }
}
