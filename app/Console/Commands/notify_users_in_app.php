<?php

namespace App\Console\Commands;

use App\Services\Notices\NoticesService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class notify_users_in_app extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify_users:in_app';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifies all users if they have a show to watch in the current hour.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Started in app noticies @ '.Carbon::now());
        $notices = new NoticesService();
        $notices->in_app_notice();
        Log::info('Finished in app noticies @ '.Carbon::now());
        return;
    }
}
