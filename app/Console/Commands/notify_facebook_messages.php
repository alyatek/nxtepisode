<?php

namespace App\Console\Commands;

use App\Services\Notices\NoticesService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class notify_facebook_messages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:facebook_message';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alerts users with schedule for the current hour with a facebook message';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Started facebook noticies @ '.Carbon::now());
        $notices = new NoticesService();
        $notices->facebook_message();
        Log::info('Finished facebook noticies @ '.Carbon::now());
        return;
    }
}
