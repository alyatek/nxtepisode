<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\notify_users_email',
        'App\Console\Commands\notify_users_in_app',
        'App\Console\Commands\notify_users_webpush',
        'App\Console\Commands\notify_facebook_messages',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('notify_users:email')
                  ->everyThirtyMinutes()->runInBackground();
         $schedule->command('notify_users:in_app')
                  ->everyThirtyMinutes()->runInBackground();
         $schedule->command('notify_users:webpush')
                  ->everyThirtyMinutes()->runInBackground();
         $schedule->command('notify:facebook_message')
                  ->everyThirtyMinutes()->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
