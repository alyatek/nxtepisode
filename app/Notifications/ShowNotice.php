<?php

namespace App\Notifications;

use App\Events\NotifyUsers;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Log;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;

class ShowNotice extends Notification implements ShouldQueue
{
    use Queueable;

    public $information;
    public $userid;
    public $methods;
    public $webpush;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($userid, $information, $methods, $webpush = false)
    {
        $this->information = $information;
        $this->userid      = $userid;
        $this->methods     = $methods;
        $this->webpush     = $webpush;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($this->webpush === true){
            $this->methods;
            array_push($this->methods, OneSignalChannel::class);
        }

        return $this->methods;
//        return [OneSignalChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('It\'s time to watch ' . $this->information['show_name'])
            ->markdown('mail.show-notification', [
                'show_name'           => $this->information['show_name'],
                'show_episode_name'   => $this->information['show_episode_name'],
                'show_episode_number' => $this->information['show_episode_number'],
                'show_season'         => $this->information['show_season'],
                'show_slug'           => $this->information['show_slug'],
                'user_name'           => $this->information['user_name'],
                'time'                => $this->information['time'],
                'unique_id'           => $this->information['unique_id'],
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'show_name'           => $this->information['show_name'],
            'show_episode_name'   => $this->information['show_episode_name'],
            'show_episode_number' => $this->information['show_episode_number'],
            'show_season'         => $this->information['show_season'],
            'show_slug'           => $this->information['show_slug'],
            'user_name'           => $this->information['user_name'],
            'time'                => $this->information['time'],
            'unique_id'           => $this->information['unique_id'],
        ];
    }

    public function toBroadcast($notifiable)
    {
        return event(new NotifyUsers($this->userid, [
            'show_name'           => $this->information['show_name'],
            'show_episode_name'   => $this->information['show_episode_name'],
            'show_episode_number' => $this->information['show_episode_number'],
            'show_season'         => $this->information['show_season'],
            'show_slug'           => $this->information['show_slug'],
            'user_name'           => $this->information['user_name'],
            'time'                => $this->information['time'],
            'unique_id'           => $this->information['unique_id'],
        ]));
    }

    public function toOneSignal($notifiable)
    {
        try {
            return OneSignalMessage::create()
                ->subject("Hey ".$this->information['user_name']."! It's time to watch ".$this->information['show_name']." !")
                ->body("You planned to watch ".$this->information['show_episode_name']." from Season ".$this->information['show_season']." Episode ".$this->information['show_episode_number']." now at ".$this->information['time'].".")
                ->url(url('/watchlist'))
                ->webButton(
                    OneSignalWebButton::create('start-watching')
                        ->text('Start Watching')
                        ->url(url('/watchlist/start-watching'))
                );
        } catch (\Exception $e) {
            Log::info(json_encode([$e]));
        }
    }

}
