<?php


namespace App\Services\VoyagerContent;


use App\Http\Controllers\Helpers\VoyagerContentHelper;
use App\Models\SectionContent;
use App\Models\Shows\Show\Show;
use Illuminate\Support\Facades\Cache;

class VoyagerHomeContentService extends VoyagerContentHelper
{
    public function __construct() {
        $this->section_content = new SectionContent();
    }

    public function get_user_recommended_type () {
        $data = $this->get_user_recommended_data();
        if(collect($data)->isEmpty()) return null;
        if($data->type === 1) return 'Generated';
        return 'Custom';
    }

    public function get_popular_this_week_type () {
        $data = $this->get_popular_this_week_data();
        if(collect($data)->isEmpty()) return null;
        if($data->type === 1) return 'Generated';
        return 'Custom';
    }

    public function get_small_recommended_type () {
        $data = $this->get_small_recommended_data();
        if(collect($data)->isEmpty()) return null;
        if($data->type === 1) return 'Generated';
        return 'Custom';
    }

    /**
     * Updates or Creates the recommended info
     *
     * ! INFO : 1 for generated
     *          2 for custom input
     *
     * @param $data
     */
    public function save_user_recommended($data)
    {
        if($validate = $this->validate_type($data))return $validate;

        $type = $data->type;

        $to_save = [
            'type' => 1
        ];

        if ($type == "2") {
            $to_save['type'] = 2;
            if (intval($data->show_id) > 0) $to_save['show_id'] = intval($data->show_id);
            if (!(intval($data->show_id) > 0)) return redirect()->back()
                                                                ->with(
                                                                    [
                                                                        'alert' => [
                                                                            'type' => 'warning',
                                                                            'msg'  => ['ERROR SHOW ID - Only accepts show id and not name']
                                                                        ]
                                                                    ]
                                                                );
            $to_save['image'] = $this->store_file($data->file('image'));
            $to_save['message'] = $data->message;
        }

        $to_save_stringed = json_encode($to_save);

        $section_content = \App\Models\SectionContent::updateOrCreate(
            [
                'page'    => 'home',
                'section' => 'user_recommended',
            ],
            [
                'data' => $to_save_stringed,
            ]
        );


        if (collect($section_content)->isNotEmpty()) {
            Cache::forget('user_recommended_static');
            return redirect()->back()->with(
                [
                    'alert' => [
                        'type' => 'success',
                        'msg'  => ['Updated User Recommended']
                    ]
                ]
            );
        }

        return redirect()->back()->with(
            [
                'alert' => [
                    'type' => 'warning',
                    'msg'  => ['Failed updating user recommended']
                ]
            ]
        );
    }

    /**
     * Updates or Create popular this week info
     *
     * @param $data
     */
    public function save_popular_this_week ($data) {
        if($validate = $this->validate_type($data))return $validate;

        $to_save = ['type' => 1];

        if($data->type == "2"){
            if($validate = $this->validate_popular_this_week_data($data))return $validate;
            $to_save['type'] = 2;
            $to_save['subtitle_message'] = $data->subtitle_message;
            $to_save['show_id_1'] = $data->show_id_1;
            $to_save['show_message_1'] = $data->show_message_1;
            $to_save['show_id_2'] = $data->show_id_2;
            $to_save['show_message_2'] = $data->show_message_2;
            $to_save['show_id_3'] = $data->show_id_3;
            $to_save['show_message_3'] = $data->show_message_3;
        }

        $to_save_stringed = json_encode($to_save);

        $section_content = \App\Models\SectionContent::updateOrCreate(
            [
                'page'    => 'home',
                'section' => 'popular_this_week',
            ],
            [
                'data' => $to_save_stringed,
            ]
        );

        if (collect($section_content)->isNotEmpty()) {
            return redirect()->back()->with(
                [
                    'alert' => [
                        'type' => 'success',
                        'msg'  => ['Updated Popular This Week']
                    ]
                ]
            );
        }

        return redirect()->back()->with(
            [
                'alert' => [
                    'type' => 'warning',
                    'msg'  => ['Failed updating popular this week']
                ]
            ]
        );
    }

    public function save_small_recommended ($data) {
        if($validate = $this->validate_type($data))return $validate;

        $type = $data->type;

        $to_save = [
            'type' => 1
        ];

        if ($type == "2") {
            $to_save['type'] = 2;
            $to_save['subtitle_message'] = $data->subtitle_message;
            if (intval($data->show_id) > 0) $to_save['show_id'] = intval($data->show_id);
            if (!(intval($data->show_id) > 0)) return redirect()->back()
                                                                ->with(
                                                                    [
                                                                        'alert' => [
                                                                            'type' => 'warning',
                                                                            'msg'  => ['ERROR SHOW ID - Only accepts show id and not name']
                                                                        ]
                                                                    ]
                                                                );
        }

        $to_save_stringed = json_encode($to_save);

        $section_content = \App\Models\SectionContent::updateOrCreate(
            [
                'page'    => 'home',
                'section' => 'small_recommended',
            ],
            [
                'data' => $to_save_stringed,
            ]
        );


        if (collect($section_content)->isNotEmpty()) {
            return redirect()->back()->with(
                [
                    'alert' => [
                        'type' => 'success',
                        'msg'  => ['Updated Small Recommended']
                    ]
                ]
            );
        }

        return redirect()->back()->with(
            [
                'alert' => [
                    'type' => 'warning',
                    'msg'  => ['Failed updating small recommended']
                ]
            ]
        );
    }
}
