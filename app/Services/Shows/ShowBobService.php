<?php


namespace App\Services\Shows;


use App\Models\Shows\Show\Show;

class ShowBobService
{
    public $show     = null;
    public $images   = null;
    public $seasons  = null;
    public $episodes = null;

    public function __construct($show) {
        $this->show($show);
    }

    /**
     * determines if either show id or a show instance was sent
     */
    public function show ($show) {
        if(is_int($show)) $show = Show::get(['id' => $show]);

        return $show;
    }
}
