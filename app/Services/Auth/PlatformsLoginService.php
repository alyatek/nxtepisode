<?php
namespace App\Services\Auth;

use App\Models\Users\User\Facebook;
use App\Models\Users\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PlatformsLoginService {

    public function __construct()
    {
        $this->user = new User();
        $this->facebook = new Facebook();
    }

    public function login_user_with_facebook ($user_name, $email, $facebook_id, $nickname, $facebook_name, $facebook_avatar) {

        //the creation needs the following
        // $username, $password, $email

        //check if user with same email
        $check = $this->user->get_by_email($email);

        if(collect($check)->isNotEmpty()){
            if (Auth::loginUsingId(collect($check)->get('id'))) {
                session()->flash('status_login' , 'Logged in with no problem! Nice job <i>champ</i>!');
                return redirect('/');
            }
            session()->flash('status_login' , 'The email that belongs to this Facebook account is already registered in the platform. You can recover your email down bellow!');
            return redirect('login');
        }

        //set a password
        $password = Hash::make($email.$facebook_id.rand());

        //create user
        $create = $this->user->create($user_name, $password, $email);

        //validate creation
        if(collect($create)->isEmpty()){
            session()->flash('status_login' , 'Error login in with Facebook. Please try again later.');
            return redirect('login');
        }

        $user_id = collect($create)->get('id');

        $create_facebook_data = $this->facebook->create($user_id,$facebook_id, $facebook_name, $facebook_avatar);

        if(collect($create_facebook_data)->isEmpty()){
            session()->flash('status_login' , 'Error login in with Facebook. Please try again later.');
            return redirect('login');
        }

        //login and return message
        if (Auth::loginUsingId($create->id, true)) {
            session()->flash('status_login' , 'Logged in with no problem! Nice job <i>champ</i>!');
            return redirect('/');
        }

        session()->flash('status_login' , 'Error login in with Facebook. Please try again later.');
        return redirect('login');

    }

}
