<?php


namespace App\Services\AppContent;


use App\Http\Controllers\Helpers\VoyagerContentHelper;
use App\Models\Shows\Show\Show;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AppContentService extends VoyagerContentHelper
{
    public function get_user_recommended () {
        if($content = Cache::get('user_recommended_static')) return $content;

        $data = $this->get_user_recommended_data();

        if(!$data) return null;

        if($data->type === 1){
            /**
             * @todo this will go to the func that determines the user recommended
             */
        }

        $show_id = $data->show_id;

        $get_show_info =  Show::where(['id' => $show_id])->get(['api_id', 'name', 'summary', 'slug', 'id']);
        $get_show_info[0]['media'] = Show::find($show_id)->media;
        $get_show_info[0]['image'] = $data->image;
        $get_show_info[0]['message'] = $data->message;
        if($get_show_info)
            Cache::put('user_recommended_static', $get_show_info, now()->addDays(5));
            return $get_show_info;

        return null;
    }

    public function get_popular_this_week () {
        if($content = Cache::get('popular_this_week')) return $content;

        $data = $this->get_popular_this_week_data();
        if(!$data) return null;
        if($data->type === 1){/**@todo this will go to the func that determines the user recommended**/}

        $to_display['subtitle_message'] = $data->subtitle_message;

        for($i = 1 ; $i <= 3; $i++){
            $data = collect($data)->toArray();
            $to_display['show_'. $i]['data'] = Show::where(['id' => $data['show_id_'.$i]])->get(['api_id', 'name', 'summary', 'slug', 'id']);
            $to_display['show_'. $i]['image'] = Show::find(intval($data['show_id_'.$i]))->media[0];
            $to_display['show_'. $i]['message'] = $data['show_message_'.$i];
        }

        if($to_display)
            Cache::put('popular_this_week', $to_display, now()->addDays(5));
        return $to_display;
    }

    public function get_small_recommended () {
//        if($content = Cache::get('small_recommended')) return $content;

        $data = $this->get_small_recommended_data();
        if(!$data) return null;
        if($data->type === 1){/**@todo this will go to the func that determines the user recommended**/}

        $show_id = $data->show_id;

        $get_show_info['show'] =  Show::where(['id' => $show_id])->get(['api_id', 'name', 'summary', 'slug', 'id'])->first();
        $get_show_info['media'] = Show::find($show_id)->media;
        $get_show_info['image'] = Show::find($show_id)->media[0];
        $get_show_info['message'] = $data->subtitle_message;

        if($get_show_info)
//            Cache::put('small_recommended', $get_show_info, now()->addDays(5));

        return $get_show_info;

    }
}
