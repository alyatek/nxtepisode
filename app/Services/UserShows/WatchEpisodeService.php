<?php

namespace App\Services\UserShows;

use App\Http\Controllers\Shows\Episodes\EpisodeController;
use App\Http\Controllers\Users\UserShows\UserShowsController;
use App\Models\Shows\Show\Show;
use App\Models\Users\UserShows\UserCurrentlyWatching;
use App\Models\Users\UserShows\UserShowEpisodes;
use Carbon\Carbon;

/*
 * The purpose to this service is to single update an episode in the User Show Page
 */

class WatchEpisodeService
{
    public function __construct()
    {
        $this->user_episodes      = new UserShowEpisodes();
        $this->currently_watching = new UserCurrentlyWatching();
        $this->user_shows         = new UserShowsController();
        $this->episodes           = new EpisodeController();
        $this->show               = new Show();
    }

    public function watch_episode($user_id, $show_id, $episode, $season)
    {
        #validte if not currently watching
        $currently = $this->is_user_currently_watching($user_id, $show_id);

        if ($currently === true) {
            return ['status' => false, 'content' => ['message' => ['Looks like you are still watching an episode! Finish it before watching another one.']]];
        }

        #validate if episode exists
        $validate = collect($this->user_episodes->get_episode($user_id, $show_id, $episode, $season));
        if ($validate->isEmpty()) {
            return ['status' => false, 'content' => ['message' => ['Uhmmmm this episode doesn\'t exist. Refresh the page and try again']]];
        }

        #update
        //declare time variables
        $time_start = Carbon::now();
        $time_end   = Carbon::now()->addHour(); #adiciona 1h para simular ser visto - uma questao de estetica
        $params     = collect(['started_at' => collect($time_start)->get('date'), 'ended_at' => $time_end])->toJson();
        $update     = collect($this->user_episodes->update_watched($user_id, $show_id, $episode, $season, $params));

        if ($update->isEmpty()) {
            return ['status' => false, 'content' => ['message' => ['We are sorry but we couldn\'t perform this action']]];
        }

        #in case that it the first time - it needs to update the show status
        $show_status = $this->user_shows->get_show_setup_status($show_id, $user_id);

        if ($show_status['status'] === true) {
            if ($show_status['content']['data']['setup'] === 0) {
                $this->user_shows->update_show_setup_status($show_id, $user_id, 1);
            }
        }

        #inform user
        return [
            'status' => true, 'content' => [
                'message' => [
                    'Episode watched.'
                ],
                'data'    => [
                    'params' => json_decode($params),
                    'show'   => ['episode' => $episode, 'season' => $season]
                ]
            ]
        ];
    }

    public function un_watch_episode($user_id, $show_id, $episode, $season)
    {
        #validte if not currently watching
        $currently = $this->is_user_currently_watching($user_id, $show_id);

        if ($currently === true) {
            return ['status' => false, 'content' => ['message' => ['Looks like you are still watching an episode! Finish it before performing this action.']]];
        }

        #validate if episode exists
        $validate = collect($this->user_episodes->get_episode($user_id, $show_id, $episode, $season));
        if ($validate->isEmpty()) {
            return ['status' => false, 'content' => ['message' => ['Uhmmmm this episode doesn\'t exist. Refresh the page and try again']]];
        }

        $update = collect($this->user_episodes->update_un_watched($user_id, $show_id, $episode, $season, []));

        if ($update->isEmpty()) {
            return ['status' => false, 'content' => ['message' => ['We are sorry but we couldn\'t perform this action']]];
        }

        #inform user
        return [
            'status' => true, 'content' => [
                'message' => [
                    'Reverse Watch - wow.'
                ],
                'data'    => [
                    'show' => ['episode' => $episode, 'season' => $season]
                ]
            ]
        ];
    }

    public function is_user_currently_watching($user_id, $show_id)
    {
        $get = collect($this->currently_watching->get_current($user_id, $show_id));

        if ($get->isEmpty()) {
            return false;
        }

        return true;
    }

    public function is_episode_debut_valid($episode, $season, $show_id)
    {
        $get_episode = $this->episodes->get_episode($show_id, $season, $episode);
        $show        = $this->show->get_show_network($show_id);

        if ($get_episode['status'] === false || collect($show)->isEmpty()) {
            return ['status' => false];
        }

        if(collect($show)->first()['network'] == "null"){
            $show_timezone = 'America/New_York';
        } else {
            $show_timezone = json_decode(collect($show)->first()['network'])->country->timezone;
        }

        $timestamp =
            data_get($get_episode, 'content.message.episode.airdate')
            . ' ' .
            (data_get($get_episode, 'content.message.episode.airtime') ? data_get($get_episode, 'content.message.episode.airtime') : '20:00');

        $fromTimestamp = Carbon::createFromFormat('Y-m-d H:i', $timestamp, $show_timezone)->toDateTimeString();

        $now = Carbon::now('Europe/London')->toDateTimeString();

        if ($now > $fromTimestamp) {
            return ['status' => true, 'allow' => true];
        }
        return ['status' => true, 'allow' => false, 'air_date' => data_get($get_episode, 'content.message.episode.airdate')];

    }
}