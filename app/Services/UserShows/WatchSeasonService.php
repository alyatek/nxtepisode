<?php

namespace App\Services\UserShows;

use App\Models\Users\UserShows\UserShowEpisodes;
use App\Models\Users\UserShows\UserShowsSeasons;
use Carbon\Carbon;

class WatchSeasonService
{
    public function __construct ()
    {
        $this->user_shows_episodes              = new UserShowEpisodes();
        $this->user_shows_seasons               = new UserShowsSeasons();
        $this->watch_episode_service            = new WatchEpisodeService();
    }

    public function watch_season ($user_id, $show_id, $season)
    {
        $currently_watching = $this->watch_episode_service->is_user_currently_watching($user_id, $show_id);

        if ($currently_watching === true) {
            return ['status' => false, 'content' => ['message' => ['Looks like you are still watching an episode! Finish it before performing this action.']]];
        }

        $time_start = Carbon::now();
        $time_end   = Carbon::now()->addHour(); #adiciona 1h para simular ser visto - uma questao de estetica
        $params     = collect(['started_at' => collect($time_start)->get('date'), 'ended_at' => $time_end])->toJson();
        $update     = $this->user_shows_episodes->update_episodes_per_season($user_id, $show_id, $season, $params);

        if (collect($update)->isEmpty()) {
            return ['status' => false];
        }

        return ['status' => true, 'content' => ['message' => ['Season set as watched.'], 'season' => $season]];
    }

    public function un_watch_season ($user_id, $show_id, $season)
    {
        $currently_watching = $this->watch_episode_service->is_user_currently_watching($user_id, $show_id);

        if ($currently_watching === true) {
            return ['status' => false, 'content' => ['message' => ['Looks like you are still watching an episode! Finish it before performing this action.']]];
        }

        $update = $this->user_shows_episodes->update_episodes_per_season($user_id, $show_id, $season, []);

        if (collect($update)->isEmpty()) {
            return ['status' => false];
        }

        return ['status' => true, 'content' => ['season' => $season]];
    }
}