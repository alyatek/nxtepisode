<?php
namespace App\Services\UserShows;

use App\Models\Users\UserShows\UserShowsSchedule;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class WatchLaterService {
    public function __construct ()
    {
        $this->user_schedules = new UserShowsSchedule();
    }

    public function remove_expired_schedules ($user_id = 0) {
        $user_id = ($user_id !== 0 ? $user_id : Auth::user()->id);

        $current_date = Carbon::now()->format('Y-m-d');
        $current_hour = Carbon::now()->format('H:m:s');

        #update status of the thing to 0
        $update = $this->user_schedules->update_status_expired($user_id,$current_date,$current_hour,0);

        if($update === 0){
            return response()->json(['status' => false , 'content' => ['message' => ['Nothing to clear.']]]);
        }

        return response()->json(['status' => true , 'content' => ['message' => ['Task complete with no problem chief!']]]);
    }

    public function remove_schedule ($show_id, $season, $episode, $user_id = 0) {
        if($user_id === 0){
            $user_id = Auth::user()->id;
        }

        #check if schedule exists
        $get = $this->user_schedules->get_schedule($show_id, $season, $episode, $user_id);

        if(collect($get)->isEmpty()){
            return response()->json(['status' => false, 'content' => ['message' => ['No! We couldn\'t do that. Sorry, we don\'t know why.']]]);
        }

        #delete schedule
        $remove = $this->user_schedules->remove_single($show_id, $season, $episode, $user_id);

        if(collect($remove)->isEmpty()) {
            return response()->json(['status' => false, 'content' => ['message' => ['We had some trouble, uh, finishing... Please try again later.']]]);
        }

        #say something
        return response()->json(['status' => true, 'content' => ['message' => ['Gotcha man! Deleted with no problems.']]]);
    }
}