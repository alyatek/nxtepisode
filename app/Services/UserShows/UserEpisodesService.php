<?php

namespace App\Services\UserShows;

use App\Http\Controllers\Shows\Episodes\EpisodeController;
use App\Models\Users\UserShows\UserShowEpisodes;
use Illuminate\Support\Arr;

class UserEpisodesService
{
    public function __construct()
    {
        $this->episodes     = new EpisodeController();
        $this->user_episode = new UserShowEpisodes();
    }

    public function get_all_missing_episodes($show_id, $season, $episode, $user_id = 1)
    {
        //fetches all the episodes of a show
        $episodes = $this->episodes->get_episodes(null, null, null, $show_id)['message']['content']['episodes'];

        //find episodes after the ones sent

        [$seasons_numbers, $episodes_values] = Arr::divide($episodes);
        $to_insert = [];

        //da comeca a correr a partir da season enviada
        for ($i = $season; $i <= Arr::last($seasons_numbers); $i++) {

            $to_insert[$i] = [];
            //divide em keys e valores dessas keys
            [$current_season_episodes_number, $current_season_episodes_data] = Arr::divide($episodes_values[$i - 1]);

            //faz um loop no qual se a season atual for a enviada mete o nr do episodio que foi enviado
            //ie: get_all_missing_episodes (4,2,7)
            //se tiver numa season mais a frente : vai buscar o primeiro episodio dessa season
            for (
                $e = ($i === $season ? ($episode + 1) : Arr::first($current_season_episodes_number));
                $e <= Arr::last($current_season_episodes_number); $e++
            ) {
                $to_insert[$i][$e] = $e;
            }
        }
        return $to_insert;
    }

    /**
     * The episodes must come in the following order
     *
     * [
     *      season => [
     *          episode=>episode
     *      ]
     * ]
     *
     * @param $episodes
     *
     */
    public function insert_custom_episodes($episodes,$show_id,$user_id)
    {
        foreach ($episodes as $ks => $season) {
            foreach ($season as $ke => $episodes) {
                $this->user_episode->firstOrCreate(
                    [
                        'show_id' => $show_id,
                        'user_id' => $user_id,
                        'season'  => $ks,
                        'episode' => $ke,
                    ],
                    [
                        'episode_status' => 0
                    ]
                );
            }
        }

        return true;
    }

    public function bundle_missing_episodes($show_id, $season, $episode, $user_id = 1)
    {
        $find_to_insert = $this->get_all_missing_episodes($show_id, $season, $episode, $user_id = 1);

        if ($find_to_insert === false) {
            return false;
        }
        $insert_missing = $this->insert_custom_episodes($find_to_insert, $show_id, $user_id);

        if ($insert_missing === false) {
            return false;
        }
    }
}
