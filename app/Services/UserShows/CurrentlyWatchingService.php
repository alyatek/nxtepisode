<?php

namespace App\Services\UserShows;

use App\Http\Controllers\Shows\Episodes\EpisodeController;
use App\Models\Users\UserShows\UserCurrentlyWatching;

class CurrentlyWatchingService
{

    public function __construct ()
    {
        $this->currently_watching = new UserCurrentlyWatching();
        $this->episode_controller = new EpisodeController();
        $this->watch_epi          = new WatchEpisodeService();
    }

    /**
     * this is having no use i think not sure dont delete until sure
     */
    public function update_currently_watching ($user_id, $show_id, $season, $episode = 0)
    {
        #need to find episode if 0
        if ($episode === 0) {
            $last_episode = $this->episode_controller->get_last_episode($show_id, $season);

            if (collect($last_episode)->get('status') === false) {
                return ['status' => false, 'allow' => false];
            }

            $episode = $last_episode['content']['last_episode']['episode'];
        }

//        $this->watch_epi->watch_episode($user_id, $request->show_id, $request->episode, $request->season);
        #todo

        #it might not exist one record of the yet because a user might not have started wathcing any from the watch now thing

        $update = $this->currently_watching->update_currently_watching($user_id, $show_id, $season, $episode);

        if ($update === 0) {
            return ['status' => false, 'allow' => false];
        }

        return ['status' => true, 'allow' => true];
    }
}
