<?php


namespace App\Services;


use App\User;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class TimezoneService
{

    /**
     * TimezoneService constructor.
     */
    public function __construct()
    {
        $this->user = new User();
    }

    /**
     * Gets user time zone from db
     *
     * @param null $user
     *
     * @return mixed
     */
    public function get_user_timezone($user = null)
    {
        if($user === null){
            return Auth::user()->timezone;
        }
        return $this->user->whereId($user)->Timezone()->get();
    }

}