<?php


namespace App\Services\Api;


use App\Models\Users\FacebookCodes;
use App\Models\Users\User\FacebookMessage;
use Illuminate\Support\Facades\Auth;

class FacebookServiceRegisterUser extends FacebookServiceMessages
{
    public function __construct()
    {
        $this->facebook_codes         = new FacebookCodes();
        $this->user_facebook_messages = new FacebookMessage();
    }

    public function handle_register_request($hash, $current_user = 3)
    {
        //validate hash and get the sender id
        $get = collect(
            $this->facebook_codes->select('sender_id', 'expires_at')->whereHash($hash)->where('expires_at', '>', now())
                                 ->get()->first()
        );

        if ($get->isEmpty()) {
            session()->flash(
                'facebook_registration', [
                'status'  => false,
                'message' => 'This activation code is expired. Please send a message again to get a new one!'
            ]
            );
        }

        $sender_id = $get->get('sender_id');
//      how to get the message
//        session()->get('facebook_registration')

        //get user current id
        $current_user = Auth::user()->id;

        if (config('app.debug')) {
            $current_user = 3;
        }

        //insert in users_facebook_message
        $create = collect($this->user_facebook_messages->create($current_user, $sender_id));
        if ($create->isEmpty()) {
            session()->flash(
                'facebook_registration',
                [
                    'status'  => false,
                    'message' => 'Sorry but we couldn\'t create your account right now! Try again and if the error persists contact an admin.'
                ]
            );
        }

        if ($create->isNotEmpty()) {
            session()->flash(
                'facebook_registration',
                [
                    'status'  => true,
                    'message' => 'Nice - Facebook Messages are now configurated! <br> Go to <a href="/watchlater">Watchlater - Scheduling</a> and start planning if you haven\'t already!'
                ]
            );
            $this->configuration_success_message($sender_id);
        }
        return redirect(url('/watchlater/settings'));
    }
}