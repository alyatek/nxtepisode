<?php


namespace App\Services\Api;


use GuzzleHttp\Client;

class FacebookServiceMessages
{

    private function fire_request($message, $sender_id, $type = "RESPONSE")
    {
        /**
         * maybe a template message would be fun?
         * check this out https://developers.facebook.com/docs/messenger-platform/send-messages/template/generic
         */
        $client = new Client();
        //this token needs to be in a global env var

//        $token  = 'EAAHgAyJ17BIBAEZBKgm9pseYuiVxtWVn6x9RuunAyfkclHleX7TWNvYfHZCf114ZAe6om1D76wH0XTXoJJyKHiiJLjnU4dk1M3nLMUeQmzfs6ixeBmMVUcP9v76HBuyK776DCS7rzFth3VR0QcShkXZCuKFuZB7z0FoXL6LpdNIa9XVp6rqoq';
        $token  = config('app.facebook_messages_key');
        $result = $client->post(
            'https://graph.facebook.com/v3.2/me/messages?access_token=' . $token,
            [
                'form_params' =>
                    [
                        'messaging_type' => $type,
                        'recipient'      =>
                            [
                                'id' => $sender_id
                            ],
                        'message'        =>
                            [
                                'text' => $message
                            ]
                    ]
            ]
        );
        return $result;
    }

    public function notice_message($sender_id, $data)
    {
        return $this->fire_request(
            "Hey {$data['user_name']}! It's time to watch {$data['show_name']}. You planned to watch {$data['show_episode_name']} - Episode {$data['show_episode_number']} from Season {$data['show_season']} now @ {$data['time']}. Start watching now: ".url('/watchlist'),
            $sender_id
        );
    }

    protected function sorry_message($sender_id)
    {
        return $this->fire_request('Sorry but we were unable to fulfill that request. Try again later :(', $sender_id);
    }

    protected function configuration_success_message($sender_id)
    {
        return $this->fire_request('Thank you for using this service. Hope you enjoy! 😊', $sender_id);
    }

    protected function disable_message($sender_id, $hash)
    {
        return $this->fire_request(
            'Hey... we heard you were dropping out :( we\'ll miss u <3. Here\'s the link: ' . url('/facebook/messages/disable/' . $hash),
            $sender_id
        );
    }

    protected function code_message($hash, $sender_id)
    {
        return $this->fire_request(
            'Hi! ' .
            'To activate open ' . url('/') . '/facebook/messages/activate/' . $hash . ' - Thank you!',
            $sender_id
        );
    }
    
    protected function fail_message ($s_id) {
        return $this->fire_request(
            'Sorry but we don\'t recognize that command - Type MENU for a list of commands',
            $s_id
        );
    }

    protected function menu_message ($sender_id) {
        return $this->fire_request(
            'Heres is the list of the commands you can currently use: NOTIFICATION - Link to active your account | MENU/HELP - Displays the available commands',
            $sender_id
        );
    }
}