<?php


namespace App\Services\Api;


use App\Models\Users\FacebookCodes;
use App\Models\Users\User\Facebook;
use App\Models\Users\User\FacebookMessage;
use Illuminate\Support\Facades\Auth;

class FacebookServiceRemoveUser extends FacebookServiceMessages
{
    function __construct()
    {
        $this->user_facebook_message = new FacebookMessage();
        $this->facebook_codes        = new FacebookCodes();
    }

    public function send_disable_message($user_id)
    {
        //find sender id
        $user_sender_id = collect($this->user_facebook_message->get_sender_id($user_id));

        //create url
        $url = md5(base64_encode(md5(md5($user_sender_id->get('sender_id') . md5(rand(0, 100) . $user_id)))));

        //store url
        $this->facebook_codes->store_disable_code($user_id, $url);

        //send message
        $this->disable_message($user_sender_id->get('sender_id'), $url);
    }

    public function disable_user($hash)
    {
        //validate hash
        $validate = $this->facebook_codes->whereHash($hash)->whereType('removal')->get(['user_id'])->first();

        //couldnt find -> sned message
        if (collect($validate)->isEmpty()) {
            session()->flash(
                'facebook_registration',
                [
                    'status'  => false,
                    'message' => 'Unable to disable Facebook Message because we couldn\'t find a request from you. Try again by pressing the disable button on the service.'
                ]
            );
            return redirect('/watchlater/settings');
        }

        // call disable func
        $disable = collect($this->user_facebook_message->disable($validate->user_id));
        //failed -> send message
        if(collect($disable)->isEmpty()){
            session()->flash(
                'facebook_registration',
                [
                    'status'  => false,
                    'message' => 'Unable to disable Facebook Message because we couldn\'t find a request from you. Try again by pressing the disable button on the service.'
                ]
            );
            return redirect('/watchlater/settings');
        }

        //success -> send message
        session()->flash(
            'facebook_registration',
            [
                'status'  => true,
                'message' => 'Disabled with no problem. If you want to activate again just send a message with the content of NOTIFICATION'
            ]
        );
        return redirect('/watchlater/settings');
    }
}