<?php

namespace App\Services\Api;

use App\Models\Users\FacebookCodes;
use Carbon\Carbon;
use Illuminate\Support\Str;

class FacebookService extends FacebookServiceMessages
{
    public function __construct()
    {
        $this->facebook_codes = new FacebookCodes();
    }

    /**
     * Receives the data from the request and handles it
     * sending it to the proper location
     *
     * @param null $data
     *
     * @return \Psr\Http\Message\ResponseInterface|string
     */
    public function handle_facebook_message_webhook($data = null)
    {
//        $data       = '{"object":"page","entry":[{"id":"1235821016568856","time":1555357346905,"messaging":[{"sender":{"id":"2501405493224105"},"recipient":{"id":"1235821016568856"},"timestamp":1555356575740,"message":{"mid":"7iZCjlQiPt7q55Pg7-LlD5oY_RHji5W_mBIMnre--uammluGQKQDM94mkDLXq_VC3Tko4HWG0sGA_bS4lvUuog","seq":300385,"sticker_id":369239263222822,"attachments":[{"type":"image","payload":{"url":"https:\/\/scontent.xx.fbcdn.net\/v\/t39.1997-6\/39178562_1505197616293642_5411344281094848512_n.png?_nc_cat=1&_nc_ad=z-m&_nc_cid=0&_nc_zor=9&_nc_ht=scontent.xx&oh=f4fe639d55a6849b51410c769a4c06b4&oe=5D2E0775","sticker_id":369239263222822}}]}}]}]}';
        $data_array = json_decode($data, true);
        $message = '';

        $sender_id = $data_array['entry'][0]['messaging'][0]['sender']['id'];
        //its a message request
        if (isset($data_array['entry'][0]['messaging'][0]['message']['text'])) {
            $message = $data_array['entry'][0]['messaging'][0]['message']['text'];
            switch (Str::singular(strtolower($message))) {
                case 'notification':
                    return $this->generate_notification_answer($sender_id);
                    break;
                case 'help':
                    return $this->menu_message($sender_id);
                    break;
                case 'menu':
                    return $this->menu_message($sender_id);
                    break;
                default:
                    return $this->fail_message($sender_id);
                    break;

            }
        }else {
            return $this->fail_message($sender_id);
        }

        return $message;
    }

    /**
     * Fires a message to the sender with their data
     *
     * @param $sender_id
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function generate_notification_answer($sender_id)
    {
        //make a code
        $hash = $this->generate_hash();
        //insert that code in db
        $store = $this->store_hash($hash, $sender_id);

//        if (collect($store)->isEmpty()) {
//            //send message apologizing
//            $this->sorry_message($sender_id);
//        }

        //tell that code in a message
        return $this->code_message($hash, $sender_id);
    }

    /**
     *
     * Generates and checks in db if the code exists or not until on that doesnt works and retrieves it
     * @return string
     */
    protected function generate_facebook_code()
    {
        //generate a code
        $code = strtoupper($this->six_character_code_generator());

        //validate if none exists already stored in db
        $validate = $this->facebook_codes->whereCode($code)->get();

        if (!collect($validate)->isEmpty()) {
            $this->generate_facebook_code();
        }

        return $code;
    }

    protected function generate_hash () {
        //generate hash
        $get_six_character = $this->six_character_code_generator();
        $md5_six_character = md5($get_six_character);
        $b64_md5 = base64_encode($md5_six_character);
        $hash = md5($b64_md5);

        //check db
        //validate if none exists already stored in db
        $validate = $this->facebook_codes->whereHash($hash)->get();

        if (!collect($validate)->isEmpty()) {
            $this->generate_hash();
        }
        return $hash;
    }

    /**
     *
     * DEPRECATED
     *
     * Stores the sender and the code in the db
     *
     * @param $code
     * @param $sender_id
     *
     * @return FacebookCodes
     */
    private function store_code($code, $sender_id)
    {
        $fcodes             = new FacebookCodes();
        $fcodes->user_id    = 0;
        $fcodes->code       = $code;
        $fcodes->sender_id  = $sender_id;
        $fcodes->expires_at = Carbon::now()->addHours(2);

        $fcodes->save();

        return $fcodes;
    }

    private function store_hash ($hash, $sender_id) {
        $fcodes             = new FacebookCodes();
        $fcodes->hash       = $hash;
        $fcodes->sender_id  = $sender_id;
        $fcodes->expires_at = Carbon::now()->addHours(2);

        $fcodes->save();

        return $fcodes;
    }

    /**
     * Generates a 6 character combination of letters and numbers in upper case
     */
    public function six_character_code_generator()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($permitted_chars), 0, 6);
    }
}