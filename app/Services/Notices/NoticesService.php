<?php

namespace App\Services\Notices;

use App\Http\Controllers\Shows\Episodes\EpisodeController;
use App\Http\Controllers\Shows\Show\ShowController;
use App\Models\Shows\Show\Show;
use App\Models\Users\User\FacebookMessage;
use App\Models\Users\User\User;
use App\Models\Users\UserShows\UserScheduleConfigurations;
use App\Models\Users\UserShows\UserShowsSchedule;
use App\Notifications\ShowNotice;
use App\Services\Api\FacebookServiceMessages;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class NoticesService
{
    public function __construct ()
    {
        $this->configurations = new UserScheduleConfigurations();
        $this->schedule       = new UserShowsSchedule();

        $this->user     = new User();
        $this->show     = new Show();
        $this->episodes = new EpisodeController();

        $this->facebook_messages = new FacebookMessage();

        $this->programmed_between_now = $this->get_programmed_between_now();
    }

    public function mail_notice ()
    {
        #gets all episodes scheduled to the between current how
        $schedules = $this->get_programmed_between_now();

        if (collect($schedules)->isEmpty()) {
            return [false];
        }

        #gets all users with email configured
        $users = $this->get_users_with_mail_configuration();

        $_info = [];
        $y     = 0;
        #finds if user as this method and inserts him along with data to a temp variable
        foreach ($schedules as $schedule_user_id) {
            $user_id = collect($schedule_user_id)->get('user_id');

            if (collect($users)->contains('user_id', $user_id)) {
                $_info[$y]['user_id'] = $schedule_user_id['user_id'];
                $_info[$y]['show_id'] = $schedule_user_id['show_id'];
                $_info[$y]['season']  = $schedule_user_id['season'];
                $_info[$y]['episode'] = $schedule_user_id['episode'];
                $_info[$y]['time']    = $schedule_user_id['schedule_time'];
                $y++;
            }
        }

        if (collect($_info)->isEmpty()) {
            return ['status' => false];
        }

        $compiled_info = [];
        #fill the temp variables with the respective data
        foreach ($_info as $k => $item) {
            $item = collect($item);
            #get the user information
            $user = $this->user->get_by_id($item->get('user_id'));
            #get show information
            $show = $this->show->get_show_data_plain($item->get('show_id'));
            #get the episode information
            $episode                                            = $this->episodes->get_episode($item->get('show_id'), $item->get('season'), $item->get('episode'));
            $episode['content']['message']['episode']['number'] = $item->get('episode');

            $compiled_info[$k]['user_id'] = $user;
            $compiled_info[$k]['show_id'] = $show;
            $compiled_info[$k]['time']    = $item['time'];
            $compiled_info[$k]['season']  = $schedule_user_id['season'];
            $compiled_info[$k]['episode'] = $episode['content']['message']['episode'];
        }

        if (collect($compiled_info)->isEmpty()) {
            return ['status' => false];
        }

        foreach ($compiled_info as $notice) {
            $notice            = collect($notice);
            $time              = Carbon::parse($notice['time']);
            $random_unique_key = md5(rand() . $time . $notice->get('user_id'));
            $this->send_notice($notice->get('user_id'), [
                'show_name'           => $notice->get('show_id')['name'],
                'show_episode_name'   => $notice->get('episode')['name'],
                'show_episode_number' => $notice->get('episode')['number'],
                'show_season'         => $notice->get('season'),
                'show_slug'           => $notice->get('show_id')['slug'],
                'user_name'           => $notice->get('user_id')['username'],
                'time'                => $time->format('g:i A'),
                'unique_id'           => $random_unique_key,
            ], ['database',
                'mail']);
        }

        return $compiled_info;
    }

    public function in_app_notice ()
    {
        #gets all episodes scheduled to the between current how
        $schedules = $this->get_programmed_between_now();

        if (collect($schedules)->isEmpty()) {
            return [false];
        }

        #gets all users with email configured
        $users = $this->get_users_with_mail_configuration();

        $_info = [];
        $y     = 0;
        #finds if user as this method and inserts him along with data to a temp variable
        foreach ($schedules as $schedule_user_id) {
            $user_id = collect($schedule_user_id)->get('user_id');
            if (collect($users)->contains('user_id', $user_id)) {
                $_info[$y]['methods'] = ['broadcast'];
            } elseif(collect($users)->get('status') === false) {
                $_info[$y]['methods'] = ['database', 'broadcast'];
            }

            $_info[$y]['user_id'] = $schedule_user_id['user_id'];
            $_info[$y]['show_id'] = $schedule_user_id['show_id'];
            $_info[$y]['season']  = $schedule_user_id['season'];
            $_info[$y]['episode'] = $schedule_user_id['episode'];
            $_info[$y]['time']    = $schedule_user_id['schedule_time'];
            $y++;
        }

        if (collect($_info)->isEmpty()) {
            return ['status' => false];
        }

        $compiled_info = [];
        #fill the temp variables with the respective data
        foreach ($_info as $k => $item) {
            $item = collect($item);
            #get the user information
            $user = $this->user->get_by_id($item->get('user_id'));
            #get show information
            $show = $this->show->get_show_data_plain($item->get('show_id'));
            #get the episode information
            $episode                                            = $this->episodes->get_episode($item->get('show_id'), $item->get('season'), $item->get('episode'));
            $episode['content']['message']['episode']['number'] = $item->get('episode');

            $compiled_info[$k]['user_id'] = $user;
            $compiled_info[$k]['show_id'] = $show;
            $compiled_info[$k]['time']    = $item['time'];
            $compiled_info[$k]['season']  = $schedule_user_id['season'];
            $compiled_info[$k]['episode'] = $episode['content']['message']['episode'];
            $compiled_info[$k]['methods'] = $item->get('methods');
        }

        if (collect($compiled_info)->isEmpty()) {
            return ['status' => false];
        }

        foreach ($compiled_info as $notice) {
            $notice            = collect($notice);
            $time              = Carbon::parse($notice['time']);
            $random_unique_key = md5(rand() . $time . $notice->get('user_id'));

            $this->send_notice($notice->get('user_id'), [
                'show_name'           => $notice->get('show_id')['name'],
                'show_episode_name'   => $notice->get('episode')['name'],
                'show_episode_number' => $notice->get('episode')['number'],
                'show_season'         => $notice->get('season'),
                'show_slug'           => $notice->get('show_id')['slug'],
                'user_name'           => $notice->get('user_id')['username'],
                'time'                => $time->format('g:i A'),
                'unique_id'           => $random_unique_key,
            ], $notice->get('methods'));
        }

        return $compiled_info;
    }

    public function web_push_notice ()
    {
        #gets all episodes scheduled to the between current how
        $schedules = $this->get_programmed_between_now();

        if (collect($schedules)->isEmpty()) {
            return ['status' => false, 'content' => ['message' => ['Nothing programmed']]];
        }

        #gets all users with webpush configured
        $users = $this->get_user_with_web_push_configured();

        $_info = [];
        $y     = 0;
        #finds if user as this method and inserts him along with data to a temp variable
        foreach ($schedules as $schedule_user_id) {
            $user_id = collect($schedule_user_id)->get('user_id');

            if (collect($users)->contains('user_id', $user_id)) {
                $_info[$y]['user_id'] = $schedule_user_id['user_id'];
                $_info[$y]['show_id'] = $schedule_user_id['show_id'];
                $_info[$y]['season']  = $schedule_user_id['season'];
                $_info[$y]['episode'] = $schedule_user_id['episode'];
                $_info[$y]['time']    = $schedule_user_id['schedule_time'];
                $y++;
            }
        }

        if (collect($_info)->isEmpty()) {
            return ['status' => false];
        }

        $compiled_info = [];
        #fill the temp variables with the respective data
        foreach ($_info as $k => $item) {
            $item = collect($item);
            #get the user information
            $user = $this->user->get_by_id($item->get('user_id'));
            #get show information
            $show = $this->show->get_show_data_plain($item->get('show_id'));
            #get the episode information
            $episode                                            = $this->episodes->get_episode($item->get('show_id'), $item->get('season'), $item->get('episode'));
            $episode['content']['message']['episode']['number'] = $item->get('episode');

            $compiled_info[$k]['user_id'] = $user;
            $compiled_info[$k]['show_id'] = $show;
            $compiled_info[$k]['time']    = $item['time'];
            $compiled_info[$k]['season']  = $schedule_user_id['season'];
            $compiled_info[$k]['episode'] = $episode['content']['message']['episode'];
        }

        if (collect($compiled_info)->isEmpty()) {
            return ['status' => false];
        }

        foreach ($compiled_info as $notice) {
            $notice            = collect($notice);
            $time              = Carbon::parse($notice['time']);
            $random_unique_key = md5(rand() . $time . $notice->get('user_id'));
            $this->send_web_notice($notice->get('user_id'), [
                'show_name'           => $notice->get('show_id')['name'],
                'show_episode_name'   => $notice->get('episode')['name'],
                'show_episode_number' => $notice->get('episode')['number'],
                'show_season'         => $notice->get('season'),
                'show_slug'           => $notice->get('show_id')['slug'],
                'user_name'           => $notice->get('user_id')['username'],
                'time'                => $time->format('g:i A'),
                'unique_id'           => $random_unique_key,
            ]);
        }

        return $compiled_info;
    }

    public function facebook_message () {
        $schedules = collect($this->programmed_between_now);
        if($schedules->isEmpty()){
            return [false];
        }

        $users_with_facebook_message = $this->get_user_with_facebook_messages_configured();
        $users_facebook = [];
        foreach($users_with_facebook_message as $k =>$u){
            $users_facebook[$u['user_id']]['user_id'] = $u['user_id'];
            $users_facebook[$u['user_id']]['sender_id'] = $u['sender_id'];
        }

        $_info = [];
        $y     = 0;

        foreach ($schedules as $schedule_user_id) {
            $user_id = collect($schedule_user_id)->get('user_id');

            if (collect($users_with_facebook_message)->contains('user_id', $user_id)) {
                $_info[$y]['user_id'] = $schedule_user_id['user_id'];
                $_info[$y]['sender_id'] = $users_facebook[$schedule_user_id['user_id']]['sender_id'];
                $_info[$y]['show_id'] = $schedule_user_id['show_id'];
                $_info[$y]['season']  = $schedule_user_id['season'];
                $_info[$y]['episode'] = $schedule_user_id['episode'];
                $_info[$y]['time']    = $schedule_user_id['schedule_time'];
                $y++;
            }
        }

        if (collect($_info)->isEmpty()) {
            return ['status' => false];
        }

        $compiled_info = [];
        #fill the temp variables with the respective data
        foreach ($_info as $k => $item) {
            $item = collect($item);
            #get the user information
            $user = $this->user->get_by_id($item->get('user_id'));
            #get show information
            $show = $this->show->get_show_data_plain($item->get('show_id'));
            #get the episode information
            $episode                                            = $this->episodes->get_episode($item->get('show_id'), $item->get('season'), $item->get('episode'));
            $episode['content']['message']['episode']['number'] = $item->get('episode');

            $compiled_info[$k]['user_id'] = $user;
            $compiled_info[$k]['sender_id'] = $item->get('sender_id');
            $compiled_info[$k]['show_id'] = $show;
            $compiled_info[$k]['time']    = $item['time'];
            $compiled_info[$k]['season']  = $schedule_user_id['season'];
            $compiled_info[$k]['episode'] = $episode['content']['message']['episode'];
        }

        if (collect($compiled_info)->isEmpty()) {
            return ['status' => false];
        }

        $facebook_send_message = new FacebookServiceMessages();

        foreach ($compiled_info as $message) {
            $notice            = collect($message);
            $time              = Carbon::parse($notice['time']);
            $random_unique_key = md5(rand() . $time . $notice->get('user_id'));
            $facebook_send_message->notice_message($notice->get('sender_id'), [
                'show_name'           => $notice->get('show_id')['name'],
                'show_episode_name'   => $notice->get('episode')['name'],
                'show_episode_number' => $notice->get('episode')['number'],
                'show_season'         => $notice->get('season'),
                'show_slug'           => $notice->get('show_id')['slug'],
                'user_name'           => $notice->get('user_id')['username'],
                'time'                => $time->format('g:i A'),
                'unique_id'           => $random_unique_key,
            ]);
        }

        return $compiled_info;
    }

    public function get_users_with_mail_configuration ()
    {
        $get = $this->configurations->get_users_with_method('email');
        if (collect($get)->isEmpty()) {
            return ['status' => false];
        }
        return $get;
    }

    public function get_user_with_web_push_configured ()
    {
        $get_pc      = collect($this->configurations->get_users_with_method('onesignal_pc'));
        $get_android = collect($this->configurations->get_users_with_method('onesignal_android'));

        $get = $get_pc->merge($get_android);

        return array_unique($get->all());
    }

    public function get_user_with_facebook_messages_configured () {
        $get = $this->facebook_messages->whereStatus(1)->get(['sender_id','user_id']);
        if (collect($get)->isEmpty()) {
            return ['status' => false];
        }
        return $get;
    }

    public function get_programmed_between_now ()
    {
        #get current hour
        #take 10 min before to a var
        #take 10 min after for a var
        #send it to the function

        $programmed = $this->schedule
            ->get_users_for_current_schedule(Carbon::now()->subMinutes(10)->toTimeString(), Carbon::now()->addMinutes(10)->toTimeString(), Carbon::now()->toDateString());
        Log::info('Number of current scheduled'. count($programmed));
        Log::info('Current Server Time: '. now());
        return $programmed;
    }

    public function get_unread_notifications ()
    {
        $user   = \App\User::find(Auth::user()->id);
        $unread = [];

        foreach ($user->unreadNotifications as $notification) {
            $unread[] = collect($notification)->get('data');
        }

        if (collect($unread)->isEmpty()) {
            return ['status'  => true,
                    'content' => ['count' => 0]];
        }

        return ['status'  => true,
                'content' => ['count'         => count($unread),
                              'notifications' => $unread]];
    }

    public function send_web_notice ($user, $message_info)
    {
        Notification::send(\App\User::find($user['id']), new ShowNotice($user['id'], $message_info, [], true));
    }

    public function send_notice ($user, $message_info, $methods)
    {
        Notification::send(\App\User::find($user['id']), new ShowNotice($user['id'], $message_info, $methods));
    }

    public function verify_email_notification ($key)
    {
        $user = \App\User::find(Auth::user()->id);

        foreach ($user->unreadNotifications as $notification) {

            $data = collect($notification['data']);

            if (isset($data['unique_id'])) {
                if ($key === $data['unique_id']) {
                    $notification->markAsRead();
                }
            }
        }
    }

    public function verify_all_email_notification ()
    {
        $user = \App\User::find(Auth::user()->id);

        foreach ($user->unreadNotifications as $notification) {
            $notification->markAsRead();
        }
    }
}
