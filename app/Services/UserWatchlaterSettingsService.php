<?php

namespace App\Services;

use App\Http\Controllers\Users\UserShows\UserScheduleConfigurationsController;
use App\Models\Users\UserShows\UserScheduleConfigurations;
use Illuminate\Support\Facades\Auth;

class UserWatchlaterSettingsService
{


    public function __construct()
    {
        $this->configurations = new UserScheduleConfigurations();
    }

    /**
     * Makes an entry in the database when email is inserted or sends to update if already exists one with the same method
     *
     * @param $method_value
     * @param $method_configuration
     * @return array
     */
    public function make_method($method_value, $method_configuration)
    {
        $user_id = Auth::user()->id;

        $verify = $this->verify_method_exists($user_id, $method_configuration);

        if ($verify['exists'] === true) {
            return $this->update_method($user_id, $method_value, $method_configuration);
        }

        $create = $this->configurations->create($user_id, $method_value, $method_configuration);

        if ($create === null) {
            return ['status' => false,
                    'content' => ['message' => ['Could not configure this notification method right now. Try again later please.']]];
        }

        return ['status' => true,
                'content' => ['message' => ['Notification configured with success!'],
                              'configuration' => collect($create)->only(['method_value',
                                                                         'configuration_method'])]];
    }

    /**
     * Verify if a user already configured a certain method
     *
     * @param $user_id
     * @param $method
     * @return array
     */
    public function verify_method_exists($user_id, $method)
    {
        $check = $this->configurations->get_user_method($user_id, $method);
        if (collect($check)->isEmpty()) {
            return ['exists' => false];
        }
        return ['exists' => true];
    }

    /**
     * Updates the method requested
     *
     * @param $user_id
     * @param $method_value
     * @param $method_configuration
     * @return array
     */
    public function update_method($user_id, $method_value, $method_configuration)
    {
        $update = $this->configurations->update_user_method($user_id, $method_value, $method_configuration);
        if (collect($update)->isEmpty()) {
            return ['status' => false,
                    'content' => ['message' => ['Could not update.']]];
        }
        return ['status' => true,
                'content' => ['message' => ['Notification updated with success!']]];
    }

    /**
     * Gets the method status for a user - if configured or not
     *
     * @param $user_id
     * @param $method_configuration
     * @return array
     */
    public function user_method_status($method_configuration)
    {
        $user_id = Auth::user()->id;

        $check = $this->verify_method_exists($user_id, $method_configuration);

        if ($check['exists'] === false) {
            return ['status' => false, 'content'=>['configured'=>false]];
        }

        $get_user_configuration = $this->configurations->get_user_method($user_id,$method_configuration);

        return ['status' => true,
                'content' => ['configured' => true, 'data' => $get_user_configuration]];
    }

    /**
     * Removes user from the email method configuration
     */
    public function remove_email_method () {
        $user_email = Auth::user()->email;

        $check = $this->configurations->get_configuration_from_mail($user_email);
        if(collect($check)->isEmpty()){
            return ['status' => false, 'content'=>['message' =>['Method already removed. Please refresh the page!']]];
        }
        //check if mail exists to not fuck up the db query delete

        $delete = $this->configurations->remove_email_configuration($user_email);
        if(collect($delete)->isEmpty()){
            return ['status' => false, 'content'=>['message' =>['Method already removed. Please refresh the page!']]];
        }

        return ['status' => true, 'content'=>['message' =>['Method removed with no problem! Feel free to configure again anytime you want!']]];
    }
}
