<?php

namespace App\Services\Watchlater;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Users\User\UserPremiumController;
use App\Models\Shows\Show\Show;
use App\Models\Users\UserShows\LimitTracker;
use App\Models\Users\UserShows\UserShowsSchedule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserWatchlaterShowsScheduledService extends Controller
{

    public function __construct()
    {
        $this->user           = Auth::user();
        $this->user_schedules = new UserShowsSchedule();
        $this->shows          = new Show();
        $this->user_premium   = new UserPremiumController();
        $this->limit_tracker  = new LimitTracker();
    }

    public function get_shows_planned()
    {
        $get = $this->user_schedules->get_shows_plain(Auth::user()->id);

        if (collect($get)->isEmpty()) {
            return [
                'status'  => false,
                'content' => ['message' => ['No episodes are planned.']]
            ];
        }

//        foreach(){
//
//        }
//        $show_info = $this->shows->get_show_data_plain(1);

        $grouped['planned'] = collect($get)->groupBy('show_id')->toArray();

        $grouped['shows'] = collect($grouped['planned'])->map(
            function ($item, $key) {
                return $this->shows->get_show_data_plain($key);
            }
        )->all();

        return [
            'status'  => true,
            'content' => $grouped
        ];
    }

    public function get_limit_plans_of_show($show_id)
    {
        $get_user_premium_status = collect($this->user_premium->get_user($this->user['id']));

        if ($get_user_premium_status->get('premium') === false) { #treat user as none premium
            return $this->get_limit_plans_of_free($show_id);
        }

        return $get_user_premium_status;
    }

    public function get_limit_plans_of_free($show_id)
    {
        $user_id = Auth::user()->id;# user id

        $get_current_limit = $this->limit_tracker->get($user_id, $show_id); #gets data

        if (collect($get_current_limit)->isEmpty()) {
            $get_current_limit = $this->limit_tracker->create($user_id, $show_id, $new_limit);
        }#if empty creates - most likely used in the first time

        if (collect($get_current_limit)->isEmpty()) {
            return [
                'status'  => false,
                'content' => ['message' => ['Uhmmmm we can\'t figure out what this error is! Anyway the nerds were already notified. Try again later, sorry xoxo']]
            ];
        } # if nothing is here by now it returns an error

        $current_limit = collect($get_current_limit)->get('current_limit');#define the limit

        if ($current_limit === 0) {
            return ['status' => false];
        }#welp if 0 nothing left
        return ['status' => true]; #yay
    }

    public function get_limit_plans_of_premium($show_id)
    {

    }

    public function update_limit_lower($show_id, $user_id)
    {
//        //get
//        $get_current_limit = $this->limit_tracker->get($user_id, $show_id);
//        //define current limit
//        $current_limit = collect($get_current_limit)->get('current_limit');
//        //remove one
//        $new_limit = ($current_limit - 1);
//
//        //update record
//        $update = $this->limit_tracker->update_limit($user_id,$show_id,$new_limit);
//
//        if(collect($update)->isEmpty()){
//            Log::alert('Failed to update the limit tracker');
//        }
//        return ;
    }

    /**
     * This function will update the schedules if there is any of those episodes in a bulk watch update
     */
    public function update_schedule_status_bulk($user_id, $show_id, $season)
    {
        $find_exists = $this->user_schedules->where('user_id', $user_id)->where('show_id', $show_id)
                                            ->where('season', $season)->get();

        if (collect($find_exists)->isEmpty()) {
            return ['status' => false, 'allow' => true];
        }

        $update = $this->user_schedules->update_bulk_watch($user_id, $show_id, $season);

        if ($update === 0) {
            return ['status' => false, 'allow' => false];
        }

        return ['status' => true, 'allow' => true];;
    }

    /**
     * Clears expired unwatched episodes
     *
     */
    public function clean_expired_unwatched_shows () {
        //find if any are planned
        $get = $this->user_schedules
            ->ScheduleDateOlderThan(now()->toDateString())
            ->Watched(0)
            ->get();

        //get only the entries id
        $clean_array = Arr::pluck($get, 'id');

        //clean them
        return $this->user_schedules->remove_bulk($clean_array);
    }
}
