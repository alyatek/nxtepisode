<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PasswordResetEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct ($content)
    {
        $this->token = $content['token'];
        $this->ip    = $content['ip'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build ()
    {
        return $this->from('noreply@nxtepisode.com')
            ->view('pages.auth.forgot_password.forgot-password-reset-mail')
            ->with([
            'token'  => $this->token,
            'ip' => $this->ip,
        ]);
    }
}
