<?php

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */

define('LARAVEL_START', microtime(true));

//REMOVE FOR NGINX TODO
//https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection
header("X-XSS-Protection: 1; mode=block");

//https://developer.mozilla.org/pt-PT/docs/Web/HTTP/Headers/Strict-Transport-Security#Exemplos
header("Strict-Transport-Security: max-age=31536000; includeSubDomains");

//https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
header("X-Frame-Options: deny");

//https://developer.mozilla.org/pt-PT/docs/Web/HTTP/Headers/X-Content-Type-Options
header("X-Content-Type-Options: nosniff");

//https://helmetjs.github.io/docs/crossdomain/
header("X-Permitted-Cross-Domain-Policies: none");

//https://developer.mozilla.org/pt-BR/docs/Web/HTTP/CSP
//header("Content-Security-Policy: default-src 'self'");

header_remove('Server');
header_remove('X-Powered-By');
/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

require __DIR__ . '/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__ . '/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);
