$(document.body).on('click', 'a', function () {
    $('.collapse').on('show.bs.collapse', function () {
        $(this)
            .parent()
            .parent()
            .parent()
            .find('.collapser')
            .html('')
            .html('<i class="flaticon-close"></i> Close')
        ;
    }).on('hidden.bs.collapse', function () {
        $(this)
            .parent()
            .parent()
            .parent()
            .find('.collapser')
            .html('')
            .html('<i class="fas fa-list-ol"></i> Episodes')
        ;
    });

    var $myGroup = $('#show_seasons');
    $myGroup.on('show.bs.collapse', '.collapse', function () {
        $myGroup.find('.collapse.in').collapse('hide');
    });

});

$.fn.extend({
    animateCss: function (animationName, callback) {
        var animationEnd = (function (el) {
            var animations = {
                animation: 'animationend',
                OAnimation: 'oAnimationEnd',
                MozAnimation: 'mozAnimationEnd',
                WebkitAnimation: 'webkitAnimationEnd',
            };

            for (var t in animations) {
                if (el.style[t] !== undefined) {
                    return animations[t];
                }
            }
        })(document.createElement('div'));

        this.addClass('animated ' + animationName).one(animationEnd, function () {
            $(this).removeClass('animated ' + animationName);

            if (typeof callback === 'function') {
                callback();
            }
        });

        return this;
    },
});

$(function () {

});

var intObj = {
    template: 3,
    speed: 0.5,
};
var indeterminateProgress = new Mprogress(intObj);

let loveCounter = 0;

function messageMessage(message = 'Something happend. And I\'m not sure why because the idiot who coded me didn\'t send a proper message to me, so what you are reading is a default text. Opsie.', position = 'topRight') {
    iziToast.show({
        message: message,
        position: position,
        transitionIn: "bounceInDown",
        transitionOut: "fadeOutRight",
        timeout: 10000,
    });
}

function messageSuccess(message = 'Noice.', position = 'topRight') {
    iziToast.success({
        message: message,
        position: position,
        transitionIn: "bounceInDown",
        transitionOut: "fadeOutRight",
    });
}

function messageFailed(message = 'opsie.', position = 'topRight') {
    iziToast.error({
        message: message,
        position: position,
        transitionIn: 'bounceInDown',
        transitionOut: 'fadeOutRight',
    });
}

function makeWarning(message, type = 'info', block = 'messages', delay = 0) {
    setTimeout(function() {
        switch (type) {
            case 'info':
                $(block).html('<div class="alert alert-info" role="alert">' + message + ' </div>');
                break;
            case 'primary':
                $(block).html('<div class="alert alert-primary" role="alert">' + message + ' </div>');
                break;
            case 'secondary':
                $(block).html('<div class="alert alert-secondary" role="alert">' + message + ' </div>');
                break;
            case 'success':
                $(block).html('<div class="alert alert-success" role="alert">' + message + ' </div>');
                break;
            case 'warning':
                $(block).html('<div class="alert alert-warning" role="alert"> ' + message + '</div>');
                break;
            case 'error':
                $(block).html('<div class="alert alert-danger" role="alert">' + message + ' </div>');
                break;
            default:
                $(block).html('<div class="alert alert-info" role="alert">Error occurred.</div>');
                break;
        }
    }, delay);
}

function parsePlanURL(url_string =1) {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('plan') && urlParams.has('api_id')) {
        return [urlParams.get('plan'), urlParams.get('api_id')];
    }
    return null;
}


function basicValidation(res, callback, skipBaseValidation = false, debug = false) {
    if (skipBaseValidation === true) {
        callback(res);
        return;
    }

    if (debug === true) {
        console.log(res);
        return;
    }

    if (res) {
        if (res.status === false) {
            if (res.content) {
                messageFailed(iterateMessages(res.content.message));
                return;
            } else {
                messageFailed('Sorry but we cannot figure out what went wrong.');
                return;
            }
        }
        callback(res);
        return;
    }

}

function ajaxRequest(url, data, method, callback, debug = false, skipBaseValidation = false) {

    indeterminateProgress.start();
    $.ajax({
        url: url,
        method: method,
        data: data,
        dataType: 'json',
        success: function (response) {
            setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
            basicValidation(response, callback, skipBaseValidation, debug);
        },
        error: function (z, x, y) {
            setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
            btnEndLoading($('body').find('.fa-spin').parent().attr('id'), 'Change', false);
            try {
                contentErrorAlert(z, z.responseJSON.message);
            } catch (e) {
                console.log('error');
            }
        }
    });
}

function slugify(text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
}

function iterateMessages(messages) {
    var output = "";

    $.each(messages, function (key, message) {
        output += "<p>" + message + "</p>";
    });

    return output;
}

function iterateValidatorMessages(messages) {
    let html = '';

    if (_.size(messages) === 1) {
        return iterateMessages(messages);
    }
    _.each(messages, function (message) {
        _.each(message, function (individual) {
            html += '<p>' + individual + '</p>';
        });
    });
    return html;
}


//
//

function btnLoading(id) {
    var currentw = $('#' + id).width();
    $("#" + id)
        .attr('disabled', true)
        .html('<i class="fas fa-circle-notch fa-spin"></i>')
        .width(currentw);
}

function btnEndLoading(id, txt, status = false) {
    $("#" + id).attr('disabled', status);
    $("#" + id).html(txt);
}

// function btn_changeStatus (btnid, message = null, status = false) {
//
//     if(!$('#'+btnid).attr('disabled')){
//         btnLoading(btnid, message, status);
//         return;
//     }
//
//     btnEndLoading(btnid,message, status);
// }

function contentLoading() {
    $('.content').append('<div class="page-load"><i class="fas fa-circle-notch fa-spin fa-3x"></i><p>Loading</p></div>');
}

function contentReset() {
    $('.page-load').fadeOut();
    $('.page-load').remove();
}

function contentError(r, r_name) {
    if (r.status) {
        // if(r.status === 500){
        contentReset();
        $('.content').html('<div class="container"><div class="alert alert-danger" role="alert">' +
            '<p><b>' + r.status + ' ' + r_name + '</b></p>' +
            '<p>If this error persists please contact an administrator. To do so press <a href="' + APP_URL + '/error/form">here</a>.</p>' +
            '<p>Thank you.</p>' +
            '</div></div>');
        // }
    } else {
        contentReset();
        $('.content > .container').html('<div class="alert alert-danger" role="alert">' +
            '<p><b>' + r + ' ' + r_name + '</b></p>' +
            '<p>If this error persists please contact an administrator. To do so press <a href="' + APP_URL + '/error/form">here</a>.</p>' +
            '<p>Thank you.</p>' +
            '</div>');
    }
}

function contentErrorAlert(r, r_name) {
    if (r.status) {
        var message = '<p><b>' + r.status + ' ' + r_name + '</b></p>' +
            '<p>If this error persists please contact an administrator. To do so press <a href="' + APP_URL + '/error/form">here</a>.</p>' +
            '<p>Thank you.</p>';

        if (r.responseJSON) {
            if (r.responseJSON.status === false) {
                pMessage({title: 'Error!', message: message, type: 'error'});
                return;
            }
        }

        pMessage({title: 'Error!', message: r_name, type: 'error'});
        return;
    }
    pMessage({title: 'Error!', message: r_name, type: 'error'});
}

function makeSuccessContent(data) {
    $("#content").fadeOut();

    $('#content').html('<div class="alert alert-success" role="alert">' +
        '  <h4 class="alert-heading">Noice!</h4>' +
        iterateMessages(data.message) +
        '</div>');

    $("#content").fadeIn();
}

/********************/

function cMessageClear() {
    $('.alert').fadeOut();
    $('.alert').removeClass('alert-*');
    $('.alert').html('');
}

function cMessage(data, id = '') {
    if(id !== ''){
        $('#'+id).html("<h5>Oops!</h5><div></div>");
        $('#'+id+' > div').html(iterateMessages(data.message));
        $('#'+id).addClass('alert-' + data.type);
        $('#'+id).fadeIn();
        return;
    }
    $('.alert '+section).html("<h5>Oops!</h5><div></div>");
    $('.alert '+section+' > div').html(iterateMessages(data.message));
    $('.alert '+section).addClass('alert-' + data.type);
    $('.alert '+section).fadeIn();
}

function pMessage(data) {
    if (data.type === 'success') {
        messageSuccess(data.message);
    } else if (data.type === 'warning') {
        messageMessage(data.message);
    } else if (data.type === 'error') {
        messageFailed(data.message);
    } else {
        messageMessage(data.message);
    }
}

/********************/

function startWatchingEpisode(show_id, btn) {
    btnLoading('startWatchingBtn');
    $.ajax({
        url: APP_URL + '/watchlist/show/episodes/current/start',
        method: "post",
        data: {show_id: show_id, _token: APP_CSRF},
        dataType: 'json',
        success: function (response) {
            updateUserShowCurrentlyWatching(
                show_id,
                true,
                true,
                response.episode
            );
            if (response.status === true) {
                let template_success = _.template('<div class="text-center"><h1>Noice!</h1><%= message %></div>');
                $('.fancybox-content').html(template_success({
                    message: iterateMessages(response.content.message),
                }));
            }
        },
        error: function () {
            alert("error");
            let template_error = _.template('U don fuked up');
            $('.fancybox-content').html(template_error({
                error: iterateMessages(response.content.message),
            }));
        }
    });
}

function callbackAnswer(response) {
    var status = '';
    var messages = iterateMessages(response.content.message);

    if (response.status === true) {
        messageMessage(messages);
    } else if (response.status === false) {
        messageFailed(messages);
    }
}

function callWatchNext(user_show_page = false) {
    const show_id = $('#show_id').val();

    $.fancybox.open({
        src: APP_URL + `/watchlist/show/get/watch-next/html/${show_id}`,
        // src : '#watch-next-episode',
        type: 'ajax'
    });
}

function updateUserShowCurrentlyWatching(showId, isCurrentlyWatching = false, usingOtherSource = false, otherSourceData = {}) {
    if (isCurrentlyWatching === false) {
        //disable
        //stop shwoing the block
        $('#currently_watching_area').fadeOut(300);
        return;
    }

    if ($('#currently_watching_area').css('display') === 'none') {
        $('#currently_watching_area').fadeIn(300);
    }

    if (usingOtherSource === true) {
        const episode_name = otherSourceData.name;
        const episode_season = otherSourceData.season;
        const episode_nr = otherSourceData.episode;
        //@todo

        $('#currently_watching_name').html(episode_name);
        $('#currently_watching_season').html(episode_season);
        $('#currently_watching_episode').html(episode_nr);
        $('#currently_watching_episode_finish_message').html(`<a onclick="callWatchNext(true)" href="javascript:;">Do you want to finish it?</a>`);

    }

}

function addToMyWatchlist(id) {
    indeterminateProgress.start();
    // btn_changeStatus();
    btnLoading(id);
    $.ajax({
        url: APP_URL + '/my-watchlist/add/show',
        method: "post", //First change type to method here
        data: {id: id, _token: APP_CSRF},
        dataType: 'json',
        success: function (response) {
            setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
            btnEndLoading(id, 'Added to watchlist', true);
            callbackAnswer(response);
        },
        error: function (response, r2, r3) {
            setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
            btnEndLoading(id, 'Unkown error.', true);
            contentErrorAlert(response, r3);
        }

    });
}

function handleFormRequest(form, id) {
    if(id === 'aregister'){
        btnLoading('btnSubmitRegister');
    }else{
        btnLoading('btnSubmit');
    }

    cMessageClear();

    var url = $(form).attr('action');
    var data = $(form).serialize();

    $.ajax({
        url: url,
        method: "post",
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.status == true) {
                if(id === 'aregister'){
                    btnEndLoading('btnSubmitRegister', 'Success!');
                }else{
                    btnEndLoading('btnSubmit', 'Success!');
                }

                makeSuccessContent({"message": response.message});

                if (response.redirect) {
                    $(this).delay(500, function () {
                        window.location.replace(response.redirect);
                    });
                }
                return true;
            }

            if (response.status == false) {
                cMessage({
                    "message": response.message,
                    "type": "warning"
                }, id);
                if(id === 'aregister'){
                    btnEndLoading('btnSubmitRegister', 'Register',false);
                }else{
                    btnEndLoading('btnSubmit', 'Login',false);
                }
                return true;
            }
            if(id === 'aregister'){
                btnEndLoading('btnSubmitRegister', 'Oops!',true);
            }else{
                btnEndLoading('btnSubmit', 'Oops!',true);
            }
        },
        error: function () {
            pMessage({
                "title": "Oh oh!",
                "message": "A not so common error occurred. Contact an admin.",
                "type": "alert"
            });
            btnEndLoading('btnSubmitRegister', 'Oops', true);
        }

    });
}

function doSearch(form) {
    var search_val = $('#search_input').val();
    var url = $(form).attr('action');
    url = url + '/' + search_val;
    window.location.replace(url);
}

function shaveIt(element = 'p') {
    $(element).shave(50);
}

function shaveItShowsTitles(element = 'h5') {
    $(element).shave(50);
}

function formatGenresToString(genres) {
    var genres_f = '';


    if (!_.isUndefined(genres)) {

        var x_g = 0;
        var size = _.size(genres);
        var lits = '<ul class="genres-list">';

        _.each(genres, function (genre) {
            if (x_g == size) {
                lits += genre;
            } else {
                lits += ' <li>' + genre + ' </li>';
            }
            x_g++;
        });

        lits = lits + '</ul>';

    }

    return lits;
}

function checkInModal(showid, showslug) {
    templateCheckInModal_1(showid, showslug);
    $('#modalCheckIn').modal({keyboard: false, backdrop: "static"});
}

/********************/

function get_episodes_check_in_modal(show_id, season_id) {
    indeterminateProgress.start();
    $.ajax({
        url: APP_URL + '/watchlist/show/episodes/season',
        method: "post",
        data: {_token: APP_CSRF, show_id: show_id, season_id: season_id},
        // dataType: 'json' ,
        success: function (response) {
            setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
            $('#modalCheckIn').find('.modal-body').animateCss('fadeOut');
            templateCheckInModal_episodes_checks(response, season_id);
            // console.log(response);
        },
        error: function () {
            setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
            alert("error");
        }

    });
}

function get_latest_watching() {
    $.ajax({
        url: APP_URL + '/watchlist/show/watching/latest',
        method: 'post',
        data: {_token: APP_CSRF},
        dataType: 'json',
        success: function (response) {
            var latestWatchingTemplate = _.template($('#latestWatchingTemplate').html());

            if (response.status === true) {
                $('#latest-watching').html(latestWatchingTemplate(response.content.data));
            }
        },
        error: function (x, y, z) {
            contentError(x, z);
        }
    });
}

function updateSeasonsUser() {
    btnLoading('btnUpdateSeasons');
    var data = $('#seasons_form').serialize();

    $.ajax({
        url: APP_URL_FULL + '/update/seasons',
        method: 'post',
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.status === true) {
                $($('#modalCheckIn').find('.modal-body')).animateCss('fadeOut', function () {
                    templateModalSuccess($('#modalCheckIn').find('.modal-body'));
                });
            } else if (response.status === false) {
                pMessage({
                    title: 'noOoOu!',
                    message: response.content.message,
                    type: 'warning',

                });
                $('body').find('.modal.fade').modal('toggle');
            }
        },
        error: function () {
            alert('error --- 2');
        }

    });
}

function updateEpisodesUser() {
    var data = $('#episodes_form').serialize();

    $.ajax({
        url: APP_URL_FULL + '/update/episodes',
        method: 'post',
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.status === true) {
                $($('#modalCheckIn').find('.modal-body')).animateCss('fadeOut', function () {
                    templateModalSuccess($('#modalCheckIn').find('.modal-body'));
                });
            } else if (response.status === false) {
                pMessage({
                    title: 'noOoOu!',
                    message: response.content.message,
                    type: 'warning',

                });
                $('body').find('.modal.fade').modal('toggle');
            }
        },
        error: function () {
            alert('error --- 2');
        }

    });
}

/********************/

function templateCheckInModal_1(showapiid, showslug) { //question - checkin episode or a season
    var slug = "'" + showslug + "'";
    var content = '<div class="row text-center" id="cards_container">' +
        '<div class="col-sm-6">' +
        '    <div class="card" id="left">' +
        '      <a href="javascript:void(0)" onclick="makeCheckIn_season(' + showapiid + ', ' + slug + ')"> ' +
        '<div class="card-body">' +
        '        <h5 class="card-title">Check in seasons</h5>' +
        '        <p class="card-text"><svg class="icon">' +
        '<use xlink:href="#popcorn-3" />' +
        '</svg> </p>' +
        '      </div>' +
        '</a>' +
        '    </div>' +
        '  </div>' +
        '  <div class="col-sm-6">' +
        '    <div class="card" id="right">' +
        '      <a href="javascript:void(0)" onclick="makeCheckIn_episode(' + showapiid + ', ' + slug + ')">' +
        '<div class="card-body">' +
        '        <h5 class="card-title">Check in episodes</h5>' +
        '        <p class="card-text"><svg class="icon">' +
        '<use xlink:href="#soda-2" />' +
        '</svg> </p>' +
        '      </div>' +
        '</a>' +
        '    </div>' +
        '  </div>' +
        '<div class="col-sm-12"><p style="margin-top:25px">You can only perform this action once.</p></div>' +
        '</div>';
    $('#modalCheckIn').find('.modal-body').html(content);
}

function templateModalSuccess(element) {
    //clean element
    $(element).html('');

    //message
    $(element).html('<div class="card">' +
        '  <div class="card-body text-center animated jello delay-1s">' +
        '    <i class="flaticon-tick wobble-success"></i><h3>Updated with success!</h3>' +
        '  </div>' +
        '</div>');

    $(element).animateCss('fadeIn');
}

function templateCheckInModal_season(data) {
    var content = '<div class="card">' +
        '  <form id="seasons_form"><input type="hidden" name="_token" value="' + APP_CSRF + '"><ul class="list-group list-group-flush">';

    if (data.status === true) {
        content += '';
        _.each(data.message.content, function (season, index) {
            if (index !== 'id') {
                var airdate = $.format.date(season.premiereDate + " 00:00:00", " dd MMM yyyy ");
                var enddate = $.format.date(season.endDate + " 00:00:00", " dd MMM yyyy");
                var current_episode = moment(moment()).isBetween(airdate, enddate);
                content +=
                    '    <li class="list-group-item ' + (current_episode ? 'current-season' : '') + '"><div class="row"><div class="col-md-' + (!current_episode ? '10' : '12') + '"><h5>Season ' + season.number + '</h5> <i class="flaticon-calendar"></i> ' + airdate + ' to ' + enddate + ' <i class="flaticon-numbered-list"></i> ' + season.episodeOrder + ' Episodes</div>' + (!current_episode ? '<div class="col-md-2 text-center" style="margin-top:15px;"><input type="checkbox" id="' + index + '" name="seasons[]" value="' + index + '"></div>' : '') + '</div></li>';
            }
        });
    }
    content += '<input type="hidden" name="show_id" value="' + data.message.content.id + '">';
    content += '  </ul>' +
        '</div>' +
        '<div class="card btn-seasons-update">' +
        '<button type="button" id="btnUpdateSeasons" onclick="updateSeasonsUser()" class="btn btn-dark">Update</button></div>';


    $('.card#right').animateCss('zoomOut', function () {
        $('#cards_container').remove();
    });
    $('.card#left').animateCss('zoomOut', function () {
        $('#cards_container').remove();
    });

    $(".card").bind('oanimationend animationend webkitAnimationEnd', function () {
        $('#modalCheckIn').find('.modal-body').animateCss('bounceInLeft', function () {
            $('#modalCheckIn').find('.modal-body').html(content);
            $('input').iCheck({checkboxClass: 'icheckbox_flat', radioClass: 'iradio_flat'});

            // $('input').on('ifChecked', function (event) {
            //     $('[name=seasons]').val(($('[name=seasons]').val() ? $('[name=seasons]').val() + ',' : '') + $(this).attr('id'));
            // });
        });

    });
}

function templateCheckInModal_episode(data) {
    // var content = '<div class="card">' +
    //     '  <form id="seasons_form"><input type="hidden" name="_token" value="' + APP_CSRF + '">' +
    //     '<div class="list-group">';

    var content = '<div class="list-group">';

    if (data.status === true) {
        content += '';
        _.each(data.message.content, function (season, index) {
            if (index !== 'id') {
                var airdate = $.format.date(season.premiereDate + " 00:00:00", " dd MMM yyyy ");
                var enddate = $.format.date(season.endDate + " 00:00:00", " dd MMM yyyy");
                var current_episode = moment(moment()).isBetween(airdate, enddate);
                var season_id = (parseInt(index) + 1);
                var show_id = data.message.show_id;
                content += '  <a href="javacript:void(0)" onclick="get_episodes_check_in_modal(' + show_id + ',' + season_id + ')" class="list-group-item list-group-item-action">' +
                    '  <h5>Season ' + season.number + '</h5>  ' +
                    '  </a>';
            }
        });
    }
    content += '</div>';
    // content += '<input type="hidden" name="show_id" value="' + data.message.content.id + '">';
    // content += '  </div>' +
    //     '</div>';


    $('.card#right').animateCss('zoomOut', function () {
        $('#cards_container').remove();
    });
    $('.card#left').animateCss('zoomOut', function () {
        $('#cards_container').remove();
    });

    $('.card').bind('oanimationend animationend webkitAnimationEnd', function () {
        $('#modalCheckIn').find('.modal-body').animateCss('bounceInLeft', function () {
            $('#modalCheckIn').find('.modal-body').html(content);
            $('input').iCheck({checkboxClass: 'icheckbox_flat', radioClass: 'iradio_flat'});

            // $('input').on('ifChecked', function (event) {
            //     $('[name=seasons]').val(($('[name=seasons]').val() ? $('[name=seasons]').val() + ',' : '') + $(this).attr('id'));
            // });
        });

    });
}

function templateCheckInModal_episodes_checks(data, season_id) {
    indeterminateProgress.start();
    if (data.status === false) {
        setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
        alert();
        return;
    }

    var content = '<div class="card">' +
        '  <form id="episodes_form">' +
        '<input type="hidden" name="_token" value="' + APP_CSRF + '">' +
        '<ul class="list-group list-group-flush">';

    if (data.status === true) {
        _.each(data.episodes[0], function (episode, index) {

            var airdate = $.format.date(episode.airdate + " 00:00:00", " dd MMM yyyy ");
            content +=
                '    <li class="list-group-item"><div class="row"><div class="col-md-10">' +
                '<h5>Episode ' + index + '</h5> ' +
                '<i class="flaticon-calendar"></i> ' + airdate +
                '<p>' + episode.name + '</p>' +
                '</div><div class="col-md-2 text-center" style="margin-top:15px;">' +
                '<input type="checkbox" id="' + index + '" name="episodes[]" value="' + index + '">' +
                '</div></li>';
        });
    }
    content += '<input type="hidden" name="show_id" value="' + data.show_id + '">';
    content += '<input type="hidden" name="season_id" value="' + season_id + '">';
    content += '  </ul></form>' +
        '</div>';
    content += '<div class="card btn-seasons-update">' +
        '<button type="button" id="btnUpdateEpisodes" onclick="updateEpisodesUser()" class="btn btn-dark">Update</button></div>';
    $('#modalCheckIn').find('.modal-body').html('');

    $("#modalCheckIn").find('.modal-body').bind('oanimationend animationend webkitAnimationEnd', function () {
        $('#modalCheckIn').find('.modal-body').animateCss('bounceInLeft', function () {
            $('#modalCheckIn').find('.modal-body').html(content);
            $('input').iCheck({checkboxClass: 'icheckbox_flat', radioClass: 'iradio_flat'});
        });
        setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
    });
}

function templateSearch(data) {
    if (data.status == true) {
        contentReset();
        _.each(data.content, function (content) {
            var show = content.show;

            var name = function () {
                if (show.name) {
                    return show.name;
                }
                return 'Unknown';
            };
            var summary = function () {
                if (show.summary) {
                    return $(show.summary).text();
                }
                return 'Not defined';
            };
            var rating = function () {
                if (show.rating) {
                    if (show.rating.average !== null) {
                        return show.rating.average;
                    }
                    return 'Not rated';
                }
                return 'Not rated';
            };

            var language = function () {
                if (show.language) {
                    return show.language;
                }
                return 'Language not defined'
            };
            var image = function () {
                if (!_.isNull(show.image)) {
                    return show.image.medium;
                }
                return APP_URL + '/img/img-not-found.png';
            };

            var genres = formatGenresToString(show.genres);

            var block = '<div class="col-md-12">' +
                '                        <div class="card">' +
                '                            <div class="card-body">' +
                '                                <div class="row text-center text-lg-left">' +
                '                                    <div class="col col-lg-2 mt-auto mb-auto">' +
                '                                        <a href="' + APP_URL + '/show/' + show.id + '/' + slugify(name()) + '"><img onerror="defaultImg(this);" src="' + image() + '" class="img-thumbnail " alt="alt"></a>' +
                '                                    </div>' +
                '                                    <div class="col col-lg-10 mt-auto mb-auto">' +
                '                                        <a href="' + APP_URL + '/show/' + show.id + '/' + slugify(name()) + '"><h5 class="mt-0 mb-1">' + name() + '</h5></a>' +
                '<small> ' + rating() + '<i clas></i> / ' + language() + '</small>' +
                '<br><small> ' + genres + '</small>' +
                '                                        <p>' + summary() + '</p>' +
                '                                        <div class="">' +
                '                                        <a href="javascript:void(0)" onclick="addToMyWatchlist(' + show.id + ')" class="btn btn-dark "><i class="fas fa-plus"></i>Add to watchlist</a>' +
                '                                        </div>' +
                '                                    </div>' +
                '                                </div>' +
                '                            </div>' +
                '                        </div>' +
                '                    </div>';

            $('.show-search').append(block);
            // var schedule_time = show.schedule.time;
        });
        shaveIt();
    }
}

function templateShowPage(data) {

    if (data.status == true) {
        contentReset();

        var show = data.message.content;

        var name = function () {
            if (show.name) {
                return show.name;
            }
            return 'Unknown';
        };
        var summary = function () {
            if (show.summary) {
                return show.summary;
            }
            return 'Not defined';
        };
        var rating = function () {
            if (show.rating) {
                var rating = JSON.parse(show.rating);

                if (rating.average !== null) {
                    return rating.average;
                }
                return 'Not rated';
            }
            return 'Not rated';
        };
        // var status = function () {
        //     if (show.status) {
        //         return show.status;
        //     }
        //     return 'Unkown status';
        // };
        var language = function () {
            if (show.language) {
                return show.language;
            }
            return 'Language not defined';
        };

        var image = function () {
            if (show.media.original) {
                return APP_URL + '/' + show.media.original.path;
            }
            return APP_URL + '/img/img-not-found.png';
        };

        var run_time = function () {
            if (show.runtime) {
                return show.runtime;
            }
            return 'Run time not defined';
        }

        var genres = formatGenresToString(JSON.parse(show.genres));

        templateShowPageSeasons(show.seasons, data.message.content.api_id);

        let show_slug = "'" + show.slug + "'";

        var content_info = '<div class="col-md-4">' +
            '                <div class="shadow p-5 mb-12 bg-white rounded shadow-custom ">' +
            '                    <div class="card bg-dark text-white card-clear-bg hover-conatiner">' +
            '                        <img onerror="defaultImg(this);" class="card-img hover-img" src="' + image() + '" alt="Card image">' +
            '                        <div class="card-img-overlay card-img-overlay-bottom hover-middle">' +
            '                            <a href="" class="img-fav">' +
            '                                <i class="far fa-heart fa-6x"></i>' +
            '                            </a>' +
            '                        </div>' +
            '                    </div>' +
            '                </div>' +
            '' +
            '            </div>' +
            '            <div class="col-md-8">' +
            '                <div class="row">' +
            '                    <div class="col-md-12">' +
            '                        <h3 class="show-title"> ' + name() +
            // '                            <small class="text-muted">- '+status()+'</small>' +
            '                        </h3>' +
            '                        <h6> <i class="far fa-star"></i>' + rating() + ' <i class="fas fa-language"></i>' + language() + '</h6>' +
            '                        <h6 class="show-genres">' + genres + '</h6>' +
            '                        <p class="show-description">' + summary() + '</p>' +
            '                    </div>' +
            '                </div>' +
            '                <div class="row">' +
            '                    <div class="col-md-12">' +
            '                        <a href="javascript:void(0)" id="btnAddToWatchlist" onclick="addToMyWatchlist(' + show.api_id + ')" class="btn btn-secondary"><i class="fas fa-plus"></i>Add to watchlist</a>' +
            '                    </div>' +
            '                </div>' +
            '                <div class="row show-basics">' +
            '                    <div class="col-md-12">' +
            '                        <p><b>Runtime</b>: ~' + run_time() + ' mins</p>' +
            '                        <p><b>Seasons</b>: ' + (_.size(show.seasons) - 1) + '</p>' +
            '                    </div>' +
            '                </div>' +
            '            </div>';
    }

    $('#show_info').html(content_info);
}

function templateShowPageSeasons(data, api_id) {
    const in_watchlist = $('#in_watchlist').val();
    var block = '';
    _.each(data, function (season, index) {
        if (index === 'id') {
            return;
        }
        block += '<div class="row latest-watching-notification">' +
            '                    <div class="col-md-12">' +
            '                        <div class="card">' +
            '                            <div class="card-body">' +
            '                                <div class="row">' +
            '                                    <div class="col-md-8">' +
            '                                        <h5 class="mt-0 mb-1">Season ' + season.number + '' +
            (season.endDate && season.premiereDate ?
                ' <small>(' + moment(season.premiereDate + ' 00:00:00').format('Do MMMM \'YY') + ' to ' + moment(season.endDate + ' 00:00:00').format('Do MMMM \'YY') + ')</small>'
                : '<small> <b>(Currently Airing)</b></small>') +
            '' +
            '</h5>' +
            '                                        <p class="card-text">' + (season.episodeOrder ? season.episodeOrder : '') + ' Episodes</p>' +
            '                                    </div>' +
            '                                    <div class="col-md-4 mt-3 mt-md-0">' +
            '                                        <a id="collapse_seasons_' + season.number + '" href="javascript:void(0)" data-parent="#show_seasons" onclick="makeSeasonEpisodes(' + season.number + ',' + season.id + ')" class="btn btn-secondary fright collapser"><i class="fas fa-list-ol"></i>Episodes' +
            '                                        </a>' +
            (in_watchlist == 'no'? '<a href="javascript:void(0)" onclick="messageMessage(\'Add to you watchlist before you start planning!\')" data-toggle="tooltip" data-placement="top" title="Add to watchlist first!" class="btn btn-secondary float-left float-md-right mr-3"><i class="far fa-calendar"></i></a>'  : '<a href="' + APP_URL + '/watchlater?plan=' + data.id + '&api_id=' + api_id + '" data-toggle="tooltip" data-placement="top" title="Start planning episodes!" class="btn btn-secondary float-left float-md-right mr-3"><i class="far fa-calendar"></i></a>' )+
            '                                    </div>' +
            '                                </div>' +
            '                                <div class="row">' +
            '                                    <div class="col-md-12">' +
            '                                       <div class="collapse" id="episodes_' + season.number + '">' +
            '                                           <div class="card-body card-season-shows" id="body">' +
            '                                           </div>' +
            '                                       </div>' +
            '                                   </div>' +
            '                               </div>' +
            '                            </div>' +
            '                        </div>' +
            '                    </div>' +
            '                </div>';
    });
    $('#show_seasons').append(block);
    $('[data-toggle="tooltip"]').tooltip();
}

function templateWatchlistWatchlist(data) {
    if (data.status === true) {
        var shows = data.content.message.shows;
        var content = '';
        _.each(shows, function (show, index) {

            var slug = "'" + show.slug + "'";
            var icons_check_html = ''

            // if (show.setup === 0 || show.setup === "0") {
            icons_check_html = '' +
                '<div class="col-md-12" style="float:left">' +
                '<a href="javascript:void(0)" onclick="checkNext(' + show.id + ')" class="watchlist-btns btn btn-secondary btn-round btn-sm"><i class="fas fa-eye"></i> Watch</a>' +
                '<a href="' + APP_URL_FULL + '/show/' + show.id + '" class="watchlist-btns btn btn-secondary btn-round btn-sm ml-1"><i class="fas fa-cog"></i> Settings</a>' +
                '</div>';
            // } else {
            //     icons_check_html = '<div class="col-md-12" style="float:left"><a href="javascript:void(0)" onclick="checkNext(' + show.id + ')" class="watchlist-btns btn btn-secondary btn-round btn-sm"><i class="fas fa-eye"></i></a></div>';
            // }

            content += '<div class="col-sm col-lg-3 text-center">' +
                '                    <div class="shadow p-3 mb-5 bg-white rounded shadow-custom ">' +
                '                        <div class="card bg-dark text-white card-clear-bg card-height">' +
                '                            <a href="' + APP_URL_FULL + '/show/' + show.id + '"><img onerror="defaultImg(this);" class="card-img img-fluid card-img-fix" src="' + (show.media ? (show.media.small ? APP_URL + '/' + show.media.small.path : APP_URL + '/img/img-not-found.png') : APP_URL + '/img/img-not-found.png') + '" alt="Card image"></a>' +
                '                            <div class="card-img-overlay cfade card-img-overlay-bottom">' +
                '                                <div class="row btns-row">' +
                icons_check_html +
                '                               </div>' +
                '                                <a href="' + APP_URL_FULL + '/show/' + show.id + '"><h5 class="card-title">' + show.name + '</h5></a>' +
                '                            </div>' +
                '                        </div>' +
                '                    </div>' +
                '                </div>';
        });

        $('#watchlist').html(content);
        $('[data-toggle="tooltip"]').tooltip();
        shaveItShowsTitles();
        return;
    }
    let error_template = _.template($('#templateNoShows').html());
    $('#watchlist').html(error_template);
    setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
}

function templateWatchlistWatching(res) {
    if (res.status === false) {
        contentError('Ups', 'Error Unkown');
        return;
    }

    if (_.isEmpty(res.content.data)) {
        let template = _.template($('#emptyBlock').html());
        $('#watching_list').html(template());
        return;
    }

    let template = _.template($('#watchingBlockTemplate').html());

    $('#watching_list').html(template({episodes: res.content.data}));
}

function templateWatchlistShowInfo(res) {
    if (res.status === false) {
        contentError('Error - ', 'Could not get the content');
        return;
    }

    //template header
    let template_data = _.template($('#showInfoTemplate').html());
    const data = res.content.data;
    $('#show_info').html(template_data(data));

    let externals = JSON.parse(data.externals);
    // let externals_clean = {
    //     imdb: "",
    //     site: `<a href="${data.official_site}">Official Site</a>`,
    // };
    let externals_clean = new Array;
    externals_clean[0] = `<a target="_blank" href="${data.official_site}">Official Site</a>`;
    //make links for externals
    let n = 1;
    _.each(externals, function (val, key) {
        switch (key) {
            case 'imdb' :
                externals_clean[n] = `<a target="_blank" href="https://www.imdb.com/title/${val}/">IMDb</a>`;
                n++;
                break;
        }
    });

    //template collapsable info
    let template_info = _.template($('#about_show_template').html());
    $('#about_show').html(template_info({data: data, externals_clean: externals_clean}));

    _.each(res.content.data.media, function (img) {
        if (img.size === "original") {
            $('.parallax').css('background-image', 'url(' + APP_URL + '/' + img.path + ')');
        }
    });

    $('#removeBtn').on('click', function (evt) {
        evt.preventDefault();

        Swal({
            title: 'Are you sure?',
            html: "You will lose all the information. <br> If you add this show back again it will start from zero.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, remove it!'
        }).then((result) => {
            if (result.value) {
                requestShowRemove($('#removeBtn').data('id'));
            }
        });
    });
}

function templateWatchlistShowSeasonsEpisodes(res) {
    if (res.status === false) {
        contentError('Uh OH!', 'We can\'t figure out what error was this. sorry!');
        return;
    }

    const data = res.content.data;
    let template = _.template($('#showSeasonsEpisodesTemplates').html());

    $('#SeasonsEpisodes').html(template({
        seasons_episodes: data,
        episodes: res.content.show_episodes.message.content.episodes
    }));

    $('[data-toggle="popover"]').popover({html: true, trigger: 'focus'});
    $('[data-toggle="tooltip"]').tooltip();
}

function templateUserFavorites(res) {
    //error template
    if (res.status === false) {
        if (res.content.favorites === false) {
            let template_error = _.template($('#templateNoFavorites').html());
            $('#pageContent').html(template_error({message: iterateMessages(res.content.data)}));
            return;
        }
        contentError('Unknown error.', 'We are sorry.');
        return;
    }

    let template = _.template($('#templateFavorites').html());
    $('#pageContent').html(template({shows: res.content.data.shows}));
    $('[data-toggle="tooltip"]').tooltip();
    return;
}

function templateTop5HomePage(res) {

    if (res.status === false) {
        //error template
        let template_error = _.template($('#homeTop5TemplateError').html());
        $('#contentMostPopular').html(template_error);
        return;
    }

    let template = _.template($('#homeTop5Template').html());
    $('#contentMostPopular').html(template({data: res.content.data.top_5}));
}

/********************/

function EpisodesModal(season_nr, data) {

    $('#episodes_' + season_nr).collapse();

    var content = '<div class="card col-md-12" style="">' +
        '  <ul class="list-group list-group-flush">';

    _.each(data.episodes, function (episode_content, episode_nr) {


        if (episode_content.airdate) {
            var image = '';

            if (episode_content.image) {
                image = episode_content.image.medium;
            }

            var date = $.format.date(episode_content.airdate + " " + episode_content.airtime + ':00', " dd MMM yyyy - h:mm a");

            content += '<li class="list-group-item">' +
                '<div class="row">' +
                '<div class="col-md-4"><img onerror="defaultImg(this);" class="img-responsive episodes-list" src="' + image + '" alt="season_' + season_nr + '"></div>' +
                '<div class="col-md-8">' +
                '<h5 class="card-title">' + episode_nr + ' - ' + episode_content.name + '</h5>' +
                '<h6 class="card-subtitle mb-2 text-muted"><i class="flaticon-calendar"></i> ' + date + '</h6>' +
                '<p class="card-text">' + episode_content.summary + '</p>' +
                '</div>' +
                '</div>' +
                '</li>';
        }
    });

    content += '  </ul>' +
        '</div>';

    $('#episodes_' + season_nr + ' > #body').html(content);
}

function EpisodesCollapseBtn(season_nr) {
    $('#collapse_seasons_' + season_nr)
        .removeAttr('onclick')
        .attr('data-toggle', 'collapse')
        .attr('href', '#episodes_' + season_nr)
        .attr('role', 'button')
        .attr('aria-expanded', 'false')
        .attr('data-parent', '#show_seasons');

}

/********************/

function makeSearchRequest(method, url, data) {
    contentLoading();
    $.ajax({
        url: url,
        method: method, //First change type to method here
        data: data,
        dataType: 'json',
        success: templateSearch,
        error: function (x, z, y) {
            answer = {status: false};
        }
    });
};

function makeShowPage(method, url, data) {
    contentLoading();

    //asks for the show data
    $.ajax({
        url: url,
        method: method,
        data: data,
        dataType: 'json',
        success: templateShowPage,
        error: function (x, z, y) {
            contentError(x, y)
        }
    });

}

function makeSeasonEpisodes(seasonnr, api_id) {
    $.ajax({
        url: APP_URL_FULL + "/season/" + seasonnr + "/episodes",
        method: "post", //First change type to method here
        data: {_token: APP_CSRF},
        // dataType: 'json',
        success: function (response) {
            if (response.status == true) {
                EpisodesModal(seasonnr, response.message.content);
                EpisodesCollapseBtn(seasonnr);
            } else {

            }
        },
        error: function () {
            alert("error -- 1");
        }
    });
}

function makeCheckIn_season(showapiid, showslug) {
    //templateCheckInModal_season
    indeterminateProgress.start();
    $.ajax({
        url: APP_URL + '/show/' + showapiid + '/' + showslug + '/seasons',
        method: "post",
        data: {_token: APP_CSRF},
        // dataType: 'json',
        success: function (response) {
            setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
            $('.card#right>a').attr('disabled', true).addClass('disable-href');
            $('.card#left>a').attr('disabled', true).addClass('disable-href');
            templateCheckInModal_season(response);
        },
        error: function () {
            setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
            alert("error");
        }

    });
}

function makeCheckIn_episode(showapiid, showslug) {
    indeterminateProgress.start();
    $.ajax({
        url: APP_URL + '/show/' + showapiid + '/' + showslug + '/seasons',
        method: "post",
        data: {_token: APP_CSRF},
        // dataType: 'json',
        success: function (response) {
            setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
            $('.card#right>a').attr('disabled', true).addClass('disable-href');
            $('.card#left>a').attr('disabled', true).addClass('disable-href');
            templateCheckInModal_episode(response);
            // templateCheckInModal_season(response);
        },
        error: function () {
            setInterval(function(){
                indeterminateProgress.end();
            }, 1000);
            alert("error");
        }
    });
}

function checkNext(show_id, watching) {

    let template_success = _.template($('#StartNextTemplateSuccess').html());
    let template_error = _.template($('#StartNextTemplateError').html());

    $.ajax({
        url: APP_URL + '/watchlist/show/episodes/next',
        method: "post",
        data: {show_id: show_id, _token: APP_CSRF},
        // dataType: 'json',
        success: function (response) {
            if (response.status === true) {
                if (response.next_allowed === false) {
                    currentlyWatching(show_id, response, watching);
                    return false;
                }

                nextWatching(show_id, response);
                $('#ModalStartNext').modal('show');
                return false;
            } else if (response.status === false) {
                messageMessage(iterateMessages(response.content.message));
            }

            $('#nextbody').html(template_error({
                error: (response.content ? iterateMessages(response.content.message) : 'Uncommon Error. Contact administrator.'),
            }));

        },
        error: function (e, x, a) {
            $('#nextbody').html(template_error({
                error: 'Unkown error.',
            }));
            console.log({e, x, a});
        }
    });

}

function currentlyWatching(show_id, response, watching) {

    let template_dnf = _.template($('#DNFTemplate').html());
    const episode = response.episode;

    if(!response.episode){
        messageFailed(iterateMessages(response.content.message),'topCenter');
        return;
    }

    $('#nextbody').html(template_dnf({
        message: response.content.message,
        episode_name: episode.name,
        season: episode.season,
        episode: episode.episode,
        show_id: show_id,
    }));
    $('#ModalStartNext').modal('show');
    return;
}

function nextWatching(show_id, response) {
    let template_question = _.template($('#StartNextTemplate').html());

    const episode = response.episode;

    $('#nextbody').html(template_question({
        episodename: episode.name,
        seasonnumber: episode.season,
        seasonepisode: episode.episode,
        showid: show_id,
    }));
}

function postNextEpisode(show_id) {

    $.ajax({
        url: APP_URL + '/watchlist/show/episodes/current/start',
        method: "post",
        data: {show_id: show_id, _token: APP_CSRF},
        dataType: 'json',
        success: function (response) {
            if (response.status === true) {
                let template_success = _.template($('#StartNextTemplateSuccess').html());
                $('#nextbody').html(template_success({
                    message: iterateMessages(response.content.message),
                }));
                get_latest_watching();
            }
        },
        error: function () {
            alert("error");
            let template_error = _.template($('#StartNextTemplateError').html());
            $('#nextbody').html(template_error({
                error: iterateMessages(response.content.message),
            }));
        }
    });
}

function updateFinishCurrent(show_id, callback = false, watching = false, new_method = 0) {
    if (new_method === 1) {
        btnLoading($('.fancybox-content').find('button').attr('id'));
    }
    //ajax request
    $.ajax({
        url: APP_URL + '/watchlist/show/episodes/current/end',
        method: "post",
        data: {show_id: show_id, _token: APP_CSRF},
        dataType: 'json',
        success: function (response) {
            if (response.status === true) {
                if (new_method !== 0) {
                    requestWatchlistShowSeasonsEpisodes(templateWatchlistShowSeasonsEpisodes);
                    $.fancybox.close();
                    updateUserShowCurrentlyWatching(0, false, false);
                    setTimeout(function () {
                        $.fancybox.open({
                            src: APP_URL + `/watchlist/show/get/watch-next/html/${show_id}`,
                            // src : '#watch-next-episode',
                            type: 'ajax'
                        });
                    }, 400);
                    return;
                }

                let template_success = _.template($('#StartNextTemplateSuccess').html());
                $('#nextbody').html(template_success({
                    message: iterateMessages(response.content.message),
                }));

                if (callback === true) {
                    if (watching === true) {
                        requestWatchlistWatching(templateWatchlistWatching);
                        setTimeout(function () {
                            $('.modal.show').modal('toggle');
                        }, 1300);
                        return;
                    }
                    if (new_method === 0) {
                        setTimeout(function () {
                            checkNext(show_id);
                        }, 1600);
                    }

                }
                if (new_method === 0) {
                    get_latest_watching();
                }
                return;
            }

            if (new_method === 1) {
                $('.fancybox-content').html('<h3>NoOoOOOOoOoOOO!</h3><p>Something happened and we do not know what. But our master as been informed of such unfortune!</p>');
                return;
            }

            let template_error = _.template($('#StartNextTemplateError').html());
            $('#nextbody').html(template_error({
                error: iterateMessages(response.content.message),
            }));
        },
        error: function () {
            alert("error");
        }
    });
}

function handleUpdateFavoritesGlobal (res) {
    if(res.status === false){
        messageFailed('Could not add to Favorites. Sorry!');
        return;
    }

    messageMessage('Done with no problem!');
}

function handleUpdatedFavorite(res) {
    if (res.status === false) {
        pMessage({
            title: 'Oh no!',
            message: 'Sorry! But we couldn\'t make the love between you and this show official. 😭',
            type: 'warning'
        });
        loveCounter = 1;
        return;
    }

    const data = res.content.data;

    if (loveCounter !== 0) {
        if (data.favorite_status === true) {
            pMessage({
                title: 'YES!',
                message: 'I now pronounce you show and spectator! You may watch more!',
                type: 'success'
            });
            loveCounter = 0;
        } else {
            pMessage({title: 'OK!', message: 'This show is no longer a favorite!', type: 'success'});
            loveCounter = 0;
        }
    } else {
        if (data.favorite_status === 'added') {
            pMessage({title: 'Yay!', message: 'Show added to favorites!', type: 'success'});
        } else {
            pMessage({title: 'Yes Sir!', message: 'Show removed from favorites!', type: 'success'});
        }

    }

    let w = $('#favoritedBtn');
    let nw = $('#favoriteBtn');
    if (!w.hasClass('hide_el')) {
        w.animateCss('bounceOut', function () {
            w.addClass('hide_el');
            nw.show().animateCss('bounceIn');
        });
    } else {
        nw.animateCss('bounceOut', function () {
            nw.hide();
            w.removeClass('hide_el').animateCss('bounceIn');
        });
    }
}

function requestWatchlistWatching(callback) {
    ajaxRequest(APP_URL + '/watchlist/watching', {_token: APP_CSRF}, 'post', callback);
}

function requestWatchlistShowInfo(callback) {
    ajaxRequest(APP_URL_FULL, {_token: APP_CSRF}, 'post', callback)
}

function requestWatchlistShowSeasonsEpisodes(callback) {
    ajaxRequest(APP_URL_FULL + '/seasons-episodes', {_token: APP_CSRF}, 'post', callback);
}

function requestShowFavorite(show_id, callback, url = APP_URL_FULL + '/favorite') {
    ajaxRequest(url, {_token: APP_CSRF, show_id: show_id}, 'post', callback)
}

function requestShowFavorite_favorites(show_id, slug) {
    ajaxRequest(APP_URL + '/watchlist/show/' + show_id + '/favorite', {
        _token: APP_CSRF,
        show_id: show_id
    }, 'post', handleFavoriteRemove(slug))
}

function requestShowRemove(show_id) {
    ajaxRequest(APP_URL + '/watchlist/show/' + show_id + '/remove', {_token: APP_CSRF}, 'post', handleShowRemove);
}

function requestUserFavorites(callback = templateUserFavorites) {
    ajaxRequest(APP_URL_FULL, {_token: APP_CSRF}, 'post', callback,false, true);
}

function requestTop5(callback) {
    ajaxRequest(APP_URL + '/shows/top-5', {_token: APP_CSRF}, 'post', callback)
}

function handleShowRemove(res) {
    if (res.status === false) {
        pMessage({
            title: 'Ops!',
            message: 'Could not remove right now! Sorry.',
            type: 'warning'
        });
        return;
    }
    window.location.replace(APP_URL + '/watchlist');
}

function handleFavoriteRemove(slug) {
    $('#' + slug + '-card').animateCss('fadeOut', function () {
        $('#' + slug + '-card').remove();
        $('[data-toggle="tooltip"], .tooltip').tooltip("hide");
    });
}


function defaultImg(img) {
    img.onerror = "";
    img.src = APP_URL + '/img/img-not-found.png';
}

