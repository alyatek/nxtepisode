function ajaxRequest(url, data, method, callback) {
    $.ajax({
        url: url,
        method: method,
        data: data,
        dataType: 'json',
        success: function (response) {
            callback(response);
        },
        error: function (z,x,y) {
            console.log('error');
            console.log(z);
        }
    });
}

function requestUsersTemplate () {
    requestUsers(templateUsersTable);
}

function requestUsers (callback) {
    ajaxRequest(APP_URL+'/admin/user/get',{},'get',callback);
}

function requestTop5Shows () {
    ajaxRequest(APP_URL+'/admin/shows/update/top-5',{_token:APP_CSRF},'post',refreshTop5Info);
}

function removeUser(id){
    ajaxRequest(APP_URL+'/admin/user/disable/'+id,{_token:APP_CSRF},'post',requestUsersTemplate);
}

function enableUser(id){
    ajaxRequest(APP_URL+'/admin/user/enable/'+id,{_token:APP_CSRF},'post',requestUsersTemplate);
}

function templateUsersTable (res) {
    let template = _.template($('#usersTableTemplate').html());
    console.log(res);
    $('#users_data').html(template({users:res}));
    $('#dataTable').DataTable();
    // t.draw();
}

function refreshTop5Info(res){
    console.log(res);
}
